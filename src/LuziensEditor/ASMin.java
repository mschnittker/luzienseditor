/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import static LuziensEditor.EditSH.txtCode;
import static LuziensEditor.WebColor.toHexString;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

/**
 *
 * @author markusschnittker
 */
public class ASMin extends JDialog{
    
    RSyntaxTextArea txtCode;
    
    ASMin(){
        setTitle("Inline Assembler");
        setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
        setLayout(new BorderLayout());
        
        /* Info */
        JPanel pnlInfo = new JPanel();
        pnlInfo.setLayout(new BorderLayout());
        pnlInfo.setBackground(Color.WHITE);
        JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
        txtInfo.setEditable(false);
        txtInfo.setBackground(Color.WHITE);
        txtInfo.setText("<b>Inline Assembler</b><br>Hier kannst du Assambler Code in deinen C/C++ Code einfügen.<br><br>");
        pnlInfo.add(txtInfo,BorderLayout.PAGE_START);
        add(pnlInfo,BorderLayout.NORTH);
        
        
        
        JPanel pnlMain = new JPanel();
        txtCode = new RSyntaxTextArea(20,60);
        txtCode.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_ASSEMBLER_X86);
        RTextScrollPane scCode = new RTextScrollPane(txtCode);
        txtCode.setLineWrap(true);
        txtCode.setWrapStyleWord(true);
//        txtCode.addKeyListener(new KeyAdapter(){
//              public void keyPressed(KeyEvent e)
//              {
//                int key = e.getKeyCode();
//                if (key == KeyEvent.VK_ENTER)
//                {
//                   
//                }
//              }
//            });  
        pnlMain.add(scCode);
        add(pnlMain,BorderLayout.CENTER);
        
        
        
        /* Buttons */
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new FlowLayout());
        
        JButton btnCopy = new JButton("Kopieren");
            btnCopy.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent aeXXX){
                    String code = txtCode.getText();
                    
                    String text = "asm(\"" + 
                                    code +
                                  "\");";
                    StringSelection selection = new StringSelection(text);
                    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                    clipboard.setContents(selection, selection);
                }
            });
        
        JButton btnDispose = new JButton("Abbrechen");
        btnDispose.addActionListener(new ActionListener(){
           @Override public void actionPerformed(ActionEvent ev){
               dispose();
           } 
        });
        
        pnlButtons.add(btnCopy);
        pnlButtons.add(btnDispose);
        
        add(pnlButtons,BorderLayout.SOUTH);
        
        pack();
        setVisible(true);
    }
}

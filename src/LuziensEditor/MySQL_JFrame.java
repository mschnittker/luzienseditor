package LuziensEditor;

import java.sql.*;
import javax.swing.table.DefaultTableModel;

import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
/*
 * MySQL_JFrame.java
 *
 * Created on 10. Oktober 2003, 22:33
 */

/**
 *
 * @author  Administrator
 */


public class MySQL_JFrame extends JFrame {
    
    private Connection con = null;
   
    private String user = null;
    private String password = null;

    private String status = null;
    private String driverClass = null;
    private String dbUrl = null;
    
    private DefaultComboBoxModel dbs_DCBM = null;
    private DefaultComboBoxModel tbls_DCBM = null;
    private DatabaseMetaData dbmd = null;
    private DefaultTableModel tblDataModel = null;
    
    private boolean connected = false;
    
    private Vector tables = null;
    
    /** Creates new form MySQL_JFrame */
    public MySQL_JFrame() {
        
        initComponents();
        
    }
    
    private void listDatabases(){
        if(connected && dbmd != null){
            try{
                ResultSet rs = dbmd.getCatalogs();
                dbs_DCBM = new DefaultComboBoxModel();
                while(rs.next()){
                    dbs_DCBM.addElement(rs.getString(1));
                }
                dbs_jComboBox1.setModel(dbs_DCBM);
                
            }catch(SQLException sqle){
                sqle.printStackTrace();
            }
        }
    }
    
    //F�r jede Datenbank Verf�gbare Tabellen ermitteln
    private void listTables(){
        String database = null;
        if (con != null && dbmd != null){
            try{
                database = (String)this.dbs_jComboBox1.getSelectedItem();
                tbls_DCBM = new DefaultComboBoxModel();
                ResultSet rs = dbmd.getTables(database,null, null, new String[]{"TABLE"});
                
                while(rs.next()){
                    tbls_DCBM.addElement(rs.getString(3));
                }
                
                this.tbls_jComboBox1.setModel(tbls_DCBM);
                              
                
            }catch(SQLException sqle){
                sqle.printStackTrace();
            }
            
        }
    }
    
    private void showTableContent(){
        if (con != null && dbmd != null){
            try{
                
                Statement stmt = con.createStatement();
                String db = (String)this.dbs_jComboBox1.getSelectedItem();
                if (db == null || db == "")
                    return;
                stmt.execute("USE " + db);
                String tbl = (String)this.tbls_jComboBox1.getSelectedItem();
                if (tbl == null || tbl == "")
                    return;
                ResultSet rs = stmt.executeQuery("SELECT * FROM " + tbl);
                tblDataModel = new DefaultTableModel();
                Vector clmHeader = new Vector();
                Vector dataVector = new Vector();
                
                ResultSetMetaData rsmd = rs.getMetaData();
                int clmCnt = rsmd.getColumnCount();
                for(int i = 1; i <= clmCnt;i++)
                    clmHeader.addElement(rsmd.getColumnName(i));
                
                while(rs.next()){
                    Vector rowVector = new Vector();
                    for(int i = 1; i <= clmCnt; i++){
                        rowVector.addElement(rs.getString(i));
                    }
                    dataVector.addElement(rowVector);
                }
                
                tblDataModel.setDataVector(dataVector,clmHeader);
                
                this.jTable1.setModel(tblDataModel);
                this.jTable1.updateUI();
                
               // System.out.println("finished");

            }catch(SQLException sqle){
                sqle.printStackTrace();
            }
        }
    }
    
    private void initDB(){
        //Treiberklasse Laden
        try{
            Thread.currentThread().getContextClassLoader().loadClass(driverClass).newInstance();
        }catch(ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }catch(InstantiationException ie){
            ie.printStackTrace();
        }catch(IllegalAccessException iae){
            iae.printStackTrace();
        }
        
        //Verbindung zur Datenbank Aufbauen
        
        try{
            this.con = DriverManager.getConnection(this.dbUrl,this.user,this.password);
            this.connected = true;
            this.status_jTextField1.setText("connected");
            //System.out.println("Connected");
            dbmd = con.getMetaData();
            
            //Verf�gbare Datenbanken ermitteln
            listDatabases();
            
            // con.close();
        }catch(SQLException sqle){
            sqle.printStackTrace();
        }
        
        
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        dc_jLabel1 = new javax.swing.JLabel();
        dc_jTextField1 = new javax.swing.JTextField();
        user_jLabel1 = new javax.swing.JLabel();
        un_jTextField1 = new javax.swing.JTextField();
        pwd_jLabel1 = new javax.swing.JLabel();
        pwd_jTextField1 = new javax.swing.JTextField();
        connect_jButton2 = new javax.swing.JButton();
        status_jLabel1 = new javax.swing.JLabel();
        status_jTextField1 = new javax.swing.JTextField();
        dburl_jLabel1 = new javax.swing.JLabel();
        dburl_jTextField1 = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        dbs_jLabel1 = new javax.swing.JLabel();
        dbs_jComboBox1 = new javax.swing.JComboBox();
        tbls_jLabel1 = new javax.swing.JLabel();
        tbls_jComboBox1 = new javax.swing.JComboBox();
        shw_jButton1 = new javax.swing.JButton();

        setTitle("MySQL_Table Viewer");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setMinimumSize(new java.awt.Dimension(640, 500));
        jPanel1.setPreferredSize(new java.awt.Dimension(640, 500));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jPanel2.setBackground(new java.awt.Color(153, 204, 255));
        jPanel2.setMinimumSize(new java.awt.Dimension(640, 300));
        jPanel2.setPreferredSize(new java.awt.Dimension(640, 300));
        jScrollPane1.setMinimumSize(new java.awt.Dimension(640, 300));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(640, 300));
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(jTable1);

        jPanel2.add(jScrollPane1, new java.awt.GridBagConstraints());

        jPanel1.add(jPanel2, new java.awt.GridBagConstraints());

        jPanel3.setLayout(new java.awt.GridBagLayout());

        jPanel3.setBackground(new java.awt.Color(255, 204, 102));
        jPanel3.setMinimumSize(new java.awt.Dimension(640, 200));
        jPanel3.setPreferredSize(new java.awt.Dimension(640, 200));
        jPanel4.setLayout(new java.awt.GridBagLayout());

        jPanel4.setMinimumSize(new java.awt.Dimension(350, 200));
        jPanel4.setPreferredSize(new java.awt.Dimension(350, 200));
        dc_jLabel1.setText("Driver Class:");
        jPanel4.add(dc_jLabel1, new java.awt.GridBagConstraints());

        dc_jTextField1.setColumns(15);
        dc_jTextField1.setText("org.gjt.mm.mysql.Driver");
        jPanel4.add(dc_jTextField1, new java.awt.GridBagConstraints());

        user_jLabel1.setText("Username:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel4.add(user_jLabel1, gridBagConstraints);

        un_jTextField1.setColumns(15);
        un_jTextField1.setText("Administrator");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        jPanel4.add(un_jTextField1, gridBagConstraints);

        pwd_jLabel1.setText("Password:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        jPanel4.add(pwd_jLabel1, gridBagConstraints);

        pwd_jTextField1.setColumns(15);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        jPanel4.add(pwd_jTextField1, gridBagConstraints);

        connect_jButton2.setText("Connect");
        connect_jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                connect_jButton2MousePressed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        jPanel4.add(connect_jButton2, gridBagConstraints);

        status_jLabel1.setText("Status:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        jPanel4.add(status_jLabel1, gridBagConstraints);

        status_jTextField1.setColumns(7);
        status_jTextField1.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        jPanel4.add(status_jTextField1, gridBagConstraints);

        dburl_jLabel1.setText("Database URL:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel4.add(dburl_jLabel1, gridBagConstraints);

        dburl_jTextField1.setColumns(15);
        dburl_jTextField1.setText("jdbc:mysql://localhost:3306/");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanel4.add(dburl_jTextField1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel3.add(jPanel4, gridBagConstraints);

        jPanel5.setLayout(new java.awt.GridBagLayout());

        jPanel5.setMinimumSize(new java.awt.Dimension(290, 200));
        jPanel5.setPreferredSize(new java.awt.Dimension(290, 200));
        dbs_jLabel1.setText("Databases:");
        jPanel5.add(dbs_jLabel1, new java.awt.GridBagConstraints());

        dbs_jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "                                                 " }));
        dbs_jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dbs_jComboBox1ActionPerformed(evt);
            }
        });

        jPanel5.add(dbs_jComboBox1, new java.awt.GridBagConstraints());

        tbls_jLabel1.setText("Tables:       ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel5.add(tbls_jLabel1, gridBagConstraints);

        tbls_jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "                                                 " }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanel5.add(tbls_jComboBox1, gridBagConstraints);

        shw_jButton1.setText("Show Content");
        shw_jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shw_jButton1ActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        jPanel5.add(shw_jButton1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanel3.add(jPanel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel1.add(jPanel3, gridBagConstraints);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }//GEN-END:initComponents
    
    private void shw_jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_shw_jButton1ActionPerformed
        // Add your handling code here:
        
        //Analysiere die Tabellen Struktur und zeige sie an ...
        
        showTableContent();
        
    }//GEN-LAST:event_shw_jButton1ActionPerformed
    
    private void dbs_jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dbs_jComboBox1ActionPerformed
        // Add your handling code here:
        //System.out.println((String)this.dbs_jComboBox1.getSelectedItem());
        listTables();
        
    }//GEN-LAST:event_dbs_jComboBox1ActionPerformed
    
    private void connect_jButton2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_connect_jButton2MousePressed
        // Add your handling code here:
        this.driverClass = this.dc_jTextField1.getText();
        this.user = this.un_jTextField1.getText();
        this.password = this.pwd_jTextField1.getText();
        this.dbUrl = this.dburl_jTextField1.getText();
        
        this.status_jTextField1.setText("connecting...");
        if (!connected)
            initDB();
        else this.status_jTextField1.setText("connected");
        
    }//GEN-LAST:event_connect_jButton2MousePressed
    
    /** Exit the Application */
    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        if (con != null){
            try{
                con.close();
            }catch(SQLException sqle){
                sqle.printStackTrace();
            }
        }
        
        System.exit(0);
    }//GEN-LAST:event_exitForm
    
    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        new MySQL_JFrame().show();
//    }
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton connect_jButton2;
    private javax.swing.JComboBox dbs_jComboBox1;
    private javax.swing.JLabel dbs_jLabel1;
    private javax.swing.JLabel dburl_jLabel1;
    private javax.swing.JTextField dburl_jTextField1;
    private javax.swing.JLabel dc_jLabel1;
    private javax.swing.JTextField dc_jTextField1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel pwd_jLabel1;
    private javax.swing.JTextField pwd_jTextField1;
    private javax.swing.JButton shw_jButton1;
    private javax.swing.JLabel status_jLabel1;
    private javax.swing.JTextField status_jTextField1;
    private javax.swing.JComboBox tbls_jComboBox1;
    private javax.swing.JLabel tbls_jLabel1;
    private javax.swing.JTextField un_jTextField1;
    private javax.swing.JLabel user_jLabel1;
    // End of variables declaration//GEN-END:variables
    
}

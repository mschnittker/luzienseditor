/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author markusschnittker
 */
public class CopyToXampp {
    String path;
    String name;
    String xampp;
    
    void deleteDir(File path) {
      for (File file : path.listFiles()) {
         if (file.isDirectory())
            deleteDir(file);
         file.delete();
      }
      path.delete();
   }
    
    CopyToXampp(String path,String name) throws FileNotFoundException, IOException{
        this.path = path;
        this.name = name;
        
        if(System.getProperty("os.name").equals("Mac OS") || System.getProperty("os.name").equals("Mac OS X")){
            String xampp_path = "/Applications/XAMPP/xamppfiles/htdocs/";
            File f = new File(xampp_path); 
            if (f.isDirectory()) { 
                xampp = xampp_path;
 
            } else { 
                JOptionPane.showMessageDialog(null,"Bitte XAMPP nach \"/Applications/XAMPP/\" installieren","Luzien's Editor v0.2",JOptionPane.ERROR_MESSAGE);
            }
        }
        else if(System.getProperty("os.name").equals("Linux")){
            String xampp_path = "/opt/lampp/htdocs/";
            File f = new File(xampp_path); 
            if (f.isDirectory()) { 
                xampp = xampp_path;
            } else {  
                JOptionPane.showMessageDialog(null,"Bitte XAMPP nach \"/opt/lampp/\" installieren","Luzien's Editor v0.2",JOptionPane.ERROR_MESSAGE);
            }
        }
        
        String out_xampp = xampp + File.separator + name + File.separator;
        File X_out = new File(out_xampp);
        if(X_out.isDirectory()){
            // löschen
            deleteDir(X_out);
            // neu erstellen
            X_out.mkdir();
        }
        else{
            X_out.mkdir();
        }
        
        File in = new File(path);
        File out = new File(out_xampp);
        
        CopyDirectory cpDir = new CopyDirectory();
        cpDir.copyDir(in, out);
        
        JOptionPane.showMessageDialog(null, "Das Projekt befindet sich jetzt im XAMPP Ordner.\n" +
        "Du kannst es in deinem Browser unter \"localhost/"+ name + "\" öffnen.\n","Luzien's Editor v0.2",JOptionPane.INFORMATION_MESSAGE);

            
    }
}

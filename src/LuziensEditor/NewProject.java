/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author markus
 */
 public class NewProject extends JDialog{
     boolean visible;
     NewProject(boolean visible){
         setTitle("Neues Projekt");
            setDefaultCloseOperation(1);
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            JPanel pnlInfo = new JPanel();
            pnlInfo.setBackground(Color.WHITE);
            JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
            txtInfo.setEditable(false);
            txtInfo.setText("<b>Neues Projekt</b><br>Hier kannst du ein neues Projekt anlegen.<br>");
            
            pnlInfo.add(txtInfo);
            c.gridx = 0;
            c.gridy = 0;
            c.fill = 1;
            add(pnlInfo, c);
            JLabel lblLanguage = new JLabel("Kategorien :");
            c.gridx = 0;
            c.gridy = 1;
            c.fill = 1;
            c.insets = new Insets(10, 10, 0, 10);
            add(lblLanguage, c);
            JPanel pnlLanguage = new JPanel();
            pnlLanguage.setLayout(new FlowLayout());
            ButtonGroup dlgGroup = new ButtonGroup();
            final JRadioButton rbJava = new JRadioButton("Java");
            final JRadioButton rbC = new JRadioButton("C");
            final JRadioButton rbCpp = new JRadioButton("C++");
            final JRadioButton rbWeb = new JRadioButton("Web");
            final JRadioButton rbGAS = new JRadioButton("Assembler");
            dlgGroup.add(rbJava);
            dlgGroup.add(rbC);
            dlgGroup.add(rbCpp);
            dlgGroup.add(rbWeb);
            dlgGroup.add(rbGAS);
            pnlLanguage.add(rbJava);
            pnlLanguage.add(rbC);
            pnlLanguage.add(rbCpp);
            pnlLanguage.add(rbWeb);
            pnlLanguage.add(rbGAS);
            c.gridx = 0;
            c.gridy = 2;
            c.fill = 1;
            c.insets = new Insets(0, 10, 10, 10);
            add(pnlLanguage, c);
            JLabel lblName = new JLabel("Projektname :");
            c.gridx = 0;
            c.gridy = 3;
            c.fill = 1;
            c.insets = new Insets(10, 10, 0, 10);
            add(lblName, c);
            final JTextField txtName = new JTextField();
            c.gridx = 0;
            c.gridy = 4;
            c.fill = 1;
            c.insets = new Insets(10, 10, 0, 10);
            add(txtName, c);
            JLabel lblVersion = new JLabel("Version :");
            c.gridx = 0;
            c.gridy = 5;
            c.fill = 1;
            c.insets = new Insets(10, 10, 0, 10);
            add(lblVersion, c);
            final JTextField txtVersion = new JTextField();
            txtVersion.setText("1.0");
            c.gridx = 0;
            c.gridy = 6;
            //c.fill = 1;
            c.insets = new Insets(10, 10, 0, 10);
            add(txtVersion, c);
            JLabel lblProgrammierer = new JLabel("Programmierer :");
            c.gridx = 0;
            c.gridy = 7;
            c.fill = 1;
            c.insets = new Insets(10, 10, 0, 10);
            add(lblProgrammierer, c);
            final JTextField txtProgrammierer = new JTextField();
            //txtVersion.setText("1.0");
            c.gridx = 0;
            c.gridy = 8;
            c.fill = 1;
            c.insets = new Insets(10, 10, 0, 10);
            add(txtProgrammierer, c);
            JLabel lblBeschreibung = new JLabel("Beschreibung :");
            c.gridx = 0;
            c.gridy = 9;
            c.fill = 1;
            c.insets = new Insets(10, 10, 0, 10);
            add(lblBeschreibung, c);
            final JTextField txtBeschreibung = new JTextField();
            //txtVersion.setText("1.0");
            c.gridx = 0;
            c.gridy = 10;
            c.fill = 1;
            c.insets = new Insets(10, 10, 0, 10);
            add(txtBeschreibung, c);
            JLabel lblFolder = new JLabel("Projektordner :");
            c.gridx = 0;
            c.gridy = 11;
            c.fill = 1;
            c.insets = new Insets(10, 10, 0, 10);
            add(lblFolder, c);
            String path = (new StringBuilder()).append(System.getProperty("user.home")).append("/LuziensProjects/").toString();
            JTextField txtFolder = new JTextField(path);
            txtFolder.setEditable(false);
            c.gridx = 0;
            c.gridy = 12;
            c.fill = 1;
            c.insets = new Insets(10, 10, 0, 10);
            add(txtFolder, c);
            JPanel pnlCreate = new JPanel();
            pnlCreate.setLayout(new FlowLayout());
            JButton btnCreate = new JButton("Projekt erstellen");
            btnCreate.addActionListener(new ActionListener() {
               @Override public void actionPerformed(ActionEvent ev10)
                {
                    LuziensEditor.FileStream newFile = new LuziensEditor.FileStream();
                    String AppName = txtName.getText();
                    String ClassName = txtName.getText();
                    ClassName = AppName.toLowerCase();
                    ClassName = AppName.replace(" ", "");
                    AppName = AppName.replace(" ", "");
                    String home = System.getProperty("user.home");
                    File file2 = new File((new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/build/").toString());
                    file2.mkdirs();
                    File file = new File((new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/src/").toString());
                    if(!file.exists())
                        if(file.mkdirs())
                        {
                            if(rbJava.isSelected())
                            {
                                String input = (new StringBuilder()).append("\n/*\n *\n * @author \n * @version \n *\n */\npublic class ").append(ClassName).append(" {\n").append("\n").append("    /**\n").append("     * @param args the command line arguments\n").append("     */\n").append("    public static void main(String[] args) {\n").append("        // TODO code application logic here\n").append("    }\n").append("}").toString();
                                try
                                {
                                    File file3 = new File((new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/src/").append(AppName).toString());
                                    file3.mkdirs();
                                    newFile.write((new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/src/").append(AppName).append("/").append(AppName).append(".java").toString(), input);
                                    LuziensEditor.textTab.addTab((new StringBuilder()).append(AppName).append(".java").toString(), new LuziensEditor.JavaEditor(input, (new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/src/").append(AppName).append("/").append(AppName).append(AppName).append(".java").toString()));
                                    String LibPfad = System.getProperty("user.home") + "/LuziensProjects/" + AppName + "/lib/";
                                    File LibFile = new File(LibPfad);
                                    LibFile.mkdirs();
                                    int i = LuziensEditor.textTab.getTabCount() - 1;
                                    LuziensEditor.textTab.setTabComponentAt(i, new ButtonTabComponent(LuziensEditor.textTab));
                                }
                                catch(IOException ex)
                                {
                                    Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            } else
                            if(rbC.isSelected())
                            {
                                String input = "#include <stdio.h>\n\n/*\n *\n * @author \n * @version \n *\n */\n    int main (int argc, char * argv []){\n        // TODO code application logic here\n        \n        return 0;\n    }\n";
                                try
                                {
                                    newFile.write((new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/src/").append(AppName).append(".c").toString(), input);
                                    LuziensEditor.textTab.addTab((new StringBuilder()).append(AppName).append(".c").toString(), new LuziensEditor.CPPEditor(input, (new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/src/").append(AppName).append(".c").toString()));
                                    int i = LuziensEditor.textTab.getTabCount() - 1;
                                    LuziensEditor.textTab.setTabComponentAt(i, new ButtonTabComponent(LuziensEditor.textTab));
                                }
                                catch(IOException ex)
                                {
                                    Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            } else
                            if(rbCpp.isSelected())
                            {
                                String input = "#include <stdio.h>\n#include <iostream>\nusing namespace std;\n/*\n *\n * @author \n * @version \n *\n */\n    int main (int argc, char * argv []){\n        // TODO code application logic here\n        \n        return 0;\n    }\n";
                                try
                                {
                                    newFile.write((new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/src/").append(AppName).append(".cpp").toString(), input);
                                    LuziensEditor.textTab.addTab((new StringBuilder()).append(AppName).append(".cpp").toString(), new LuziensEditor.CPPEditor(input, (new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/src/").append(AppName).append(".cpp").toString()));
                                    int i = LuziensEditor.textTab.getTabCount() - 1;
                                    LuziensEditor.textTab.setTabComponentAt(i, new ButtonTabComponent(LuziensEditor.textTab));
                                }
                                catch(IOException ex)
                                {
                                    Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            } else
                            if(rbWeb.isSelected())
                            {
                                String input = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n<head>\n\t<style type=\"text/css\">\n  </style>\n\t<script type=\"text/javascript\">\n   <!--\n   /* ... enter your code here ... */\n   -->\n \n\t</script>\t\n\t<title></title>\n</head>\n<body>\n\n</body>\n</html>";
                                try
                                {
                                    newFile.write((new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/src/").append("index.html").toString(), input);
                                    LuziensEditor.textTab.addTab((new StringBuilder()).append(AppName).append(".html").toString(), new LuziensEditor.HTMLEditor(input, (new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/src/").append("index.html").toString()));
                                    int i = LuziensEditor.textTab.getTabCount() - 1;
                                    LuziensEditor.textTab.setTabComponentAt(i, new ButtonTabComponent(LuziensEditor.textTab));
                                }
                                catch(IOException ex)
                                {
                                    Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }else
                            if(rbGAS.isSelected())
                            {
                                String input = ".section .data\n" +
                                                "\n" +
                                                ".section .text\n" +
                                                "\n" +
                                                ".global _start\n" +
                                                "_start:\n" +
                                                "	movl $1, %eax\n" +
                                                "	movl $0, %ebx\n" +
                                                "	int $0x80";
                                try
                                {
                                    newFile.write((new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/src/").append(AppName).append(".S").toString(), input);
                                    LuziensEditor.textTab.addTab((new StringBuilder()).append(AppName).append(".S").toString(), new LuziensEditor.ASMEditor(input, (new StringBuilder()).append(home).append("/LuziensProjects/").append(AppName).append("/src/").append(AppName).append(".S").toString()));
                                    int i = LuziensEditor.textTab.getTabCount() - 1;
                                    LuziensEditor.textTab.setTabComponentAt(i, new ButtonTabComponent(LuziensEditor.textTab));
                                }
                                catch(IOException ex)
                                {
                                    Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        } else
                        {
                            System.out.println("Failed to create directory!");
                            JOptionPane.showMessageDialog(null, "Failed to create Project!", "Luzien's Editor v0.4", 0);
                        }
                    String name = AppName;
                    String version = txtVersion.getText();
                    String programmierer = txtProgrammierer.getText();
                    String beschreibung = txtBeschreibung.getText();
                    String pfad = System.getProperty("user.home") + "/LuziensProjects/" + name + "/src/";
                   try {
                       LuziensEditor.timeline_src.NewProject timelineproject = new LuziensEditor.timeline_src.NewProject(name,version,programmierer,beschreibung,pfad);
                   } catch (IOException ex) {
                       Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                   }
                    System.exit(0);
                }

 
            }
);
            JButton btnDispose = new JButton("Abbrechen");
            btnDispose.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent ae)
                {
                    dispose();
                }

            }
);
            pnlCreate.add(btnCreate);
            pnlCreate.add(btnDispose);
            c.gridx = 0;
            c.gridy = 13;
            c.fill = 1;
            c.insets = new Insets(10, 10, 10, 10);
            add(pnlCreate, c);
            pack();
            setVisible(visible);
     }
 }

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
/**
 *
 * @author markusschnittker
 */

public class MySqlMonitor extends JFrame {
    JTable table;
    private DefaultTableModel tblDataModel = null;
    private DatabaseMetaData dbmd = null;
    ResultSet rsDatabase;
    
    private Connection conn;
    private ImageIcon iconDatabase = new ImageIcon(getClass().getResource("Toolbar/database.png"));
    private ImageIcon iconTable = new ImageIcon(getClass().getResource("Toolbar/table.png"));
    private ImageIcon iconField = new ImageIcon(getClass().getResource("Toolbar/field.png"));
    
    public javax.swing.JScrollPane scroller;
    public javax.swing.JTree tree;
    
    String datenbank,tabelle;
     void showTableContent(){
        if (conn != null){
            try{
                
                Statement stmt = conn.createStatement();
                String db = datenbank;
                if (db == null || db == "")
                    return;
                stmt.execute("USE " + db);
                String tbl = tabelle;
                if (tbl == null || tbl == "")
                    return;
                ResultSet rs = stmt.executeQuery("SELECT * FROM " + tbl);
                tblDataModel = new DefaultTableModel();
                Vector clmHeader = new Vector();
                Vector dataVector = new Vector();
                
                ResultSetMetaData rsmd = rs.getMetaData();
                int clmCnt = rsmd.getColumnCount();
                for(int i = 1; i <= clmCnt;i++)
                    clmHeader.addElement(rsmd.getColumnName(i));
                
                while(rs.next()){
                    Vector rowVector = new Vector();
                    for(int i = 1; i <= clmCnt; i++){
                        rowVector.addElement(rs.getString(i));
                    }
                    dataVector.addElement(rowVector);
                }
                
                tblDataModel.setDataVector(dataVector,clmHeader);
                
                this.table.setModel(tblDataModel);
                this.table.updateUI();
                
               // System.out.println("finished");

            }catch(SQLException sqle){
                sqle.printStackTrace();
            }
        }
    }


    /** Creates new form JTreeMySQL */
    
    String host,user,pass;
    public MySqlMonitor(String host,String user,String pass) {
        
        this.host = host;
        int port = 3306;
        this.user = user;
        this.pass = pass;
        
        scroller = new javax.swing.JScrollPane();
        tree = new javax.swing.JTree();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("MySQL Monitor");
        setLayout(new BorderLayout());

        /* Info */
        JPanel pnlOben = new JPanel();
        pnlOben.setLayout(new BorderLayout());
        
        JPanel pnlInfo = new JPanel();
        pnlInfo.setLayout(new BorderLayout());
        pnlInfo.setBackground(Color.WHITE);
        JEditorPane txtInfo = new JEditorPane();
        txtInfo.setContentType("text/html");
        txtInfo.setEditable(false);
        txtInfo.setBackground(Color.WHITE);
        txtInfo.setText("<b>MySQL Monitor</b><br>Hier kannst du Datenbanken anlegen und bearbeiten.<br><br>");
        pnlInfo.add(txtInfo,BorderLayout.PAGE_START);
        pnlOben.add(pnlInfo,BorderLayout.NORTH);
        
        JToolBar tbpMenu = new JToolBar();
        JButton btnEnde = new JButton("<-");
        tbpMenu.add(btnEnde);
        
        pnlOben.add(tbpMenu,BorderLayout.SOUTH);
        
        add(pnlOben,BorderLayout.NORTH);
        
        /* Links -> Datenbank anzeigen */
        scroller.setName("scroller"); // NOI18N

        tree.setName("tree"); // NOI18N
        scroller.setViewportView(tree);
        
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(user + "@" + host + ":" + port);
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.exit(0);
        } catch (InstantiationException ex) {
            ex.printStackTrace();
            System.exit(0);
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
            System.exit(0);
        }
        try {
            conn = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/mysql", user, pass);
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.exit(0);
        }
        try {
            Statement sDatabase = conn.createStatement();
            rsDatabase = sDatabase.executeQuery("SHOW DATABASES");
            rsDatabase.beforeFirst();
            while (rsDatabase.next()) {
                DefaultMutableTreeNode nodeDatabase = new DefaultMutableTreeNode(rsDatabase.getString("Database"));
                root.add(nodeDatabase);
                Statement sTable = conn.createStatement();
                ResultSet rsTable = sTable.executeQuery("SHOW TABLES FROM `" + rsDatabase.getString("Database") + "`");
                rsTable.beforeFirst();
                while (rsTable.next()) {
                    DefaultMutableTreeNode nodeTable = new DefaultMutableTreeNode(rsTable.getString("Tables_in_" + rsDatabase.getString("Database")));
                    nodeDatabase.add(nodeTable);
                    Statement sField = conn.createStatement();
//                    ResultSet rsField = sField.executeQuery("SHOW FIELDS FROM `" + rsDatabase.getString("Database") + "`.`" + rsTable.getString("Tables_in_" + rsDatabase.getString("Database")) + "`");
//                    rsField.beforeFirst();
//                    while (rsField.next()) {
//                        DefaultMutableTreeNode nodeField = new DefaultMutableTreeNode(rsField.getString("Field") + " - " + rsField.getString("Type"));
//                        nodeTable.add(nodeField);
//                    }
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.exit(0);
        }
        tree.setModel(new DefaultTreeModel(root));
        tree.setCellRenderer(new TreeCellRenderer() {

            @Override
            public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
                JLabel label = new JLabel(value.toString());
                if (tree.getPathForRow(row).getPathCount() == 2) {
                    label.setIcon(iconDatabase);
                } else if (tree.getPathForRow(row).getPathCount() == 3) {
                    label.setIcon(iconTable);
                } else if (tree.getPathForRow(row).getPathCount() == 4) {
                    label.setIcon(iconField);
                }
                if (selected) {
                    label.setOpaque(true);
                    label.setBackground(tree.getBackground().darker());
                }
                if (hasFocus) {
                    label.setBorder(BorderFactory.createLineBorder(Color.RED));
                }
                return label;
            }
        });
        
        tree.addTreeSelectionListener(new TreeSelectionListener(){
            @Override public void valueChanged(TreeSelectionEvent se) {

                  Object node = tree.getLastSelectedPathComponent();  
                  String name;  
                  name = (node == null) ? "NONE" : node.toString();
                  datenbank = name;

                  JOptionPane.showMessageDialog(null, datenbank + " " + tabelle);
              }
        });
        
        scroller.setName("scroller"); // NOI18N

        tree.setName("tree"); // NOI18N
        //scroller.setViewportView(tree);
        scroller = new javax.swing.JScrollPane(tree);
        add(scroller,BorderLayout.WEST);
        

        /* Main Bereich */
        table = new JTable(tblDataModel);
        add(table,BorderLayout.CENTER);
       
        
        /* Statusbar */
        JPanel statusBar = new JPanel();
        statusBar.setLayout(new FlowLayout());
        add(statusBar, BorderLayout.SOUTH);
        statusBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        statusBar.setBorder(BorderFactory.createEtchedBorder());
        statusBar.add(new JLabel("Verbunden als " + user + "@" + host + ":" + port));
        setSize(600,400);
        setVisible(true);
    }

}
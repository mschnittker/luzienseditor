/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static LuziensEditor.LuziensEditor.FileName;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author markus
 */
public class NewFile extends JDialog{
    boolean visible;
    Object selected;
    NewFile(boolean visible){
        setTitle("Neue Datei");
            setDefaultCloseOperation(1);
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            JPanel pnlInfo = new JPanel();
            pnlInfo.setLayout(new BorderLayout());
            pnlInfo.setBackground(Color.WHITE);
            JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
            txtInfo.setEditable(false);
            txtInfo.setBackground(Color.WHITE);
            txtInfo.setText("<b>Neue Datei</b><br>Bitte wähle zuerst ein Projekt aus, dann kannst du <br>die entsprechende Datei hinzufügen.<br><br>");
//            ImageIcon DateiIcon = new ImageIcon(LuziensEditor.class.getResource("pics/neue_datei.png"));
//            JLabel txtInfo = new JLabel(DateiIcon);
            pnlInfo.add(txtInfo, "North");
            JPanel pnlChoose = new JPanel();
            pnlChoose.setLayout(new FlowLayout());
            JLabel lblProject = new JLabel("Projekt : ");
            String path = (new StringBuilder()).append(System.getProperty("user.home")).append("/LuziensProjects/").toString();
            File f = new File(path);
            String files[] = f.list();
            JComboBox cbProject = new JComboBox(files);
            cbProject.setPreferredSize(new Dimension(400, 25));
            cbProject.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent ev01)
                {
                    JComboBox cbProject = (JComboBox)ev01.getSource();
                    selected = cbProject.getSelectedItem();
                    LuziensEditor.FilePath = (new StringBuilder()).append(System.getProperty("user.home")).append("/LuziensProjects/").append(String.valueOf(selected)).toString();
                }

            }
            );
            pnlChoose.add(lblProject);
            pnlChoose.add(cbProject);
            pnlInfo.add(pnlChoose, "South");
            c.gridx = 0;
            c.gridy = 0;
            c.fill = 1;
            c.gridwidth = 2;
            c.insets = new Insets(0, 0, 10, 0);
            add(pnlInfo, c);
            JPanel pnlLanguage = new JPanel();
            pnlLanguage.setLayout(new BorderLayout());
            JLabel lblCat = new JLabel("Kategorien :");
            String LanguageList[] = {
                "Java          ", "C        ", "C++      ", "Web      ", "Andere       ","Assembler"
            };
            final JList cbLanguage = new JList(LanguageList);
            cbLanguage.setPreferredSize(new Dimension(200, 150));
            cbLanguage.setSelectedIndex(0);
            cbLanguage.setSelectionMode(1);
            cbLanguage.setLayoutOrientation(0);
            cbLanguage.setSelectedIndex(1);
            cbLanguage.addListSelectionListener(new ListSelectionListener() {

                public void valueChanged(ListSelectionEvent e)
                {
                    if(!e.getValueIsAdjusting())
                        if(cbLanguage.getSelectedIndex() == -1)
                        {
                            String select[] = {
                                " ", " ", " ", " "
                            };
                            LuziensEditor.cbFile.setListData(select);
                        } else
                        if(cbLanguage.getSelectedIndex() == 0)
                        {
                            String select[] = {
                                "Java Class", "Java Main", "Leere Java Datei", "Java JFrame Form" , "Java JDialog Form" , "Hello World Konsolenprogramm"
                            };
                            LuziensEditor.cbFile.setListData(select);
                        } else
                        if(cbLanguage.getSelectedIndex() == 1)
                        {
                            String select[] = {
                                "C Main", "C Header", " ", " "
                            };
                            LuziensEditor.cbFile.setListData(select);
                        } else
                        if(cbLanguage.getSelectedIndex() == 2)
                        {
                            String select[] = {
                                "C++ Main", "C++ Header", "C++ Class", " "
                            };
                            LuziensEditor.cbFile.setListData(select);
                        } else
                        if(cbLanguage.getSelectedIndex() == 3)
                        {
                            String select[] = {
                                "HTML Datei", "HTML 5 Datei", "PHP Datei", "CSS Datei"
                            };
                            LuziensEditor.cbFile.setListData(select);
                        }
                        if(cbLanguage.getSelectedIndex() == 4)
                        {
                            String select[] = {
                                "SH Script","Bash Script","Leere Datei","Timeline Config"
                            };
                            LuziensEditor.cbFile.setListData(select);
                        }
                        if(cbLanguage.getSelectedIndex() == 5)
                        {
                            String select[] = {
                                "Leere ASM Datei" , "Hello World Konsolenprogramm"
                            };
                            LuziensEditor.cbFile.setListData(select);
                        }
                }


            }
);
            pnlLanguage.add(lblCat, "North");
            pnlLanguage.add(cbLanguage, "South");
            c.gridx = 0;
            c.gridy = 1;
            c.gridwidth = 1;
            c.insets = new Insets(0, 10, 10, 10);
            add(pnlLanguage, c);
            JPanel pnlFile = new JPanel();
            pnlFile.setLayout(new BorderLayout());
            JLabel lblFile = new JLabel("Dateien : ");
            LuziensEditor.cbFile = new JList();
            LuziensEditor.cbFile.setPreferredSize(new Dimension(200, 150));
            LuziensEditor.cbFile.addListSelectionListener(new ListSelectionListener() {

                public void valueChanged(ListSelectionEvent ev)
                {
                    if(!ev.getValueIsAdjusting())
                        if(LuziensEditor.cbFile.getSelectedValue().equals("Java Class"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine leere Java Class Datei");
                            LuziensEditor.KindOfFile = "Java Class";
                            LuziensEditor.FileName = ".java";
                            LuziensEditor.ContentOfFile = "/*\n * \n *\n */\n\n/**\n *\n * @author \n */\npublic class NewClass {\n    \n}";
                        } else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("Java Main"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine Java Main Datei");
                            LuziensEditor.KindOfFile = "Java Main";
                            LuziensEditor.FileName = ".java";
                            LuziensEditor.ContentOfFile = "/*\n * \n * \n */\n\n/**\n *\n * @author \n */\npublic class NewMain {\n\n    /**\n     * @param args the command line arguments\n     */\n    public static void main(String[] args) {\n        // TODO code application logic here\n    }\n}";
                        } else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("Leere Java Datei"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine leere Java Datei");
                            LuziensEditor.KindOfFile = "Leere Java Datei";
                            LuziensEditor.FileName = ".java";
                            LuziensEditor.ContentOfFile = "/*\n * \n * \n */\n\n/**\n *\n * @author \n */\n\n\n\n\n\n\n\n\n";
                        } else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("C Main"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine C Main Datei");
                            LuziensEditor.KindOfFile = "C Main";
                            LuziensEditor.FileName = ".c";
                            LuziensEditor.ContentOfFile = "#include <stdio.h>\n\nint main( int argc, const char* argv[] )\n{\n\treturn 0;\n}";
                        } else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("C Header"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine C Header Datei");
                            LuziensEditor.KindOfFile = "C Header";
                            LuziensEditor.FileName = ".h";
                            LuziensEditor.ContentOfFile = "#ifndef MYHEADER_H\n#define MYHEADER_H\n \n \n\n \n\n\n \n#endif /* MYHEADER_H */";
                        } else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("C++ Main"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine C++ Main Datei");
                            LuziensEditor.KindOfFile = "C++ Main";
                            LuziensEditor.FileName = ".cpp";
                            LuziensEditor.ContentOfFile = "#include <iostream>\nusing namespace std;\n\nint main( int argc, const char* argv[] )\n{\n\n\n\nreturn 0;\n}";
                        } else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("C++ Header"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine C++ Header Datei");
                            LuziensEditor.KindOfFile = "C++ Header";
                            LuziensEditor.FileName = ".h";
                            LuziensEditor.ContentOfFile = "#ifndef MYHEADER_H\n#define MYHEADER_H\n \n \n\n \n\n\n \n#endif /* MYHEADER_H */";
                        } else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("C++ Class"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine C++ Class Datei");
                            LuziensEditor.KindOfFile = "C++ Class";
                            LuziensEditor.FileName = ".cpp";
                            LuziensEditor.ContentOfFile = "class CLASSNAME\n{\n  public:\n    \n \n  protected:\n    \n \n  private:\n    \n \n  \n};";
                        } else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("HTML Datei"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine neue HTML Datei mit \nmit JavaScript und CSS");
                            LuziensEditor.KindOfFile = "HTML Datei";
                            LuziensEditor.FileName = ".html";
                            LuziensEditor.ContentOfFile = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n<head>\n\t<style type=\"text/css\">\n</style>\n\t<script type=\"text/javascript\">\n   <!--\n   /* ... enter your code here ... */\n   -->\n \n\t</script>\t\n\t<title></title>\n</head>\n<body>\n\n</body>\n</html>";
                        } else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("HTML 5 Datei"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine html 5 Datei");
                            LuziensEditor.KindOfFile = "HTML 5 Datei";
                            LuziensEditor.FileName = ".html";
                            LuziensEditor.ContentOfFile = "<!DOCTYPE html>\n<html>\n<head>\n<meta charset=\"utf-8\" />\n<title></title>\n</head>\n<body>\n\n</body>\n</html>";
                        } else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("PHP Datei"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine PHP Datei");
                            LuziensEditor.KindOfFile = "PHP Datei";
                            LuziensEditor.FileName = ".php";
                            LuziensEditor.ContentOfFile = "<?php\n\n?>";
                        } else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("CSS Datei"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine CSS Datei");
                            LuziensEditor.KindOfFile = "CSS Datei";
                            LuziensEditor.FileName = ".css";
                            LuziensEditor.ContentOfFile = "";
                        }
                        else if(LuziensEditor.cbFile.getSelectedValue().equals("SH Script"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine SH Datei");
                            LuziensEditor.KindOfFile = "SH Datei";
                            LuziensEditor.FileName = ".sh";
                            LuziensEditor.ContentOfFile = "#!/bin/sh";
                        }
                        else if(LuziensEditor.cbFile.getSelectedValue().equals("Bash Script"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine BASH Datei");
                            LuziensEditor.KindOfFile = "Bash Datei";
                            LuziensEditor.FileName = ".bash";
                            LuziensEditor.ContentOfFile = "#!/bin/bash";
                        }
                        else if(LuziensEditor.cbFile.getSelectedValue().equals("Leere Datei"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine leere Datei");
                            LuziensEditor.KindOfFile = "Leere Datei";
                            LuziensEditor.FileName = ".txt";
                            LuziensEditor.ContentOfFile = "";
                        }
                        
                        else if(LuziensEditor.cbFile.getSelectedValue().equals("Timeline Config"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine Timeline Config Datei");
                            LuziensEditor.KindOfFile = "Timeline config Datei";
                            LuziensEditor.FileName = ".conf";
                            LuziensEditor.ContentOfFile = "[Name]\n" +
                                                          "[Version]\n" +
                                                          "[Developer]\n" +
                                                          "[Description]\n" +
                                                          "[Path]\n" + 
                                                          "# Enter your code without []";
                        }
                        else if(LuziensEditor.cbFile.getSelectedValue().equals("Leere ASM Datei"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine leere ASM Datei");
                            LuziensEditor.KindOfFile = "Leere *.S Datei";
                            LuziensEditor.FileName = ".S";
                            LuziensEditor.ContentOfFile = ".section .data\n" +
                                                            "\n" +
                                                            ".section .text\n" +
                                                            "\n" +
                                                            ".global _start\n" +
                                                            "_start:\n" +
                                                            "	movl $1, %eax\n" +
                                                            "	movl $0, %ebx\n" +
                                                            "	int $0x80";
                        }
                        else if(LuziensEditor.cbFile.getSelectedValue().equals("Hello World Konsolenprogramm"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt ein Konsolenprogramm, das \"Hello World\" aus gibt");
                            LuziensEditor.KindOfFile = "Eine *.java Datei";
                            LuziensEditor.FileName = ".java";
                            LuziensEditor.ContentOfFile = "\n" +
                                                            "/**\n" +
                                                            " *\n" +
                                                            " * @author \n" +
                                                            " */\n" +
                                                            "public class NewMain {\n" +
                                                            " \n" +
                                                            "    /**\n" +
                                                            "     * @param args the command line arguments\n" +
                                                            "     */\n" +
                                                            "    public static void main(String[] args) {\n" +
                                                            "        // TODO code application logic here\n" +
                                                            "        System.out.println(\"Hello World\");\n" +
                                                            "    }\n" +
                                                            "}";
                        }
                        else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("Java JFrame Form"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine Datei, die ein Java JFrame beinhaltet.");
                            LuziensEditor.KindOfFile = "Java JFrame Form";
                            LuziensEditor.FileName = ".java";
                            LuziensEditor.ContentOfFile = "/*\n" +
                                                            " * To change this template, choose Tools | Templates\n" +
                                                            " * and open the template in the editor.\n" +
                                                            " */\n" +
                                                            "\n" +
                                                            "\n" +
                                                            "import java.awt.BorderLayout;\n" +
                                                            "import javax.swing.JFrame;\n" +
                                                            "\n" +
                                                            "/**\n" +
                                                            " *\n" +
                                                            " * @author \n" +
                                                            " */\n" +
                                                            "public class NewMain {\n" +
                                                            "   static class CLASSNAME extends JFrame{\n" +
                                                            "        public void CLASSNAME(){\n" +
                                                            "            setTitle(\"\");\n" +
                                                            "            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);\n" +
                                                            "            setLayout(new BorderLayout());\n" +
                                                            "            \n" +
                                                            "            pack();\n" +
                                                            "            setVisible(true);\n" +
                                                            "        }\n" +
                                                            "    }\n" +
                                                            "    /**\n" +
                                                            "     * @param args the command line arguments\n" +
                                                            "     */\n" +
                                                            "    public static void main(String[] args) {\n" +
                                                            "        // TODO code application logic here\n" +
                                                            "        CLASSNAME frmMain = new CLASSNAME();\n" +
                                                            "    }\n" +
                                                            "}";
                        }else
                        if(LuziensEditor.cbFile.getSelectedValue().equals("Java JDialog Form"))
                        {
                            LuziensEditor.FileInfo.setText("Erstellt eine Datei, die ein Java JDialog beinhaltet.");
                            LuziensEditor.KindOfFile = "Java JDialog Form";
                            LuziensEditor.FileName = ".java";
                            LuziensEditor.ContentOfFile = "/*\n" +
                                                        " * To change this template, choose Tools | Templates\n" +
                                                        " * and open the template in the editor.\n" +
                                                        " */\n" +
                                                        "\n" +
                                                        "\n" +
                                                        "import java.awt.BorderLayout;\n" +
                                                        "import javax.swing.JDialog;\n" +
                                                        "\n" +
                                                        "\n" +
                                                        "/**\n" +
                                                        " *\n" +
                                                        " * @author \n" +
                                                        " */\n" +
                                                        "public class NewMain {\n" +
                                                        "   static class CLASSNAME extends JDialog{\n" +
                                                        "        public void CLASSNAME(){\n" +
                                                        "            setTitle(\"\");\n" +
                                                        "            setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);\n" +
                                                        "            setLayout(new BorderLayout());\n" +
                                                        "            \n" +
                                                        "            pack();\n" +
                                                        "            setVisible(true);\n" +
                                                        "        }\n" +
                                                        "    }\n" +
                                                        "    /**\n" +
                                                        "     * @param args the command line arguments\n" +
                                                        "     */\n" +
                                                        "    public static void main(String[] args) {\n" +
                                                        "        // TODO code application logic here\n" +
                                                        "        NewMain.CLASSNAME frmMain = new NewMain.CLASSNAME();\n" +
                                                        "    }\n" +
                                                        "}";
                        }
                }

            }
);
            pnlFile.add(lblFile, "North");
            pnlFile.add(LuziensEditor.cbFile, "South");
            c.gridx = 1;
            c.gridy = 1;
            c.gridwidth = 1;
            add(pnlFile, c);
            JPanel pnlButton = new JPanel();
            pnlButton.setLayout(new GridBagLayout());
            GridBagConstraints cBtn = new GridBagConstraints();
            LuziensEditor.FileInfo = new JTextArea(3, 42);
            LuziensEditor.FileInfo.setEditable(false);
            JPanel pnlBtn = new JPanel();
            pnlBtn.setLayout(new FlowLayout());
            JButton btnCreate = new JButton("Neue Datei");
            btnCreate.addActionListener(new ActionListener() {

             @Override public void actionPerformed(ActionEvent evt01)
                {
                    FileWriter writer = null;

                    try
                    {   
                        String filename;
                        if(FileName.equals(".conf")){
                            filename = JOptionPane.showInputDialog(null, "Wie soll die neue Datei heißen ? ","timeline");
                        }
                        else{
                            filename = JOptionPane.showInputDialog(null, "Wie soll die neue Datei heißen ? ");
                        }
                            
                        filename = filename + FileName;
                        String content = LuziensEditor.ContentOfFile;
                        String path = null;
                        if(FileName.equals(".java")){
                            path = (new StringBuilder()).append(LuziensEditor.FilePath).append("/src/").append(String.valueOf(selected)).append("/").append(filename).toString();
                        }
                        else{
                            path = (new StringBuilder()).append(LuziensEditor.FilePath).append("/src/").append(filename).toString();
                        }
                        File file = new File(path);
                        writer = new FileWriter(file);
                        writer.write(content);
                        writer.close();
                        String name = filename;
                        name = name.replace("/", "");
                        LuziensEditor.textTab.addTab(name, new LuziensEditor.Editor(LuziensEditor.ContentOfFile, path));
                        int i = LuziensEditor.textTab.getTabCount() - 1;
                        LuziensEditor.textTab.setTabComponentAt(i, new ButtonTabComponent(LuziensEditor.textTab));
                        IOException ex;
                        try
                        {
                            writer.close();
                        }
                        // Misplaced declaration of an exception variable
                        catch(IOException ex0)
                        {
                            Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex0);
                        }
                        try
                        {
                            writer.close();
                        }
                        // Misplaced declaration of an exception variable
                        catch(IOException ex1)
                        {
                        }
                        try
                        {
                            writer.close();
                        }
                        catch(IOException ex2)
                        {

                        }
                    }
                    catch(IOException ex)
                    {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);

                    } finally {
                        try {
                            writer.close();
                        } catch (IOException ex) {
                            Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    
                  tbLeft.validate();
                  //System.exit(0);
                }

                
            }
);
            JButton btnDispose = new JButton("Abbrechen");
            btnDispose.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent ae)
                {
                    dispose();
                }

            }
);
            pnlBtn.add(btnCreate);
            pnlBtn.add(btnDispose);
            cBtn.gridx = 0;
            cBtn.gridy = 0;
            cBtn.fill = 1;
            cBtn.gridwidth = 2;
            cBtn.insets = new Insets(10, 0, 10, 0);
            pnlButton.add(LuziensEditor.FileInfo, cBtn);
            cBtn.gridx = 0;
            cBtn.gridy = 1;
            cBtn.insets = new Insets(0, 10, 0, 10);
            pnlButton.add(pnlBtn, cBtn);
            c.gridx = 0;
            c.gridy = 2;
            c.insets = new Insets(0, 10, 0, 10);
            c.fill = 1;
            c.gridwidth = 2;
            add(pnlButton, c);
            pack();
            setVisible(visible);
        }
}

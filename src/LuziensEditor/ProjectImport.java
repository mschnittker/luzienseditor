/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;

/**
 *
 * @author markusschnittker
 */
public class ProjectImport extends JDialog{
    boolean visible;
    
    JTextField txtName;
    JTextField txtPath;
    ProjectImport(boolean visible){
        this.visible = visible;
        
        setTitle("Projekt importieren");
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setLayout(new BorderLayout());
        
        /* Info */
        JPanel pnlInfo = new JPanel();
        pnlInfo.setLayout(new BorderLayout());
        pnlInfo.setBackground(Color.WHITE);
        JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
        txtInfo.setEditable(false);
        txtInfo.setBackground(Color.WHITE);
        txtInfo.setText("<b>Projekt importieren</b><br>Hier kannst du ein Projekt in Luzien importieren.<br><br>");
        pnlInfo.add(txtInfo,BorderLayout.PAGE_START);
        add(pnlInfo,BorderLayout.NORTH);
        
        /* Main */
        JPanel pnlMain = new JPanel();
        pnlMain.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        JLabel lblName = new JLabel("Projektname : ");
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(10,0,0,0);
        pnlMain.add(lblName,c);
        
        txtName = new JTextField(25);
        c.gridx = 1;
        c.gridy = 0;
        pnlMain.add(txtName,c);
        
        JLabel lblPath = new JLabel("Pfad : ");
        c.gridx = 0;
        c.gridy = 1;
        pnlMain.add(lblPath,c);
        
        txtPath = new JTextField(25);
        c.gridx = 1;
        c.gridy = 1;
        pnlMain.add(txtPath,c);
        
        JButton btnFile = new JButton("...");
        btnFile.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ev){
                JFileChooser fc = new JFileChooser();
//                fc.setFileFilter( new FileFilter()
//                {
//                  @Override public boolean accept( File f )
//                  {
//                    return f.isDirectory() ||
//                      f.getName().toLowerCase().endsWith( ".txt" );
//                  }
//                  @Override public String getDescription()
//                  {
//                    return "Texte";
//                  }
//                } );
                int state = fc.showOpenDialog( null );
                if ( state == JFileChooser.APPROVE_OPTION )
                {
                  File file = fc.getCurrentDirectory();
                  txtPath.setText(file.getPath() + "/");
                }
            }
        });
        c.gridx = 2;
        c.gridy = 1;
        pnlMain.add(btnFile,c);
        
        add(pnlMain,BorderLayout.CENTER);
        
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new FlowLayout());
        
        JButton btnCreate = new JButton("Projekt importieren");
        btnCreate.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ev){
                String name = txtName.getText();
                String path = txtPath.getText();
                
                /* Ordner in LuziensProjects erstellen */
                String Luzien_Path = System.getProperty("user.home") + File.separator + "LuziensProjects" + 
                                     File.separator + name;
                
                String Luzien_Path_Build = System.getProperty("user.home") + File.separator + "LuziensProjects" + 
                                     File.separator + name + File.separator + "build";
                
                String Luzien_Path_Src = System.getProperty("user.home") + File.separator + "LuziensProjects" + 
                                     File.separator + name + File.separator + "src";
                
                File ProjectDir = new File(Luzien_Path);
                File ProjectDir_Build = new File(Luzien_Path_Build);
                File ProjectDir_Src = new File(Luzien_Path_Src);
                
                File ProjectHome = new File(path);
                
                if(!ProjectDir.isDirectory()){
                    ProjectDir.mkdir();
                    ProjectDir_Build.mkdir();
                    ProjectDir_Src.mkdir();
                    /* Dateien in den "src" Ordner kopieren */
                    CopyDirectory create = new CopyDirectory(); 
                    try {
                        create.copyDir(ProjectHome, ProjectDir_Src);
                        JOptionPane.showMessageDialog(null, "Projekt erfolgreich importiert!", "Luzien's Editor v0.2", JOptionPane.INFORMATION_MESSAGE);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(ProjectImport.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(ProjectImport.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else{
                    JOptionPane.showMessageDialog(null, "Der Projektordner \"LuziensProjects\" existiert bereits!", "Luzien's Editor v0.2", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        JButton btnDispose = new JButton("Abbrechen");
        btnDispose.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ev){
                dispose();
            }
        });
        
        pnlButtons.add(btnCreate);
        pnlButtons.add(btnDispose);
        add(pnlButtons,BorderLayout.SOUTH);
        
        pack();
        setVisible(visible);
    }
}

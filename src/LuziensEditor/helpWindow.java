/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;

/**
 *
 * @author markus
 */
public class helpWindow extends JDialog{
        boolean visible;
        
        helpWindow(){}
        
        // [F4] -> Java
        void Java(boolean visible){
            setTitle("Java");
            setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
            
            setLayout(new BorderLayout());
            
            final JTextArea txtInfo = new JTextArea(20,40);
            
            txtInfo.setDragEnabled(true);
            txtInfo.setEditable(false);
            
            String strInfo = "ABORT\n" +
                             "ALLBITS\n" +
                             "BOTTOM_ALIGNMENT\n" +
                             "CENTER_ALIGNMENT\n" +
                             "DEFAULT_MODALITY_TYPE\n" +
                             "DISPOSE_ON_CLOSE\n" +
                             "DO_NOTHING_ON_CLOSE\n" +
                             "EXIT\n" +
                             "EXIT_ON_CLOSE\n" +
                             "FRAMEBITS\n" +
                             "HEIGHTS\n" +
                             "HIDE_ON_CLOSE\n" +
                             "LEFT_ALIGNMENT\n" +
                             "PROPERTIES\n" +
                             "RIGHT_ALIGNMENT\n" +
                             "SOMEBITS\n" +
                             "TOP_ALIGNMENT\n" +
                             "WIDTH\n" +
                             "accessibleContex\n" +
                             "rootPane\n" +
                             "rootPaneCheckingEnabled\n" +
                             "action(Event event,Object o)\n"+
                             "add(Component cmpnt)\n"+
                             "add(PopupMenu pm)\n"+
                             "add(Component cmpnt, Object o)\n"+
                             "add(Component cmpnt, int i)\n"+
                             "add(String string, Component cmpnt)\n"+
                             "add(Component  cmpnt, Object o, int i)\n" +
                             "addComponentListener(ComponentListener c1)\n" +
                             "addContainerListener(ContainerListener c1)\n" +
                             "addFocusListener(FokusListener f1)\n" +
                             "addHirarchyBoundsListener(HirarchyBoundsListener h1)\n" +
                             "addHirarchyListener(HirarchyListener h1)\n" +
                             "addImpl(Object o, Component cmppnt, int i)\n" +
                             "addInputMethodListener(InputMethodListener il)\n" +
                             "addKeyListener(KeyListener kl)\n" +
                             "addMouseListener(MouseListener ml)\n" +
                             "addMouseMotionListener(MouseMotionListener ml)\n" +
                             "addMouseWheelListener(MouseWheelListener ml)\n" +
                             "addNotify()\n" +
                             "addPropertyChangeListener(PropertyChangeListener pl)\n" +
                             "addPropertyChangeListener(String string, PropertyChangeListener pl)\n" +
                             "addWindowFocusListener(WindowFocusListener wl)\n" +
                             "addWindowListener(WindowsListener wl)\n" +
                             "addWindowStateListener(WindowStateListener wl)\n" +
                             "applyComponentOrientation(ComponentOrientation co)\n" +
                             "applyResourceBundle(ResourceBundle rb)\n" +
                             "applyResourceBundle(String string)\n" +
                             "areFocusTraversalKeySet(int i)\n" +
                             "bounds()\n" +
                             "CheckImage(Image image, ImageObserver io)\n" +
                             "CheckImage(Image image, int i, int il, ImageObserver io)\n" +
                             "clone()\n" +
                             "coalesceEvent(AWTEvent awte, AWTEvent awtel)\n" +
                             "contains(Point point)\n" +
                             "contains(int i, int il)\n" +
                             "countComponents()\n" +
                             "createBufferStrategy(int i)\n" +
                             "createBufferStrategy(int i, BufferCapabilities bc)\n" +
                             "createImage(ImageProducer ip)\n" +
                             "createImage(int i, int il)\n" +
                             "createRootPane()\n" +
                             "createVolatileImage(int i, int il)\n" +
                             "createVolatileImage(int i, int il, ImageCapabilities ic)\n" +
                             "deliverEvent(Event event)\n" +
                             "dialogInit()\n" +
                             "disable()\n" +
                             "disableEvents(long l)\n" +
                             "dispatchEvent(AWTEvent awte)\n" +
                             "dispose()\n" +
                             "doLayout()\n" +
                             "enable()\n" +
                             "enable(boolean bln)\n" +
                             "enableEvents(long l)\n" +
                             "enableInputMethods(boolean bln)\n" +
                             "equals(Object o)\n" +
                             "finalize()\n" +
                             "findComponentAt(Point point)\n" +
                             "findComponentAt(int i, int il)\n" +
                             "firePropertyChange(String string, Object o, Object o1)\n" +
                             "firePropertyChange(String string, boolean bln, boolean bln1)\n" +
                             "firePropertyChange(String string, byte b, byte b1)\n" +
                             "firePropertyChange(String string, char c, char c1)\n" +
                             "firePropertyChange(String string, double d, double d1)\n" +
                             "firePropertyChange(String string, float f, float f1)\n" +
                             "firePropertyChange(String string, int i, int i1)\n" +
                             "firePropertyChange(String string, long l, long l1)\n" +
                             "firePropertyChange(String string, short s, short s1)\n" +
                             "getAccessibleContext()\n" +
                             "getAlignmentX()\n" +
                             "getAlignmentY()\n" +
                             "getBackground()\n" +
                             "getBaseline(int i, int il)\n" +
                             "getBaselineResizeBehavior()\n" +
                             "getBounds()\n" +
                             "getBounds(Rectangle rctngl)\n" +
                             "getBufferStrategy()\n" +
                             "getClass()\n" +
                             "getColorModel()\n" +
                             "getComponent(int i)\n" +
                             "getComponentAt(Point point)\n" +
                             "getComponentAt(int i, int il)\n" +
                             "getComponentCount()\n" +
                             "getComponentListeners()\n" +
                             "getComponentOrientation()\n" +
                             "getComponentZOrder(Component cmpnt)\n" +
                             "getComponents()\n" +
                             "getContainerListeners()\n" +
                             "getContantPane()\n" +
                             "getCursor()\n" +
                             "getDafaultCloseOperation()\n" +
                             "getDropTarget()\n" +
                             "getFocusCycleRootAncestor()\n" +
                             "getFocusListeners()\n" +
                             "getFocusOwner()\n" +
                             "getFocusTraversalKeys(int i)\n" +
                             "getFocusTraversalKeysEnabled()\n" +
                             "getFocusTraversalPolicy()\n" +
                             "getFocusableWindowState()\n" +
                             "getFont()\n" +
                             "getFontMetrics(Font font)\n" +
                             "getForeground()\n" +
                             "getClassPane()\n" +
                             "getGraphics()\n" +
                             "getGraphicsConfiguration()\n" +
                             "getHeight()\n" +
                             "getHierarchyBoundsListeners()\n" +
                             "getHierarchyListeners()\n" +
                             "getIconImages()\n" +
                             "getIgnoreRepaint()\n" +
                             "getInputContext()\n" +
                             "getInputMethodListeners()\n" +
                             "getInputMethodRequests()\n" +
                             "getInsets()\n" +
                             "getJMenuBar()\n" +
                             "getKeyListeners()\n" +
                             "getLayeredPane()\n" +
                             "getLayout()\n" +
                             "getListeners(Class<T> type)\n" +
                             "getLocale()\n" +
                             "getLocation()\n" +
                             "getLocation(Point point)\n" +
                             "getLocationOnScreen()\n" +
                             "getMaximumSize()\n" +
                             "getMinimumSize()\n" +
                             "getModalExclusionType()\n" +
                             "getModalityType()\n" +
                             "getMostRecentFocusOwner()\n" +
                             "getMouseListeners()\n" +
                             "getMouseMotionListeners()\n" +
                             "getMousePosition()\n" +
                             "getMousePosition(boolean bln)\n" +
                             "getMouseWheelListeners()\n" +
                             "getName()\n" +
                             "getOwnedWindows()\n" +
                             "getOwner()\n" +
                             "getParent()\n" +
                             "getPeer()\n" +
                             "getPreferredSize()\n" +
                             "getPropertyChangeListeners()\n" +
                             "getPropertyChangeListeners(String string)\n" +
                             "getRootPane()\n" +
                             "getSize()\n" +
                             "getSize(Dimension dmnsn)\n" +
                             "getTitle()\n" +
                             "getToolKit()\n" +
                             "getTransferHandler()\n" +
                             "getTreeLock()\n" +
                             "getWarningString()\n" +
                             "getWidth()\n" +
                             "getWindowFocusListener()\n" +
                             "getWindowListener()\n" +
                             "getWindowStateListener()\n" +
                             "getX()\n" +
                             "getY()\n" +
                             "getFocus(Event event, Object o)\n" +
                             "handleEvent(Event event)\n" +
                             "hasFocus()\n" +
                             "hashCode()\n" +
                             "hide()\n" +
                             "imageUpdate(Image image, int i, int i1, int i2, int i3, int i4)\n" +
                             "insets()\n" +
                             "inside(int i, int il)\n" +
                             "invalidate()\n" +
                             "isActive()\n" +
                             "isAlwaysOnTop()\n" +
                             "isAlwaysOnTopSupported()\n" +
                             "isAncestorOf(Component cmpnt)\n" +
                             "isBackgroundSet()\n" +
                             "isCursorSet()\n" +
                             "isDisplayable()\n" +
                             "isDoubleBuffered()\n" + 
                             "isEnabled()\n" +
                             "isFocusCycleRoot()\n" +
                             "isFocusCycleRoot(Container cntnr)\n" + 
                             "isFocusOwner()\n" +
                             "isFocusTraversable()\n" +
                             "isFocusTraversalPolicyProvider()\n" + 
                             "isFocusTraversalPolicySet()\n" +
                             "isFocusable()\n" +
                             "isFocusableWindow()\n" +
                             "isFocused()\n" +
                             "isFontSet()\n" +
                             "isForegroundSet()\n" +
                             "isLightweight()\n" +
                             "isLocationByPlatform()\n" +
                             "isMaximumSizeSet()\n" +
                             "isMinimumSizeSet()\n" +
                             "isModal()\n" +
                             "isOpaque()\n" +
                             "isPreferredSizeSet()\n" +
                             "isResizable()\n" +
                             "isRootPaneCheckingEnabled()\n" +
                             "isShowing()\n" +
                             "isUndecorated()\n" +
                             "isValid()\n" +
                             "isVisible()\n" +
                             "Java(boolean visible)\n" +
                             "keyDown(Event event, int i)\n" +
                             "keyUp(Event event, int i)\n" +
                             "layout()\n" +
                             "list()\n" +
                             "list(PrintStream stream)\n" +
                             "list(PrintWriter writer)\n" +
                             "list(PrintStream stream, int i)\n" +
                             "list(PrintWriter writer, int i)\n" +
                             "locate(int i, int i1)\n" +
                             "lacation()\n" +
                             "lostFocus(Event event, Object o)\n" +
                             "minimumSize()\n" +
                             "mouseDown(Event event, int i)\n" +
                             "mouseDrag(Event event, int i)\n" +
                             "mouseEnter(Event event, int i)\n" +
                             "mouseExit(Event event, int i)\n" +
                             "mouseMove(Event event, int i)\n" +
                             "mouseUp(Event event, int i)\n" +
                             "move(int i, int i1)\n" +
                             "nextFocus()\n" +
                             "notify()\n" +
                             "notifyAll()\n" +
                             "pack()\n" +
                             "paint(Graphics grphcs)\n" +
                             "paintAll(Graphics grphcs)\n" +
                             "paintComponents(Graphics grphcs)\n" +
                             "paramString()\n" +
                             "postEvent(Event event)\n" +
                             "preferredSize()\n" +
                             "prepareImage(Image image, ImageObserver io)\n" +
                             "prepareImage(Image image, int i, int i1, ImageObserver io)\n" +
                             "print(Graphics grphcs)\n" +
                             "printAll(Graphics grphcs)\n" +
                             "printComponents(Graphics grphcs)\n" +
                             "processComponentEvent(ComponentEvent ce)\n" +
                             "processContainerEvent(ContainerEvent ce)\n" +
                             "processEvent(AWTEvent awte)\n" +
                             "processFocusEvent(FocusEvent fe)\n" +
                             "processHierarchyBoundsEvent(HierarchyEvent he)\n" +
                             "processHierarchyEvent(HierarchyEvent he)\n" +
                             "processInputMethodEvent(InputMethodEvent ime)\n" +
                             "processKeyEvent(KeyEvent ke)\n" +
                             "processMouseEvent(MouseEvent me)\n" +
                             "processMouseMotionEvent(MouseEvent me)\n" +
                             "processMouseWheelEvent(MouseWheelEvent mwe)\n" +
                             "processWindowEvent(WindowEvent we)\n" +
                             "processWindowFocusEvent(WindowEvent we)\n" +
                             "processWindowStateEvent(WindowEvent we)\n" +
                             "remove(Component cmpnt)\n" +
                             "remove(MenuComponent mc)\n" +
                             "remove(int i)\n" +
                             "removeAll\n" +
                             "removeComponentListener(ComponentListener cl)\n" +
                             "removeContainerListener(ContainerListener cl)\n" +
                             "removeFocusListener(FocusListener fl)\n" +
                             "removeHierarchyBoundsListener(HierarchyBoundsListener hl)\n" +
                             "removeHierarchyListener(HierarchyListener hl)\n" +
                             "removeInputMethodListener(InputMethodListener iml)\n" +
                             "removeKeyListener(KeyListener kl)\n" +
                             "removeMouseListener(MouseListener ml)\n" +
                             "removeMouseMotionListener(MouseMotionListener ml)\n" +
                             "removeMouseWheelListener(MouseWheelListener ml)\n" +
                             "removeNotify()\n" +
                             "removePropertyChangeListener(PropertyChangeListener pl)\n" +
                             "removePropertyChangeListener(String string, PropertyChangeListener pl)\n" +
                             "removeWindowFocusListener(WindowFocusListener wl)\n" +
                             "removeWindowListener(WindowListener wl)\n" +
                             "removeWindowStateListener(WindowStateListener wl)\n" +
                             "repaint()\n" +
                             "repaint(long l)\n" +
                             "repaint(int i, int i1, int i2, int i3)\n" +
                             "repaint(long l, int i, int i1, int i2, int i3)\n" +
                             "requestFocus()\n" +
                             "requestFocus(boolean bln)\n" +
                             "requestFocusInWindow()\n" +
                             "requestFocusInWindow(boolean bln)\n" +
                             "reshape(int i, int i1, int i2, int i3)\n" +
                             "resize(Dimension dmnsn)\n" +
                             "resize(int i, int i1)\n" +
                             "setAlwaysOnTop(boolean bln)\n" +
                             "setBackground(Color color)\n" +
                             "setBounds(Rectangle rctngl)\n" +
                             "setBounds(int i, int i1, int i2, int i3)\n" +
                             "setComponentOrientation(ComponentOrientation co)\n" +
                             "setComponentZOrder(Component cmpnt, int i)\n" +
                             "setContantPane(Container cntnr)\n" +
                             "setCursor(Cursor cursor)\n" +
                             "setDefaultCloseOperation(int i)\n" +
                             "setDropTarged(DropTarget dt)\n" +
                             "setEnabled(boolean bln)\n" +
                             "setFocusCycleRoot(boolean bln)\n" +
                             "setFocusTraversalKeys(int i, Set<? extends AWTKeyStroke > set)\n" +
                             "setFocusTraversalKeysEnabled(boolean bln)\n" +
                             "setFocusTraversalPolicy(FocusTraversalPolicy ftp)\n" +
                             "setFocusTraversalPolicyProvider(boolean bln)\n" +
                             "setFocusable(boolean bln)\n" +
                             "setFocusableWindowState(boolean bln)\n" +
                             "setFont(Font font)\n" +
                             "setForeground(Color color)\n" +
                             "setGlassPane(Component cmpnt)\n" +
                             "setIconImage(Icon icon)\n" +
                             "setIconImages(List<? extends Image > list)\n" +
                             "setIgnorRepaint(boolean bln)\n" +
                             "setJMenuBar(JMenuBar jmb)\n" +
                             "setLayeredPane(JLayeredPane jlp)\n" +
                             "setLayout(LayoutManager lm)\n" +
                             "setLocale(Locale locale)\n" +
                             "setLocation(Point point)\n" +
                             "setLocation(int i, int i1)\n" +
                             "setLocationByPlatform(boolean bln)\n" +
                             "setLocationRelativTo(Component cmpnt)\n" +
                             "setMaximumSize(Dimension dmnsn)\n" +
                             "setMinimumSize(Dimension dmnsn)\n" +
                             "setModal(boolean bln)\n" +
                             "setModalExclusionType(ModalExclusionType met)\n" +
                             "setModalityType(ModalityType mt)\n" +
                             "setName(String string)\n" +
                             "setPreferredSize(Dimension dmnsn)\n" +
                             "setResizable(boolean bln)\n" +
                             "setRootPane(JRootPane jrp)\n" +
                             "setRootPaneCheckingEnabled(boolean bln)\n" +
                             "setSize(Dimension dmnsn)\n" +
                             "setSize(int i, int i1)\n" +
                             "setTitle(String string)\n" +
                             "setTransferHandler(TransferHandler th)\n" +
                             "setUndecorated(boolean bln)\n" +
                             "setVisible(boolean bln)\n" +
                             "show()\n" +
                             "show(boolean bln)\n" +
                             "size()\n" +
                             "toBack()\n" +
                             "toFront()\n" +
                             "toString()\n" +
                             "transferFocus()\n" +
                             "transferFocusBackward()\n" +
                             "transferFocusDownCycle()\n" +
                             "transferFocusUpCycle()\n" +
                             "update(Graphics grphcs)\n" +
                             "validate()\n" +
                             "validateTree()\n" +
                             "wait()\n" +
                             "wait(long l)\n"+
                             "wait(long l, int i)\n";

                  
            
            txtInfo.setText(strInfo);
            
            JScrollPane scrInfo = new JScrollPane(txtInfo);
            add(scrInfo,BorderLayout.NORTH);
            
            final JTextField txtInput = new JTextField();
            add(txtInput,BorderLayout.SOUTH);
            
            /* If the user enters a search-string, and then press enter,
             * is the term you're looking for light gray colored
             */
            txtInput.addKeyListener(new KeyAdapter(){
              public void keyPressed(KeyEvent e)
              {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_ENTER)
                {
                   String search = txtInput.getText();
                   String text = txtInfo.getText();
                   
                   Highlighter.HighlightPainter painter = 
                        new DefaultHighlighter.DefaultHighlightPainter( Color.LIGHT_GRAY );
                    
                   
                    int offset = text.indexOf(search);
                    int length = search.length();

                    while ( offset != -1)
                    {
                        try
                        {
                            txtInfo.getHighlighter().addHighlight(offset, offset + length, painter);
                            offset = text.indexOf(search, offset+1);
                        }
                        catch(BadLocationException ble) { System.out.println(ble); }
                    }
                }
              }
            });  
            
            //setSize(400, 250);
            pack();
            setVisible(visible);
        }
        
        // [F5] -> C/C++
        void C(boolean visible){
            setTitle("C/C++");
            setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
            setLayout(new BorderLayout());
            
            final JTextArea txtInfo = new JTextArea(20,40);
            txtInfo.setDragEnabled(true);
            txtInfo.setEditable(false);
            
            String strInfo = "";
           
            
            txtInfo.setText(strInfo);
            
            JScrollPane scrInfo = new JScrollPane(txtInfo);
            add(scrInfo,BorderLayout.NORTH);
            
            final JTextField txtInput = new JTextField();
            add(txtInput,BorderLayout.SOUTH);

            txtInput.addKeyListener(new KeyAdapter(){
              public void keyPressed(KeyEvent e)
              {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_ENTER)
                {
                   String search = txtInput.getText();
                   String text = txtInfo.getText();
                   
                   Highlighter.HighlightPainter painter = 
                        new DefaultHighlighter.DefaultHighlightPainter( Color.LIGHT_GRAY );
                    
                   
                    int offset = text.indexOf(search);
                    int length = search.length();

                    while ( offset != -1)
                    {
                        try
                        {
                            txtInfo.getHighlighter().addHighlight(offset, offset + length, painter);
                            offset = text.indexOf(search, offset+1);
                        }
                        catch(BadLocationException ble) { System.out.println(ble); }
                    }
                }
              }
            });  
            
            //setSize(400, 350);
            pack();
            setVisible(visible);
        }
        
        // [F6] -> HTML
        void HTML(boolean visible){
            setTitle("HTML");
            setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
            setLayout(new BorderLayout());
            
            final JTextArea txtInfo = new JTextArea(20,40);
            txtInfo.setDragEnabled(true);
            txtInfo.setEditable(false);
            
            String strInfo = "<a></a>\n" +
                            "<abbr></abbr>\n" +
                            "<acronym></acronym>\n" +
                            "<adress></adress>\n" +
                            "<applet></applet>\n" +
                            "<area></area>\n" +
                            "<b></b>\n" +
                            "<base>\n" +
                            "<basefont>\n" +
                            "<bdo></bdo>\n" +
                            "<big></big>\n" +
                            "<blockquote></blockquote>\n" +
                            "<body></body>\n" +
                            "<br>\n" +
                            "<button></button>\n" +
                            "<caption></caption>\n" +
                            "align=\\\"center\\\"\n" +
                            "<cite></cite>\n" +
                            "<col>\n" +
                            "<colgroup></colgroup>\n" +
                            "<dd>\n" +
                            "<del></del>\n" +
                            "<dfn></dfn>\n" +
                            "<dir></dir>\n" +
                            "<div></div>\n" +
                            "<dl></dl>\n" +
                            "<dt></dt>\n" +
                            "<em></em>\n" +
                            "<fieldset></fieldset>\n" +
                            "<font></font>\n" +
                            "<form></form>\n" +
                            "<frame></frame>\n" +
                            "<frameset></frameset>\n" +
                            "<h1></h1>\n" +
                            "<h2></h2>\n" +
                            "<h3></h3>\n" +
                            "<h4></h4>\n" +
                            "<h5></h5>\n" +
                            "<h6></h6>\n" +
                            "<head></head>\n" +
                            "<hr>\n" +
                            "<html></html>\n" +
                            "<i></i>\n" +
                            "<iframe></iframe>\n" +
                            "<img></img>\n" +
                            "<input></input>\n" +
                            "<ins></ins>\n" +
                            "<isindex>\n" +
                            "<kbd></kbd>\n" +
                            "<label></label>\n" +
                            "<legend></legend>\n" +
                            "<li></li>\n" +
                            "<link></link>\n" +
                            "<map></map>\n" +
                            "<menu></menu>\n" +
                            "<meta></meta>\n" +
                            "<noframes></noframes>\n" +
                            "<noscript></noscript>\n" +
                            "<object></object>\n" +
                            "<ol></ol>\n" +
                            "<optgroup></optgroup>\n" +
                            "<option></option>\n" +
                            "<p></p>\n" +
                            "<param></param>\n" +
                            "<pre></pre>\n" +
                            "<q></q>\n" +
                            "<s></s>\n" +
                            "<samp></samp>\n" +
                            "<script></script>\n" +
                            "<select></select>\n" +
                            "<small></small>\n" +
                            "<span></span>\n" +
                            "<strike></strike>\n" +
                            "<strong></strong>\n" +
                            "<style></style>\n" +
                            "<sub></sub>\n" +
                            "<sup</sup>\n" +
                            "<table></table>\n" +
                            "<tbody></tbody>\n" +
                            "<td></td>\n" +
                            "<textarea></textarea>\n" +
                            "<tfoot></tfoot>\n" +
                            "<th></th>\n" +
                            "<thead></thead>\n" +
                            "<title></title>\n" +
                            "<tr></tr>\n" +
                            "<tt></tt>\n" +
                            "<u></u>\n" +
                            "<ul></ul>\n" +
                            "<var></var>";
           
            
            txtInfo.setText(strInfo);
            
            JScrollPane scrInfo = new JScrollPane(txtInfo);
            add(scrInfo,BorderLayout.NORTH);
            
            final JTextField txtInput = new JTextField();
            add(txtInput,BorderLayout.SOUTH);

            txtInput.addKeyListener(new KeyAdapter(){
              public void keyPressed(KeyEvent e)
              {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_ENTER)
                {
                   String search = txtInput.getText();
                   String text = txtInfo.getText();
                   
                   Highlighter.HighlightPainter painter = 
                        new DefaultHighlighter.DefaultHighlightPainter( Color.LIGHT_GRAY );
                    
                   
                    int offset = text.indexOf(search);
                    int length = search.length();

                    while ( offset != -1)
                    {
                        try
                        {
                            txtInfo.getHighlighter().addHighlight(offset, offset + length, painter);
                            offset = text.indexOf(search, offset+1);
                        }
                        catch(BadLocationException ble) { System.out.println(ble); }
                    }
                }
              }
            });  
            
            //setSize(400, 350);
            pack();
            setVisible(visible);
        }
        
        // [F7] -> HTML 5
        void HTML5(boolean visible){
            setTitle("HTML 5");
            setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
            setLayout(new BorderLayout());
            
            final JTextArea txtInfo = new JTextArea(20,40);
            txtInfo.setDragEnabled(true);
            txtInfo.setEditable(false);
            
            String strInfo = "";
           
//            DefaultMutableTreeNode HTML5_a = new DefaultMutableTreeNode("<a></a>");
//        DefaultMutableTreeNode HTML5_abbr = new DefaultMutableTreeNode("<abbr></abbr>");
//        DefaultMutableTreeNode HTML5_address = new DefaultMutableTreeNode("<adress></adress>");
//        DefaultMutableTreeNode HTML5_area = new DefaultMutableTreeNode("<area></area>");
//        DefaultMutableTreeNode HTML5_article = new DefaultMutableTreeNode("<article></article>");
//        DefaultMutableTreeNode HTML5_aside = new DefaultMutableTreeNode("<aside></aside>");
//        DefaultMutableTreeNode HTML5_audio = new DefaultMutableTreeNode("<audio></audio>");
//        DefaultMutableTreeNode HTML5_b = new DefaultMutableTreeNode("<b></b>");
//        DefaultMutableTreeNode HTML5_base = new DefaultMutableTreeNode("<base>");
//        DefaultMutableTreeNode HTML5_bdi = new DefaultMutableTreeNode("<bdi></bdi>");
//        DefaultMutableTreeNode HTML5_bdo = new DefaultMutableTreeNode("<bdo></bdo>");
//        DefaultMutableTreeNode HTML5_blockquote = new DefaultMutableTreeNode("<blockquote></blockquote>");
//        DefaultMutableTreeNode HTML5_body = new DefaultMutableTreeNode("<body></body>");
//        DefaultMutableTreeNode HTML5_br = new DefaultMutableTreeNode("<br>");
//        DefaultMutableTreeNode HTML5_button = new DefaultMutableTreeNode("<button></button>");
//        DefaultMutableTreeNode HTML5_canvas = new DefaultMutableTreeNode("<canvas></canvas>");
//        DefaultMutableTreeNode HTML5_caption = new DefaultMutableTreeNode("<caption></caption>");
//        DefaultMutableTreeNode HTML5_cite = new DefaultMutableTreeNode("<cite></cite>");
//        DefaultMutableTreeNode HTML5_col = new DefaultMutableTreeNode("<col>");
//        DefaultMutableTreeNode HTML5_colgroup = new DefaultMutableTreeNode("<colgroup></colgroup>");
//        DefaultMutableTreeNode HTML5_command = new DefaultMutableTreeNode("<command></command>");
//        DefaultMutableTreeNode HTML5_datalist = new DefaultMutableTreeNode("<datalist></datalist>");
//        DefaultMutableTreeNode HTML5_dd = new DefaultMutableTreeNode("<dd>");
//        DefaultMutableTreeNode HTML5_del = new DefaultMutableTreeNode("<del></del>");
//        DefaultMutableTreeNode HTML5_details = new DefaultMutableTreeNode("<details></details>");
//        DefaultMutableTreeNode HTML5_dfn = new DefaultMutableTreeNode("<dfn></dfn>");
//        DefaultMutableTreeNode HTML5_dialog = new DefaultMutableTreeNode("<dialog></dialog>");
//        DefaultMutableTreeNode HTML5_div = new DefaultMutableTreeNode("<div></div>");
//        DefaultMutableTreeNode HTML5_dl = new DefaultMutableTreeNode("<dl></dl>");
//        DefaultMutableTreeNode HTML5_dt = new DefaultMutableTreeNode("<dt></dt>");
//        DefaultMutableTreeNode HTML5_em = new DefaultMutableTreeNode("<em></em>");
//        DefaultMutableTreeNode HTML5_embed = new DefaultMutableTreeNode("<embed></embed>");
//        DefaultMutableTreeNode HTML5_fieldset = new DefaultMutableTreeNode("<fieldset></fieldset>");
//        DefaultMutableTreeNode HTML5_figcaption = new DefaultMutableTreeNode("<figcaption></figcaption>");
//        DefaultMutableTreeNode HTML5_figure = new DefaultMutableTreeNode("<figure></figure>");
//        DefaultMutableTreeNode HTML5_footer = new DefaultMutableTreeNode("<footer></footer>");
//        DefaultMutableTreeNode HTML5_form = new DefaultMutableTreeNode("<form></form>");
//        DefaultMutableTreeNode HTML5_h1 = new DefaultMutableTreeNode("<h1></h1>");
//        DefaultMutableTreeNode HTML5_h2 = new DefaultMutableTreeNode("<h2></h2>");
//        DefaultMutableTreeNode HTML5_h3 = new DefaultMutableTreeNode("<h3></h3>");
//        DefaultMutableTreeNode HTML5_h4 = new DefaultMutableTreeNode("<h4></h4>");
//        DefaultMutableTreeNode HTML5_h5 = new DefaultMutableTreeNode("<h5></h5>");
//        DefaultMutableTreeNode HTML5_h6 = new DefaultMutableTreeNode("<h6></h6>");
//        DefaultMutableTreeNode HTML5_head = new DefaultMutableTreeNode("<head></head>");
//        DefaultMutableTreeNode HTML5_header = new DefaultMutableTreeNode("<header></header>");
//        DefaultMutableTreeNode HTML5_hgroup = new DefaultMutableTreeNode("<hgroup></hgroup>");
//        DefaultMutableTreeNode HTML5_hr = new DefaultMutableTreeNode("<hr>");
//        DefaultMutableTreeNode HTML5_html = new DefaultMutableTreeNode("<html></html>");
//        DefaultMutableTreeNode HTML5_i = new DefaultMutableTreeNode("<i></i>");
//        DefaultMutableTreeNode HTML5_iframe = new DefaultMutableTreeNode("<iframe></iframe>");
//        DefaultMutableTreeNode HTML5_img = new DefaultMutableTreeNode("<img></img>");
//        DefaultMutableTreeNode HTML5_input = new DefaultMutableTreeNode("<input></input>");
//        DefaultMutableTreeNode HTML5_ins = new DefaultMutableTreeNode("<ins></ins>");
//        DefaultMutableTreeNode HTML5_isindex = new DefaultMutableTreeNode("<isindex>");
//        DefaultMutableTreeNode HTML5_kbd = new DefaultMutableTreeNode("<kbd></kbd>");
//        DefaultMutableTreeNode HTML5_keygen = new DefaultMutableTreeNode("<keygen></keygen>");
//        DefaultMutableTreeNode HTML5_label = new DefaultMutableTreeNode("<label></label>");
//        DefaultMutableTreeNode HTML5_legend = new DefaultMutableTreeNode("<legend></legend>");
//        DefaultMutableTreeNode HTML5_li = new DefaultMutableTreeNode("<li></li>");
//        DefaultMutableTreeNode HTML5_link = new DefaultMutableTreeNode("<link></link>");
//        DefaultMutableTreeNode HTML5_map = new DefaultMutableTreeNode("<map></map>");
//        DefaultMutableTreeNode HTML5_mark = new DefaultMutableTreeNode("<mark></mark>");
//        DefaultMutableTreeNode HTML5_menu = new DefaultMutableTreeNode("<menu></menu>");
//        DefaultMutableTreeNode HTML5_meta = new DefaultMutableTreeNode("<meta></meta>");
//        DefaultMutableTreeNode HTML5_meter = new DefaultMutableTreeNode("<meter></meter>");
//        DefaultMutableTreeNode HTML5_nav = new DefaultMutableTreeNode("<nav></nav>");
//        DefaultMutableTreeNode HTML5_noscript = new DefaultMutableTreeNode("<noscript></noscript>");
//        DefaultMutableTreeNode HTML5_object = new DefaultMutableTreeNode("<object></object>");
//        DefaultMutableTreeNode HTML5_ol = new DefaultMutableTreeNode("<ol></ol>");
//        DefaultMutableTreeNode HTML5_optgroup = new DefaultMutableTreeNode("<optgroup></optgroup>");
//        DefaultMutableTreeNode HTML5_option = new DefaultMutableTreeNode("<option></option>");
//        DefaultMutableTreeNode HTML5_output = new DefaultMutableTreeNode("<output></output>");
//        DefaultMutableTreeNode HTML5_p = new DefaultMutableTreeNode("<p></p>");
//        DefaultMutableTreeNode HTML5_param = new DefaultMutableTreeNode("<param></param>");
//        DefaultMutableTreeNode HTML5_pre = new DefaultMutableTreeNode("<pre></pre>");
//        DefaultMutableTreeNode HTML5_progress = new DefaultMutableTreeNode("<progress></progress>");
//        DefaultMutableTreeNode HTML5_q = new DefaultMutableTreeNode("<q></q>");
//        DefaultMutableTreeNode HTML5_rp = new DefaultMutableTreeNode("<rp></rp>");
//        DefaultMutableTreeNode HTML5_rt = new DefaultMutableTreeNode("<rt></rt>");
//        DefaultMutableTreeNode HTML5_ruby = new DefaultMutableTreeNode("<ruby></ruby>");
//        DefaultMutableTreeNode HTML5_s = new DefaultMutableTreeNode("<s></s>");
//        DefaultMutableTreeNode HTML5_samp = new DefaultMutableTreeNode("<samp></samp>");
//        DefaultMutableTreeNode HTML5_script = new DefaultMutableTreeNode("<script></script>");
//        DefaultMutableTreeNode HTML5_section = new DefaultMutableTreeNode("<section></section>");
//        DefaultMutableTreeNode HTML5_select = new DefaultMutableTreeNode("<select></select>");
//        DefaultMutableTreeNode HTML5_small = new DefaultMutableTreeNode("<small></small>");
//        DefaultMutableTreeNode HTML5_source = new DefaultMutableTreeNode("<source></source>");
//        DefaultMutableTreeNode HTML5_span = new DefaultMutableTreeNode("<span></span>");
//        DefaultMutableTreeNode HTML5_strong = new DefaultMutableTreeNode("<strong></strong>");
//        DefaultMutableTreeNode HTML5_style = new DefaultMutableTreeNode("<style></style>");
//        DefaultMutableTreeNode HTML5_sub = new DefaultMutableTreeNode("<sub></sub>");
//        DefaultMutableTreeNode HTML5_summary = new DefaultMutableTreeNode("<summary></summary>");
//        DefaultMutableTreeNode HTML5_sup = new DefaultMutableTreeNode("<sup</sup>");
//        DefaultMutableTreeNode HTML5_table = new DefaultMutableTreeNode("<table></table>");
//        DefaultMutableTreeNode HTML5_tbody = new DefaultMutableTreeNode("<tbody></tbody>");
//        DefaultMutableTreeNode HTML5_td = new DefaultMutableTreeNode("<td></td>");
//        DefaultMutableTreeNode HTML5_textarea = new DefaultMutableTreeNode("<textarea></textarea>");
//        DefaultMutableTreeNode HTML5_tfoot = new DefaultMutableTreeNode("<tfoot></tfoot>");
//        DefaultMutableTreeNode HTML5_th = new DefaultMutableTreeNode("<th></th>");
//        DefaultMutableTreeNode HTML5_thead = new DefaultMutableTreeNode("<thead></thead>");
//        DefaultMutableTreeNode HTML5_time = new DefaultMutableTreeNode("<time></time>");
//        DefaultMutableTreeNode HTML5_title = new DefaultMutableTreeNode("<title></title>");
//        DefaultMutableTreeNode HTML5_tr = new DefaultMutableTreeNode("<tr></tr>");
//        DefaultMutableTreeNode HTML5_track = new DefaultMutableTreeNode("<track></track>");
//        DefaultMutableTreeNode HTML5_u = new DefaultMutableTreeNode("<u></u>");
//        DefaultMutableTreeNode HTML5_ul = new DefaultMutableTreeNode("<ul></ul>");
//        DefaultMutableTreeNode HTML5_var = new DefaultMutableTreeNode("<var></var>");
//        DefaultMutableTreeNode HTML5_video = new DefaultMutableTreeNode("<video></video>");
//        DefaultMutableTreeNode HTML5_wbr = new DefaultMutableTreeNode("<wbr></wbr>");
            
            txtInfo.setText(strInfo);
            
            JScrollPane scrInfo = new JScrollPane(txtInfo);
            add(scrInfo,BorderLayout.NORTH);
            
            final JTextField txtInput = new JTextField();
            add(txtInput,BorderLayout.SOUTH);

            txtInput.addKeyListener(new KeyAdapter(){
              public void keyPressed(KeyEvent e)
              {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_ENTER)
                {
                   String search = txtInput.getText();
                   String text = txtInfo.getText();
                   
                   Highlighter.HighlightPainter painter = 
                        new DefaultHighlighter.DefaultHighlightPainter( Color.LIGHT_GRAY );
                    
                   
                    int offset = text.indexOf(search);
                    int length = search.length();

                    while ( offset != -1)
                    {
                        try
                        {
                            txtInfo.getHighlighter().addHighlight(offset, offset + length, painter);
                            offset = text.indexOf(search, offset+1);
                        }
                        catch(BadLocationException ble) { System.out.println(ble); }
                    }
                }
              }
            });  
            
            //setSize(400, 350);
            pack();
            setVisible(visible);
        }
    }

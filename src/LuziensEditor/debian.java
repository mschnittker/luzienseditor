/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author markusschnittker
 */
public class debian {
    String file_path;
    String package_name;
    String version;
    String section;
    String priority;
    String architecture;
    String depends;
    String installed_size;
    String maintainer;
    String hompage;
    String description;
    String description_long;
    String install_dir;
    
    debian(String file_path,String package_name,String version,String section,String priority,String architecture,String depends,String installed_size,String maintainer,String hompage,String description,String description_long,String install_dir) throws FileNotFoundException, IOException, InterruptedException{
        this.file_path = file_path;
        this.package_name = package_name;
        this.version = version;
        this.section = section;
        this.priority = priority;
        this.architecture = architecture;
        this.depends = depends;
        this.installed_size = installed_size;
        this.maintainer = maintainer;
        this.hompage = hompage;
        this.description = description;
        this.description_long = description_long;
        this.install_dir = install_dir;
    
//        // dateipfad
//        System.out.printf("file path : ");
//        Scanner sc_fil = new Scanner(System.in);
//        String file_path = sc_fil.nextLine();
//        
//        // package
//        System.out.printf("package : ");
//        Scanner sc_pac = new Scanner(System.in);
//        String package_name = sc_pac.nextLine();
//        
//        // version
//        System.out.printf("version : ");
//        Scanner sc_ver = new Scanner(System.in);
//        String version = sc_ver.nextLine();
//        
//        // section
//        System.out.printf("section (admin,devel,doc,editors,math,misc,science,utils...) : ");
//        Scanner sc_sec = new Scanner(System.in);
//        String section = sc_sec.nextLine();
//        
//        // priority
//        System.out.printf("priority (std:extra) : ");
//        Scanner sc_pri = new Scanner(System.in);
//        String priority = sc_pri.nextLine();
//        
//        // architecture
//        System.out.printf("architecture (std:all,i386,amd64...) : ");
//        Scanner sc_arc = new Scanner(System.in);
//        String architecture = sc_arc.nextLine();
//        
//        // depends
//        System.out.printf("depends : ");
//        Scanner sc_dep = new Scanner(System.in);
//        String depends = sc_dep.nextLine();
//        
//        // installed size (kb)
//        System.out.printf("installed size (kb) : ");
//        Scanner sc_ins = new Scanner(System.in);
//        String installed_size = sc_ins.nextLine();
//        
//        // maintainer
//        System.out.printf("maintainer (name <email>) : ");
//        Scanner sc_mai = new Scanner(System.in);
//        String maintainer = sc_mai.nextLine();
//        
//        // homepage
//        System.out.printf("homepage : ");
//        Scanner sc_hom = new Scanner(System.in);
//        String hompage = sc_hom.nextLine();
//        
//        // description (short)
//        System.out.printf("short description : ");
//        Scanner sc_des = new Scanner(System.in);
//        String description = sc_des.nextLine();
//        
//        // description (long)
//        System.out.printf("long description : ");
//        Scanner sc_des_long = new Scanner(System.in);
//        String description_long = sc_des_long.nextLine();
//        
//        // installationsordner
//        System.out.printf("install directory (/usr/share/ : ");
//        Scanner sc_ind = new Scanner(System.in);
//        String install_dir = sc_ind.nextLine();
        
        // .../name/DEBIAN/
        String path1 = file_path + File.separator + package_name + File.separator + "DEBIAN";
        File debian = new File(path1);
        debian.mkdirs();
        
        // erstelle .../name/DEBIAN/control
        String control_path = path1 + File.separator + "control";
        String content ="Package: " + package_name + "\n" +
                        "Version: " + version + "\n" +
                        "Section: " + section + "\n" +
                        "Priority: " + priority + "\n" +
                        "Architecture: " + architecture + "\n" +
                        "Depends: " + depends + "\n" +
                        "Installed-Size: " + installed_size + "\n" +
                        "Maintainer: " + maintainer + "\n" +
                        "Homepage: " + hompage + "\n" +
                        "Description: " + description + "\n" +
                        " " + description_long + "\n";
        
        FileStream crFile = new FileStream();
        crFile.write(control_path, content);
        
        // .../name/usr/share/
        String path2 = file_path + File.separator + package_name + File.separator + install_dir;
        File files = new File(path2);
        files.mkdirs();

        // kopiere die dateien in den "path2"
        String Files[] = new String[100];
        String Filename[] = new String[100];

        File input = new File(file_path);
        File[] fileList1 = input.listFiles();

        int k = 0; // Name

        for(File f : fileList1) {            
            if(!f.getName().equals(package_name)){
                Filename[k] = String.valueOf(f.getName());

                String file_in = file_path + File.separator + Filename[k];

                String file_out = path2 + File.separator + Filename[k];

                File in = new File(file_in);
                File out = new File(file_out);

                CopyDirectory cpFiles = new CopyDirectory();
                try {

                    cpFiles.copyFile(in, out);
                } catch (IOException ex) {
                    Logger.getLogger(debian.class.getName()).log(Level.SEVERE, null, ex);
                }

                ++k;
            }
        }
        
//        // Menü erstellen
//        System.out.printf("do you want to create a menu entry? [Y/N] ");
//        Scanner sc_menu = new Scanner(System.in);
//        String app_menu = sc_menu.nextLine();
//        
//        if(app_menu.equals("Y") || app_menu.equals("y")){
//            // name
//            System.out.printf("name : %s\n",package_name);
//            
//            // comment
//            System.out.printf("comment : %s\n",description);
//            
//            // exec
//            System.out.printf("exec (path for the console) : ");
//            Scanner sc_exec = new Scanner(System.in);
//            String exec = sc_exec.nextLine();
//            
//            // icon
//            System.out.printf("icon (filename) : ");
//            Scanner sc_icon = new Scanner(System.in);
//            String icon = sc_icon.nextLine();
//            
//            // terminal
//            System.out.printf("terminal (true/false) : ");
//            Scanner sc_terminal = new Scanner(System.in);
//            String terminal = sc_terminal.nextLine();
//            
//            // type
//            System.out.printf("type (application,directory,link) : ");
//            Scanner sc_type = new Scanner(System.in);
//            String type = sc_type.nextLine();
//            
//            // categories
//            System.out.printf("categories (separate them with \";\") : ");
//            Scanner sc_categories = new Scanner(System.in);
//            String categories = sc_categories.nextLine();
//            
//            // startupnotify
//            System.out.printf("startupnotify (std:false) : ");
//            Scanner sc_startupnotify = new Scanner(System.in);
//            String startupnotify = sc_startupnotify.nextLine();
//            
//            // erstelle .../usr/share/applications
//            String desk_path = "/usr/share/applications/" + package_name + ".desktop";
//            String content_desk ="[Desktop Entry]\n" +
//                            "Name=" + package_name + "\n" +
//                            "Comment=" + description + "\n" +
//                            "Exec=" + exec + "\n" +
//                            "Icon=" + icon + "\n" +
//                            "Terminal=" + terminal + "\n" +
//                            "Type=" + type + "\n" +
//                            "Categories=" + categories + "\n" +
//                            "StartupNotify=" + startupnotify + "\n";
//
//            FileStream deskFile = new FileStream();
//            deskFile.write(desk_path, content_desk);
//            
//            // befehl ausführen : dpkg -b ./helloworld helloworld.deb
//            String out = "dpkg -b " + file_path + " " + file_path + File.separator + package_name + ".deb";
//            System.out.println("build the file with:");
//            System.out.println(out);
//        }
//        else if(app_menu.equals("N") || app_menu.equals("n")){
            // befehl ausführen : dpkg -b ./helloworld helloworld.deb
            String out = "sudo dpkg -b " + file_path + File.separator + package_name;
            JOptionPane.showMessageDialog(null, out, "Luzien's Editor v0.2", JOptionPane.INFORMATION_MESSAGE);
//            System.out.println("build the file with:");
//            System.out.println(out);
//        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author markusschnittker
 */


public class WebColor extends JDialog{
    public static String toHexString(Color c) {
      StringBuilder sb = new StringBuilder("#");

      if (c.getRed() < 16) sb.append('0');
      sb.append(Integer.toHexString(c.getRed()));

      if (c.getGreen() < 16) sb.append('0');
      sb.append(Integer.toHexString(c.getGreen()));

      if (c.getBlue() < 16) sb.append('0');
      sb.append(Integer.toHexString(c.getBlue()));

      return sb.toString();
    }
    
    boolean visible;
    JTextField txtColor;

    WebColor(boolean visible){
        this.visible = visible;
        setTitle("HTML Farben");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
        
        /* Info */
        JPanel pnlInfo = new JPanel();
        pnlInfo.setLayout(new BorderLayout());
        pnlInfo.setBackground(Color.WHITE);
        JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
        txtInfo.setEditable(false);
        txtInfo.setBackground(Color.WHITE);
        txtInfo.setText("<b>HTML Farbcodes</b><br>Hier kannst du den Hexwert einer Farbe berechnen.<br><br>");
        pnlInfo.add(txtInfo,BorderLayout.PAGE_START);
        add(pnlInfo,BorderLayout.NORTH);
        
        /* Benutzereingaben */
        JPanel pnlInput = new JPanel();
        pnlInput.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        JLabel lblColor = new JLabel("Farbe (Hex) : ");
        c.gridx = 0;
        c.gridy = 0;
        pnlInput.add(lblColor,c);
        
        txtColor = new JTextField(20);
        c.gridx = 1;
        c.gridy = 0;
        pnlInput.add(txtColor,c);
        
        
        add(pnlInput,BorderLayout.CENTER);
        
        /* Buttons */
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new FlowLayout());
        
        JButton btnCreate = new JButton("Farbauswahl");
        btnCreate.addActionListener(new ActionListener(){
           @Override public void actionPerformed(ActionEvent ev){
               Color color = JColorChooser.showDialog(null, 
                    "Farben", null);

                String out = toHexString(color);
                txtColor.setText(out);

           } 
        });
        
        JButton btnCopy = new JButton("Kopieren");
            btnCopy.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent aeXXX){
                    String code = txtColor.getText();
                    StringSelection selection = new StringSelection(code);
                    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                    clipboard.setContents(selection, selection);
                }
            });
        
        JButton btnDispose = new JButton("Abbrechen");
        btnDispose.addActionListener(new ActionListener(){
           @Override public void actionPerformed(ActionEvent ev){
               dispose();
           } 
        });
        
        pnlButtons.add(btnCreate);
        pnlButtons.add(btnCopy);
        pnlButtons.add(btnDispose);
        
        add(pnlButtons,BorderLayout.SOUTH);
        
        pack();
        setVisible(visible);
        
        
        

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import static LuziensEditor.LuziensEditor.CMain;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author markus schnittker
 */
public class FileStream {

        private String Filename;
        private String Content;

        FileStream() {
        }

        FileStream(String Filename, String Content) {
            this.Filename = Filename;
            this.Content = Content;
        }
        
        // Datei einlesen
        public void read(String Filename) throws IOException {
            FileReader inputFile = new FileReader(Filename);
            BufferedReader inputContent = new BufferedReader(inputFile);


            String row = inputContent.readLine();
            while (row != null) {
                System.out.println(row);
                row = inputContent.readLine();
            }


            inputContent.close();
        }
        
        // Datei erstellen oder überschreiben
        public void write(String Filename, String Content) throws IOException {
            File file = new File(Filename);
            FileWriter writer = new FileWriter(file);
            writer.write(Content);
            writer.close();
        }
        
        // Text an Datei anhängen
        public void append(String Filename, String Content) throws IOException {
            File file = new File(Filename);
            FileWriter writer = new FileWriter(file);
            writer.append(Content.toString());
            writer.close();
        }
    }
    
    

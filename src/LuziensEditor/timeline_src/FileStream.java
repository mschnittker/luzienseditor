/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor.timeline_src;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author markus schnittker
 */
public class FileStream {

        private String Filename;
        private String Content;

        FileStream() {
        }

        FileStream(String Filename, String Content) {
            this.Filename = Filename;
            this.Content = Content;
        }
        
        // Datei einlesen
        public void read(String Filename) throws IOException {
            FileReader inputFile = new FileReader(Filename);
            BufferedReader inputContent = new BufferedReader(inputFile);


            String row = inputContent.readLine();
            while (row != null) {
                System.out.println(row);
                row = inputContent.readLine();
            }


            inputContent.close();
        }
        
        // Datei erstellen oder überschreiben
        public void write(String Filename, String Content) throws IOException {
            File file = new File(Filename);
            FileWriter writer = new FileWriter(file);
            writer.write(Content);
            writer.close();
        }
        
        // Text an Datei anhängen
        public void append(String Filename, String Content) throws IOException {
            File file = new File(Filename);
            FileWriter writer = new FileWriter(file);
            writer.append(Content.toString());
            writer.close();
        }
    }

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor.timeline_src;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author markus schnittker
 */
public class CopyDirectory { 
     
    private BufferedInputStream in = null; 
    private BufferedOutputStream out = null; 
    
    // Kopiert Ordner
    public void copyDir(File quelle, File ziel) throws FileNotFoundException, IOException { 
         
        File[] files = quelle.listFiles(); 
        ziel.mkdirs(); 
        for (File file : files) { 
            if (file.isDirectory()) { 
                copyDir(file, new File(ziel.getAbsolutePath() + System.getProperty("file.separator") + file.getName())); 
            } 
            else { 
                copyFile(file, new File(ziel.getAbsolutePath() + System.getProperty("file.separator") + file.getName())); 
            } 
        } 
    } 
    
    // Kopiert Dateien
    public void copyFile(File file, File ziel) throws FileNotFoundException, IOException { 

        in = new BufferedInputStream(new FileInputStream(file)); 
        out = new BufferedOutputStream(new FileOutputStream(ziel, true)); 
        int bytes = 0; 
        while ((bytes = in.read()) != -1) { 
            out.write(bytes); 
        } 
        in.close(); 
        out.close(); 
    } 
} 

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor.timeline_src;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author markusschnittker
 */
public class CompleteDay {
    String name;
    String date;
    String time;
    
    
  void deleteDir(File path) {
      for (File file : path.listFiles()) {
         if(file.isDirectory() && !file.getName().equals("temp") && !file.getName().equals(date))
            deleteDir(file);
         file.delete();
      }
      path.delete();
   }
  
  void deleteTemp(File path) {
      for (File file : path.listFiles()) {
         if(file.isDirectory() && file.getName().equals("temp"))
            deleteDir(file);
         file.delete();
      }
      path.delete();
   }
  
    
   public CompleteDay(String name,String date,String time) throws FileNotFoundException{
        this.name = name;
        this.date = date;
        this.time = time;
        
        if(date.equals("today")){
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
            Date datenow = new Date();
            date = dateFormat.format(datenow);
        }
        
        //String time;
        
        // Pfad des Projekts /home/.../timeline/.../...
        String tlProject = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator + date + File.separator;

        // Alle Dateien des Programmordners auflisten
        String Dirs[] = new String[100];
        
        File dir = new File(tlProject);
        File[] fileList = dir.listFiles();

        
        int i = 0; // Dateipfad
        
        System.out.println("\t+" + name);
        
        for(File f : fileList) {
           System.out.println("\t|_" + f.getName());
           if(f.isDirectory() && (!f.getName().equals("start"))){   
                File dirSUB = new File(f.getAbsolutePath());
                File[] SUBfileList = dirSUB.listFiles();
                for(File f_sub : SUBfileList) {
                    System.out.println("\t  |_" + f_sub.getName());
                    if(f_sub.isDirectory()){   
                        File dirSUBSUB = new File(f_sub.getAbsolutePath());
                        File[] SUBSUBfileList = dirSUBSUB.listFiles();
                        for(File f_subsub : SUBSUBfileList) {
                            System.out.println("\t    |_" + f_subsub.getName());
                        }
                   }
                }
           }
           //Dirs[i] = String.valueOf(f.getName());
           //++i;
        }
        
//        System.out.println("\nfinal time of the day to complete? : ");
//        Scanner sc_time = new Scanner(System.in);
//        time = sc_time.nextLine();
        
        // Pfad mit Zeitangabe
        String Project = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator + date + File.separator + time + File.separator;
        
        
//        // Löschen Ja oder Nein
//        System.out.printf("Do you want to delete the other files of the day [Y/N] : ");
//        Scanner sc_flag = new Scanner(System.in);
//        String flag = sc_flag.nextLine();
//
//        // Wenn ja dann löschen
//        if(flag.equals("Y") || flag.equals("y")){
            
            // kopiere alle dateien in den ordner temp
            String Files[] = new String[100];
            String Filename[] = new String[100];

            File input = new File(Project);
            File[] fileList1 = input.listFiles();

            int k = 0; // Name

            for(File f : fileList1) {            

                Filename[k] = String.valueOf(f.getName());
                
                String file_in = Project + File.separator + Filename[k];
                
                String temp = tlProject + File.separator + "temp";
                File tempFile = new File(temp);
                tempFile.mkdir();
                
                String file_out = tlProject + File.separator + "temp" + File.separator + Filename[k];
                
                File in = new File(file_in);
                File out = new File(file_out);

                CopyDirectory cpFiles = new CopyDirectory();
                try {
                    cpFiles.copyFile(in, out);
                } catch (IOException ex) {
                    Logger.getLogger(CompleteDay.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                ++k;
            } 
            
            // alle ordner ausser temp löschen          
            deleteDir(new File(tlProject));
            
            // alle Dateien aus temp kopieren
            String Files2[] = new String[100];
            String Filename2[] = new String[100];

            
            String file_temp = tlProject + File.separator + "temp" + File.separator;
            File input2 = new File(file_temp);
            File[] fileList2 = input2.listFiles();

            int m = 0; // Name
            int n = 0;
            
            for(File h : fileList2) {            

                Filename2[m] = String.valueOf(h.getName());
                Files2[n] = String.valueOf(h.getAbsolutePath());

                //String file_in2 = Filename2[m];

                
                String file_out2 = tlProject + Filename2[m];
                
                File in2 = new File(Files2[m]);
                File out2 = new File(file_out2);

                CopyDirectory cpFiles2 = new CopyDirectory();
                try {
                    cpFiles2.copyFile(in2, out2);
                } catch (IOException ex) {
                    Logger.getLogger(CompleteDay.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                ++m;
                ++n;
            } 
            
            // temp ordner löschen
            String temp_dir = tlProject + File.separator + "temp";
            deleteTemp(new File(temp_dir));
//        }
    }
    
    
}

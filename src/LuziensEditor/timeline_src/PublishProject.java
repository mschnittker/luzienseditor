/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor.timeline_src;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author markus schnittker
 */
public class PublishProject {
    String name;
    String date;
    
    String os;
    String xampp;
    
   void deleteDir(File path) {
      for (File file : path.listFiles()) {
         if (file.isDirectory())
            deleteDir(file);
         file.delete();
      }
      path.delete();
   }
    
    PublishProject(String name,String date) throws IOException{
        this.name = name;
        this.date = date;
        
        // OS & XAMPP Pfad überprüfen 
        
        if(System.getProperty("os.name").equals("Mac OS") || System.getProperty("os.name").equals("Mac OS X")){
            String xampp_path = "/Applications/XAMPP/htdocs/";
            File f = new File(xampp_path); 
            if (f.isDirectory()) { 
                os = "MacOS";
                xampp = xampp_path;
 
            } else { 
                os = "MacOS";
                System.out.println("Please install xampp to \"/Applications/XAMPP/\"");
                System.exit(0);
            }
        }
        else if(System.getProperty("os.name").equals("Linux")){
            String xampp_path = "/opt/lampp/htdocs/";
            File f = new File(xampp_path); 
            if (f.isDirectory()) { 
                os = "Linux";
                xampp = xampp_path;
            } else {  
                os = "Linux";
                System.out.println("Please install xampp to \"/opt/lampp/\"");
                System.exit(0);
            }
        }
        else{ // Windows
            String xampp_path = "C:\\xampp\\htdocs\\";
            File f = new File(xampp_path); 
            if (f.isDirectory()) { 
                os = "Windows";
                xampp = xampp_path;
            } else {  
                os = "Windows";
                System.out.println("Please install xampp to \"c:\\xampp\\\"");
                System.exit(0);
            }
        }

        // "end" Datei erstellen
        String end_path = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + 
                          File.separator + "end"; //+ File.separator;
        
        // "end" Ordner löschen
        File DirEnd = new File(end_path);
        if(DirEnd.isDirectory()){
            deleteDir(DirEnd);
            // "end" Ordner erstellen
            DirEnd.mkdir();
        }
        else{
            // "end" Ordner erstellen
            DirEnd.mkdir();
        }

        // "README.txt" Datei im Ordner "end" erstellen
        //String readme = "README.txt";
        String readme_path = end_path + File.separator + "news.txt";
        System.out.printf("what changes have been made : \n");
        Scanner sc_readme = new Scanner(System.in);
        String README_Content = sc_readme.nextLine(); 
                         
        FileStream addREADMEFile = new FileStream();
        addREADMEFile.write(readme_path, README_Content);
        
        // Dateien in den "end" Ordner kopieren
        String Files[] = new String[100];
        String Filename[] = new String[100];

        String Project = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator + date + File.separator;
        
        File input = new File(Project);
        File[] fileList = input.listFiles();

        int i = 0; 

        for(File f : fileList) {            

            Filename[i] = String.valueOf(f.getName());

            String file_in = Project + File.separator + Filename[i];

            String file_out = end_path + File.separator + Filename[i];

            File in = new File(file_in);
            File out = new File(file_out);

            CopyDirectory cpFiles = new CopyDirectory();
            try {
                cpFiles.copyFile(in, out);
            } catch (IOException ex) {
                Logger.getLogger(CompleteDay.class.getName()).log(Level.SEVERE, null, ex);
            }

            ++i;
        }
        
        // "index.html" Datei im Ordner "end" erstellen
//        String access = "index.html";
//        String access_path = end_path + File.separator + access;
//        String content ="<HTML>\n" +
//                            "<HEAD>\n" +
//                            "<TITLE>timeline server</TITLE>\n" +
//                            "</HEAD>\n" +
//                            "<BODY>\n" +
//                            "<H2>" + name + "</H2>\n" +
//                            "(changed at " + date + " )\n" + 
//                            "\n<? \n" +
//                            "   $ verz = opendir  ( '.' ); \n" +
//                            "   while ( $ file  =  readdir  ( $ verz )) { \n" +
//                            "      if ( $ file  ! =  \".\"  &&  $ file  ! =  \"..\" ) \n" +
//                            "         echo  \"<a href = \"' .. '\">' . $ file . '</ a>' ; \n" +
//                            "   } \n" +
//                            "   closedir ( $ verz ); \n" +
//                            "?>\n" +
//                            "</BODY>\n" +
//                            "</HTML>"; 
//                         
//        FileStream addFile = new FileStream();
//        addFile.write(access_path, content);
        

        String out_xampp = xampp + File.separator + name + File.separator;
        File X_out = new File(out_xampp);
        if(X_out.isDirectory()){
            // löschen
            deleteDir(X_out);
            // neu erstellen
            X_out.mkdir();
        }
        else{
            X_out.mkdir();
        }
        
        
        // "end" Ordner in den xampp pfad kopieren 
        File in = new File(end_path);
        File out = new File(out_xampp);
        
        CopyDirectory cpDir = new CopyDirectory();
        cpDir.copyDir(in, out);
        
        // Internetadresse ausgeben
        System.out.printf("\nthe page is reachable under \"localhost/%s/\"\n",name);
        
    }
}
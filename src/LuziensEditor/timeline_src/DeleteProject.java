/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor.timeline_src;

import java.io.File;
import java.util.Scanner;

/**
 *
 * @author markus schnittker
 */
public class DeleteProject {
    String name;
    
    void deleteDir(File path) {
      for (File file : path.listFiles()) {
         if (file.isDirectory())
            deleteDir(file);
         file.delete();
      }
      path.delete();
   }
    
  public DeleteProject(String name){
        this.name = name;
        
        // Pfad des Projekts /home/.../timeline/.../...
        String tlProject = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator;
        
//        // Löschen Ja oder Nein
//        System.out.printf("Do you want to delete the Project " + "\"" + name + "\" [Y/N] : ");
//        Scanner sc_flag = new Scanner(System.in);
//        String flag = sc_flag.nextLine();
        
        // Wenn ja dann löschen
//        if(flag.equals("Y") || flag.equals("y")){
            deleteDir(new File(tlProject));
//        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor.timeline_src;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Scanner;

/**
 *
 * @author markus schnittker
 */
public class NewProject{
        String name;
        String version;
        String description;
        String path;
        String developer;
        
        public NewProject(String name,String version,String developer,String description,String path) throws IOException{
            this.name = name;
            this.version = version;
            this.developer = developer;
            this.description = description;
            this.path = path;
            
//            // Name der Datei
//            System.out.printf("Name          : %s \n",name);
//            
//            // Versionsnummer
//            System.out.printf("Version       : ");
//            Scanner sc_ver = new Scanner(System.in);
//            version = sc_ver.nextLine();
//            
//            // Name des Programmierers
//            System.out.printf("Developer     : ");
//            Scanner sc_dev = new Scanner(System.in);
//            developer = sc_dev.nextLine();
//            
//            // Kurze Beschreibung
//            System.out.printf("Description   : ");
//            Scanner sc_des = new Scanner(System.in);
//            description = sc_des.nextLine();
//            
//            // Pfad des Programms
//            System.out.printf("Path          : ");
//            Scanner sc_pat = new Scanner(System.in);
//            path = sc_pat.nextLine();
            
            // Erstelle den TimeLine Ordner unter /home/.../timeline/...
            String tlFolder = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator; 
            
            File addFolder = new File(tlFolder);
            addFolder.mkdirs();
            
            // Inhalt der config Datei
            String conf = name            + "\n" // RunProject.java -> array[0]
                        + version         + "\n" // RunProject.java -> array[1]
                        + developer       + "\n" // RunProject.java -> array[2]
                        + description     + "\n" // RunProject.java -> array[3]
                        + path            ;      // RunProject.java -> array[4]
            
            // Name der config Datei
            String target = tlFolder + "timeline.conf";
            
            // Erstelle die config Datei unter /timeline/.../timeline.conf
            FileStream file = new FileStream();
            file.write(target, conf);
            
            // Pfad des "backup" Ordners
            String BackupPath = tlFolder + "start";
            
            // Pfad des "end" Ordners
            String EndPath = tlFolder + "end";
            
            // Erstelle den "backup" Ordner
            File addBackupFolder = new File(BackupPath);
            addBackupFolder.mkdir();
            
            // Erstelle den "backup" Ordner
            File addEndFolder = new File(EndPath);
            addEndFolder.mkdir();
            
            // Überprüfen ob der eingegebene Pfad stimmt
            File Path = new File(path);
            if(Path.isDirectory()){
                // Kopiere alle Quellcode Dateien in den "backup" Ordner
                CopyDirectory create = new CopyDirectory(); 
                create.copyDir(Path, addBackupFolder);
            }
        }
    }

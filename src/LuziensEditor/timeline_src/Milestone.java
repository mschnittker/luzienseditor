/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor.timeline_src;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author markusschnittker
 */
public class Milestone {
    String name;
    String NewVersion;
    
    void deleteDir(File path) {
      for (File file : path.listFiles()) {
         if(file.isDirectory())
            deleteDir(file);
         file.delete();
      }
      path.delete();
   }
    
    public Milestone(String name,String NewVersion) throws FileNotFoundException, IOException{
        this.name = name;
        this.NewVersion = NewVersion;
        
        // Pfad zur "timeline.conf" Datei
        String tlConf = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator + "timeline.conf";
        
        // Version des Projekts aus der timeline.conf Datei einlesen
        String line;
        String array[] =  new String[100];
        final String path;
        final String version;
        final String developer;
        final String description;
        try{
                FileReader fread = new FileReader(tlConf); 
                BufferedReader in = new BufferedReader(fread); 
			
                for(int i = 0;(line = in.readLine())!=null; i++){ 
                    array[i] = line; 			
                }
            }
        catch(IOException e){
		// Fehlerbehandlung
        }
	
        
        version = array[1];
        developer = array[2];
        description = array[3];
        path = array[4];
        
        // Ordner mit der aktuellen Version anlegen
        String ProjectPath = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator + "milestone" + File.separator + version + File.separator;
        File ProjectDir = new File(ProjectPath);
        if(!ProjectDir.isDirectory()){
            ProjectDir.mkdirs();
        }
        
        // Pfad vom "start" Ordner 
        String StartPath = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator + "start";
        File Path2Start = new File(StartPath);

//        try{
//            CopyDirectory copy = new CopyDirectory();
//            copy.copyDir(Path2Start,ProjectDir);
//        }
//        catch(FileNotFoundException ex){
//            // Fehlerbehandlung !
//        }
        
        // alle Dateine in den "version" Ordner
        String AppPath = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator;
        String Files[] = new String[100];
        String Filename[] = new String[100];
        
        File dir = new File(AppPath);
        File[] fileList = dir.listFiles();

        int i = 0; // Dateipfad
        int k = 0; // Dateiname
        
        File Directory = new File(path);
        CopyDirectory copyAll = new CopyDirectory();
        copyAll.copyDir(Directory,ProjectDir);
        
//        for(File f : fileList) { 
//           if(f.isDirectory() && !f.getName().equals("start") && !f.getName().equals("end") && !f.getName().equals("milestone")){
//               Files[i] = String.valueOf(f.getPath());
//               Filename[k] = String.valueOf(f.getPath());
//               String Path = Files[i];
//               File Directory = new File(Path);
//               CopyDirectory copyAll = new CopyDirectory();
//               copyAll.copyDir(Directory,ProjectDir);
//               //JOptionPane.showMessageDialog(null, Path);
//               ++i;
//               ++k;
// 
//           }
//        }
        
        // "start" Ordner löschen
        deleteDir(Path2Start);
        
        // "start" Ordner erstellen
        if(!Path2Start.isDirectory()){
            Path2Start.mkdir();
        }
        
        // Dateien von "version" nach "start" kopieren
        CopyDirectory copyNew = new CopyDirectory();
        copyNew.copyDir(ProjectDir,Path2Start);
        
        // "end" Ordner löschen
        String EndPath = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator + "end";
        File Path2End = new File(EndPath);
        deleteDir(Path2End);
        
        // "end" Ordner erstellen
        if(!Path2End.isDirectory()){
            Path2End.mkdir();
        }
        
        // neue Version abfragen
//        String NewVersion;
//        System.out.printf("How is the current version number of  %s ?\n",name);
//        Scanner sc_version = new Scanner(System.in);
//        NewVersion = sc_version.nextLine();
        
        // "timeline.conf" neue Version eintragen
        String content = name + "\n" +
                         NewVersion + "\n" + 
                         developer + "\n" +
                         description + "\n" +
                         path ;
        
        FileStream write = new FileStream();
        write.write(tlConf, content);
        
        // alle Dateine bis auf "end", "start" , "version" und die "timeline.conf" löschen
        for(File f : fileList) { 
           if(f.isDirectory() && !f.getName().equals("start") && !f.getName().equals("end") && !f.getName().equals("milestone")){
               Files[i] = String.valueOf(f.getPath());
               Filename[k] = String.valueOf(f.getPath());
               String Path = Files[i];
               File DirectoryX = new File(Path);
               deleteDir(DirectoryX);
               //JOptionPane.showMessageDialog(null, Path);
               ++i;
               ++k;
 
           }
        }
    }
}

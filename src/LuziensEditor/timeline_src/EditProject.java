/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor.timeline_src;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author markusschnittker
 */
public class EditProject {
    String name;
    String version;
    String developer;
    String description;
    String path;
    
    public EditProject(String name,String version,String developer,String description,String path) throws IOException{
        this.name = name;
        this.version = version;
        this.developer = developer;
        this.description = description;
        this.path = path;
        
        // Pfad zur timeline.conf
        String tlConf = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator + "timeline.conf";
        
        
        
        
//        // Versionsnummer
//        System.out.printf("Version       : ");
//        Scanner sc_ver = new Scanner(System.in);
//        version = sc_ver.nextLine();
//
//        // Name des Programmierers
//        System.out.printf("Developer     : ");
//        Scanner sc_dev = new Scanner(System.in);
//        developer = sc_dev.nextLine();
//
//        // Kurze Beschreibung
//        System.out.printf("Description   : ");
//        Scanner sc_des = new Scanner(System.in);
//        description = sc_des.nextLine();
//
//        // Pfad des Programms
//        System.out.printf("Path          : ");
//        Scanner sc_pat = new Scanner(System.in);
//        path = sc_pat.nextLine();
        
        // Datei mit neuem Inhalt überschreiben
        String content = name + "\n" +
                         version + "\n" + 
                         developer + "\n" +
                         description + "\n" +
                         path ;
        
        FileStream write = new FileStream();
        write.write(tlConf, content);
    }
}

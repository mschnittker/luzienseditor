/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor.timeline_src;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author markusschnittker
 */
public class CompareFiles{
    String name;
    String file;
    String date1;
    String date2;
    public CompareFiles(String name,String file,String date1,String date2){
        this.name = name;
        this.file = file;
        this.date1 = date1;
        this.date2 = date2;
        
        
        String File1Path;
        if(date1.equals("start")){
            File1Path = System.getProperty("user.home") + File.separator + "timeline" + File.separator +
                                name + File.separator + "start" + File.separator + file;
        }
        else{
            File1Path = System.getProperty("user.home") + File.separator + "timeline" + File.separator +
                                name + File.separator + date1 + File.separator + file;
        }
        
        
        String File2Path = System.getProperty("user.home") + File.separator + "timeline" + File.separator +
                          name + File.separator + date2 + File.separator + file;
        
        
        File file1 = new File(File1Path);
        File file2 = new File(File2Path);
        if((file1.isFile()) && (file1.isFile())){
                // 1. Datei in array einlesen
                String line1;
                String array1[] =  new String[30000];

                try{
                        FileReader fread1 = new FileReader(File1Path); 
                        BufferedReader in1 = new BufferedReader(fread1); 

                        for(int i = 0;(line1 = in1.readLine())!=null; i++){
                                array1[i] = line1; 			
                        }
                }
                catch(IOException e){
                        // Fehlerbehandlung
                }
                

                
                // 2. Datei in array einlesen
                String line2;
                String array2[] =  new String[30000];

                try{
                        FileReader fread2 = new FileReader(File2Path); 
                        BufferedReader in2 = new BufferedReader(fread2); 

                        for(int j = 0;(line2 = in2.readLine())!=null; j++){
                                array2[j] = line2; 			
                        }
                }
                catch(IOException e){
                        // Fehlerbehandlung
                }

                
                
                // Datei 1 & 2 vergleichen
                int diff = 0;
                long file1_le = file1.length();
                long file2_le = file2.length();
                
                System.out.println("\nDifferens \n");
                try{
                    if(file1_le < file2_le){
                        for(int k = 0;k<=file2.length();k++){
                            if(!array1[k].equals(array2[k])){
                                System.out.printf("Line %d : %s\n",k+1,array2[k]);
                                diff++;
                            }
                        }
                    }
                    else{
                        for(int k = 0;k<=file1.length();k++){
                            if(!array1[k].equals(array2[k])){
                                System.out.printf("Line %d : %s\n",k+1,array2[k]);
                                diff++;
                            }
                        }
                    }
                }
                catch(NullPointerException e1){
                        // Fehlerbehandlung
                }
                
                System.out.printf("\n%d item(s) found\n",diff);
        }
    }
}

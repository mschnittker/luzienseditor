/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor.timeline_src;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author markusschnittker
 */
public class ListProjects {
    ListProjects(){
        
        // Pfad zum timeline Ordner
        String tlPath = System.getProperty("user.home") + File.separator + "timeline" + File.separator;
        
        // File erzeugen
        File dir = new File(tlPath);
        File[] fileList = dir.listFiles();

        // Zählschleife 
        int i = 0; 
        
        // Alle Namen ausgeben
        for(File f : fileList){            
           //System.out.println(f.getName());
           try{
               for(int j = 0;j<=f.length();j++){
                    // Daten des Projekts aus der timeline.conf Datei einlesen
                    String tlConf = System.getProperty("user.home") + File.separator + "timeline" + File.separator + fileList[j].getName() + File.separator + "timeline.conf";
                    
                    // .DS_Store ignorieren
                    if(!fileList[j].getName().equals(".DS_Store")){
                        // Inhalt aus timeline.conf
                        String line;
                        String array[] = new String[5];
                        String Name;
                        String Version;
                        String Developer;
                        String Description;
                        String Path;

                        try{
                                FileReader fread = new FileReader(tlConf); 
                                BufferedReader in = new BufferedReader(fread); 

                                for(int k = 0;(line = in.readLine())!=null; k++){ 
                                    array[k] = line; 			
                                }
                            }
                        catch(IOException e){
                                // Fehlerbehandlung
                        }

                        Name = array[0];
                        Version = array[1];
                        Developer = array[2];
                        Description = array[3];
                        Path = array[4];

                        System.out.println();
                        System.out.println(Name + " " + Version);
                        System.out.println(Developer);
                        System.out.println(Description);
                        System.out.println(Path);
                        System.out.println();
                    }
               }
            }
            catch(ArrayIndexOutOfBoundsException e)
            {
            // Fehlerbehandlung
            }
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor.timeline_src;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

/**
 *
 * @author markus schnittker
 */
public class ShowProject {
    String name;
    public ShowProject(String name){
        this.name = name;
        
        // Pfad des Projekts /home/.../timeline/.../...
        String tlProject = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator;
        
        // Alle Dateien des Programmordners auflisten
        String Dirs[] = new String[100];
        
        File dir = new File(tlProject);
        File[] fileList = dir.listFiles();

        
        int i = 0; // Dateipfad
        
        System.out.println("\t+" + name);
        
        for(File f : fileList) {
           System.out.println("\t|_" + f.getName());
           if(f.isDirectory() && (!f.getName().equals("start"))){   
                File dirSUB = new File(f.getAbsolutePath());
                File[] SUBfileList = dirSUB.listFiles();
                for(File f_sub : SUBfileList) {
                    System.out.println("\t  |_" + f_sub.getName());
                    if(f_sub.isDirectory()){   
                        File dirSUBSUB = new File(f_sub.getAbsolutePath());
                        File[] SUBSUBfileList = dirSUBSUB.listFiles();
                        for(File f_subsub : SUBSUBfileList) {
                            System.out.println("\t    |_" + f_subsub.getName());
                        }
                   }
                }
           }
           //Dirs[i] = String.valueOf(f.getName());
           //++i;
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor.timeline_src;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import LuziensEditor.timeline_src.CopyDirectory;
/**
 *
 * @author markus schnittker
 */
public class RunProject {
    String name;
    
    // Intervall der Prüfung (in Sekunden)
    static int INTERVALL = 120; 
    
    public RunProject(String name) throws FileNotFoundException{
        this.name = name;
        
        // Pfad des Projekts /home/.../timeline/.../...
        String tlProject = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator;
        
        // Das aktuelle Datum 
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
	Date date = new Date();
        
        // Aktuelle Zeit
        DateFormat timeFormat = new SimpleDateFormat("HH.mm.ss");
	Date time = new Date();
        
        
        String DateNow = dateFormat.format(date);
        String TimeNow = timeFormat.format(time);
            
        // Name des temp Ordners. Zum Beispiel : "01.01.2001"
        String DateFolder = dateFormat.format(date);

        // Pfad des temp Ordners
        final String dfPath = tlProject + DateFolder;
        
        // Pfad zur timeline.conf
        String tlConf = System.getProperty("user.home") + File.separator + "timeline" + File.separator + name + File.separator + "timeline.conf";
        
        // Pfad des Projekts aus der timeline.conf Datei einlesen
        String line;
        String array[] =  new String[100];
        final String path;
        try{
                FileReader fread = new FileReader(tlConf); 
                BufferedReader in = new BufferedReader(fread); 
			
                for(int i = 0;(line = in.readLine())!=null; i++){ 
                    array[i] = line; 			
                }
            }
        catch(IOException e){
		// Fehlerbehandlung
        }
		
        path = array[4];

        // Erstelle den Ordner mit dem aktuellen Datum, wenn er noch nicht vorhanden ist.
        File addDateFolder = new File(dfPath);
        if(addDateFolder.isDirectory()){
            try {
                FileModi(path,dfPath);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(RunProject.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            addDateFolder.mkdir();
            FileModi(path,dfPath);
        }
    }
    
    void FileModi(String AppPath,String dfPath) throws FileNotFoundException{
        // Alle Dateien des Programmordners auflisten
        String Files[] = new String[100];
        String Filename[] = new String[100];
        String Date[] = new String[100];
        
        File dir = new File(AppPath);
        File[] fileList = dir.listFiles();

        int i = 0; // Dateipfad
        int k = 0; // Dateiname
        int j = 0; // Datum
        
        for(File f : fileList) {            
           Files[i] = String.valueOf(f.getAbsolutePath());
           ++i;
           
           Filename[k] = String.valueOf(f.getName());
           ++k;

           String time;
           long ts;
           DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
           ts=f.lastModified();
           time = df.format(new Date(ts)); 
           Date[j] = time;
           ++j;
        }    
        
        // Das aktuelle Datum 
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
	Date date = new Date();
        
        // Aktuelle Zeit
        DateFormat timeFormat = new SimpleDateFormat("HH.mm.ss");
	Date time = new Date();
        
        
        String DateNow = dateFormat.format(date);
        String TimeNow = timeFormat.format(time);
        
        // Überprüfen ob eine Datei aktuell geändert wurde
        int b = 0;
        for(String a : Date){
            try{
                if(Date[b].equals(DateNow)){
                    // Wenn ja, dann Ordner erstellen bsp. .../01.01.2001/13.05.26/...
                    String tfPath = dfPath + File.separator + TimeNow;
                    File TimeFolder = new File(tfPath);
                    TimeFolder.mkdir();
                    
                    // Und die neue Datei in den Ordner kopieren
                    File inFile = new File(Files[b]);
                    String outPath = tfPath + File.separator + Filename[b];
                    File outFile = new File(outPath);
                    CopyDirectory cpFile = new CopyDirectory();
                    try {
                        cpFile.copyFile(inFile, outFile);
                    } catch (IOException ex) {
                        Logger.getLogger(RunProject.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                b++;
            }
            catch(NullPointerException e)
            {
            // Fehlerbehandlung
            }
        }
        
    }
}

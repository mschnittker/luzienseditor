/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author markusschnittker
 */
public class Chmod extends JDialog{
    boolean visible;
    
    JTextField txtOwner;
    JTextField txtGroup;
    JTextField txtOther;
    
    static int varOwner = 0;
    static int varOwner_read = 0;
    static int varOwner_write = 0;
    static int varOwner_execute = 0;
    
    static int varGroup = 0;
    static int varGroup_read = 0;
    static int varGroup_write = 0;
    static int varGroup_execute = 0;
    
    static int varOther = 0;
    static int varOther_read = 0;
    static int varOther_write = 0;
    static int varOther_execute = 0;
    
    Chmod(boolean visible){
        this.visible = visible;
        setTitle("Chmod");
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setLayout(new BorderLayout());
        
        /* Info */
        JPanel pnlInfo = new JPanel();
        pnlInfo.setLayout(new BorderLayout());
        pnlInfo.setBackground(Color.WHITE);
        JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
        txtInfo.setEditable(false);
        txtInfo.setBackground(Color.WHITE);
        txtInfo.setText("<b>Zugriffsrechte</b><br>Hier kannst du die Werte von Chmod berechnen.<br><br>");
        pnlInfo.add(txtInfo,BorderLayout.PAGE_START);
        add(pnlInfo,BorderLayout.NORTH);
        
        /* Main */
        JPanel pnlMain = new JPanel();
        pnlMain.setLayout(new BorderLayout());
            JPanel pnlChoose = new JPanel();
            pnlChoose.setLayout(new GridLayout(0,3));
            
            final JPanel owner = new JPanel();
            owner.setLayout(new GridLayout(4,0));
            JLabel lblOwner = new JLabel("Eigentümer :");
            final JCheckBoxMenuItem owner_read = new JCheckBoxMenuItem("lesen");
            final JCheckBoxMenuItem owner_write = new JCheckBoxMenuItem("schreiben");
            final JCheckBoxMenuItem owner_execute = new JCheckBoxMenuItem("ausführen");
            owner.add(lblOwner);
            owner.add(owner_read);
            owner.add(owner_write);
            owner.add(owner_execute);
            pnlChoose.add(owner);
            
            JPanel group = new JPanel();
            group.setLayout(new GridLayout(4,0));
            JLabel lblgroup = new JLabel("Gruppe :");
            final JCheckBoxMenuItem group_read = new JCheckBoxMenuItem("lesen");
            final JCheckBoxMenuItem group_write = new JCheckBoxMenuItem("schreiben");
            final JCheckBoxMenuItem group_execute = new JCheckBoxMenuItem("ausführen");
            group.add(lblgroup);
            group.add(group_read);
            group.add(group_write);
            group.add(group_execute);
            pnlChoose.add(group);
            
            JPanel other = new JPanel();
            other.setLayout(new GridLayout(4,0));
            JLabel lblother = new JLabel("Andere :");
            final JCheckBoxMenuItem other_read = new JCheckBoxMenuItem("lesen");
            final JCheckBoxMenuItem other_write = new JCheckBoxMenuItem("schreiben");
            final JCheckBoxMenuItem other_execute = new JCheckBoxMenuItem("ausführen");
            other.add(lblother);
            other.add(other_read);
            other.add(other_write);
            other.add(other_execute);
            pnlChoose.add(other);
            
            JPanel pnlCode = new JPanel();
            pnlCode.setLayout(new FlowLayout());
            txtOwner = new JTextField(1);
            txtGroup = new JTextField(1);
            txtOther = new JTextField(1);

            pnlCode.add(txtOwner);
            pnlCode.add(txtGroup);
            pnlCode.add(txtOther);
            
            JPanel pnlButtons = new JPanel();
            pnlButtons.setLayout(new FlowLayout());
            
            JButton btnCalc = new JButton("Berechnen");
            btnCalc.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    if(owner_read.isSelected())
                        varOwner_read = 4; 
                    else
                        varOwner_read = 0; 
                    
                    if(owner_write.isSelected())
                        varOwner_write = 2; 
                    else
                        varOwner_write = 0;
                    
                    if(owner_execute.isSelected())
                        varOwner_execute = 1; 
                    else
                        varOwner_execute = 0;
                    
                    varOwner = varOwner_read + varOwner_write + varOwner_execute;
                    txtOwner.setText(String.valueOf(varOwner));
                    
                    if(group_read.isSelected())
                        varGroup_read = 4; 
                    else
                        varGroup_read = 0; 
                    
                    if(group_write.isSelected())
                        varGroup_write = 2; 
                    else
                        varGroup_write = 0;
                    
                    if(group_execute.isSelected())
                        varGroup_execute = 1; 
                    else
                        varGroup_execute = 0;
                    
                    varGroup = varGroup_read + varGroup_write + varGroup_execute;
                    txtGroup.setText(String.valueOf(varGroup));
                    
                    if(other_read.isSelected())
                        varOther_read = 4; 
                    else
                        varOther_read = 0; 
                    
                    if(other_write.isSelected())
                        varOther_write = 2; 
                    else
                        varOther_write = 0;
                    
                    if(other_execute.isSelected())
                        varOther_execute = 1; 
                    else
                        varOther_execute = 0;
                    
                    varOther = varOther_read + varOther_write + varOther_execute;
                    txtOther.setText(String.valueOf(varOther));
                   
                }
            });
            
            JButton btnDispose = new JButton("Abbrechen");
            btnDispose.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    dispose();
                }
            });
            
            pnlButtons.add(btnCalc);
            pnlButtons.add(btnDispose);
        
        pnlMain.add(pnlChoose,BorderLayout.NORTH);
        pnlMain.add(pnlCode,BorderLayout.CENTER);
        pnlMain.add(pnlButtons,BorderLayout.SOUTH);
            
        
        add(pnlMain,BorderLayout.CENTER);
        pack();
        setVisible(visible);
    }
}

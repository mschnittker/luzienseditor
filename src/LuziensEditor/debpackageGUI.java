/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author markusschnittker
 */
public class debpackageGUI extends JDialog{
    boolean visible;
    int MAX = 30;
    
    JTextField txtfile_path;
    JTextField txtpackage_name;
    JTextField txtversion;
    JTextField txtsection;
    JTextField txtpriority;
    JTextField txtarchitecture;
    JTextField txtdepends;
    JTextField txtinstalled_size;
    JTextField txtmaintainer;
    JTextField txthompage;
    JTextField txtdescription;
    JTextField txtdescription_long;
    JTextField txtinstall_dir;
    
    String file_path;
    String package_name;
    String version;
    String section;
    String priority;
    String architecture;
    String depends;
    String installed_size;
    String maintainer;
    String hompage;
    String description;
    String description_long;
    String install_dir;
    
    
    debpackageGUI(boolean visible){
        this.visible = visible;
        
        setTitle("*.deb Paket erstellen");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
        
        /* Info */
        JPanel pnlInfo = new JPanel();
        pnlInfo.setLayout(new BorderLayout());
        pnlInfo.setBackground(Color.WHITE);
        JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
        txtInfo.setEditable(false);
        txtInfo.setBackground(Color.WHITE);
        txtInfo.setText("<b>*.deb Pakete erstellen</b><br>Hier kannst du *.deb Pakete für Debian und Ubuntu Linux erstellen.<br><br>");
        pnlInfo.add(txtInfo,BorderLayout.PAGE_START);
        add(pnlInfo,BorderLayout.NORTH);
        
        /* Benutzereingaben */
        JPanel pnlInput = new JPanel();
        pnlInput.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        JLabel lblfile_path = new JLabel("Dateipfad : *");
        c.insets = new Insets(10,0,0,0);
        c.gridx = 0;
        c.gridy = 0;
        pnlInput.add(lblfile_path,c);
        
        txtfile_path = new JTextField(MAX);
        c.gridx = 1;
        c.gridy = 0;
        pnlInput.add(txtfile_path,c);
        
        JLabel lblpackage_name = new JLabel("Paketname : *");
        c.gridx = 0;
        c.gridy = 1;
        pnlInput.add(lblpackage_name,c);
        
        txtpackage_name = new JTextField(MAX);
        c.gridx = 1;
        c.gridy = 1;
        pnlInput.add(txtpackage_name,c);
        
        JLabel lblversion = new JLabel("Version : *");
        c.gridx = 0;
        c.gridy = 2;
        pnlInput.add(lblversion,c);
        
        txtversion = new JTextField(MAX);
        c.gridx = 1;
        c.gridy = 2;
        pnlInput.add(txtversion,c);
        
        JLabel lblsection = new JLabel("Kategorie : *");
        c.gridx = 0;
        c.gridy = 3;
        pnlInput.add(lblsection,c);
        
        txtsection = new JTextField(MAX);
        txtsection.setText("admin,devel,doc,editors,math,misc,science,utils...");
        c.gridx = 1;
        c.gridy = 3;
        pnlInput.add(txtsection,c);
        
        JLabel lblpriority = new JLabel("Wichtigkeit : *");
        c.gridx = 0;
        c.gridy = 4;
        pnlInput.add(lblpriority,c);
        
        txtpriority = new JTextField(MAX);
        txtpriority.setText("extra");
        c.gridx = 1;
        c.gridy = 4;
        pnlInput.add(txtpriority,c);
        
        JLabel lblarchitecture = new JLabel("Architektur : *");
        c.gridx = 0;
        c.gridy = 5;
        pnlInput.add(lblarchitecture,c);
        
        txtarchitecture = new JTextField(MAX);
        txtarchitecture.setText("all,i386,amd64...");
        c.gridx = 1;
        c.gridy = 5;
        pnlInput.add(txtarchitecture,c);
        
        add(pnlInput,BorderLayout.CENTER);
        
        JLabel lbldepends = new JLabel("Abhängigkeit : ");
        c.gridx = 0;
        c.gridy = 6;
        pnlInput.add(lbldepends,c);
        
        txtdepends = new JTextField(MAX);
        c.gridx = 1;
        c.gridy = 6;
        pnlInput.add(txtdepends,c);
        
        JLabel lblinstalled_size = new JLabel("Größe des Pakets (kb): *");
        c.gridx = 0;
        c.gridy = 7;
        pnlInput.add(lblinstalled_size,c);
        
        txtinstalled_size = new JTextField(MAX);
        c.gridx = 1;
        c.gridy = 7;
        pnlInput.add(txtinstalled_size,c);
        
        JLabel lblmaintainer = new JLabel("Hersteller (Name <Email>): *");
        c.gridx = 0;
        c.gridy = 8;
        pnlInput.add(lblmaintainer,c);
        
        txtmaintainer = new JTextField(MAX);
        c.gridx = 1;
        c.gridy = 8;
        pnlInput.add(txtmaintainer,c);
        
        JLabel lblhompage = new JLabel("Hompage : *");
        c.gridx = 0;
        c.gridy = 9;
        pnlInput.add(lblhompage,c);
        
        txthompage = new JTextField(MAX);
        c.gridx = 1;
        c.gridy = 9;
        pnlInput.add(txthompage,c);
        
        JLabel lbldescription = new JLabel("Kurze Beschreibung : *");
        c.gridx = 0;
        c.gridy = 10;
        pnlInput.add(lbldescription,c);
        
        txtdescription = new JTextField(MAX);
        c.gridx = 1;
        c.gridy = 10;
        pnlInput.add(txtdescription,c);
        
        JLabel lbldescription_long = new JLabel("Beschreibung : *");
        c.gridx = 0;
        c.gridy = 11;
        pnlInput.add(lbldescription_long,c);
        
        txtdescription_long = new JTextField(MAX);
        c.gridx = 1;
        c.gridy = 11;
        pnlInput.add(txtdescription_long,c);
        
        JLabel lblinstall_dir = new JLabel("Installationspfad : *");
        c.gridx = 0;
        c.gridy = 12;
        pnlInput.add(lblinstall_dir,c);
        
        txtinstall_dir = new JTextField(MAX);
        txtinstall_dir.setText("/usr/share/");
        c.gridx = 1;
        c.gridy = 12;
        pnlInput.add(txtinstall_dir,c);
        
        add(pnlInput,BorderLayout.CENTER);
        
        /* Buttons */
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new FlowLayout());
        
        JButton btnCreate = new JButton("Paket erstellen");
        btnCreate.addActionListener(new ActionListener(){
           @Override public void actionPerformed(ActionEvent ev){
               file_path = txtfile_path.getText();
               package_name = txtpackage_name.getText();
               version = txtversion.getText();
               section = txtsection.getText();
               priority = txtpriority.getText();
               architecture = txtarchitecture.getText();
               depends = txtdepends.getText();
               installed_size = txtinstalled_size.getText();
               maintainer = txtmaintainer.getText();
               hompage = txthompage.getText();
               description = txtdescription.getText();
               description_long = txtdescription_long.getText();
               install_dir = txtinstall_dir.getText();
               try {
                   debian create = new debian(file_path,package_name,version,section,priority,architecture,depends,installed_size,maintainer,hompage,description,description_long,install_dir);
               } catch (FileNotFoundException ex) {
                   Logger.getLogger(debpackageGUI.class.getName()).log(Level.SEVERE, null, ex);
               } catch (IOException ex) {
                   Logger.getLogger(debpackageGUI.class.getName()).log(Level.SEVERE, null, ex);
               } catch (InterruptedException ex) {
                   Logger.getLogger(debpackageGUI.class.getName()).log(Level.SEVERE, null, ex);
               }
           } 
        });
        
        JButton btnDispose = new JButton("Abbrechen");
        btnDispose.addActionListener(new ActionListener(){
           @Override public void actionPerformed(ActionEvent ev){
               dispose();
           } 
        });
        
        pnlButtons.add(btnCreate);
        pnlButtons.add(btnDispose);
        
        add(pnlButtons,BorderLayout.SOUTH);
        
        pack();
        setVisible(visible);
    }
}

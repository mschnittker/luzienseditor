/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author markusschnittker
 */
public class JavaLook extends JDialog{
    boolean visible;
    JTextArea txtCode;
    JavaLook(boolean visible){
        this.visible = visible;
        setTitle("Java LookAndFeel");
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setLayout(new BorderLayout());
        
        /* Info */
        JPanel pnlInfo = new JPanel();
        pnlInfo.setLayout(new BorderLayout());
        pnlInfo.setBackground(Color.WHITE);
        JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
        txtInfo.setEditable(false);
        txtInfo.setBackground(Color.WHITE);
        txtInfo.setText("<b>Java LookAndFeel</b><br>Hier kannst du das Look and Feel für dein Projekt auswählen.<br><br>");
        pnlInfo.add(txtInfo,BorderLayout.PAGE_START);
        add(pnlInfo,BorderLayout.NORTH);
        
        /* Main */
        JPanel pnlMain = new JPanel();
        pnlMain.setLayout(new BorderLayout());
            JPanel pnlChoose = new JPanel();
            pnlChoose.setLayout(new GridLayout(0,3));
            
            ImageIcon gtkIcon = new ImageIcon(LuziensEditor.class.getResource("pics/gtk.png"));
            JButton GTK = new JButton(gtkIcon);
            GTK.setToolTipText("gtk");
            pnlChoose.add(GTK);
            GTK.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ev){
                    txtCode.setText("");
                    txtCode.setText("try {\n" +
                            "UIManager.setLookAndFeel(\"com.sun.java.swing.plaf.gtk.GTKLookAndFeel\");\n" +
                            "} \n" +
                            "catch (UnsupportedLookAndFeelException e) {\n" +
                            "// handle exception\n" +
                            "}\n" +
                            "catch (ClassNotFoundException e) {\n" +
                            "// handle exception\n" +
                            "}\n" +
                            "catch (InstantiationException e) {\n" +
                            "// handle exception\n" +
                            "}\n" +
                            "catch (IllegalAccessException e) {\n" +
                            "// handle exception\n" +
                            "}");
                }
            });
            
            ImageIcon motifIcon = new ImageIcon(LuziensEditor.class.getResource("pics/motif.png"));
            JButton MOTIF = new JButton(motifIcon);
            MOTIF.setToolTipText("motif");
            pnlChoose.add(MOTIF);
            MOTIF.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ev){
                    txtCode.setText("");
                    txtCode.setText("try {\n" +
                            "UIManager.setLookAndFeel(\"com.sun.java.swing.plaf.motif.MotifLookAndFeel\");\n" +
                            "} \n" +
                            "catch (UnsupportedLookAndFeelException e) {\n" +
                            "// handle exception\n" +
                            "}\n" +
                            "catch (ClassNotFoundException e) {\n" +
                            "// handle exception\n" +
                            "}\n" +
                            "catch (InstantiationException e) {\n" +
                            "// handle exception\n" +
                            "}\n" +
                            "catch (IllegalAccessException e) {\n" +
                            "// handle exception\n" +
                            "}");
                }
            });
            
            ImageIcon metalIcon = new ImageIcon(LuziensEditor.class.getResource("pics/metal.png"));
            JButton METAL = new JButton(metalIcon);
            METAL.setToolTipText("metal");
            pnlChoose.add(METAL);
            METAL.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ev){
                    txtCode.setText("");
                    txtCode.setText("try {\n" +
                            "UIManager.setLookAndFeel(\"javax.swing.plaf.metal.MetalLookAndFeel\");\n" +
                            "} \n" +
                            "catch (UnsupportedLookAndFeelException e) {\n" +
                            "// handle exception\n" +
                            "}\n" +
                            "catch (ClassNotFoundException e) {\n" +
                            "// handle exception\n" +
                            "}\n" +
                            "catch (InstantiationException e) {\n" +
                            "// handle exception\n" +
                            "}\n" +
                            "catch (IllegalAccessException e) {\n" +
                            "// handle exception\n" +
                            "}");
                }
            });
            
            JPanel pnlCode = new JPanel();
            pnlCode.setLayout(new BorderLayout());
            txtCode = new JTextArea(10,5);
            txtCode.setEditable(false);
            txtCode.setText("try {\n" +
                            "UIManager.setLookAndFeel(\" \");\n" +
                            "} \n" +
                            "catch (UnsupportedLookAndFeelException e) {\n" +
                            "// handle exception\n" +
                            "}\n" +
                            "catch (ClassNotFoundException e) {\n" +
                            "// handle exception\n" +
                            "}\n" +
                            "catch (InstantiationException e) {\n" +
                            "// handle exception\n" +
                            "}\n" +
                            "catch (IllegalAccessException e) {\n" +
                            "// handle exception\n" +
                            "}");
            JScrollPane scCode = new JScrollPane(txtCode); 
            pnlCode.add(scCode,BorderLayout.CENTER);
            
            JPanel pnlButtons = new JPanel();
            pnlButtons.setLayout(new FlowLayout());
            
            JButton btnCopy = new JButton("Kopieren");
            btnCopy.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    String code = txtCode.getText();
                    StringSelection selection = new StringSelection(code);
                    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                    clipboard.setContents(selection, selection);
                }
            });
            
            JButton btnDispose = new JButton("Abbrechen");
            btnDispose.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    dispose();
                }
            });
            
            pnlButtons.add(btnCopy);
            pnlButtons.add(btnDispose);
        
        pnlMain.add(pnlChoose,BorderLayout.NORTH);
        pnlMain.add(pnlCode,BorderLayout.CENTER);
        pnlMain.add(pnlButtons,BorderLayout.SOUTH);
            
        
        add(pnlMain,BorderLayout.CENTER);
        pack();
        setVisible(visible);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import LuziensEditor.timeline_src.NewProject;
import LuziensEditor.timeline_src.RunProject;
import LuziensEditor.timeline_src.DeleteProject;
import LuziensEditor.timeline_src.Milestone;
import LuziensEditor.timeline_src.EditProject;
import LuziensEditor.timeline_src.ShowProject;
import LuziensEditor.timeline_src.CompleteDay;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author markusschnittker
 */
public class timelineGui extends JFrame{
    boolean visible;
    
    String txtProject; // Name des Projekts
    
    
    class createProject extends JPanel{
        
        JTextField txtVersion; // Version
        JTextField txtDeveloper; // Programmierer
        JTextField txtDescription; // Beschreibung
        JTextField txtPath; // Projektpfad
        
        String name,version,developer,description,path;
        createProject(){
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            
            JLabel lblVersion = new JLabel("Version : ");
            c.gridx = 0;
            c.gridy = 0;
            c.insets = new Insets(10,0,0,0);
            add(lblVersion,c);

            txtVersion = new JTextField();
            txtVersion.setPreferredSize(new Dimension(250,25));
            c.gridx = 1;
            c.gridy = 0;
            add(txtVersion,c);
            
            JLabel lblDeveloper = new JLabel("Programmierer : ");
            c.gridx = 0;
            c.gridy = 1;
            c.insets = new Insets(10,0,0,0);
            add(lblDeveloper,c);

            txtDeveloper = new JTextField();
            txtDeveloper.setPreferredSize(new Dimension(250,25));
            c.gridx = 1;
            c.gridy = 1;
            add(txtDeveloper,c);
            
            JLabel lblDescription = new JLabel("Beschreibung : ");
            c.gridx = 0;
            c.gridy = 2;
            c.insets = new Insets(10,0,0,0);
            add(lblDescription,c);

            txtDescription = new JTextField();
            txtDescription.setPreferredSize(new Dimension(250,25));
            c.gridx = 1;
            c.gridy = 2;
            add(txtDescription,c);
            
            JLabel lblPath = new JLabel("Pfad : ");
            c.gridx = 0;
            c.gridy = 3;
            c.insets = new Insets(10,0,0,0);
            add(lblPath,c);

            txtPath = new JTextField();
            txtPath.setPreferredSize(new Dimension(250,25));
            c.gridx = 1;
            c.gridy = 3;
            add(txtPath,c);
            
            JPanel pnlButton = new JPanel();
            pnlButton.setLayout(new FlowLayout());
            JButton btnCreate = new JButton("Erstellen");
            btnCreate.addActionListener(new ActionListener(){
               @Override public void actionPerformed(ActionEvent ev){
                   name = txtProject;
                   version = txtVersion.getText();
                   developer = txtDeveloper.getText();
                   description = txtDescription.getText();
                   path = txtPath.getText();
                   try {
                       NewProject create = new NewProject(name,version,developer,description,path);
                   } catch (IOException ex) {
                       Logger.getLogger(timelineGui.class.getName()).log(Level.SEVERE, null, ex);
                   }

               } 
            });
            
            pnlButton.add(btnCreate);
            c.gridx = 0;
            c.gridy = 4;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.gridheight = 2;
            add(pnlButton,c);
            
        }
    }
    
//    class stampProject extends JPanel{
//        stampProject(){
//            setLayout(new FlowLayout());
//            JButton btnStamp = new JButton("stamp project");
//            btnStamp.addActionListener(new ActionListener(){
//                @Override public void actionPerformed(ActionEvent ev){
//                    String name = txtProject.getText();
//                    try {
//                        RunProject stamp = new RunProject(name);
//                    } catch (FileNotFoundException ex) {
//                        Logger.getLogger(timelineGui.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//            });
//            add(btnStamp);
//        }
//    }
//    
//    class deleteProject extends JPanel{
//        deleteProject(){
//            setLayout(new FlowLayout());
//            JButton btnDel = new JButton("delete project");
//            btnDel.addActionListener(new ActionListener(){
//                @Override public void actionPerformed(ActionEvent ev){
//                    String name = txtProject.getText();
//                    DeleteProject del = new DeleteProject(name);
//                }
//            });
//            add(btnDel);
//
//        }
//    }
    
    class milestoneProject extends JPanel{
        JTextField txtNewVersion;
        
        milestoneProject(){
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            
            JLabel lblNewVersion = new JLabel("Neue Version : ");
            c.gridx = 0;
            c.gridy = 0;
            c.insets = new Insets(10,0,0,0);
            add(lblNewVersion,c);

            txtNewVersion = new JTextField();
            txtNewVersion.setPreferredSize(new Dimension(250,25));
            c.gridx = 1;
            c.gridy = 0;
            add(txtNewVersion,c);
            
            
            
            JPanel pnlButton = new JPanel();
            pnlButton.setLayout(new FlowLayout());
            JButton btnCreate = new JButton("Milestone");
            btnCreate.addActionListener(new ActionListener(){
               @Override public void actionPerformed(ActionEvent ev){
                   String name = txtProject;
                   String NewVersion = txtNewVersion.getText();
                   try {
                       Milestone milestone = new Milestone(name,NewVersion);
                   } catch (IOException ex) {
                       Logger.getLogger(timelineGui.class.getName()).log(Level.SEVERE, null, ex);
                   }

               } 
            });
            
            pnlButton.add(btnCreate);
            c.gridx = 0;
            c.gridy = 4;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.gridheight = 2;
            add(pnlButton,c);

        }
    }
    
    class editProject extends JPanel{
        JTextField txtVersion; // Version
        JTextField txtDeveloper; // Programmierer
        JTextField txtDescription; // Beschreibung
        JTextField txtPath; // Projektpfad
        
        String name,version,developer,description,path;
        editProject(){
            
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            
            JLabel lblVersion = new JLabel("Version : ");
            c.gridx = 0;
            c.gridy = 0;
            c.insets = new Insets(10,0,0,0);
            add(lblVersion,c);

            txtVersion = new JTextField();
            txtVersion.setPreferredSize(new Dimension(250,25));
            c.gridx = 1;
            c.gridy = 0;
            add(txtVersion,c);
            
            JLabel lblDeveloper = new JLabel("Programmierer : ");
            c.gridx = 0;
            c.gridy = 1;
            c.insets = new Insets(10,0,0,0);
            add(lblDeveloper,c);

            txtDeveloper = new JTextField();
            txtDeveloper.setPreferredSize(new Dimension(250,25));
            c.gridx = 1;
            c.gridy = 1;
            add(txtDeveloper,c);
            
            JLabel lblDescription = new JLabel("Beschreibung : ");
            c.gridx = 0;
            c.gridy = 2;
            c.insets = new Insets(10,0,0,0);
            add(lblDescription,c);

            txtDescription = new JTextField();
            txtDescription.setPreferredSize(new Dimension(250,25));
            c.gridx = 1;
            c.gridy = 2;
            add(txtDescription,c);
            
            JLabel lblPath = new JLabel("Pfad : ");
            c.gridx = 0;
            c.gridy = 3;
            c.insets = new Insets(10,0,0,0);
            add(lblPath,c);

            txtPath = new JTextField();
            txtPath.setPreferredSize(new Dimension(250,25));
            c.gridx = 1;
            c.gridy = 3;
            add(txtPath,c);
            
            JPanel pnlButton = new JPanel();
            pnlButton.setLayout(new FlowLayout());
            JButton btnCreate = new JButton("Bearbeiten");
            btnCreate.addActionListener(new ActionListener(){
               @Override public void actionPerformed(ActionEvent ev){
                   name = txtProject;
                   version = txtVersion.getText();
                   developer = txtDeveloper.getText();
                   description = txtDescription.getText();
                   path = txtPath.getText();
                   try {
                       EditProject edit = new EditProject(name,version,developer,description,path);
                   } catch (IOException ex) {
                       Logger.getLogger(timelineGui.class.getName()).log(Level.SEVERE, null, ex);
                   }

               } 
            });
            
            pnlButton.add(btnCreate);
            c.gridx = 0;
            c.gridy = 4;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.gridheight = 2;
            add(pnlButton,c);
        }
    }
    
    class completeProject extends JPanel{
        JTextField txtDate; // Dateum
        JTextField txtTime; // Zeit
        
        String name,date,time;
        completeProject(){

            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            
            JLabel lblDate = new JLabel("Datum : ");
            c.gridx = 0;
            c.gridy = 0;
            c.insets = new Insets(10,0,0,0);
            add(lblDate,c);

            txtDate = new JTextField();
            txtDate.setPreferredSize(new Dimension(250,25));
            c.gridx = 1;
            c.gridy = 0;
            add(txtDate,c);
            
            JLabel lblTime = new JLabel("Zeit : ");
            c.gridx = 0;
            c.gridy = 1;
            c.insets = new Insets(10,0,0,0);
            add(lblTime,c);

            txtTime = new JTextField();
            txtTime.setPreferredSize(new Dimension(250,25));
            c.gridx = 1;
            c.gridy = 1;
            add(txtTime,c);
            
            
            JPanel pnlButton = new JPanel();
            pnlButton.setLayout(new FlowLayout());
            JButton btnCreate = new JButton("Fertigstellen");
            btnCreate.addActionListener(new ActionListener(){
               @Override public void actionPerformed(ActionEvent ev){
                   name = txtProject;
                   date = txtDate.getText();
                   time = txtTime.getText();

                   try {
                       CompleteDay complete = new CompleteDay(name,date,time);
                   } catch (IOException ex) {
                       Logger.getLogger(timelineGui.class.getName()).log(Level.SEVERE, null, ex);
                   }

               } 
            });
            
            pnlButton.add(btnCreate);
            c.gridx = 0;
            c.gridy = 4;
            c.fill = GridBagConstraints.HORIZONTAL;
            c.gridheight = 2;
            add(pnlButton,c);

        }
    }
    
    class publishProject extends JPanel{
        publishProject(){
            

        }
    }
    
    timelineGui(boolean visible){
        this.visible = visible;
        
        setTitle("Timeline");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
        
        /* Info */
        JPanel pnlInfo = new JPanel();
        pnlInfo.setLayout(new BorderLayout());
        pnlInfo.setBackground(Color.WHITE);
        JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
        txtInfo.setEditable(false);
        txtInfo.setBackground(Color.WHITE);
        txtInfo.setText("<b>Timeline</b><br>Hier kannst du deinen Source Code in einer \"Zeitlinie\" verwalten.<br><br>");
        pnlInfo.add(txtInfo,BorderLayout.PAGE_START);
        add(pnlInfo,BorderLayout.NORTH);
        
        /* Main */
        JPanel pnlMain = new JPanel();
        pnlMain.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        JLabel lblProject = new JLabel("Projektname : ");
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(10,0,0,0);
        pnlMain.add(lblProject,c);
        
        String path = (new StringBuilder()).append(System.getProperty("user.home")).append("/timeline/").toString();
        File f = new File(path);
        String files[] = f.list();
        JComboBox cbProject = new JComboBox(files);
        cbProject.setPreferredSize(new Dimension(300, 25));
        cbProject.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ev01)
            {
                JComboBox cbProject = (JComboBox)ev01.getSource();
                Object selected = cbProject.getSelectedItem();
                txtProject = (new StringBuilder()).append(String.valueOf(selected)).toString();
                //JOptionPane.showMessageDialog(null, txtProject);
            }

        }
        );
        c.gridx = 1;
        c.gridy = 0;
        pnlMain.add(cbProject,c);
        
        
        
        
        JTabbedPane tbpChoose = new JTabbedPane(JTabbedPane.SCROLL_TAB_LAYOUT);
        tbpChoose.addTab("Projekt erstellen", new createProject());
//        tbpChoose.addTab("Stamp", new stampProject());
//        tbpChoose.addTab("Delete", new deleteProject());
        tbpChoose.addTab("Milestone setzen", new milestoneProject());
        tbpChoose.addTab("Projekt bearbeiten", new editProject());
        tbpChoose.addTab("Projekt fertigstellen", new completeProject());
        //tbpChoose.addTab("Publish", new publishProject());
        
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 2;
        c.fill = GridBagConstraints.HORIZONTAL;
        pnlMain.add(tbpChoose,c);
        
        add(pnlMain,BorderLayout.CENTER);
        
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new GridLayout(2,3));
        //GridBagConstraints d = new GridBagConstraints();
        
        JButton btnShow = new JButton("Projekt anzeigen");
        btnShow.addActionListener(new ActionListener(){
           @Override public void actionPerformed(ActionEvent ev){
               String name = txtProject;
               ShowProject show = new ShowProject(name);
           } 
        });
//        d.gridx = 0;
//        d.gridy = 0;
//        d.insets = new Insets(10,10,10,10);
        pnlButtons.add(btnShow);
        JButton btnStamp = new JButton("Projekt stempeln");
        btnStamp.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ev){
                    String name = txtProject;
                    try {
                        RunProject stamp = new RunProject(name);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(timelineGui.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
//        d.gridx = 0;
//        d.gridy = 1;
//        d.insets = new Insets(10,10,10,10);
        pnlButtons.add(btnStamp);
        JButton btnDel = new JButton("Projekt löschen");
            btnDel.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ev){
                    String name = txtProject;
                    
                    DeleteProject del = new DeleteProject(name);
                }
            });
//        d.gridx = 1;
//        d.gridy = 0;
//        d.insets = new Insets(0,10,10,10);
        pnlButtons.add(btnDel);
        JButton btnDispose = new JButton("Abbrechen");
        btnDispose.addActionListener(new ActionListener(){
           @Override public void actionPerformed(ActionEvent ev){
               dispose();
           } 
        });
//        
//        d.gridx = 1;
//        d.gridy = 1;
//        d.insets = new Insets(0,10,10,10);
        pnlButtons.add(btnDispose);
        
        add(pnlButtons,BorderLayout.SOUTH);
        pack();
        setVisible(visible);
    }
}

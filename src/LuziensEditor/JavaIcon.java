/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author markusschnittker
 */
public class JavaIcon extends JDialog {
    boolean visible;
    
    JTextField txtPath;
    JTextArea txtCode;
    
    JavaIcon(boolean visible){
        this.visible = visible;
        setTitle("Java - Bild einfügen");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
        
        /* Info */
        JPanel pnlInfo = new JPanel();
        pnlInfo.setLayout(new BorderLayout());
        pnlInfo.setBackground(Color.WHITE);
        JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
        txtInfo.setEditable(false);
        txtInfo.setBackground(Color.WHITE);
        txtInfo.setText("<b>Bild einfügen</b><br>Hier kannst du ein Bild in deinen Java Source Code einfügen.<br><br>");
        pnlInfo.add(txtInfo,BorderLayout.PAGE_START);
        add(pnlInfo,BorderLayout.NORTH);
        
        /* Main */
        JPanel pnlCenter = new JPanel();
        pnlCenter.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
            /* File choose */
            JPanel pnlChoose = new JPanel();
            pnlChoose.setLayout(new FlowLayout());
            JLabel lblName = new JLabel("Bildname : ");
            txtPath = new JTextField(25);
            //JButton btnFileChoose = new JButton("open");
            JButton btnFile = new JButton("...");
        btnFile.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ev){
                JFileChooser fc = new JFileChooser();
//                fc.setFileFilter( new FileFilter()
//                {
//                  @Override public boolean accept( File f )
//                  {
//                    return f.isDirectory() ||
//                      f.getName().toLowerCase().endsWith( ".txt" );
//                  }
//                  @Override public String getDescription()
//                  {
//                    return "Texte";
//                  }
//                } );
                int state = fc.showOpenDialog( null );
                if ( state == JFileChooser.APPROVE_OPTION )
                {
                  File file = fc.getSelectedFile();
                  txtPath.setText(file.getAbsolutePath());
                }
            }
        });
            pnlChoose.add(lblName);
            pnlChoose.add(txtPath);
            pnlChoose.add(btnFile);
            c.gridx = 0;
            c.gridy = 0;
            c.fill = 1;
            pnlCenter.add(pnlChoose,c);
            
            /* Code */
            JPanel pnlCode = new JPanel();
            pnlCode.setLayout(new BorderLayout());
            txtCode = new JTextArea(5,25);
            txtCode.setEditable(false);
            txtCode.setText("");
            JScrollPane scCode = new JScrollPane(txtCode);
            pnlCode.add(scCode,BorderLayout.CENTER);
            c.gridx = 0;
            c.gridy = 1;
            c.fill = 1;
            pnlCenter.add(pnlCode,c);
       
        add(pnlCenter,BorderLayout.CENTER);
        
        /* Buttons */
        JPanel pnlButtons = new JPanel();
        pnlButtons.setLayout(new FlowLayout());
        
        JButton btnCreate = new JButton("Code erstellen");
        btnCreate.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ev){
                String name = txtPath.getText();
                String Icon = "ImageIcon Picture = new ImageIcon(getClass().getResource(\""+ name +"\"));";
                txtCode.setText(Icon);
            }
        });
        
        JButton btnCopy = new JButton("Kopieren");
        btnCopy.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ev){
                String code = txtCode.getText();
                StringSelection selection = new StringSelection(code);
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection, selection);
            }
        });
        
        JButton btnDispose = new JButton("Abbrechen");
        btnDispose.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ev){
                dispose();
            }
        });
        
        pnlButtons.add(btnCreate);
        pnlButtons.add(btnCopy);
        pnlButtons.add(btnDispose);
        
        add(pnlButtons,BorderLayout.SOUTH);
        
        pack();
        setVisible(visible);
    }
}

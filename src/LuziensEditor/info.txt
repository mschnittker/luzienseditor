********************************************************************************
*                           Luzien's Editor                                    *
*                             version 0.4.4                                    *
******************************************************************************** 


INCLUDE FILES 
--------------------------------------------------------------------------------
LUZIEN
 * LuziensEditor.java        : Main File
 * ButtonTabComponent.java   : erstellt schließbare Editor Tabs
 * Commands.java             : Luziens Shell
 * ASM.java                  : ASM Register anzeigen
 * JavaLook.java             : Look and feel
 * Chmod.java                : Berechnung der Chmod Werte
 * EditSH.java               : Editor für java/c/cpp .sh build Dateien (txtConsole)
 * FileStream.java           : Datei IO
 * JavaIcon.java             : Bilder / Icons in Java einbinden
 * CopyToXampp.java          : kopiert web dokumente in den xampp/htdocs ordner
 * ProjectImport.java        : importiert projekte von anderen programmen
 * WebColor.java             : Farben in HEX Code für HTML Seiten
 * timelineGUI.java          : GUI für das konsolenprogramm timeline
 * MySqlMonitor.java         : zeigt alle mysql db's in einem jtree an
 * ASMin.java                : GAS Code in C/C++ einfügen
 * Notizen.java              : Notizen erstellen und bearbeiten

TIMELINE
 * timelineGui.java          : timeline GUI

DEBPACKAGE
 * debian.java               : debpackage funktionen
 * debpackageGUI.java        : debpackage GUI


PATH 
--------------------------------------------------------------------------------
 * Projektpfad  : HOME/LuziensProjects/...
 * QuickCode    : HOME/LuziensProjects/Luzien/QuickCode/...
 * Luziens Pfad : AppPath/Luzien.jar
                  AppPath/lib/jsyntaxpane-0.9.5-b29.jar
                  AppPath/lib/swing-layout-1.0.4.jar
                  LuziensProjects/Luzien/Linux/luzien.o
                  AppPath/MacOS/luzien.o
 * Timeline     : HOME/LuziensProjects/Luzien/timeline/
                  HOME/LuziensProjects/Luzien/timeline/timeline.jar (root)
 * NASM         : HOME/LuziensProjects/Luzien/MacOS/nasm/   

Linux
 * Luzien       : /usr/share/luzien/Editor.java
               

START 
--------------------------------------------------------------------------------
MacOS :
sudo java -jar /.../luzien.jar

Linux (nur root!):
sudo java -jar /usr/share/luzien/Editor.java

KONSOLEN KOMMANDOS
--------------------------------------------------------------------------------
 * ?            : Zeigt alle Kommandos von Luzien
 * clr          : Den Bildschirm leeren
 * exit         : Luzien beenden
 * c            : startet QuickCode C
 * cpp          : startet QuickCode C++
 * lnf          : startet Java LookAndFeel
 * chmod        : Den Chmod Wert berechnen

BUGS
--------------------------------------------------------------------------------
 * ProjectView  : Die Projektanzeige (tree) lädt sich nicht dynamisch.
                  Bei jeder Änderung muss Luzien neu gestartet werden.
 * Compile      : Kompilierte Programme können nicht in der eigenen Shell 
                  ausgeführt werden, wenn sie eine Eingabe verlangen.
                  Luzien stürtzt dann ab !

TODO
--------------------------------------------------------------------------------

 v0.4.4

 * JTableExample.java umschreiben nach MySqlMonitor.java

--------------------------------------------------------------------------------
 * asm register anzeigen  (JNI -> c+asm)
 * Hex Editor
 * FTP client
 * UML 
 * Java wysiwyg editor
 * Microkontroller (hex datei übertragen)
 * Schaltpläne / testen u. Proggen 
--------------------------------------------------------------------------------
 ?

 * Editor       : Java -> Toolbar -> AWT (tbMenu_java_awt)
 * JAVA jni
--------------------------------------------------------------------------------
 * ProjectView  : PopUp -> löschen,umbenennen,aktualisieren...
--------------------------------------------------------------------------------




















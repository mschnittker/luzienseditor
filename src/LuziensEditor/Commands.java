/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 *
 * @author markusschnittker
 */
public class Commands extends JPanel{
    Commands(){
        setLayout(new BorderLayout());
        final JTextArea txtCommands = new JTextArea();
        JScrollPane spCommands = new JScrollPane(txtCommands);
        
        txtCommands.setBackground(Color.BLACK);
        txtCommands.setForeground(Color.WHITE);
        txtCommands.setFont(new Font("Courier New", Font.PLAIN, 13));
        txtCommands.setEditable(true);
        
        add(spCommands,BorderLayout.CENTER);
        
        txtCommands.setText("Luzien>\nGebe \"?\" für alle Kommandos ein :\n");
        
        txtCommands.addKeyListener(new KeyAdapter(){
          
          @Override public void keyPressed(KeyEvent e)
          {
            int key = e.getKeyCode();
            if (key == KeyEvent.VK_ENTER)
            {
                String cmd = txtCommands.getText();
                String[] array = cmd.split("\n");
                String lastLine = array[array.length - 1];

                if(lastLine.equals("?") || lastLine.equals("?")){
                    txtCommands.append("\nLuziens Kommandos : \n\n");
                    txtCommands.append("\tLuzien\n");
                    txtCommands.append("\t-----------------------------------------\n");
                    txtCommands.append("\t?     - zeigt alle Kommandos von Luzien\n");
                    txtCommands.append("\tclr   - den Bildschirm leeren\n");
                    txtCommands.append("\texit  - Luzien beenden\n\n");
                    txtCommands.append("\tC/C++\n");
                    txtCommands.append("\t-----------------------------------------\n");
                    txtCommands.append("\tc     - startet QuickCode C\n");
                    txtCommands.append("\tcpp   - startet QuickCode C++\n\n");
                    txtCommands.append("\tJava\n");
                    txtCommands.append("\t-----------------------------------------\n");
                    txtCommands.append("\tlnf   - startet Java LookAndFeel\n\n");
                    txtCommands.append("\tScript\n");
                    txtCommands.append("\t-----------------------------------------\n");
                    txtCommands.append("\tchmod   - den Chmod Werte berechnen\n\n");
                    txtCommands.append("\nLuzien>\nGebe \"?\" für alle Kommandos ein :\n");
                    txtCommands.append("");
                }
                else if(lastLine.equals("clr")){
                    txtCommands.setText("");
                    txtCommands.setText("Luzien>\nGebe \"?\" für alle Kommandos ein :\n");
                }
                else if(lastLine.equals("exit")){
                    System.exit(0);
                }
                else if(lastLine.equals("c")){
                    LuziensEditor.QuickCheck qcC = new LuziensEditor.QuickCheck();
                    qcC.C(true);
                    txtCommands.append("");
                    txtCommands.append("\nLuzien>\nGebe \"?\" für alle Kommandos ein :\n");
                    txtCommands.append("");
                }
                else if(lastLine.equals("cpp")){
                    LuziensEditor.QuickCheck qcCpp = new LuziensEditor.QuickCheck();
                    qcCpp.Cpp(true);
                    txtCommands.append("");
                    txtCommands.append("\nLuzien>\nGebe \"?\" für alle Kommandos ein :\n");
                    txtCommands.append("");
                }
                else if(lastLine.equals("lnf")){
                    JavaLook lnf = new JavaLook(true);
                    txtCommands.append("");
                    txtCommands.append("\nLuzien>\nGebe \"?\" für alle Kommandos ein :\n");
                    txtCommands.append("");
                }
                else if(lastLine.equals("chmod")){
                    Chmod chmod = new Chmod(true);
                    txtCommands.append("");
                    txtCommands.append("\nLuzien>\nGebe \"?\" für alle Kommandos ein :\n");
                    txtCommands.append("");
                }
                else{
                    txtCommands.setText("");
                    txtCommands.setText("Luzien>\nGebe \"?\" für alle Kommandos ein :\n");
                }
            }
          }
        });  
    }
}

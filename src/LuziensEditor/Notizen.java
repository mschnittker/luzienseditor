/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static LuziensEditor.LuziensEditor.FileStream;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

/**
 *
 * @author markusschnittker
 */



public class Notizen extends JFrame{
    JDesktopPane dpMain;
    DefaultListModel listenModell;
    final JList liste;
    
    String Dateiname;
    String Dateipfad;
    String Inhalt;
    
    void neu(String Dateiname,String Dateipfad) throws IOException{
        this.Dateiname = Dateiname;
        this.Dateipfad = Dateipfad;
        
        final String Pfad = System.getProperty("user.home") + "/LuziensProjects/Luzien/Notes/" + Dateiname;
        
        JInternalFrame note = new JInternalFrame(Dateiname, false, 
            true, true, true);
        
        final RSyntaxTextArea txtNote = new RSyntaxTextArea();
        txtNote.addKeyListener(new KeyAdapter(){
              public void keyPressed(KeyEvent e)
              {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_CONTROL)
                {
                   FileStream speichern = new FileStream();
                    try {
                        speichern.append(Pfad, txtNote.getText());
                    } catch (IOException ex) {
                        Logger.getLogger(Notizen.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
              }
            });
        //txtNote.read(new FileReader(Dateipfad), "");
        JScrollPane scNote = new JScrollPane(txtNote);
        note.add(scNote);
        
        note.setLocation(20,20);
        note.setSize(200,200);
        note.show();
        
        dpMain.add(note);
    }
    
    void anzeigen(String Dateiname,String Dateipfad) throws IOException{
        this.Dateiname = Dateiname;
        this.Dateipfad = Dateipfad;
        
        final String Pfad = System.getProperty("user.home") + "/LuziensProjects/Luzien/Notes/" + Dateiname;
        
        JInternalFrame note = new JInternalFrame(Dateiname, false, 
            true, true, true);
        
        final RSyntaxTextArea txtNote = new RSyntaxTextArea();
        txtNote.read(new FileReader(Dateipfad), "");
        txtNote.addKeyListener(new KeyAdapter(){
              public void keyPressed(KeyEvent e)
              {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_CONTROL)
                {
                   FileStream speichern = new FileStream();
                    try {
                        speichern.append(Pfad, txtNote.getText());
                    } catch (IOException ex) {
                        Logger.getLogger(Notizen.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
              }
            });  
        
        JScrollPane scNote = new JScrollPane(txtNote);
        note.add(scNote);
        
        note.setLocation(20,20);
        note.setSize(200,200);
        note.show();
        
        dpMain.add(note);
    }
    
    void loeschen(File path){
         if (path.isFile()){
            path.delete();
         }
    }
    
    
    Notizen(){
        setTitle("Notizen");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLayout(new BorderLayout());
        
         /* Info */
        
        JPanel oben = new JPanel();
        oben.setLayout(new BorderLayout());
        
        JPanel pnlInfo = new JPanel();
        pnlInfo.setLayout(new BorderLayout());
        pnlInfo.setBackground(Color.WHITE);
        JEditorPane txtInfo = new JEditorPane();
        txtInfo.setContentType("text/html");
        txtInfo.setEditable(false);
        txtInfo.setBackground(Color.WHITE);
        txtInfo.setText("<b>Notizen</b><br>Hier kannst du Notizen anlegen und bearbeiten.<br><br>");
        pnlInfo.add(txtInfo,BorderLayout.PAGE_START);
        oben.add(pnlInfo,BorderLayout.NORTH);
        
        JToolBar tbMenu = new JToolBar();
        
        ImageIcon Neu = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLComment.png"));
        JButton btnNew = new JButton(Neu);
        btnNew.setToolTipText("Neue Notiz");
        btnNew.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ev01){
                String name = JOptionPane.showInputDialog(null,"Name der Notiz :","",JOptionPane.INFORMATION_MESSAGE);
                name = name + ".note";
                String pfad = System.getProperty("user.home") + "/LuziensProjects/Luzien/Notes/" + name;
                
                
                try {
                    neu(name,pfad);
                } catch (IOException ex) {
                    Logger.getLogger(Notizen.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                FileStream erstellen = new FileStream();
                try {
                    erstellen.write(pfad,"");
                } catch (IOException ex) {
                    Logger.getLogger(Notizen.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                listenModell.addElement(name);
            }
        });
        
        
        ImageIcon Löschen = new ImageIcon(LuziensEditor.class.getResource("Toolbar/loeschen.png"));
        JButton btnDel = new JButton(Löschen);
        btnDel.setToolTipText("Notiz löschen");
        btnDel.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ev02){
                String name = String.valueOf(liste.getSelectedValue());
                String pfad = System.getProperty("user.home") + "/LuziensProjects/Luzien/Notes/" + name;
                
                File delFile = new File(pfad);
                loeschen(delFile);
                
                listenModell.removeElement(name);

            }
        });
        
        
        tbMenu.add(btnNew);
        tbMenu.add(btnDel);
        
        oben.add(tbMenu,BorderLayout.SOUTH);
        
        add(oben,BorderLayout.NORTH);
        
        /* Links */
        listenModell = new DefaultListModel();
        
        String pfad = System.getProperty("user.home") + "/LuziensProjects/Luzien/Notes";
        File dateien = new File(pfad);
        if(!dateien.isDirectory()){
            dateien.mkdirs();
        }
        else{
            String Files[] = new String[100];
            String Filename[] = new String[100];

            File dir = new File(pfad);
            File[] fileList = dir.listFiles();

            int i = 0; // Dateipfad
            int k = 0; // Dateiname

            for(File f : fileList) {    
                if(!f.getName().equals(".DS_Store")){
                   Files[i] = String.valueOf(f.getAbsolutePath());
                   ++i;

                   Filename[k] = String.valueOf(f.getName());
                   listenModell.add(k, Filename[k]);
                   ++k;
                }

            }  
        }
        

        liste = new JList(listenModell);
        liste.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent evt) {
                try {
                    String name = String.valueOf(liste.getSelectedValue());
                    String pfad = System.getProperty("user.home") + "/LuziensProjects/Luzien/Notes/" + name;

                    anzeigen(name,pfad);
                } catch (IOException ex) {
                    Logger.getLogger(Notizen.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        

        
        /* Main */
        dpMain = new JDesktopPane();
        dpMain.setBackground(Color.WHITE);
        
       
        JSplitPane groundSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                liste, dpMain);

        groundSplit.setOneTouchExpandable(true);
        groundSplit.setDividerLocation(130);
        
        add(groundSplit,BorderLayout.CENTER);
        
        /* Statusbar */
        JPanel statusBar = new JPanel();
        statusBar.setLayout(new FlowLayout());
        add(statusBar, BorderLayout.SOUTH);
        statusBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        statusBar.setBorder(BorderFactory.createEtchedBorder());
        statusBar.add(new JLabel("Speichern mit der [CTRL] Taste"));
        
        setSize(600,400);
        setVisible(true);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static LuziensEditor.LuziensEditor.txtCpponsole;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

/**
 *
 * @author markus
 */
public class QuickCheck extends JDialog{
        QuickCheck(){}
        
        void C(boolean visible){
            setTitle("C");
            setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints ();
            
            // Textfield with function declaration
            JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
            txtInfo.setText("<b>QuickCheck C</b><br>" +
                            "Gebe den Code ein, der sonst in der Main Funktion steht.<br><br>");
            txtInfo.setEditable(false);
            txtInfo.setBackground(Color.WHITE);
            
            c.gridx = 0;
            c.gridy = 0;
            c.gridwidth = 2;
            c.fill = 1;
            
            add(txtInfo,c);
            
            // Textfield with the code
            final RSyntaxTextArea txtCppode = new RSyntaxTextArea(20,50);
            txtCppode.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_C);
            txtCppode.setText("int i;\n" +
                            "printf(\"Please enter an integer value: \");\n" +
                            "scanf(\"%d\",&i);\n" +
                            "printf(\"The value you entered is  %d\\n\",i);");
            RTextScrollPane scCode = new RTextScrollPane(txtCppode);
            
            c.gridx = 0;
            c.gridy = 1;
            c.insets = new Insets(10,10,0,10);  //top padding
            c.gridwidth = 2;
            
            c.fill = 1;
            
            add(scCode,c);
            
            JPanel pnlButtons = new JPanel();
            pnlButtons.setLayout(new FlowLayout());

            
            JButton btnCopy = new JButton("Kopieren");
            btnCopy.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    String code = txtCppode.getText();
                    StringSelection selection = new StringSelection(code);
                    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                    clipboard.setContents(selection, selection);
                }
            });
            
           
            
            JButton btnRun = new JButton("Starten");
            btnRun.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    txtCpponsole.setText("");
                    
                    String userHome = System.getProperty("user.home");
                    String buildPath = userHome + File.separator + "LuziensProjects" + File.separator + "Luzien" + File.separator + "QuickCode" + File.separator + "a.c";
                    String name = "a.c";
                    String output = buildPath.replace("a.c","a.o");
                    String option = "c";
                    String text = txtCppode.getText();
                    String out = 
                                 "#include <stdio.h>\n"+
                                 "#include <stdlib.h>\n"+
                                 "\n" +
                                 "int main(){\n"+
                                 text + "\n" +
                                 "return 0;\n" + 
                                 "}\n";
                    
                    CProject = "a.o";
                    CMain = "a.c";
                    gccFlags = " -o";
                    String run = "echo Zum Ausführen, öffne die Konsole und gebe folgendes ein : " + output + "\n";
                    runC = run;
                    
                    LuziensEditor.FileStream createFile = new LuziensEditor.FileStream();
                    try {
                        createFile.write(buildPath, out);
                    } catch (IOException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    try {
                        run(ConArgs, buildPath, name, option);
                    } catch (IOException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    
                    
                }
            });
            
            JButton btnDispose = new JButton("Abbrechen");
            btnDispose.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    dispose();
                }
            });
            
            pnlButtons.add(btnRun);
            pnlButtons.add(btnCopy);
            pnlButtons.add(btnDispose);
            
            c.gridx = 0;
            c.gridy = 2;
            add(pnlButtons,c);
            
            pack();
            setVisible(visible);
        }
        
        void Cpp(boolean visible){
            setTitle("C++");
            setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
            setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints ();
            
            // Textfield with function declaration
            JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
            txtInfo.setText("<b>QuickCode C++</b><br>" +
                            "Gebe den Code ein, der sonst in der Main Funktion steht.<br><br>");
            txtInfo.setEditable(false);
            txtInfo.setBackground(Color.WHITE);
            
            c.gridx = 0;
            c.gridy = 0;
            c.gridwidth = 2;
            c.fill = 1;
            
            add(txtInfo,c);
            
            // Textfield with the code
            final RSyntaxTextArea txtCppode = new RSyntaxTextArea(20,50);
            txtCppode.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CPLUSPLUS);
            txtCppode.setText("int i;\n" +
                            "cout << \"Please enter an integer value: \";\n" +
                            "cin >> i;\n" +
                            "cout << \"The value you entered is \" << i << endl;");
            RTextScrollPane scCode = new RTextScrollPane(txtCppode);
            
            c.gridx = 0;
            c.gridy = 1;
            c.insets = new Insets(10,10,0,10);  //top padding
            c.gridwidth = 2;
            
            c.fill = 1;
            
            add(scCode,c);
            
            JPanel pnlButtons = new JPanel();
            pnlButtons.setLayout(new FlowLayout());
            
            JButton btnCopy = new JButton("Kopieren");
            btnCopy.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    String code = txtCppode.getText();
                    StringSelection selection = new StringSelection(code);
                    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                    clipboard.setContents(selection, selection);
                }
            });
            
            
            JButton btnRun = new JButton("Starten");
            btnRun.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    txtCpponsole.setText("");
                    
                    String userHome = System.getProperty("user.home");
                    String buildPath = userHome + File.separator + "LuziensProjects"  + File.separator + "Luzien" + File.separator + "QuickCode" + File.separator + "a.cpp";
                    String name = "a.cpp";
                    String output = buildPath.replace("a.cpp","a.o");
                    String option = "cpp";
                    String text = txtCppode.getText();
                    String out = "#include <iostream>\n"+
                                 "\n" +
                                 "using namespace std;\n" +
                                 "\n" +
                                 "int main(){\n"+
                                 text + "\n" +
                                 "return 0;\n" + 
                                 "}\n";
                    
                    CppProject = "a.o";
                    CppMain = "a.cpp";
                    gppFlags = " -o";
                    String run = "echo Zum Ausführen, öffne die Konsole und gebe folgendes ein : " + output + "\n";
                    runCPP = run;
                    
                    LuziensEditor.FileStream createFile = new LuziensEditor.FileStream();
                    try {
                        createFile.write(buildPath, out);
                    } catch (IOException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    try {
                        run(ConArgs, buildPath, name, option);
                    } catch (IOException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    
                    
                }
            });
            
            JButton btnDispose = new JButton("Abbrechen");
            btnDispose.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    dispose();
                }
            });
            
            pnlButtons.add(btnRun);
            pnlButtons.add(btnCopy);
            pnlButtons.add(btnDispose);
            
            
            c.gridx = 0;
            c.gridy = 2;
            add(pnlButtons,c);
            
            pack();
            setVisible(visible);
        }
    }

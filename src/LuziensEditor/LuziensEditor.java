/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.text.DefaultEditorKit;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.net.URISyntaxException;
import java.net.URL;
//import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.tree.DefaultMutableTreeNode;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.autocomplete.ShorthandCompletion;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.fife.ui.rtextarea.SearchContext;
import org.fife.ui.rtextarea.SearchEngine;


/**
 *
 * @author Markus Schnittker
 * @since May 2013
 * @version Luzien's Editor v0.4
 * 
 */


public class LuziensEditor {
    static JTabbedPane tbLeft;
    
    static String cssBereich;
    static String cssEigenschaft;
    
    static String Text1,Pfad1,Text2,Pfad2,Ergebnis;
    
    static JTree Variabeln;
    static DefaultMutableTreeNode Var_root;
    
    static JTextArea txtVar;
    static JTextArea txtKla;
    //static JTextArea txtFun;
    
    static JTree fileTree;
    static JTree fileTreeXXX;
    static LuziensEditor.Project Files;
    static JScrollPane ProjectScroll;
    // MYSQL
    static JTextField txtBenutzer;
    static JTextField txtDB;
    static JPasswordField txtPasswort;
    static String Datenbank;
    static String Benutzername;
    static String Passwort;
    
    /* New File Dialog */
    static JList cbFile;
    static JTextArea FileInfo;
    static String FilePath;
    static String KindOfFile;
    static String ContentOfFile;
    static String FileName;

    /* OperatingSystem */
    static String os;
    
    // XAMPP
    static String XAMPP_RUN;
    static JButton btnRun;
    static JLabel lblRun;
    
    /* Konsole */
    static JTabbedPane ConsoleTab = new JTabbedPane();
    static JTextArea txtCpponsole = new JTextArea(6,20);
    static JTextArea txtDebugger = new JTextArea(6,20);
    static String[] ConArgs; // System args
    static String option; // Script language
    static String AppName; // App Name
    static String path; // App Path
    static String sh_path; // path to sh file for build editor
    static String man_path; // path to sh file for build editor
    static String Debugger_Path; // gdb /.../.../*.c
    static String Console_Path;
    
    /* GPP Optionen */
    static String CppMain;
    static String CppProject;
    static String runCPP;
    static String cppName;
    static String cppFiles = "";
    static String gppFlags; /* g++ flags */
    
    /* GCC Optionen */
    static String CMain; // the main file
    static String CProject; // user defined project name
    static String runC; // run or not 
    static String cName; // the file name *.c
    static String gccFiles = "";
    static String gccFlags;/* gcc flags */
    
    /* GAS Options */
    static String asmMain; // the main file
    static String asmProject; // user defined project name
    static String runASM; // run or not 
    static String asmName; // the file name *.c
    static String asmFiles = "";
    static String asmFlags; /* GAS flags *
 

 
    // COMPILER START --------------------------------------------------------------------------------
    
    /* Die Funktion "run" dient als Schnittstelle zur Systemkonsole.
     * Wenn der Benutzer auf den "build & run" Button drückt, werden die untenstehenden
     * Parameter an die Funktion "run" übergeben.
     * Diese werden dann an die Konsole weitergeleitet, und die Ausgabe der Konsole 
     * wird in der TextArea "txtCpponsole" angezeigt.
     */
    
    /*
     * @param args the command line arguments
     *
     * Konsolen Parameter bei der Übergabe an "run()" 
     *------------------------------------------------------------------------------
     *          | Konsolen args | Pfad zum Prog. | Programmname | Java? c? ...
     *-------------------------------------------------------------------------------
     */
   static void run(String[] args, String path, String name,String option) throws IOException, InterruptedException {
        redirectSystemStreams();
        
        // get the args from Main()
        ProcessBuilder builder = new ProcessBuilder(args);
        
        // cut the file name from the path
        String Path2App = path.replace(name, "");
        String Path2Build = Path2App.replace("src","build");
  
               if (option.equals("java")) {
                   
                   String JPath2App = path.replace(name, "");
                   JPath2App = JPath2App.replaceAll("//", "/");
                   //JOptionPane.showMessageDialog(null, JPath2App);
                    String jarName = name.replace(".java", ".jar");
                    String className = name.replace(".java", ".class");
                    String MFClass = name.replace(".java", "");
                    String MFPath = JPath2App.replace("src/" + MFClass + "/", "manifest.mf");
                    String BuildPath = JPath2App.replace("src/" + MFClass + "/", "build/");
                    //JOptionPane.showMessageDialog(null, BuildPath);
                    String MFContent = (new StringBuilder()).append("Manifest-Version: 1.0\nClass-Path: .\nMain-Class: ").append(MFClass).append("\n").toString();
                    FileStream Manifest = new FileStream();
                    Manifest.write(MFPath, MFContent);
                    String java = (new StringBuilder()).append("#!/bin/sh \ncd ").append(JPath2App).append("\n").append("javac -g:none -target 1.6 *.java \n").append("jar cvmf ").append(MFPath).append(" ").append(BuildPath).append(jarName).append(" *.class").append("\n").append("java -jar ").append(BuildPath).append(jarName).append("\n").append("\n").toString();
                    String shPath = (new StringBuilder()).append(BuildPath).append("/java.sh").toString();
                    sh_path = shPath;
                    man_path = MFPath;
                    FileStream createFile = new FileStream();
                    createFile.write(shPath, java);
                    if(os.equals("Linux"))
                        Console_Path = (new StringBuilder()).append("sudo sh ").append(shPath).toString();

                    else if(os.equals("MacOS"))
                        Console_Path = (new StringBuilder()).append("sudo sh ").append(shPath).toString();
                    Debugger_Path = (new StringBuilder()).append("jdb ").append(BuildPath).append(jarName).toString();
                    txtDebugger.setText("");
                    txtCpponsole.setText("");
                    txtDebugger.setText((new StringBuilder()).append("debug:\nZum debuggen bitte die Konsole öffnen und folgendes eingeben : ").append(Debugger_Path).append("\n").append("start with -> run").toString());

                    System.out.println("compile:");

                    String java_out = (new StringBuilder()).append("Zum Ausführen bitte die Konsole öffnen und folgendes eingeben : \nsudo sh ").append(shPath).append("\n").toString();
                    System.out.println(java_out);
                    String userHome = System.getProperty("user.home");
                    String buildPath = (new StringBuilder()).append(userHome).append(File.separator).append("LuziensProjects").append(File.separator).toString();
                    String LuzienPath = "";
                    if(os.equals("MacOS"))
                        LuzienPath = (new StringBuilder()).append(System.getProperty("user.dir")).append("/MacOS/").toString();
                    else
                    if(os.equals("Linux"))
                        LuzienPath = (new StringBuilder()).append(System.getProperty("user.home")).append("/LuziensProjects/Luzien/Linux/").toString();
                    else
                        LuzienPath = (new StringBuilder()).append(System.getProperty("user.dir")).append("/Windows/").toString();
                    String out = (new StringBuilder()).append(LuzienPath).append("luzien.o ").append(JPath2App).append(" javaSH.sh").toString();
                    Runtime r = Runtime.getRuntime();
                    Process p = r.exec(out);
                    p.waitFor();
                    BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    for(String line = ""; (line = b.readLine()) != null;)
                        System.out.println(line);
//--------------------------------------------------------------------------
           
            } else if (option.equals("GAS")) {
                
//                    String compiler_path = "";
//                    if(os.equals("MacOS")){
//                        compiler_path = System.getProperty("user.home") + "/LuziensProjects/Luzien/MacOS/GAS/GAS ";
//                    }
//                    else if(os.equals("Linux")){
//                        compiler_path = "GAS ";
//                    }
                    
                    String AppO = name.replace(".S", ".o");
                    String AppNothing = AppO.replaceAll(".o", " ");
                    String asmSH = "#!/bin/sh \necho Wechsel Verzeichnis nach :" + Path2App + "\n" +
                                 "cd " + Path2App +  "\n" +
                                 //"echo transmitted \"" + asmFlags + "\" to GAS\n" + 
                                 "gcc -c " + name + "\n" + 
                                 "ld " + AppO + "\n" +
                                 runASM + "\n" + "\n";
                    String shPath = (new StringBuilder()).append(Path2Build).append("/asm.sh").toString();
                    sh_path = shPath;
                    if(os.equals("MacOS"))
                    {
                        Console_Path = (new StringBuilder()).append(Path2Build).append("/asm.sh").toString();
                        System.out.println("compile:");
                    } else
                    if(os.equals("Linux"))
                    {
                        Console_Path = (new StringBuilder()).append("sudo sh ").append(shPath).toString();
                        String output_text = (new StringBuilder()).append("Zum Ausführen bitte die Konsole öffnen und folgendes eingeben : \n").append(Console_Path).append("\n").toString();
                        System.out.println("Komiliere:");
                        System.out.println(output_text);
                    }
                    FileStream createFile = new FileStream();
                    createFile.write(shPath, asmSH);
                    String userHome = System.getProperty("user.home");
                    String buildPath = (new StringBuilder()).append(userHome).append(File.separator).append("LuziensProjects").append(File.separator).toString();
                    String LuzienPath = "";
                    if(os.equals("MacOS"))
                        LuzienPath = (new StringBuilder()).append(System.getProperty("user.dir")).append("/MacOS/").toString();
                    else
                    if(os.equals("Linux"))
                        LuzienPath = (new StringBuilder()).append(System.getProperty("user.home")).append("/LuziensProjects/Luzien/Linux/").toString();
                    else
                        LuzienPath = (new StringBuilder()).append(System.getProperty("user.dir")).append("/Windows/").toString();
                    String out = (new StringBuilder()).append(LuzienPath).append("luzien.o ").append(Path2Build).append(" asm.sh").toString();
                    Runtime r = Runtime.getRuntime();
                    Process p = r.exec(out);
                    p.waitFor();
                    BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    for(String line = ""; (line = b.readLine()) != null;)
                        System.out.println(line);
//--------------------------------------------------------------------------
             } else if (option.equals("c")) {
                    String AppO = name.replace(".c", ".o");
                    String cSH = (new StringBuilder()).append("#!/bin/sh \necho Wechsel zum Verzeichnis ").append(Path2App).append("\n").append("cd ").append(Path2App).append("\n").append("echo Übergebe \"").append(gccFlags).append("\" an gcc\n").append("gcc").append(gccFlags).append(" ").append(Path2Build).append(CProject).append(" ").append(CMain).append(" ").append(gccFiles).append("\n").append(runC).append("\n").append("\n").toString();
                    String shPath = (new StringBuilder()).append(Path2Build).append("/c.sh").toString();
                    sh_path = shPath;
                    if(os.equals("MacOS"))
                    {
                        Console_Path = (new StringBuilder()).append(Path2Build).append(CProject).toString();
                        System.out.println("Kompiliere:");
                    } else
                    if(os.equals("Linux"))
                    {
                        Console_Path = (new StringBuilder()).append("sudo sh ").append(shPath).toString();
                        String output_text = (new StringBuilder()).append("Zum Ausführen bitte die Konsole öffnen und folgendes eingeben : \n").append(Console_Path).append("\n").toString();
                        System.out.println("Kompiliere:");
                        System.out.println(output_text);
                    }
                    FileStream createFile = new FileStream();
                    createFile.write(shPath, cSH);
                    String userHome = System.getProperty("user.home");
                    String buildPath = (new StringBuilder()).append(userHome).append(File.separator).append("LuziensProjects").append(File.separator).toString();
                    String LuzienPath = "";
                    if(os.equals("MacOS"))
                        LuzienPath = (new StringBuilder()).append(System.getProperty("user.dir")).append("/MacOS/").toString();
                    else
                    if(os.equals("Linux"))
                        LuzienPath = (new StringBuilder()).append(System.getProperty("user.home")).append("/LuziensProjects/Luzien/Linux/").toString();
                    else
                        LuzienPath = (new StringBuilder()).append(System.getProperty("user.dir")).append("/Windows/").toString();
                    String out = (new StringBuilder()).append(LuzienPath).append("luzien.o ").append(Path2Build).append(" c.sh").toString();
                    Runtime r = Runtime.getRuntime();
                    Process p = r.exec(out);
                    p.waitFor();
                    BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    for(String line = ""; (line = b.readLine()) != null;)
                        System.out.println(line);
 //--------------------------------------------------------------------------
              } else if (option.equals("cpp")) {

                    String cppSH = (new StringBuilder()).append("#!/bin/sh \necho Wechsel zum Verzeichnis ").append(Path2App).append("\n").append("cd ").append(Path2App).append("\n").append("echo Übergebe  \"").append(gppFlags).append("\" an g++\n").append("g++").append(gppFlags).append(" ").append(Path2Build).append(CppProject).append(" ").append(CppMain).append(" ").append(cppFiles).append("\n").append(runCPP).append("\n").append("\n").toString();
                    String shPath = (new StringBuilder()).append(Path2Build).append("cpp.sh").toString();
                    sh_path = shPath;
                    if(os.equals("MacOS"))
                    {
                        Console_Path = (new StringBuilder()).append(Path2Build).append(CppProject).toString();
                        System.out.println("Kompiliere:");
                    } else
                    if(os.equals("Linux"))
                    {
                        Console_Path = (new StringBuilder()).append("sudo sh ").append(shPath).toString();
                        String output_text = (new StringBuilder()).append("Zum Ausführen bitte die Konsole öffnen und folgendes eingeben : \n").append(Console_Path).append("\n").toString();
                        System.out.println("compile:");
                        System.out.println(output_text);
                    }
                    FileStream createFile = new FileStream();
                    createFile.write(shPath, cppSH);
                    String userHome = System.getProperty("user.home");
                    String buildPath = (new StringBuilder()).append(userHome).append(File.separator).append("LuziensProjects").append(File.separator).toString();
                    String LuzienPath = "";
                    if(os.equals("MacOS"))
                        LuzienPath = (new StringBuilder()).append(System.getProperty("user.dir")).append("/MacOS/").toString();
                    else
                    if(os.equals("Linux"))
                        LuzienPath = (new StringBuilder()).append(System.getProperty("user.home")).append("/LuziensProjects/Luzien/Linux/").toString();
                    else
                        LuzienPath = (new StringBuilder()).append(System.getProperty("user.dir")).append("/Windows/").toString();
                    String out = (new StringBuilder()).append(LuzienPath).append("luzien.o ").append(Path2Build).append(" cpp.sh").toString();
                    Runtime r = Runtime.getRuntime();
                    Process p = r.exec(out);
                    p.waitFor();
                    BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    for(String line = ""; (line = b.readLine()) != null;)
                        System.out.println(line);
                }
//--------------------------------------------------------------------------
                else if(option.equals("xampp")){
                    String out = "";
                    if(name.equals("start")){
                       if(os.equals("MacOS"))
                            out = "/Applications/xampp/xamppfiles/xampp start";
                       else if(os.equals("Linux"))
                            out = "/opt/lampp/lampp start";
                    }
                    else if(name.equals("stop")){
                       if(os.equals("MacOS"))
                            out = "/Applications/xampp/xamppfiles/xampp stop";
                       else if(os.equals("Linux"))
                            out = "/opt/lampp/lampp stop";
                    }
                    else if(name.equals("restart")){
                      if(os.equals("MacOS"))
                            out = "/Applications/xampp/xamppfiles/xampp restart";
                       else if(os.equals("Linux"))
                            out = "/opt/lampp/lampp restart";
                    }

                        Runtime r = Runtime.getRuntime();
                        Process p = r.exec(out);
                        p.waitFor();
                        BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
                        for(String line = ""; (line = b.readLine()) != null;)
                            System.out.println(line);
                }
//--------------------------------------------------------------------------
                else if(option.equals("timeline")){
                    String out = "";
                    if(name.equals("-run")){
                        String input = JOptionPane.showInputDialog(null, "Please enter the project name : ", "Luzien's Editor v0.4",JOptionPane.OK_OPTION);
                        String LuzienPath = System.getProperty("user.home") + "/LuziensProjects/Luzien/timeline/timeline.jar";
                        out = "java -jar " + LuzienPath + " -run " + input;
                    }
                    else if(name.equals("-create")){
                        String ProName = JOptionPane.showInputDialog(null, "Please enter the project name : ", "Luzien's Editor v0.4",JOptionPane.OK_OPTION);
                        String ProVersion = JOptionPane.showInputDialog(null, "Please enter the project version : ", "Luzien's Editor v0.4",JOptionPane.OK_OPTION);
                        String ProDescription = JOptionPane.showInputDialog(null, "Please enter a short description : ", "Luzien's Editor v0.4",JOptionPane.OK_OPTION);
                        String ProPath = JOptionPane.showInputDialog(null, "Please enter the project path : ", "Luzien's Editor v0.4",JOptionPane.OK_OPTION);
                        String ProDeveloper = JOptionPane.showInputDialog(null, "Please enter your name : ", "Luzien's Editor v0.4",JOptionPane.OK_OPTION);
                        
                        
                        String LuzienPath = System.getProperty("user.home") + "/LuziensProjects/Luzien/timeline/timeline.jar";
                        out = "java -jar " + LuzienPath + " -create " + ProName + " " + ProVersion + " " + ProDescription + " "
                             + ProPath + " " + ProDeveloper;
                    }
                    else if(name.equals("-delete")){
                      String input = JOptionPane.showInputDialog(null, "Please enter the project name : ", "Luzien's Editor v0.4",JOptionPane.OK_OPTION);
                        String LuzienPath = System.getProperty("user.home") + "/LuziensProjects/Luzien/timeline/timeline.jar";
                        out = "java -jar " + LuzienPath + " -delete " + input;
                    }
                    else if(name.equals("-show")){
                      String input = JOptionPane.showInputDialog(null, "Please enter the project name : ", "Luzien's Editor v0.4",JOptionPane.OK_OPTION);
                        String LuzienPath = System.getProperty("user.home") + "/LuziensProjects/Luzien/timeline/timeline.jar";
                        out = "java -jar " + LuzienPath + " -show " + input;
                         JOptionPane.showMessageDialog(null, out);
                    }
                    else if(name.equals("-list")){
                      //String input = JOptionPane.showInputDialog(null, "Please enter the project name : ", "Luzien's Editor v0.4",JOptionPane.OK_OPTION);
                        String LuzienPath = System.getProperty("user.home") + "/LuziensProjects/Luzien/timeline/timeline.jar";
                        out = "java -jar " + LuzienPath + " -list ";
                    }
                        txtCpponsole.setText("");
                        Runtime r = Runtime.getRuntime();
                        Process p = r.exec(out);
                        p.waitFor();
                        BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
                        for(String line = ""; (line = b.readLine()) != null;)
                            System.out.println(line);
                }
     
    }

      
    static void updateTextArea(final String text) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                txtCpponsole.append(text);
            }
        });
    }

    
    static void redirectSystemStreams() {
        OutputStream out = new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                updateTextArea(String.valueOf((char) b));
            }

            @Override
            public void write(byte[] b, int off, int len) throws IOException {

                updateTextArea(new String(b, off, len));
            }

            @Override
            public void write(byte[] b) throws IOException {

                write(b, 0, b.length);


            }
        };

        System.setOut(new PrintStream(out, true));
        System.setErr(new PrintStream(out, true));
      
        
    }
    // COMPILER END --------------------------------------------------------------------------------

    /*
     * In der Klasse Editor, wird das komplette Editor Fenter aufgebaut.
     * Enthalten sind die Toolbars für Java/C/C++/HTML,
     * die TextArea ( JSysntaxPane ) ,
     * Die rechte ToolBar mit Speichern,Speichern unter.... , ...
     * und die untere ToolBar mit der Syntax Wahl u.s.w.
     */
    

       
    static class HTMLEditor extends JPanel{
        String text, path;
        RSyntaxTextArea txtHTML = new RSyntaxTextArea();
        
        /* App Path */
        final JLabel sbLabel = new JLabel();
        final RTextScrollPane scrollHTML = new RTextScrollPane(txtHTML);
        static JCheckBox regexCB;
        static JCheckBox matchCaseCB;
        static JTextField searchField;
        static JTextField replaceField;
        HTMLEditor(String text, String path){
            text = text;
            path = path;
            
            setLayout(new BorderLayout());
            
            JToolBar tbMenu_generelly = new JToolBar( null, JToolBar.VERTICAL);
            JToolBar tbMenu_left = new JToolBar(null, JToolBar.VERTICAL);
            JToolBar tbMenu1 = new JToolBar();
            JToolBar tbMenu2 = new JToolBar();
            JToolBar tbMenu3 = new JToolBar();
            
            JPanel pnlMenu = new JPanel();
            pnlMenu.setLayout(new BorderLayout());
            pnlMenu.add(tbMenu1,BorderLayout.NORTH);
            pnlMenu.add(tbMenu2,BorderLayout.CENTER);
            pnlMenu.add(tbMenu3,BorderLayout.SOUTH);
            
            add(pnlMenu,BorderLayout.NORTH);
            
            
            
            // ------ PHP --------------

            /* Editor ToolBar -> php -> Run Project */
            Icon PHPRunIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/xampp.png"));
            JButton bPHPRun = new JButton(PHPRunIcon);
            bPHPRun.setBorderPainted(false);
            bPHPRun.setOpaque(false);
            bPHPRun.setToolTipText("Das Projekt in XAMPP öffnen");
            bPHPRun.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    int i = textTab.getSelectedIndex();
                    String file_name = textTab.getTitleAt(i);
                    
                    String temp = sbLabel.getText();
                    String temp_path = temp.replaceAll("/src/"+file_name, "");
                    String temp1_path = temp_path.replaceAll(System.getProperty("user.home")+"/LuziensProjects/","");
                    
                    String project_name = temp1_path;
                    String path = temp_path + "/src/";
                    
                    try {
                        CopyToXampp files2xampp = new CopyToXampp(path,project_name);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            
            // ------ HTML --------------

            /* Editor ToolBar -> HTML -> table */
            Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bHTMLtabelle = new JButton(tabelleIcon);
            bHTMLtabelle.setBorderPainted(false);
            bHTMLtabelle.setOpaque(false);
            bHTMLtabelle.setToolTipText("Tabelle");
            bHTMLtabelle.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<table border=\"1\">\n\t<tr>\n\t\t<td></td>\n\t</tr>\n</table>";
                    txtHTML.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> HTML -> table -> add row */
            Icon tabelleRowIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/addRow.png"));
            JButton bHTMLrowtabelle = new JButton(tabelleRowIcon);
            bHTMLrowtabelle.setBorderPainted(false);
            bHTMLrowtabelle.setOpaque(false);
            bHTMLrowtabelle.setToolTipText("Reihe hinzufügen");
            bHTMLrowtabelle.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<tr>\n"
                            + "     <td> </td>\n"
                            + "</tr>\n";
                    txtHTML.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> HTML -> picture */
            Icon picIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLpicture.png"));
            JButton bHTMLpic = new JButton(picIcon);
            bHTMLpic.setBorderPainted(false);
            bHTMLpic.setOpaque(false);
            bHTMLpic.setToolTipText("Bild");
            bHTMLpic.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<img src=\".jpg\" alt=\"Text\" width=\"304\" height=\"228\">";
                    txtHTML.replaceSelection(text);
                }
            });


            /* Editor ToolBar -> HTML -> link */
            Icon aIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLlink.png"));
            JButton bHTMLa = new JButton(aIcon);
            bHTMLa.setBorderPainted(false);
            bHTMLa.setOpaque(false);
            bHTMLa.setToolTipText("Link");
            bHTMLa.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<a href=\"http://link.com/\" target=\"_blank\">TEXT</a>";
                    txtHTML.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> HTML -> Comment */
            //Icon commentIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLcomment.png"));
            JButton bHTMLcomment = new JButton(/*commentIcon*/"<!--");
            bHTMLcomment.setBorderPainted(false);
            bHTMLcomment.setOpaque(false);
            bHTMLcomment.setToolTipText("Kommentar");
            bHTMLcomment.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<!-- -->";
                    txtHTML.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> HTML -> bold */
            Icon bIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/text_bold.png"));
            JButton bHTMLb = new JButton(bIcon);
            bHTMLb.setBorderPainted(false);
            bHTMLb.setOpaque(false);
            bHTMLb.setToolTipText("Text Fett");
            bHTMLb.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<b></b>";
                    txtHTML.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> HTML -> bold */
            Icon iIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/text_italic.png"));
            JButton bHTMLi = new JButton(iIcon);
            bHTMLi.setBorderPainted(false);
            bHTMLi.setOpaque(false);
            bHTMLi.setToolTipText("Text Italic");
            bHTMLi.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<i></i>";
                    txtHTML.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> HTML -> underline */
            Icon uIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/text_underline.png"));
            JButton bHTMLu = new JButton(uIcon);
            bHTMLu.setBorderPainted(false);
            bHTMLu.setOpaque(false);
            bHTMLu.setToolTipText("Text Unterstrichen");
            bHTMLu.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<u></u>";
                    txtHTML.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> HTML -> h1 */
            Icon h1Icon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/text_heading_1.png"));
            JButton bHTMLh1 = new JButton(h1Icon);
            bHTMLh1.setBorderPainted(false);
            bHTMLh1.setOpaque(false);
            bHTMLh1.setToolTipText("H1");
            bHTMLh1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<h1></h1>";
                    txtHTML.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> HTML -> h2 */
            Icon h2Icon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/text_heading_2.png"));
            JButton bHTMLh2 = new JButton(h2Icon);
            bHTMLh2.setBorderPainted(false);
            bHTMLh2.setOpaque(false);
            bHTMLh2.setToolTipText("H2");
            bHTMLh2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<h2></h2>";
                    txtHTML.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> HTML -> h3 */
            Icon h3Icon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/text_heading_3.png"));
            JButton bHTMLh3 = new JButton(h3Icon);
            bHTMLh3.setBorderPainted(false);
            bHTMLh3.setOpaque(false);
            bHTMLh3.setToolTipText("H3");
            bHTMLh3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<h3></h3>";
                    txtHTML.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> HTML -> h4 */
            Icon h4Icon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/text_heading_4.png"));
            JButton bHTMLh4 = new JButton(h4Icon);
            bHTMLh4.setBorderPainted(false);
            bHTMLh4.setOpaque(false);
            bHTMLh4.setToolTipText("H4");
            bHTMLh4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<h4></h4>";
                    txtHTML.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> HTML -> h5 */
            Icon h5Icon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/text_heading_5.png"));
            JButton bHTMLh5 = new JButton(h5Icon);
            bHTMLh5.setBorderPainted(false);
            bHTMLh5.setOpaque(false);
            bHTMLh5.setToolTipText("H5");
            bHTMLh5.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<h5></h5>";
                    txtHTML.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> HTML -> h6 */
            Icon h6Icon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/text_heading_6.png"));
            JButton bHTMLh6 = new JButton(h6Icon);
            bHTMLh6.setBorderPainted(false);
            bHTMLh6.setOpaque(false);
            bHTMLh6.setToolTipText("H6");
            bHTMLh6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<h6></h6>";
                    txtHTML.replaceSelection(text);
                }
            });

            Icon CheckIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Check.png"));
            Icon RadioIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Radio.png"));
            Icon ComboIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Combobox.png"));
            Icon FieldsetIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Fieldset.png"));
            Icon ButtonIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Button.png"));
            Icon PanelIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/jframe.png"));
            Icon TextIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/text.png"));
            Icon MehrComboIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/list.png"));
            Icon BildButtonIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/bildbutton.png"));
            Icon SubmitButtonIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Button.png"));
            Icon ResetButtonIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Button.png"));
            Icon LabelIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/label.png"));

            JButton CheckButton = new JButton(CheckIcon);
            JButton RadioButton = new JButton(RadioIcon);
            JButton ComboButton = new JButton(ComboIcon);
            JButton FieldsetButton = new JButton(FieldsetIcon);
            JButton ButtonButton = new JButton(ButtonIcon);
            JButton PanelButton = new JButton(PanelIcon);
            JButton TextButton = new JButton(TextIcon);
            JButton MehrTextButton = new JButton(TextIcon);
            JButton PasswortTextButton = new JButton(TextIcon);
            JButton MehrComboButton = new JButton(TextIcon);
            JButton BildButtonButton = new JButton(ButtonIcon);
            JButton SubmitButtonButton = new JButton(ButtonIcon);
            JButton ResetButtonButton = new JButton(ButtonIcon);
            JButton LabelButton = new JButton(LabelIcon);

            PanelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<form name=\"NAME\" method=\"post\" action=\"ACTION.php\">\n"
                            + "</form>";

                    txtHTML.replaceSelection(text);
                }
            });

            TextButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<input type=\"text\" name=\"name\" size=\"40\" maxlength=\"50\" />";

                    txtHTML.replaceSelection(text);
                }
            });

            MehrTextButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<textarea name=\"comment\" wrap=\"PHYSICAL\" cols=\"30\" rows=\"5\">\n"
                            + "</textarea>";

                    txtHTML.replaceSelection(text);
                }
            });

            PasswortTextButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<input type=\"password\" name=\"Password\" />";

                    txtHTML.replaceSelection(text);
                }
            });

            CheckButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<input type=\"checkbox\" name=\"ToDo\" value=\"Velo\" />";

                    txtHTML.replaceSelection(text);
                }
            });

            RadioButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<input type=\"radio\" name=\"Dir\" value=\"Mister\" />";

                    txtHTML.replaceSelection(text);
                }
            });

            ComboButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<select name=\"Country\" size=\"1\">\n"
                            + "<option>Germany</option>\n"
                            + "<option selected>America</option>\n"
                            + "</select>";

                    txtHTML.replaceSelection(text);
                }
            });

            MehrComboButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<select name=\"Country\" size=\"2\" multiple>\n"
                            + "<option>Germany</option>\n"
                            + "<option>America</option>\n"
                            + "</select>";

                    txtHTML.replaceSelection(text);
                }
            });

            BildButtonButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<input type=\"image\" border=\"0\" name=\"mybutton\" src=\"formbilder/button_r2_c2.gif\" width=\"122\" height=\"42\" />";

                    txtHTML.replaceSelection(text);
                }
            });

            SubmitButtonButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<input type=\"submit\" name=\"send\" value=\"Send\" />";

                    txtHTML.replaceSelection(text);
                }
            });

            ButtonButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<input type=\"button\" name=\"Button\" value=\"Push me!\" onClick=\"hello()\" />";

                    txtHTML.replaceSelection(text);
                }
            });

            ResetButtonButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<input type=\"reset\" value=\"delete\" />";

                    txtHTML.replaceSelection(text);
                }
            });

            LabelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<label>Name:</label>";

                    txtHTML.replaceSelection(text);
                }
            });

            FieldsetButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev1) {
                    String text = "<fieldset><legend>Adress</legend></fieldset>";

                    txtHTML.replaceSelection(text);
                }
            });

            CheckButton.setBorderPainted(false);
            RadioButton.setBorderPainted(false);
            ComboButton.setBorderPainted(false);
            FieldsetButton.setBorderPainted(false);
            ButtonButton.setBorderPainted(false);
            PanelButton.setBorderPainted(false);
            TextButton.setBorderPainted(false);
            MehrTextButton.setBorderPainted(false);
            PasswortTextButton.setBorderPainted(false);
            MehrComboButton.setBorderPainted(false);
            BildButtonButton.setBorderPainted(false);
            SubmitButtonButton.setBorderPainted(false);
            ResetButtonButton.setBorderPainted(false);
            LabelButton.setBorderPainted(false);

            CheckButton.setOpaque(false);
            RadioButton.setOpaque(false);
            ComboButton.setOpaque(false);
            FieldsetButton.setOpaque(false);
            ButtonButton.setOpaque(false);
            PanelButton.setOpaque(false);
            TextButton.setOpaque(false);
            MehrTextButton.setOpaque(false);
            PasswortTextButton.setOpaque(false);
            MehrComboButton.setOpaque(false);
            BildButtonButton.setOpaque(false);
            SubmitButtonButton.setOpaque(false);
            ResetButtonButton.setOpaque(false);
            LabelButton.setOpaque(false);

            CheckButton.setToolTipText("Checkbox");
            RadioButton.setToolTipText("Radiobutton");
            ComboButton.setToolTipText("Combobox");
            FieldsetButton.setToolTipText("Groupbox");
            ButtonButton.setToolTipText("Button");
            PanelButton.setToolTipText("Formular");
            TextButton.setToolTipText("Textfeld");
            MehrTextButton.setToolTipText("Multi Row Textfeld");
            PasswortTextButton.setToolTipText("Masket Textfeld");
            MehrComboButton.setToolTipText("Multi Selected");
            BildButtonButton.setToolTipText("Bildbutton");
            SubmitButtonButton.setToolTipText("Submit Button");
            ResetButtonButton.setToolTipText("Reset Button");
            LabelButton.setToolTipText("Label");

            // Browser view
            Icon browserIcon = new ImageIcon(HTMLEditor.class.getResource("Toolbar/web.png"));
            JButton browser = new JButton(browserIcon);
            browser.setBorderPainted(false);
            browser.setOpaque(false);
            browser.setToolTipText("Browser ansicht");
            browser.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    // have you save the page ? 
                    if (sbLabel.getText() == "New Document") {
                        JOptionPane.showMessageDialog(null, "Bitte die Datei speichern", "", JOptionPane.OK_OPTION);
                    } else {

                        String path = "file://" + sbLabel.getText();

                        int i = textTab.getSelectedIndex();
                        String name = textTab.getTitleAt(i);

                        path = path.replace("name", "index.html");

                        Desktop desktop = Desktop.getDesktop();
                        try {
                            desktop.browse(new URL(path).toURI());
                        } catch (IOException ex) {
                            Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);



                        } catch (URISyntaxException ex) {
                            Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
            
            /* Editor ToolBar -> Save */
            Icon ToolbarSaveIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/save.png"));
            JButton btnSave = new JButton(ToolbarSaveIcon);
            btnSave.setBorderPainted(false);
            btnSave.setOpaque(false);
            btnSave.setToolTipText("Speichern");
            tbMenu_generelly.add(btnSave);
            btnSave.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e02) {

                    int i = textTab.getSelectedIndex();

                    String text = txtHTML.getText();
                    String pfad = sbLabel.getText();

                    try {
                        FileWriter fw = new FileWriter(pfad);
                        fw.write(text);
                        JOptionPane.showMessageDialog(null, "Datei gespeichert!", "Luzien's Editor v0.4 ", JOptionPane.INFORMATION_MESSAGE);
                        fw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

            /* Editor Toolbar ->  Save as */
            Icon ToolbarSaveasIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/saveas.png"));
            JButton btnSaveAs = new JButton(ToolbarSaveasIcon);
            btnSaveAs.setBorderPainted(false);
            btnSaveAs.setOpaque(false);
            btnSaveAs.setToolTipText("Speichern unter");
            tbMenu_generelly.add(btnSaveAs);
            btnSaveAs.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev03) {
                    int i = textTab.getSelectedIndex();


                    String text = txtHTML.getText();
                    String pfad = sbLabel.getText();
                    String path = sbLabel.getText();
                    JFileChooser fc = new JFileChooser();
                    int state = fc.showSaveDialog(null);
                    if (state == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();


                        textTab.setTitleAt(i, file.getName());
                        sbLabel.setText(file.getAbsolutePath());
                        try {
                            FileWriter fw = new FileWriter(file.getAbsolutePath());
                            fw.write(text);
                            fw.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            tbMenu_generelly.addSeparator();

            /*Editor ToolBar Print */
            Icon ToolbarPrintIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/print.png"));
            JButton btnPrint = new JButton(ToolbarPrintIcon);
            btnPrint.setBorderPainted(false);
            btnPrint.setOpaque(false);
            btnPrint.setToolTipText("Datei drucken");
            tbMenu_generelly.add(btnPrint);
            btnPrint.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e05) {
                    String printname = "";

                    try {
                        PrintRequestAttributeSet attrs =
                                new HashPrintRequestAttributeSet();
                        attrs.add(OrientationRequested.PORTRAIT);
                        attrs.add(MediaSizeName.ISO_A4);
                        attrs.add(new JobName(printname, null));
                        txtHTML.print(new MessageFormat(printname),
                                new MessageFormat("Page {0}"),
                                true, null, attrs, false);
                    } catch (PrinterException e) {
                        System.err.println("Print error.");
                        System.err.println(e.getMessage());
                    }
                }
            });

            tbMenu_generelly.addSeparator();

            /* Editor ToolBar -> Cut */
            Icon ToolbarCutIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/cut.png"));
            Action cutAction = new DefaultEditorKit.CutAction();
            cutAction.putValue(Action.NAME, "");
            JButton btnCut = new JButton(cutAction);
            btnCut.setBorderPainted(false);
            btnCut.setOpaque(false);
            btnCut.setText("");
            btnCut.setIcon(ToolbarCutIcon);
            btnCut.setToolTipText("Ausschneiden");
            tbMenu_generelly.add(btnCut);


            /* Editor ToolBar -> Copy */
            Icon ToolbarCopyIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/copy.png"));
            Action copyAction = new DefaultEditorKit.CopyAction();
            copyAction.putValue(Action.NAME, "");
            JButton btnCopy = new JButton(copyAction);
            btnCopy.setBorderPainted(false);
            btnCopy.setOpaque(false);
            btnCopy.setText("");
            btnCopy.setIcon(ToolbarCopyIcon);
            btnCopy.setIcon(ToolbarCopyIcon);
            btnCopy.setToolTipText("Kopieren");
            tbMenu_generelly.add(btnCopy);

            /* Editor Toolbar -> Paste */
            Icon ToolbarPasteIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/paste.png"));
            Action pasteAction = new DefaultEditorKit.PasteAction();
            pasteAction.putValue(Action.NAME, "");
            JButton btnPaste = new JButton(pasteAction);
            btnPaste.setIcon(ToolbarPasteIcon);
            btnPaste.setBorderPainted(false);
            btnPaste.setOpaque(false);
            btnPaste.setToolTipText("Einfügen");
            tbMenu_generelly.add(btnPaste);

            Icon redoIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/redo.png"));
            JButton bredo = new JButton(redoIcon);
            bredo.setBorderPainted(false);
            bredo.setOpaque(false);
            bredo.setToolTipText("");
            bredo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    txtHTML.redoLastAction();
                }
            });
            
            Icon undoIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/undo.png"));
            JButton bundo = new JButton(undoIcon);
            bundo.setBorderPainted(false);
            bundo.setOpaque(false);
            bundo.setToolTipText("");
            bundo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    txtHTML.undoLastAction();
                }
            });
            
            Icon findIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/find.png"));
            JButton bfind = new JButton(findIcon);
            bfind.setBorderPainted(false);
            bfind.setOpaque(false);
            bfind.setToolTipText("");
            bfind.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                  
                  
                  JFrame frmSuche = new JFrame();
                  frmSuche.setTitle("Suchen und ersetzen");
                  frmSuche.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
                  frmSuche.setLayout(new BorderLayout());
                  
                      JToolBar toolBar = new JToolBar();
                      searchField = new JTextField(30);
                      toolBar.add(searchField);
                      final JButton nextButton = new JButton("Weiter");
                      nextButton.setActionCommand("FindNext");
                      nextButton.addActionListener(this);
                      toolBar.add(nextButton);
                      nextButton.addActionListener(new ActionListener() {
                         @Override public void actionPerformed(ActionEvent e) {
                            // "FindNext" => search forward, "FindPrev" => search backward
                              String command = e.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtHTML, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                         }
                      });
                      JButton prevButton = new JButton("Zurück");
                      prevButton.setActionCommand("FindPrev");
                      prevButton.addActionListener(new ActionListener(){
                          @Override public void actionPerformed(ActionEvent ev){
                              String command = ev.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtHTML, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                          }
                      });
                      toolBar.add(prevButton);
                      regexCB = new JCheckBox("Regulär");
                      //toolBar.add(regexCB);
                      matchCaseCB = new JCheckBox("Genau");
                      //toolBar.add(matchCaseCB);
                      frmSuche.add(toolBar, BorderLayout.NORTH);

                      
                    frmSuche.pack();
                    frmSuche.setLocationRelativeTo(null);
                    frmSuche.setVisible(true);
                }
            });
            
            Icon stampIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/stamp.png"));
            JButton bstamp = new JButton(stampIcon);
            bstamp.setBorderPainted(false);
            bstamp.setOpaque(false);
            bstamp.setToolTipText("");
            bstamp.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                  String Projekt = sbLabel.getText();
                  String Pfad = System.getProperty("user.home") + "/LuziensProjects/";
                  int temp = textTab.getSelectedIndex();
                  String Dateiname = textTab.getTitleAt(temp);
                 
                  Projekt = Projekt.replace(Pfad, "");
                  Projekt = Projekt.replace(Dateiname,"");
                  Projekt = Projekt.replace("/src/", "");
                    try {
                        LuziensEditor.timeline_src.RunProject stampproject = new LuziensEditor.timeline_src.RunProject(Projekt);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            
            
            JButton b1 = new JButton("1");
            b1.setBorderPainted(false);
            b1.setOpaque(false);
            b1.setToolTipText("Dokument 1");
            b1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    Text1 = txtHTML.getText();
                    Pfad1 = sbLabel.getText();
                }
            });
            
            JButton b2 = new JButton("2");
            b2.setBorderPainted(false);
            b2.setOpaque(false);
            b2.setToolTipText("Dokument 2");
            b2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    Text2 = txtHTML.getText();
                    Pfad2 = sbLabel.getText();
                }
            });

            /* Editor ToolBar -> HTML -> h4 */
            Icon linksIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/left.png"));
            JButton bHTMLlinks = new JButton(linksIcon);
            bHTMLlinks.setBorderPainted(false);
            bHTMLlinks.setOpaque(false);
            bHTMLlinks.setToolTipText("Links ausrichten");
            bHTMLlinks.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<div style=\"text-align: left\"></div>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon cenIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/center.png"));
            JButton bHTMLmitte = new JButton(cenIcon);
            bHTMLmitte.setBorderPainted(false);
            bHTMLmitte.setOpaque(false);
            bHTMLmitte.setToolTipText("Zentrieren");
            bHTMLmitte.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<div style=\"text-align: center\"></div>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon rechtsIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/right.png"));
            JButton bHTMLrechts = new JButton(rechtsIcon);
            bHTMLrechts.setBorderPainted(false);
            bHTMLrechts.setOpaque(false);
            bHTMLrechts.setToolTipText("Rechts ausrichten");
            bHTMLrechts.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<div style=\"text-align: right\"></div>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon enterIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/enter.png"));
            JButton bHTMLbr = new JButton(enterIcon);
            bHTMLbr.setBorderPainted(false);
            bHTMLbr.setOpaque(false);
            bHTMLbr.setToolTipText("Zeilenumbruch");
            bHTMLbr.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<br>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            //Icon rechtsIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/right.png"));
            JButton bHTMLleer = new JButton("&nbsp;");
            bHTMLleer.setBorderPainted(false);
            bHTMLleer.setOpaque(false);
            bHTMLleer.setToolTipText("Festes Leerzeichen");
            bHTMLleer.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "&nbsp;";
                    txtHTML.replaceSelection(text);
                }
            });
            
            
            
// ICONS FEHLEN -------------------------------------------------------------------------
            Icon p = new ImageIcon(LuziensEditor.class.getResource("Toolbar/p.png"));
            JButton bHTMLp = new JButton(p);
            bHTMLp.setBorderPainted(false);
            bHTMLp.setOpaque(false);
            bHTMLp.setToolTipText("Absatz");
            bHTMLp.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<p></p>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            //Icon rechtsIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/right.png"));
            JButton bHTMLp1 = new JButton("A+1");
            bHTMLp1.setBorderPainted(false);
            bHTMLp1.setOpaque(false);
            bHTMLp1.setToolTipText("Schriftgröße +1");
            bHTMLp1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<span style=\"font-size: smaller;\"></span>";
                    txtHTML.replaceSelection(text);
                }
            });
            
             //Icon rechtsIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/right.png"));
            JButton bHTMLm1 = new JButton("A-1");
            bHTMLm1.setBorderPainted(false);
            bHTMLm1.setOpaque(false);
            bHTMLm1.setToolTipText("Schriftgröße -1");
            bHTMLm1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<span style=\"font-size: larger;\"></span>";
                    txtHTML.replaceSelection(text);
                }
            });
            
             //Icon rechtsIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/right.png"));
            JButton bHTMLt = new JButton("Av1");
            bHTMLt.setBorderPainted(false);
            bHTMLt.setOpaque(false);
            bHTMLt.setToolTipText("Tiefstellen");
            bHTMLt.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<sub></sub>";
                    txtHTML.replaceSelection(text);
                }
            });
            
             //Icon rechtsIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/right.png"));
            JButton bHTMLh = new JButton("A^1");
            bHTMLh.setBorderPainted(false);
            bHTMLh.setOpaque(false);
            bHTMLh.setToolTipText("Hochstellen");
            bHTMLh.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<sup></sup>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon hr = new ImageIcon(LuziensEditor.class.getResource("Toolbar/hr.png"));
            JButton bHTMLlinie = new JButton(hr);
            bHTMLlinie.setBorderPainted(false);
            bHTMLlinie.setOpaque(false);
            bHTMLlinie.setToolTipText("Horizontale Trennlinie");
            bHTMLlinie.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<hr>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon abbr = new ImageIcon(LuziensEditor.class.getResource("Toolbar/abbr.png"));
            JButton bHTMLabkuerzung = new JButton(abbr);
            bHTMLabkuerzung.setBorderPainted(false);
            bHTMLabkuerzung.setOpaque(false);
            bHTMLabkuerzung.setToolTipText("Abkürzung");
            bHTMLabkuerzung.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<abbr></abbr>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon acronym = new ImageIcon(LuziensEditor.class.getResource("Toolbar/acronym.png"));
            JButton bHTMLai = new JButton(acronym);
            bHTMLai.setBorderPainted(false);
            bHTMLai.setOpaque(false);
            bHTMLai.setToolTipText("Akronym");
            bHTMLai.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<acronym></acronym>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon cite = new ImageIcon(LuziensEditor.class.getResource("Toolbar/cite.png"));
            JButton bHTMLzi = new JButton(cite);
            bHTMLzi.setBorderPainted(false);
            bHTMLzi.setOpaque(false);
            bHTMLzi.setToolTipText("Zitat");
            bHTMLzi.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<cite></cite>";
                    txtHTML.replaceSelection(text);
                }
            });
            
             //Icon rechtsIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/right.png"));
            JButton bHTMLcode = new JButton("< >");
            bHTMLcode.setBorderPainted(false);
            bHTMLcode.setOpaque(false);
            bHTMLcode.setToolTipText("Code");
            bHTMLcode.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<code></code>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon dfn = new ImageIcon(LuziensEditor.class.getResource("Toolbar/dfn.png"));
            JButton bHTMLdef = new JButton(dfn);
            bHTMLdef.setBorderPainted(false);
            bHTMLdef.setOpaque(false);
            bHTMLdef.setToolTipText("Definition");
            bHTMLdef.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<dfn></dfn>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon del = new ImageIcon(LuziensEditor.class.getResource("Toolbar/del.png"));
            JButton bHTMLdel = new JButton(del);
            bHTMLdel.setBorderPainted(false);
            bHTMLdel.setOpaque(false);
            bHTMLdel.setToolTipText("Löschen");
            bHTMLdel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<del></del>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon ins = new ImageIcon(LuziensEditor.class.getResource("Toolbar/ins.png"));
            JButton bHTMLins = new JButton(ins);
            bHTMLins.setBorderPainted(false);
            bHTMLins.setOpaque(false);
            bHTMLins.setToolTipText("Einfügen");
            bHTMLins.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<ins></ins>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon kbd = new ImageIcon(LuziensEditor.class.getResource("Toolbar/kbd.png"));
            JButton bHTMLkey = new JButton(kbd);
            bHTMLkey.setBorderPainted(false);
            bHTMLkey.setOpaque(false);
            bHTMLkey.setToolTipText("Tastertur");
            bHTMLkey.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<kbd></kbd>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon samp = new ImageIcon(LuziensEditor.class.getResource("Toolbar/samp.png"));
            JButton bHTMLbsp = new JButton(samp);
            bHTMLbsp.setBorderPainted(false);
            bHTMLbsp.setOpaque(false);
            bHTMLbsp.setToolTipText("Beispiel");
            bHTMLbsp.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<samp></samp>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon var = new ImageIcon(LuziensEditor.class.getResource("Toolbar/var.png"));
            JButton bHTMLvar = new JButton(var);
            bHTMLvar.setBorderPainted(false);
            bHTMLvar.setOpaque(false);
            bHTMLvar.setToolTipText("Variable");
            bHTMLvar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<var></var>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon tr = new ImageIcon(LuziensEditor.class.getResource("Toolbar/tr.png"));
            JButton bHTMLtr = new JButton(tr);
            bHTMLtr.setBorderPainted(false);
            bHTMLtr.setOpaque(false);
            bHTMLtr.setToolTipText("Tabellenzeile");
            bHTMLtr.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<tr></tr>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon th = new ImageIcon(LuziensEditor.class.getResource("Toolbar/th.png"));
            JButton bHTMLth = new JButton(th);
            bHTMLth.setBorderPainted(false);
            bHTMLth.setOpaque(false);
            bHTMLth.setToolTipText("Tabellenkopf");
            bHTMLth.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<th></th>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon td = new ImageIcon(LuziensEditor.class.getResource("Toolbar/td.png"));
            JButton bHTMLtd = new JButton(td);
            bHTMLtd.setBorderPainted(false);
            bHTMLtd.setOpaque(false);
            bHTMLtd.setToolTipText("Tabellenzelle");
            bHTMLtd.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<td></td>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon caption = new ImageIcon(LuziensEditor.class.getResource("Toolbar/caption.png"));
            JButton bHTMLcaption = new JButton(caption);
            bHTMLcaption.setBorderPainted(false);
            bHTMLcaption.setOpaque(false);
            bHTMLcaption.setToolTipText("Tabellenbeschriftung");
            bHTMLcaption.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<caption></caption>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon ul = new ImageIcon(LuziensEditor.class.getResource("Toolbar/ul.png"));
            JButton bHTMLul = new JButton(ul);
            bHTMLul.setBorderPainted(false);
            bHTMLul.setOpaque(false);
            bHTMLul.setToolTipText("Untergeordnete Liste");
            bHTMLul.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<ul></ul>";
                    txtHTML.replaceSelection(text);
                }
            });
            
           Icon ol = new ImageIcon(LuziensEditor.class.getResource("Toolbar/ol.png"));
            JButton bHTMLol = new JButton(ol);
            bHTMLol.setBorderPainted(false);
            bHTMLol.setOpaque(false);
            bHTMLol.setToolTipText("Geordnete Liste");
            bHTMLol.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<ol></ol>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            
            Icon li = new ImageIcon(LuziensEditor.class.getResource("Toolbar/li.png"));
            JButton bHTMLli = new JButton(li);
            bHTMLli.setBorderPainted(false);
            bHTMLli.setOpaque(false);
            bHTMLli.setToolTipText("Listenelement");
            bHTMLli.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<li></li>";
                    txtHTML.replaceSelection(text);
                }
            });
            
           Icon dl = new ImageIcon(LuziensEditor.class.getResource("Toolbar/dl.png"));
            JButton bHTMLdl = new JButton(dl);
            bHTMLdl.setBorderPainted(false);
            bHTMLdl.setOpaque(false);
            bHTMLdl.setToolTipText("Definitionsliste");
            bHTMLdl.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<dl>\n" +
                                "	<dt></dt>\n" +
                                "	<dd></dd>\n" +
                                "</dl>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon dt = new ImageIcon(LuziensEditor.class.getResource("Toolbar/dt.png"));
            JButton bHTMLdt = new JButton(dt);
            bHTMLdt.setBorderPainted(false);
            bHTMLdt.setOpaque(false);
            bHTMLdt.setToolTipText("Definitionslistentitel");
            bHTMLdt.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<dt></dt>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon dd = new ImageIcon(LuziensEditor.class.getResource("Toolbar/dd.png"));
            JButton bHTMLdd = new JButton(dd);
            bHTMLdd.setBorderPainted(false);
            bHTMLdd.setOpaque(false);
            bHTMLdd.setToolTipText("Definition");
            bHTMLdd.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<dd></dd>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon mailto = new ImageIcon(LuziensEditor.class.getResource("Toolbar/mailto.png"));
            JButton bHTMLmail = new JButton(mailto);
            bHTMLmail.setBorderPainted(false);
            bHTMLmail.setOpaque(false);
            bHTMLmail.setToolTipText("EMail versenden");
            bHTMLmail.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<a href=\"mailto:EMPFÄNGER?cc=KOPIE&amp;bcc=BLINDKOPIE&amp;subject=BETREFF&amp;body=NACHRICHT\"></a>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon frame = new ImageIcon(LuziensEditor.class.getResource("Toolbar/frame.png"));
            JButton bHTMLfr = new JButton(frame);
            bHTMLfr.setBorderPainted(false);
            bHTMLfr.setOpaque(false);
            bHTMLfr.setToolTipText("Frame");
            bHTMLfr.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<frame>";
                    txtHTML.replaceSelection(text);
                }
            });
            
           Icon frameset = new ImageIcon(LuziensEditor.class.getResource("Toolbar/frameset.png"));
            JButton bHTMLfrset = new JButton(frameset);
            bHTMLfrset.setBorderPainted(false);
            bHTMLfrset.setOpaque(false);
            bHTMLfrset.setToolTipText("Frameset");
            bHTMLfrset.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<frameset></frameset>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon noframes = new ImageIcon(LuziensEditor.class.getResource("Toolbar/frame.png"));
            JButton bHTMLfrno = new JButton(noframes);
            bHTMLfrno.setBorderPainted(false);
            bHTMLfrno.setOpaque(false);
            bHTMLfrno.setToolTipText("Keine Frames");
            bHTMLfrno.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<noframes></noframes>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            Icon colors = new ImageIcon(LuziensEditor.class.getResource("Toolbar/colors.png"));
            JButton bHTMLfarben = new JButton(colors);
            bHTMLfarben.setBorderPainted(false);
            bHTMLfarben.setOpaque(false);
            bHTMLfarben.setToolTipText("HTML Farben");
            bHTMLfarben.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                   WebColor showColors = new WebColor(true);
                }
            });
            
            
            //Icon rechtsIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/right.png"));
            JButton bHTMLdiv = new JButton("DIV");
            bHTMLdiv.setBorderPainted(false);
            bHTMLdiv.setOpaque(false);
            bHTMLdiv.setToolTipText("CSS div");
            bHTMLdiv.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "<div align=\"\" class=\"\"></div>";
                    txtHTML.replaceSelection(text);
                }
            });
            
            
            //Icon rechtsIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/right.png"));
            JButton bHTMLCSS = new JButton("CSS");
            bHTMLCSS.setBorderPainted(false);
            bHTMLCSS.setOpaque(false);
            bHTMLCSS.setToolTipText("CSS...");
            bHTMLCSS.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    final JDialog dlgCSS = new JDialog();
                    dlgCSS.setTitle("CSS");
                    dlgCSS.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
                    dlgCSS.setLayout(new BorderLayout());
                    
                    JPanel pnlInfo = new JPanel();
                    pnlInfo.setLayout(new BorderLayout());
                    pnlInfo.setBackground(Color.WHITE);
                    JEditorPane txtInfo = new JEditorPane();
                    txtInfo.setContentType("text/html");
                    txtInfo.setEditable(false);
                    txtInfo.setBackground(Color.WHITE);
                    txtInfo.setText("<b>CSS</b><br>Hier kannst du alle Einstellungen für CSS vornehmen.<br><br>");
                    pnlInfo.add(txtInfo,BorderLayout.PAGE_START);
                    dlgCSS.add(pnlInfo,BorderLayout.NORTH);
                    
                    JPanel pnlMain = new JPanel();
                    pnlMain.setLayout(new GridBagLayout());
                    GridBagConstraints m = new GridBagConstraints();
                    
                    JLabel lblBereich = new JLabel("Bereich : ");
                    m.gridx = 0;
                    m.gridy = 0;
                    m.fill = 1;
                    m.insets = new Insets(10,10,0,0);
                    pnlMain.add(lblBereich,m);
                    
                    String[] BereichListe = {"var","th","table","map","kbd","i","span","q","optgroup","h5",
                                             "form","div","col","br","b","big","ul","tfoot","sup","small",
                                             "pre","ol","link","ins","html","h4","fieldset","dfn","code","body",
                                             "area","tt","acronym","tr","textarea","sub","select","param","object","li",
                                             "input","hr","h3","em","del","cite","blockquote","address","noframes","title",
                                             "td","style","script","p","noscript","legend","img","head","h2","dt",
                                             "dd","caption","bdo","abbr","frameset","thead","tbody","strong","samp","option",
                                             "meta","label","iframe","h6","h1","dl","colgroup","button","base","a",
                                             "frame"};
                    
                    final JComboBox cbBereich = new JComboBox(BereichListe);
                      cbBereich.addActionListener(new ActionListener(){
                               @Override public void actionPerformed(ActionEvent ev01){
                                   JComboBox comboBox = (JComboBox) ev01.getSource();
                                   Object selected = comboBox.getSelectedItem();
                                   cssBereich = String.valueOf(selected); 
                               }
                      });
                    m.gridx = 1;
                    m.gridy = 0;
                    m.fill = 1;
                    m.insets = new Insets(10,0,0,0);
                    pnlMain.add(cbBereich,m);
                    
                    JLabel lblEigenschaft = new JLabel("Eigenschaft : ");
                    m.gridx = 0;
                    m.gridy = 1;
                    m.fill = 1;
                    m.insets = new Insets(10,10,0,0);
                    pnlMain.add(lblEigenschaft,m);
                    
                    String[] EigenschaftListe = {"quotes","outline","counter-increment","border-collapse","clip","list-style-image","line-height","min-width","display","right",
                                                "hight","border-bottom","border-left-style","border-color","border-top-color","border-right-width","padding-bottom","margin-left","line-hight","text-transform",
                                                "background","background-image","font-size","font-family","outline-width","opacity","contant","label-layout","overflow","list-style-type",
                                                "max-height","z-index","clear","top","width","border-right","border-bottom-style","border-right","border-left-color","border-width",
                                                "border-top-width","padding-right","margin-bottom","text-shadow","text-decoration","background-position","background-color","font-weight","outline-style","cursor",
                                                "empty-cells","caption-side","vertical-align","list-style","min-height","unicode-bidi","left","border","position","border-top",
                                                "border-right-style","border-buttom-color","border-left-width","padding","padding-top","margin-right","text-indent","letter-spacing","background-attachmend","color",
                                                "font-variant","outline-color","counter-reset","border-spacing","visibility","list-style-position","white-space","max-width","direction","bottom",
                                                "flat","border-left","border-style","border-top-style","border-right-color","border-bottom-width","padding-left","margin","margin-top","text-align",
                                                "word-spacing","background-repeat","font","font-style"
                                                 };
                    final JComboBox cbEigenschaft = new JComboBox(EigenschaftListe);
                    cbEigenschaft.addActionListener(new ActionListener(){
                               @Override public void actionPerformed(ActionEvent ev01){
                                   JComboBox comboBox = (JComboBox) ev01.getSource();
                                   Object selectedx = comboBox.getSelectedItem();
                                   cssEigenschaft = String.valueOf(selectedx); 
                               }
                      });
                    m.gridx = 1;
                    m.gridy = 1;
                    m.fill = 1;
                    m.insets = new Insets(10,0,0,0);
                    pnlMain.add(cbEigenschaft,m);
                    
                    JLabel lblWert = new JLabel("Wert : ");
                    m.gridx = 0;
                    m.gridy = 2;
                    m.fill = 1;
                    m.insets = new Insets(10,10,0,0);
                    pnlMain.add(lblWert,m);
                    
                    final RSyntaxTextArea txtWert = new RSyntaxTextArea(1,15);
                    m.gridx = 1;
                    m.gridy = 2;
                    m.fill = 1;
                    m.insets = new Insets(10,0,0,0);
                    pnlMain.add(txtWert,m);
                    
                    final JTextArea txtAuflistung = new JTextArea(15,15);
                    JScrollPane scAuflistung = new JScrollPane(txtAuflistung);
                    m.gridx = 0;
                    m.gridy = 3;
                    m.fill = 2;
                    m.gridwidth = 2;
                    m.insets = new Insets(10,10,0,0);
                    pnlMain.add(scAuflistung,m);
                    
                    JPanel pnlADD = new JPanel();
                    pnlADD.setLayout(new GridLayout(2,0));
                    JButton btnADD = new JButton("+");
                    btnADD.addActionListener(new ActionListener(){
                        @Override public void actionPerformed(ActionEvent ev0111){
                            String Wert = txtWert.getText();
                         
                            txtAuflistung.append(cssEigenschaft + ": " + Wert + ";\n");
                        }
                    });
                    
                    Icon colorsX = new ImageIcon(LuziensEditor.class.getResource("Toolbar/colors.png"));
                    JButton btnFarben = new JButton(colorsX);
                    btnFarben.addActionListener(new ActionListener(){
                       @Override public void actionPerformed(ActionEvent ev){
                           WebColor showColors2 = new WebColor(true);
                       } 
                    });
                    pnlADD.add(btnFarben);
                    pnlADD.add(btnADD);
                    m.gridx = 2;
                    m.gridy = 3;
                    //m.fill = 2;
                    //m.gridwidth = 2;
                    m.insets = new Insets(10,10,0,0);
                    pnlMain.add(pnlADD,m);
                    
                    dlgCSS.add(pnlMain,BorderLayout.CENTER);
                    
                    
                    /* Buttons */
                    JPanel pnlButtons = new JPanel();
                    pnlButtons.setLayout(new FlowLayout());

                    JButton btnCopy = new JButton("Übernehmen");
                        btnCopy.addActionListener(new ActionListener(){
                            @Override public void actionPerformed(ActionEvent aeXXX){
                                String liste = txtAuflistung.getText();
                              
                                String text = cssBereich + " {\n" +
                                              liste +
                                              "}";
                                
                                txtHTML.replaceSelection(text);
                            }
                        });

                    JButton btnDispose = new JButton("Abbrechen");
                    btnDispose.addActionListener(new ActionListener(){
                       @Override public void actionPerformed(ActionEvent ev){
                           dlgCSS.dispose();
                       } 
                    });
                    
                    
        
                    pnlButtons.add(btnCopy);
                    pnlButtons.add(btnDispose);

                    dlgCSS.add(pnlButtons,BorderLayout.SOUTH);
                    
                    dlgCSS.pack();
                    dlgCSS.setVisible(true);
                }
            });
            
            //tbMenu_generelly.addSeparator();
            tbMenu_left.add(bundo);
            tbMenu_left.add(bredo);
            //tbMenu_left.add(bfind);
            tbMenu_left.add(bstamp);
            tbMenu_left.add(b1);
            tbMenu_left.add(b2);
            add(tbMenu_generelly,BorderLayout.EAST);
            add(tbMenu_left,BorderLayout.WEST);
            
            tbMenu1.add(bHTMLb);
            tbMenu1.add(bHTMLi);
            tbMenu1.add(bHTMLu);
            tbMenu1.addSeparator();
            tbMenu1.add(bHTMLlinks);
            tbMenu1.add(bHTMLmitte);
            tbMenu1.add(bHTMLrechts);
            tbMenu1.addSeparator();
            tbMenu1.add(bHTMLp);
            tbMenu1.addSeparator();
            tbMenu1.add(bHTMLp1);
            tbMenu1.add(bHTMLm1);
            tbMenu1.add(bHTMLt);
            tbMenu1.add(bHTMLh);
            tbMenu1.addSeparator();
            tbMenu1.add(bHTMLh1);
            tbMenu1.add(bHTMLh2);
            tbMenu1.add(bHTMLh3);
            tbMenu1.add(bHTMLh4);
            tbMenu1.add(bHTMLh5);
            tbMenu1.add(bHTMLh6);
            tbMenu1.addSeparator();
            tbMenu1.add(bHTMLcomment);
            tbMenu1.addSeparator();
            tbMenu1.add(browser);
            tbMenu1.add(bPHPRun);
            //tbMenu_html.add(HTMLEditor);


            tbMenu2.add(bHTMLtabelle);
            tbMenu2.add(bHTMLrowtabelle);
            tbMenu2.add(bHTMLtr);
            tbMenu2.add(bHTMLth);
            tbMenu2.add(bHTMLtd);
            tbMenu2.add(bHTMLcaption);
            tbMenu2.addSeparator();
            tbMenu2.add(bHTMLpic);
            tbMenu2.add(bHTMLa);
            tbMenu2.add(bHTMLmail);
            tbMenu2.addSeparator();
            tbMenu2.add(bHTMLleer);
            tbMenu2.add(bHTMLbr);
            tbMenu2.add(bHTMLlinie);
            tbMenu2.addSeparator();
            tbMenu2.add(bHTMLabkuerzung);
            tbMenu2.add(bHTMLai);
            tbMenu2.add(bHTMLzi);
            tbMenu2.add(bHTMLcode);
            tbMenu2.add(bHTMLdef);
            tbMenu2.add(bHTMLdel);
            tbMenu2.add(bHTMLins);
            tbMenu2.add(bHTMLkey);
            tbMenu2.add(bHTMLbsp);
            tbMenu2.add(bHTMLvar);
            //tbMenu2.addSeparator();


            tbMenu3.add(PanelButton);
            tbMenu3.add(FieldsetButton);
            tbMenu3.add(CheckButton);
            tbMenu3.add(RadioButton);
            tbMenu3.add(ComboButton);
            tbMenu3.add(MehrComboButton);
            tbMenu3.add(TextButton);
            tbMenu3.add(MehrTextButton);
            tbMenu3.add(PasswortTextButton);
            tbMenu3.add(ButtonButton);
            tbMenu3.add(BildButtonButton);
            tbMenu3.add(SubmitButtonButton);
            tbMenu3.add(ResetButtonButton);
            tbMenu3.add(LabelButton);
            tbMenu3.addSeparator();
            tbMenu3.add(bHTMLul);
            tbMenu3.add(bHTMLol);
            tbMenu3.add(bHTMLli);
            tbMenu3.add(bHTMLdl);
            tbMenu3.add(bHTMLdt);
            tbMenu3.add(bHTMLdd);
            tbMenu3.addSeparator();
            tbMenu3.add(bHTMLfr);
            tbMenu3.add(bHTMLfrset);
            tbMenu3.add(bHTMLfrno);
            tbMenu3.addSeparator();
            tbMenu3.add(bHTMLCSS);
            tbMenu3.add(bHTMLdiv);
            tbMenu3.addSeparator();
            tbMenu3.add(bHTMLfarben);
            
            pnlMenu.add(tbMenu1,BorderLayout.NORTH);
            pnlMenu.add(tbMenu2,BorderLayout.CENTER);
            pnlMenu.add(tbMenu3,BorderLayout.SOUTH);
            add(pnlMenu,BorderLayout.NORTH);
            
            JToolBar tbbottom = new JToolBar();
            JPanel frmSuche = new JPanel();
                  frmSuche.setLayout(new BorderLayout());
                  
                      //JToolBar toolBar = new JToolBar();
                      searchField = new JTextField(15);
                      //toolBar.add(searchField);
                      frmSuche.add(searchField,BorderLayout.WEST);
                      final JButton nextButton = new JButton("Weiter");
                      nextButton.setActionCommand("FindNext");
                      //nextButton.addActionListener(this);
                      //toolBar.add(nextButton);
                      frmSuche.add(nextButton,BorderLayout.CENTER);
                      nextButton.addActionListener(new ActionListener() {
                         @Override public void actionPerformed(ActionEvent e) {
                            // "FindNext" => search forward, "FindPrev" => search backward
                              String command = e.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtHTML, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                         }
                      });
                      JButton prevButton = new JButton("Zurück");
                      prevButton.setActionCommand("FindPrev");
                      prevButton.addActionListener(new ActionListener(){
                          @Override public void actionPerformed(ActionEvent ev){
                              String command = ev.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtHTML, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                          }
                      });
                      //toolBar.add(prevButton);
                      frmSuche.add(prevButton,BorderLayout.EAST);
                      regexCB = new JCheckBox("Regulär");
                      //toolBar.add(regexCB);
                      matchCaseCB = new JCheckBox("Genau");
                      //toolBar.add(matchCaseCB);
                      //frmSuche.add(toolBar, BorderLayout.NORTH);
            
            add(tbbottom,BorderLayout.SOUTH);
            tbbottom.add(frmSuche);
            
                      
            txtHTML.addKeyListener(new KeyAdapter(){
              public void keyPressed(KeyEvent e)
              {
                int key = e.getKeyCode();
//                if (key == KeyEvent.VK_F4) // F4 -> Java
//                {
//                   helpWindow showhelp = new helpWindow();
//                   showhelp.Java(true);
//                }
//                else if (key == KeyEvent.VK_F5) // F5 -> C/C++
//                {
//                   helpWindow showhelp = new helpWindow();
//                   showhelp.C(true);
//                }
                if (key == KeyEvent.VK_F6) // F6 -> HTML
                {
                   helpWindow showhelp = new helpWindow();
                   showhelp.HTML(true);
                }
//                else if (key == KeyEvent.VK_ENTER)
//                {
//                    
//                    String array[] = txtJava.getText().split("\n");
//                    for(int i = 0;i<array.length; i++){ 
//                            //array[i] = array[i].toLowerCase();
//                            array[i] = array[i].replace(";", "");
//                            
//                            if((array[i].contains("byte")) || (array[i].contains("short")) 
//                                    || array[i].contains("int") || (array[i].contains("long")) 
//                                    || (array[i].contains("char")) || (array[i].contains("boolean")) 
//                                    || (array[i].contains("float")) || (array[i].contains("double")) 
//                                    || (array[i].contains("String")) || (array[i].contains("string"))){
//                                String ausgabe = array[i];
//                                ausgabe = ausgabe.replace("{", " ");
//                                ausgabe = ausgabe.replace("}", " ");
//                                
//                                String text = txtVar.getText();
//                                if(!text.contains(ausgabe))
//                                    txtVar.append("Zeile " + i + " : " + ausgabe + "\n");
//                                    
//                            }
//                            
//                            
//                            else if((array[i].contains("class"))){
//                                String ausgabe = array[i];
//                                ausgabe = ausgabe.replace("{", " ");
//                                ausgabe = ausgabe.replace("}", " ");
//                                
//                                String text1 = txtKla.getText();
//                                if(!text1.contains(ausgabe))
//                                    txtKla.append("Zeile " + i + " : " + ausgabe + "\n");
//                            }
//                            
//                            else if((array[i].contains("void")) || (array[i].contains("byte")) || (array[i].contains("short")) 
//                                    || array[i].contains("int") || (array[i].contains("long")) 
//                                    || (array[i].contains("char")) || (array[i].contains("boolean")) 
//                                    || (array[i].contains("float")) || (array[i].contains("double")) 
//                                    || (array[i].contains("String")) || (array[i].contains("string")) 
//                                    /*&& (array[i].endsWith("{"))*/){
//                                String ausgabe = array[i];
//                                ausgabe = ausgabe.replace("{", " ");
//                                ausgabe = ausgabe.replace("}", " ");
//                                
//                                String text2 = txtVar.getText();
//                                if(!text2.contains(ausgabe))
//                                    txtVar.append("Zeile " + i + " : " + ausgabe + "\n");
//                            }
//                        }
//                    
//                }
//                else if (key == KeyEvent.VK_F7) // F7 -> HTML 5
//                {
//                   helpWindow showhelp = new helpWindow();
//                   showhelp.HTML5(true);
//                }
                
              }
            }); 
            
            txtHTML.setBackground(Color.WHITE); // JEditorPanel
            //frmEditor = new RSyntaxTextArea();
            txtHTML.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_HTML);
            
            txtHTML.setText(text);
            sbLabel.setText(path);
            
            add(scrollHTML);
        }
    } 
    static class JavaEditor extends JPanel{
        String text, path;
        RSyntaxTextArea txtJava = new RSyntaxTextArea();
        
        /* App Path */
        final JLabel sbLabel = new JLabel();
        final RTextScrollPane scrollJava = new RTextScrollPane(txtJava);
        static JCheckBox regexCB;
        static JCheckBox matchCaseCB;
        static JTextField searchField;
        static JTextField replaceField;
        JavaEditor(String text, String path) throws IOException{
            text = text;
            path = path;
            
            setLayout(new BorderLayout());
            
            JToolBar tbMenu_generelly = new JToolBar( null, JToolBar.VERTICAL);
            JToolBar tbMenu_left = new JToolBar(null, JToolBar.VERTICAL);
            JToolBar tbMenu1 = new JToolBar();
            JToolBar tbMenu2 = new JToolBar();
            JToolBar tbMenu3 = new JToolBar();
            JToolBar tbMenu_bottom = new JToolBar();
            
            // ------ JAVA --------------

            /* Editor ToolBar -> Java -> Run Project */
            Icon JavaRunIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bJavaRun = new JButton(JavaRunIcon);
            bJavaRun.setBorderPainted(false);
            bJavaRun.setOpaque(false);
            bJavaRun.setToolTipText("Projekt erstellen und starten");
            bJavaRun.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    ConsoleTab.setSelectedIndex(0);
                    String path = sbLabel.getText() + "/";
                    int i = textTab.getSelectedIndex();
                    String name = textTab.getTitleAt(i);
                    String option = "java"; // Compile java app -> Con.syntax

                    /* send arguments to the compiler 
                     * -------------------------------
                     * -> ConArgs = args f
                     * -> path = App Path
                     * -> name = App Name
                     * -> option = Script language
                     */
                    try {
                        try {
                            run(ConArgs, path, name, option);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });


            /* Editor ToolBar -> java -> jframe */
            Icon jframeIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/jframe.png"));
            JButton bJavaJFrame = new JButton(jframeIcon);
            bJavaJFrame.setBorderPainted(false);
            bJavaJFrame.setOpaque(false);
            bJavaJFrame.setToolTipText("JFrame");
            bJavaJFrame.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "javax.swing.JFrame FRAMENAME = new javax.swing.JFrame(); \n"
                            + "FRAMENAME.setTitle(\"FRAMETITLE\");  \n"
                            + "FRAMENAME.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE); \n"
                            + "\n"
                            + "/* insert your frame code here */\n"
                            + "\n"
                            + "FRAMENAME.setSize(100, 100); // or FRAMENAME.pack();\n"
                            + "FRAMENAME.setVisible(true);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jdialog */
            Icon jdialogIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/jdialog.png"));
            JButton bJavaJDialog = new JButton(jdialogIcon);
            bJavaJDialog.setBorderPainted(false);
            bJavaJDialog.setOpaque(false);
            bJavaJDialog.setToolTipText("JDialog");
            bJavaJDialog.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JDialog DIALOGNAME = new javax.swing.JDialog(); // set the dialog\n"
                            + "DIALOGNAME.setTitle(\"DIALOGTITLE\"); // title name \n"
                            + "\n"
                            + "/* insert your frame code here */\n"
                            + "\n"
                            + "int x = 100; // height\n"
                            + "int y = 50; // weight\n"
                            + "DIALOGNAME.setSize(x, y); // or FRAMENAME.pack();\n"
                            + "DIALOGNAME.setVisible(true); // show the new dialog";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jpanel */
            Icon jpanelIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/jpanel.png"));
            JButton bJavaJPanel = new JButton(jpanelIcon);
            bJavaJPanel.setBorderPainted(false);
            bJavaJPanel.setOpaque(false);
            bJavaJPanel.setToolTipText("JPanel");
            bJavaJPanel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JPanel PANELNAME = new javax.swing.JPanel(); // set the panel\n"
                            + "\n"
                            + "/* insert your frame code here */\n"
                            + "\n"
                            + "FRAMENAME.add(PANELNAME); // add the new panel";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jcolorchooser */
            Icon jcolorIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/jcolor.png"));
            JButton bJavaJColor = new JButton(jcolorIcon);
            bJavaJColor.setBorderPainted(false);
            bJavaJColor.setOpaque(false);
            bJavaJColor.setToolTipText("JColorChooser");
            bJavaJColor.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "java.awt.Color ausgewaehlteFarbe = javax.swing.JColorChooser.showDialog(null, \n"
                            + "   \"JColorChooser\", null);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jfilechooser */
            Icon jfileIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/jfile.png"));
            JButton bJavaJFileOpen = new JButton(jfileIcon);
            bJavaJFileOpen.setBorderPainted(false);
            bJavaJFileOpen.setOpaque(false);
            bJavaJFileOpen.setToolTipText("JFileChooser Öffnen Dialog");
            bJavaJFileOpen.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JFileChooser chooser = new javax.swing.JFileChooser();\n"
                            + "chooser.showOpenDialog(null);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jsave */
            Icon jsaveIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/jsave.png"));
            JButton bJavaJFileSave = new JButton(jsaveIcon);
            bJavaJFileSave.setBorderPainted(false);
            bJavaJFileSave.setOpaque(false);
            bJavaJFileSave.setToolTipText("JFileChooser Speichern Dialog");
            bJavaJFileSave.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JFileChooser chooser = new javax.swing.JFileChooser();\n"
                            + "chooser.showSaveDialog(null);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> joptionpane */
            Icon dlginputIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/dlg.png"));
            JButton bJavaJFOptionInput = new JButton(dlginputIcon);
            bJavaJFOptionInput.setBorderPainted(false);
            bJavaJFOptionInput.setOpaque(false);
            bJavaJFOptionInput.setToolTipText("JOptionPane Eingabe Dialog");
            bJavaJFOptionInput.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JOptionPane.showInputDialog(\"This is a Input Dialog\");";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> joptionpane */
            Icon dlgConfirmIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/dlg.png"));
            JButton bJavaJFOptionConfirm = new JButton(dlgConfirmIcon);
            bJavaJFOptionConfirm.setBorderPainted(false);
            bJavaJFOptionConfirm.setOpaque(false);
            bJavaJFOptionConfirm.setToolTipText("JOptionPane Confirm Dialog");
            bJavaJFOptionConfirm.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JOptionPane.showConfirmDialog(null, \"This is a Confirm Dialog\");";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> joptionpane */
            Icon dlgMessageIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/dlg.png"));
            JButton bJavaJFOptionMessage = new JButton(dlgMessageIcon);
            bJavaJFOptionMessage.setBorderPainted(false);
            bJavaJFOptionMessage.setOpaque(false);
            bJavaJFOptionMessage.setToolTipText("JOptionPane Nachrichten Dialog");
            bJavaJFOptionMessage.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JOptionPane.showMessageDialog(null, \"This is a Message Dialog\");";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> joptionpane */
            Icon dlgOptIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/dlg.png"));
            JButton bJavaJFOptionOption = new JButton(dlgOptIcon);
            bJavaJFOptionOption.setBorderPainted(false);
            bJavaJFOptionOption.setOpaque(false);
            bJavaJFOptionOption.setToolTipText("JOptionPane Optionen Dialog");
            bJavaJFOptionOption.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JOptionPane.showOptionDialog(null, \"This is a Optionsdialog\",\"Optionsdialog\",\n"
                            + "        javax.swing.JOptionPane.YES_NO_CANCEL_OPTION,\n"
                            + "        javax.swing.JOptionPane.WARNING_MESSAGE, null, \n"
                            + "        new String[]{\"A\", \"B\", \"C\"}, \"B\");";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jmenubar */
            Icon menubarIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/menubar.png"));
            JButton bJavaJMenuBar = new JButton(menubarIcon);
            bJavaJMenuBar.setBorderPainted(false);
            bJavaJMenuBar.setOpaque(false);
            bJavaJMenuBar.setToolTipText("JMenuBar");
            bJavaJMenuBar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JMenuBar JMENUBARNAME = new javax.swing.JMenuBar();\n"
                            + "FRAMENAME.setJMenuBar(JMENUBARNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jmenu */
            Icon menuIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/jmenu.png"));
            JButton bJavaJMenu = new JButton(menuIcon);
            bJavaJMenu.setBorderPainted(false);
            bJavaJMenu.setOpaque(false);
            bJavaJMenu.setToolTipText("JMenu");
            bJavaJMenu.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JMenu JMENUNAME = new javax.swing.JMenu(\"JMENUNAME\");\n"
                            + "JMENUBARNAME.add(JMENUNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jmenuitem */
            Icon menuitemIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/menuitem.png"));
            JButton bJavaJMenuItem = new JButton(menuitemIcon);
            bJavaJMenuItem.setBorderPainted(false);
            bJavaJMenuItem.setOpaque(false);
            bJavaJMenuItem.setToolTipText("JMenuItem");
            bJavaJMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JMenuItem JMENUITEMNAME = new javax.swing.JMenuItem(\"JMENUITEMNAME\");\n"
                            + "JMENUNAME.add(JMENUITEMNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jcheckboxmenuitem */
            Icon mnuCheckIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Check.png"));
            JButton bJavaJCeckBoxMenuItem = new JButton(mnuCheckIcon);
            bJavaJCeckBoxMenuItem.setBorderPainted(false);
            bJavaJCeckBoxMenuItem.setOpaque(false);
            bJavaJCeckBoxMenuItem.setToolTipText("CheckBoxMenuItem");
            bJavaJCeckBoxMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JCheckBoxMenuItem JCHECKBOXMENUITEMNAME = new javax.swing.JCheckBoxMenuItem(\"JCHECKBOXMENUITEMNAME\");\n"
                            + "JMENUNAME.add(JCHECKBOXMENUITEMNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jradiobuttonmenuitem */
            Icon mnuRadioIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Radio.png"));
            JButton bJavaJRadioButtonMenuItem = new JButton(mnuRadioIcon);
            bJavaJRadioButtonMenuItem.setBorderPainted(false);
            bJavaJRadioButtonMenuItem.setOpaque(false);
            bJavaJRadioButtonMenuItem.setToolTipText("RadioButtonMenuItem");
            bJavaJRadioButtonMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JRadioButtonMenuItem JRADIOBUTTONMENUITEMNAME = new javax.swing.JRadioButtonMenuItem(\"JRADIOBUTTONMENUITEMNAME\");\n"
                            + "JMENUNAME.add(JRADIOBUTTONMENUITEMNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jpopupmenu */
            Icon jpopupIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/jpopup.png"));
            JButton bJavaJPopUpMenu = new JButton(jpopupIcon);
            bJavaJPopUpMenu.setBorderPainted(false);
            bJavaJPopUpMenu.setOpaque(false);
            bJavaJPopUpMenu.setToolTipText("JPopUpMenu");
            bJavaJPopUpMenu.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JPopupMenu JPOPUPMENUNAME = new javax.swing.JPopupMenu();\n"
                            + "\n"
                            + "JPOPUPMENUNAME.setLocation(100, 100);\n"
                            + "JPOPUPMENUNAME.add(POPUPITEM);\n"
                            + "JPOPUPMENUNAME.setVisible(true);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jseperator */
            Icon jsepIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/sep.png"));
            JButton bJavaJSeperator = new JButton(jsepIcon);
            bJavaJSeperator.setBorderPainted(false);
            bJavaJSeperator.setOpaque(false);
            bJavaJSeperator.setToolTipText("JSeperator");
            bJavaJSeperator.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JSeparator SEPERATORNAME = new javax.swing.JSeparator();         \n"
                            + "JMENUNAME.add(SEPERATORNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jtabbedpane*/
            Icon tabIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/tabpane.png"));
            JButton bJavaJTabbedPane = new JButton(tabIcon);
            bJavaJTabbedPane.setBorderPainted(false);
            bJavaJTabbedPane.setOpaque(false);
            bJavaJTabbedPane.setToolTipText("JTabbedPane");
            bJavaJTabbedPane.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JTabbedPane JTABBEDPANENAME = new javax.swing.JTabbedPane();\n"
                            + "\n"
                            + "FRAMENAME.add(JTABBEDPANENAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jsplipane*/
            Icon splitIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/splitpane.png"));
            JButton bJavaJSplitPane = new JButton(splitIcon);
            bJavaJSplitPane.setBorderPainted(false);
            bJavaJSplitPane.setOpaque(false);
            bJavaJSplitPane.setToolTipText("JSplitPane");
            bJavaJSplitPane.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JSplitPane JSPLITPANENAME = new javax.swing.JSplitPane();\n"
                            + "\n"
                            + "JSPLITPANENAME.setLeftComponent(COMPONENTNAME);\n"
                            + "JSPLITPANENAME.setRightComponent(COMPONENTNAME);\n"
                            + "\n"
                            + "/*\n"
                            + " * JSPLITPANENAME.setTopComponent(COMPONENTNAME);\n"
                            + " * JSPLITPANENAME.setBottomComponent(COMPONENTNAME);\n"
                            + " */\n"
                            + "\n"
                            + "FRAMENAME.add(JSPLITPANENAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jscrollpane*/
            Icon scrollIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/scrollpane.png"));
            JButton bJavaJScrollPane = new JButton(scrollIcon);
            bJavaJScrollPane.setBorderPainted(false);
            bJavaJScrollPane.setOpaque(false);
            bJavaJScrollPane.setToolTipText("JScrollPane");
            bJavaJScrollPane.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JScrollPane JSCROLLPANENAME = new javax.swing.JScrollPane();\n"
                            + "\n"
                            + "JSCROLLPANENAME.add(COMPONENTNAME);\n"
                            + "        \n"
                            + "FRAMENAME.add(JSCROLLPANENAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jtoolbar*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bJavaJToolBar = new JButton(menubarIcon);
            bJavaJToolBar.setBorderPainted(false);
            bJavaJToolBar.setOpaque(false);
            bJavaJToolBar.setToolTipText("JToolBar");
            bJavaJToolBar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JToolBar JTOOLBARNAME = new javax.swing.JToolBar();\n"
                            + "        \n"
                            + "JTOOLBARNAME.add(COMPONENTNAME);\n"
                            + "        \n"
                            + "FRAMENAME.add(JTOOLBARNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jdesktoppane*/
            Icon jdesktopIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/jdesktop.png"));
            JButton bJavaJDesktopPane = new JButton(jdesktopIcon);
            bJavaJDesktopPane.setBorderPainted(false);
            bJavaJDesktopPane.setOpaque(false);
            bJavaJDesktopPane.setToolTipText("JDesktopPane");
            bJavaJDesktopPane.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JDesktopPane JDESKTOPPANENAME = new javax.swing.JDesktopPane();\n"
                            + "FRAMENAME.add(JDESKTOPPANENAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jinternalframe*/
            Icon jinternalIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/jinternal.png"));
            JButton bJavaJInternalFrame = new JButton(jinternalIcon);
            bJavaJInternalFrame.setBorderPainted(false);
            bJavaJInternalFrame.setOpaque(false);
            bJavaJInternalFrame.setToolTipText("JInternalFrame");
            bJavaJInternalFrame.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JInternalFrame JINTERNALFRAMENAME = new javax.swing.JInternalFrame(\"JINTERNALFRAMENAME\",/*boolean resizable*/ true, \n"
                            + "            /*boolean closable*/true, /*boolean maximizable*/true, /*boolean iconifiable*/true);\n"
                            + "        \n"
                            + "JDESKPANENAME.add(JINTERNALFRAMENAME);\n"
                            + "        \n"
                            + "JINTERNALFRAME.setSize(100,100);\n"
                            + "JINTERNALFRAME.setLocation(0,0);\n"
                            + "JINTERNALFRAME.show();\n"
                            + "        \n"
                            + "FRAMENAME.add(JINTERNALFRAMENAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jlabel*/
            Icon labelIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/label.png"));
            JButton bJavaJLabel = new JButton(labelIcon);
            bJavaJLabel.setBorderPainted(false);
            bJavaJLabel.setOpaque(false);
            bJavaJLabel.setToolTipText("JLabel");
            bJavaJLabel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JLabel LABELNAME = new javax.swing.JLabel(\"LABELTEXT\");\n"
                            + "        \n"
                            + "FRAMENAME.add(LABELNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jbutton*/
            Icon JButtonIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Button.png"));
            JButton bJavaJButton = new JButton(JButtonIcon);
            bJavaJButton.setBorderPainted(false);
            bJavaJButton.setOpaque(false);
            bJavaJButton.setToolTipText("JButton");
            bJavaJButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JButton BUTTONNAME = new javax.swing.JButton(\"BUTTONLABEL\");\n"
                            + "BUTTONNAME.addActionListener(new java.awt.event.ActionListener(){\n"
                            + "   @Override public void actionPerformed(java.awt.event.ActionEvent ev01){\n"
                            + "\n"
                            + "   }\n"
                            + "});\n"
                            + "\n"
                            + "FRAMENAME.add(BUTTONNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jtogglebutton*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bJavaJToggleButton = new JButton(JButtonIcon);
            bJavaJToggleButton.setBorderPainted(false);
            bJavaJToggleButton.setOpaque(false);
            bJavaJToggleButton.setToolTipText("JToggleButton");
            bJavaJToggleButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JToggleButton TOGGLEBUTTONNAME = new javax.swing.JToggleButton(\"TOGGLEBUTTONLABEL\", /*boolean selected*/true);\n"
                            + "TOGGLEBUTTONNAME.addItemListener(new java.awt.event.ItemListener(){\n"
                            + "   public void itemStateChanged(java.awt.event.ItemEvent e) {\n"
                            + "       if(e.getStateChange() == ItemEvent.SELECTED)\n"
                            + "       {\n"
                            + "\n"
                            + "       }\n"
                            + "       else\n"
                            + "       {\n"
                            + "\n"
                            + "       }\n"
                            + "    }\n"
                            + "});\n"
                            + "FRAMENAME.add(TOGGLEBUTTONNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jcheckbox*/
            //Icon JCheckBoxIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Check.png"));
            JButton bJavaJCheckBox = new JButton(mnuCheckIcon);
            bJavaJCheckBox.setBorderPainted(false);
            bJavaJCheckBox.setOpaque(false);
            bJavaJCheckBox.setToolTipText("JCheckBox");
            bJavaJCheckBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JCheckBox CHECKBOXNAME = new javax.swing.JCheckBox(\"CHECKBOXLABEL\",/*boolean selected*/false);\n"
                            + "CHECKBOXNAME.addItemListener(new java.awt.event.ItemListener(){\n"
                            + "   public void itemStateChanged(java.awt.event.ItemEvent e) {\n"
                            + "       if(e.getStateChange() == java.awt.event.ItemEvent.SELECTED)\n"
                            + "       {\n"
                            + "\n"
                            + "       }\n"
                            + "       else\n"
                            + "       {\n"
                            + "\n"
                            + "       }\n"
                            + "    }\n"
                            + "});\n"
                            + "FRAMENAME.add(CHECKBOXNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jradiobutton*/
            //Icon JRadioIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Radio.png"));
            JButton bJavaJRadioButton = new JButton(mnuRadioIcon);
            bJavaJRadioButton.setBorderPainted(false);
            bJavaJRadioButton.setOpaque(false);
            bJavaJRadioButton.setToolTipText("JRadioButton");
            bJavaJRadioButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JRadioButton RADIOBUTTONNAME = new javax.swing.JRadioButton(\"RADIOBUTTONLABEL\",/*boolean selected*/false);\n"
                            + "RADIOBUTTONNAME.addItemListener(new java.awt.event.ItemListener(){\n"
                            + "   public void itemStateChanged(java.awt.event.ItemEvent e) {\n"
                            + "       if(e.getStateChange() == java.awt.event.ItemEvent.SELECTED)\n"
                            + "       {\n"
                            + "\n"
                            + "       }\n"
                            + "       else\n"
                            + "       {\n"
                            + "\n"
                            + "       }\n"
                            + "    }\n"
                            + "});\n"
                            + "FRAMENAME.add(RADIOBUTTONNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> buttongroup*/
            Icon ButtonGroupIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Fieldset.png"));
            JButton bJavaButtonGroup = new JButton(ButtonGroupIcon);
            bJavaButtonGroup.setBorderPainted(false);
            bJavaButtonGroup.setOpaque(false);
            bJavaButtonGroup.setToolTipText("ButtonGroup");
            bJavaButtonGroup.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.ButtonGroup BUTTONGROUPNAME = new javax.swing.ButtonGroup();\n"
                            + "BUTTONGROUPNAME.add(JCHECKBOXITEM);\n"
                            + "//BUTTONGROUPNAME.add(JRADIOBUTTONITEM);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jcombobox*/
            Icon JComboIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/Combobox.png"));
            JButton bJavaJComboBox = new JButton(JComboIcon);
            bJavaJComboBox.setBorderPainted(false);
            bJavaJComboBox.setOpaque(false);
            bJavaJComboBox.setToolTipText("JComboBox");
            bJavaJComboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "String[] COMBOBOXLIST = new String[]{\"1\",\"2\",\"3\"};\n"
                            + "javax.swing.JComboBox COMBOBOXNAME = new javax.swing.JComboBox(COMBOBOXLIST);\n"
                            + "COMBOBOXNAME.addActionListener(new java.awt.event.ActionListener(){\n"
                            + "   @Override public void actionPerformed(java.awt.event.ActionEvent ev01){\n"
                            + "       javax.swing.JComboBox comboBox = (javax.swing.JComboBox) event.getSource();\n"
                            + "       Object selected = comboBox.getSelectedItem();\n"
                            + "   }\n"
                            + "});\n"
                            + "FRAMENAME.add(COMBOBOXNAME);";
                    txtJava.replaceSelection(text);
                }
            });


            /* Editor ToolBar -> java -> jlist*/
            Icon JListIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/list.png"));
            JButton bJavaJList = new JButton(JListIcon);
            bJavaJList.setBorderPainted(false);
            bJavaJList.setOpaque(false);
            bJavaJList.setToolTipText("JList");
            bJavaJList.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "String[] LIST = new String[]{\"1\",\"2\",\"3\"};\n"
                            + "javax.swing.JList LISTNAME = new javax.swing.JList(LIST);\n"
                            + "LISTNAME.addListSelectionListener(new javax.swing.event.ListSelectionListener(){\n"
                            + "   public void valueChanged(javax.swing.event.ListSelectionEvent e) {\n"
                            + "       if (!e.getValueIsAdjusting()) {\n"
                            + "           \n"
                            + "       }\n"
                            + "   }\n"
                            + "});\n"
                            + "FRAMENAME.add(LISTNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> jtextfield*/
            Icon JTextIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/text.png"));
            JButton bJavaJTextField = new JButton(JTextIcon);
            bJavaJTextField.setBorderPainted(false);
            bJavaJTextField.setOpaque(false);
            bJavaJTextField.setToolTipText("JTextField");
            bJavaJTextField.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JTextField TEXTFIELDNAME = new javax.swing.JTextField(\"TEXTFIELDLABEL\");\n"
                            + "FRAMENAME.add(TEXTFIELDNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> JPasswordField*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bJavaJPasswordField = new JButton(JTextIcon);
            bJavaJPasswordField.setBorderPainted(false);
            bJavaJPasswordField.setOpaque(false);
            bJavaJPasswordField.setToolTipText("JPasswordField");
            bJavaJPasswordField.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JPasswordField PASSWORDFIELDNAME = new javax.swing.JPasswordField(\"PASSWORDFIELDLABEL\");\n"
                            + "FRAMENAME.add(PASSWORDFIELDNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> JFormattedTextField*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bJavaJFormattedTextField = new JButton(JTextIcon);
            bJavaJFormattedTextField.setBorderPainted(false);
            bJavaJFormattedTextField.setOpaque(false);
            bJavaJFormattedTextField.setToolTipText("JFormattedTextField");
            bJavaJFormattedTextField.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JFormattedTextField FORMATTEDTEXTFIELDNAME = new javax.swing.JFormattedTextField(\"JFORMATTEDTEXTFIELDLABEL\");\n"
                            + "FRAMENAME.add(FORMATTEDTEXTFIELDNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> JtextArea*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bJavaJTextArea = new JButton(JTextIcon);
            bJavaJTextArea.setBorderPainted(false);
            bJavaJTextArea.setOpaque(false);
            bJavaJTextArea.setToolTipText("JTextArea");
            bJavaJTextArea.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JTextArea TEXTAREANAME = new javax.swing.JTextArea();\n"
                            + "FRAMENAME.add(TEXTAREANAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> Jcrollbar*/
            Icon JScrollBarIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/scroll.png"));
            JButton bJavaJScrollBar = new JButton(JScrollBarIcon);
            bJavaJScrollBar.setBorderPainted(false);
            bJavaJScrollBar.setOpaque(false);
            bJavaJScrollBar.setToolTipText("JScrollBar");
            bJavaJScrollBar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JScrollBar SCROLLBARNAME = new javax.swing.JScrollBar();\n"
                            + "SCROLLBARNAME.addAdjustmentListener( new java.awt.event.AdjustmentListener() {\n"
                            + "   @Override public void adjustmentValueChanged( java.awt.event.AdjustmentEvent e ) {\n"
                            + "\n"
                            + "   }\n"
                            + "});\n"
                            + "FRAMENAME.add(SCROLLBARNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> Jslider*/
            Icon sliderIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/slider.png"));
            JButton bJavaJSlider = new JButton(sliderIcon);
            bJavaJSlider.setBorderPainted(false);
            bJavaJSlider.setOpaque(false);
            bJavaJSlider.setToolTipText("JSlider");
            bJavaJSlider.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JSlider SLIDERNAME = new javax.swing.JSlider();\n"
                            + "SLIDERNAME.addChangeListener( new javax.swing.event.ChangeListener() {\n"
                            + "   @Override public void stateChanged( javax.swing.event.ChangeEvent e ) {\n"
                            + "\n"
                            + "   }\n"
                            + "});\n"
                            + "FRAMENAME.add(SLIDERNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> JProgressBar*/
            Icon progressbarIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/progress.png"));
            JButton bJavaJProgressBar = new JButton(progressbarIcon);
            bJavaJProgressBar.setBorderPainted(false);
            bJavaJProgressBar.setOpaque(false);
            bJavaJProgressBar.setToolTipText("JProgressBar");
            bJavaJProgressBar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "javax.swing.JProgressBar PROGRESSBARNAME = new javax.swing.JProgressBar(/*min*/0, /*max*/100);\n"
                            + "FRAMENAME.add(PROGRESSBARNAME);";
                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> flowlayout*/
            Icon layoutIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/layout.png"));
            JButton bJavaFlowLayout = new JButton(layoutIcon);
            bJavaFlowLayout.setBorderPainted(false);
            bJavaFlowLayout.setOpaque(false);
            bJavaFlowLayout.setToolTipText("FlowLayout");
            bJavaFlowLayout.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "new java.awt.FlowLayout()";

                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> borderlayout*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bJavaBorderLayout = new JButton(layoutIcon);
            bJavaBorderLayout.setBorderPainted(false);
            bJavaBorderLayout.setOpaque(false);
            bJavaBorderLayout.setToolTipText("BorderLayout");
            bJavaBorderLayout.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "new java.awt.BorderLayout()";

                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> boxlayout*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bJavaBoxLayout = new JButton(layoutIcon);
            bJavaBoxLayout.setBorderPainted(false);
            bJavaBoxLayout.setOpaque(false);
            bJavaBoxLayout.setToolTipText("BoxLayout");
            bJavaBoxLayout.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "new java.swing.BoxLayout()";

                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> cardlayout*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bJavaCardLayout = new JButton(layoutIcon);
            bJavaCardLayout.setBorderPainted(false);
            bJavaCardLayout.setOpaque(false);
            bJavaCardLayout.setToolTipText("CardLayout");
            bJavaCardLayout.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "new java.awt.CardLayout()";

                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> gridlayout*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bJavaGridLayout = new JButton(layoutIcon);
            bJavaGridLayout.setBorderPainted(false);
            bJavaGridLayout.setOpaque(false);
            bJavaGridLayout.setToolTipText("GridLayout");
            bJavaGridLayout.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "new java.awt.GridLayout()";

                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> gridbaglayout*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bJavaGridBagLayout = new JButton(layoutIcon);
            bJavaGridBagLayout.setBorderPainted(false);
            bJavaGridBagLayout.setOpaque(false);
            bJavaGridBagLayout.setToolTipText("GridBagLayout");
            bJavaGridBagLayout.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "new java.awt.GridBagLayout()\n";
                            //+ "/* GridBagConstraints c = new GridBagConstraints(); */";

                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> grouplayout*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bJavaGroupLayout = new JButton(layoutIcon);
            bJavaGroupLayout.setBorderPainted(false);
            bJavaGroupLayout.setOpaque(false);
            bJavaGroupLayout.setToolTipText("GroupLayout");
            bJavaGroupLayout.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "new javax.swing.GroupLayout()";

                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> springlayout*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton bJavaSpringLayout = new JButton(layoutIcon);
            bJavaSpringLayout.setBorderPainted(false);
            bJavaSpringLayout.setOpaque(false);
            bJavaSpringLayout.setToolTipText("SpringLayout");
            bJavaSpringLayout.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            ""
                            + "new javax.swing.SpringLayout()";

                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> comment*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton SimpleComment = new JButton("//");
            SimpleComment.setBorderPainted(false);
            SimpleComment.setOpaque(false);
            SimpleComment.setToolTipText("Einzeiliger Kommentar");
            SimpleComment.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "//";

                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> comment*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton MultiComment = new JButton("/**");
            MultiComment.setBorderPainted(false);
            MultiComment.setOpaque(false);
            MultiComment.setToolTipText("Mehrzeiliger Kommentar");
            MultiComment.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "/*\n"
                            + " *\n"
                            + " *\n"
                            + " *\n"
                            + " */\n";


                    txtJava.replaceSelection(text);
                }
            });

            //------------------------------------------------------------------
            /* Editor ToolBar -> Java -> func */
            //Icon CFuncIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bJFunc = new JButton("void");
            bJFunc.setBorderPainted(false);
            bJFunc.setOpaque(false);
            bJFunc.setToolTipText("Fügt eine void Funktion ein");
            bJFunc.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "void NAME(){}";
                    txtJava.replaceSelection(text);
                }
            });
            
            /* Editor ToolBar -> Java -> struct */
            //Icon CFuncIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bJClass = new JButton("class");
            bJClass.setBorderPainted(false);
            bJClass.setOpaque(false);
            bJClass.setToolTipText("Java Class");
            bJClass.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "class CLASSNAME {\n" +
                                    "\n" +
                                    " \n" +
                                    "    \n" +
                                    "    \n" +
                                    "  }";
                    txtJava.replaceSelection(text);
                }
            });
            
            /* Editor ToolBar -> Java -> func */
            //Icon CFuncIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bJFread = new JButton("Datei einlesen");
            bJFread.setBorderPainted(false);
            bJFread.setOpaque(false);
            bJFread.setToolTipText("Liest eine txt Datei ein");
            bJFread.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "java.io.FileReader fr = new java.io.FileReader(\"test.txt\");\n" +
                                    "    java.io.BufferedReader br = new java.io.BufferedReader(fr);\n" +
                                    "\n" +
                                    "    String line = \"\";\n" +
                                    "\n" +
                                    "    while( (line = br.readLine()) != null )\n" +
                                    "    {\n" +
                                    "      System.out.println(line);\n" +
                                    "    }\n" +
                                    "\n" +
                                    "    br.close();";
                    txtJava.replaceSelection(text);
                }
            });
            
            /* Editor ToolBar -> Java -> func */
            //Icon CFuncIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bJFwrite = new JButton("Datei schreiben");
            bJFwrite.setBorderPainted(false);
            bJFwrite.setOpaque(false);
            bJFwrite.setToolTipText("Schreibt in eine txt Datei");
            bJFwrite.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "java.io.FileWriter writer;\n" +
                                    "  java.io.File file;"  +
                                    "file = new java.io.File(\"FILENAME.txt\");\n" +
                                    "     try {\n" +
                                    "       \n" +
                                    "       writer = new java.io.FileWriter(file ,true);\n" +
                                    "       \n" +
                                    "       writer.write(\"TEXT\");\n" +
                                    "       \n" +
                                    "       writer.write(System.getProperty(\"line.separator\"));\n" +
                                    "\n" +
                                    "       writer.write(\"TEXT\");\n" +
                                    "       \n" +
                                    "       \n" +
                                    "       writer.flush();\n" +
                                    "       \n" +
                                    "       writer.close();\n" +
                                    "    } catch (java.io.IOException e) {\n" +
                                    "      e.printStackTrace();\n" +
                                    "    }\n" + 
                                    "";
                    txtJava.replaceSelection(text);
                }
            });
            
            //JPanel Jcb = new JPanel();
            //Jcb.setLayout(new FlowLayout(FlowLayout.LEFT));
            
            //JPanel JVAR = new JPanel();
            JComboBox cbJVar = new JComboBox();
            //cbJVar.setPreferredSize(new Dimension(10,25));
            cbJVar.addItem("byte");
            cbJVar.addItem("short");
            cbJVar.addItem("int");
            cbJVar.addItem("long");
            cbJVar.addItem("char");
            cbJVar.addItem("boolean");
            cbJVar.addItem("float");
            cbJVar.addItem("long");
            cbJVar.addItem("double");
            cbJVar.addItem("String");
            
            
            cbJVar.addActionListener(new ActionListener(){
               @Override public void actionPerformed(ActionEvent ev01){
                   JComboBox comboBox = (JComboBox) ev01.getSource();
                   Object selected = comboBox.getSelectedItem();
                   if(selected.equals("byte")){
                       String text = "byte";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("short")){
                       String text = "short";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("int")){
                       String text = "int";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("long")){
                       String text = "long";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("char")){
                       String text = "char";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("boolean")){
                       String text = "boolean";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("float")){
                       String text = "float";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("long")){
                       String text = "long";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("double")){
                       String text = "double";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("String")){
                       String text = "String";
                       txtJava.replaceSelection(text);
                   }
                   
               }
            });
            //JVAR.add(cbJVar);
            
            //JPanel JIMPORT = new JPanel();
            JComboBox cbJImport = new JComboBox();
            //cbJImport.setPreferredSize(new Dimension(10,25));
            cbJImport.addItem("import java.applet;"); 
            cbJImport.addItem("import java.awt;"); 
            cbJImport.addItem("import java.beans;"); 
            cbJImport.addItem("import java.io;"); 
            cbJImport.addItem("import java.lang;"); 
            cbJImport.addItem("import java.math;"); 
            cbJImport.addItem("import java.net;"); 
            cbJImport.addItem("import java.nio;"); 
            cbJImport.addItem("import java.rmi;"); 
            cbJImport.addItem("import java.security;"); 
            cbJImport.addItem("import java.sql;"); 
            cbJImport.addItem("import java.text;"); 
            cbJImport.addItem("import java.util;"); 
            cbJImport.addItem("import javax.accessibility;"); 
            cbJImport.addItem("import javax.crypto;"); 
            cbJImport.addItem("import javax.imageio;"); 
            cbJImport.addItem("import javax.management;"); 
            cbJImport.addItem("import javax.naming;"); 
            cbJImport.addItem("import javax.net;"); 
            cbJImport.addItem("import javax.print;"); 
            cbJImport.addItem("import javax.rmi;"); 
            cbJImport.addItem("import javax.sql;"); 
            cbJImport.addItem("import javax.swing;"); 
            cbJImport.addItem("import javax.transaction;"); 
            cbJImport.addItem("import javax.xml;"); 
 
            
            cbJImport.addActionListener(new ActionListener(){
               @Override public void actionPerformed(ActionEvent ev01){
                   JComboBox comboBox = (JComboBox) ev01.getSource();
                   Object selected = comboBox.getSelectedItem();
                   if(selected.equals("import java.applet;")){
                       String text = "import java.applet.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import java.awt;")){
                       String text = "import java.awt.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import java.beans;")){
                       String text = "import java.beans.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import java.io;")){
                       String text = "import java.io.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import java.lang;")){
                       String text = "import java.lang.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import java.math;")){
                       String text = "import java.math.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import java.net;")){
                       String text = "import java.net.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import java.nio;")){
                       String text = "import java.nio.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import java.rmi;")){
                       String text = "import java.rmi.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import java.security;")){
                       String text = "import java.security.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import java.sql;")){
                       String text = "import java.sql.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import java.text;")){
                       String text = "import java.text.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import java.util;")){
                       String text = "import java.util.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import javax.accessibility;")){
                       String text = "import javax.accessibility.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import javax.crypto;")){
                       String text = "import javax.crypto.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import javax.imageio;")){
                       String text = "import javax.imageio.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import javax.management;")){
                       String text = "import javax.management.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import javax.naming;")){
                       String text = "import javax.naming.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import javax.net;")){
                       String text = "import javax.net.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import javax.print;")){
                       String text = "import javax.print.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import javax.rmi;")){
                       String text = "import javax.rmi.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import javax.sql;")){
                       String text = "import javax.sql.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import javax.swing;")){
                       String text = "import javax.swing.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import javax.transaction;")){
                       String text = "import javax.transaction.*;";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("import javax.xml;")){
                       String text = "import javax.xml.*;";
                       txtJava.replaceSelection(text);
                   }
                   
               }
            });
            //JIMPORT.add(cbJImport);
            
            //JPanel JPROPERTY = new JPanel();
            JComboBox cbJProperty = new JComboBox();
            //cbJProperty.setPreferredSize(new Dimension(10,25));
            cbJProperty.addItem("System.getProperty(\"file.separator\")");            
            cbJProperty.addItem("System.getProperty(\"java.class.version\")");
            cbJProperty.addItem("System.getProperty(\"java.class.path\")");
            cbJProperty.addItem("System.getProperty(\"java.io.tmpdir\")");
            cbJProperty.addItem("System.getProperty(\"java.vendor\")");
            cbJProperty.addItem("System.getProperty(\"java.vendor.url\")");
            cbJProperty.addItem("System.getProperty(\"java.version\")");
            cbJProperty.addItem("System.getProperty(\"line.separator\")");
            cbJProperty.addItem("System.getProperty(\"os.arch\")");
            cbJProperty.addItem("System.getProperty(\"os.name\")");
            cbJProperty.addItem("System.getProperty(\"path.separator\")");
            cbJProperty.addItem("System.getProperty(\"user.dir\")");
            cbJProperty.addItem("System.getProperty(\"user.home\")");
            cbJProperty.addItem("System.getProperty(\"user.language\")");
            cbJProperty.addItem("System.getProperty(\"user.name\")");
            
            cbJProperty.addActionListener(new ActionListener(){
               @Override public void actionPerformed(ActionEvent ev01){
                   JComboBox comboBox = (JComboBox) ev01.getSource();
                   Object selected = comboBox.getSelectedItem();
                   if(selected.equals("System.getProperty(\"file.separator\")")){
                       String text = "System.getProperty(\"file.separator\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"java.class.version\")")){
                       String text = "System.getProperty(\"java.class.version\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"java.class.path\")")){
                       String text = "System.getProperty(\"java.class.path\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"java.io.tmpdir\")")){
                       String text = "System.getProperty(\"java.io.tmpdir\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"java.vendor\")")){
                       String text = "System.getProperty(\"java.vendor\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"java.vendor.url\")")){
                       String text = "System.getProperty(\"java.vendor.url\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"java.version\")")){
                       String text = "System.getProperty(\"java.version\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"line.separator\")")){
                       String text = "System.getProperty(\"line.separator\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"os.arch\")")){
                       String text = "System.getProperty(\"os.arch\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"os.name\")")){
                       String text = "System.getProperty(\"os.name\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"path.separator\")")){
                       String text = "System.getProperty(\"path.separator\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"user.dir\")")){
                       String text = "System.getProperty(\"user.dir\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"user.home\")")){
                       String text = "System.getProperty(\"user.home\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"user.language\")")){
                       String text = "System.getProperty(\"user.language\")";
                       txtJava.replaceSelection(text);
                   }
                   else if(selected.equals("System.getProperty(\"user.name\")")){
                       String text = "System.getProperty(\"user.name\")";
                       txtJava.replaceSelection(text);
                   }
                   
               }
            });
            //JPROPERTY.add(cbJProperty);
            
            //Jcb.add(JVAR);
            //Jcb.add(JIMPORT);
            //Jcb.add(JPROPERTY);
            //-----------------------------------------------------------------
            
            /* Editor ToolBar -> java -> if */
            //Icon ifIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton jif = new JButton("if");
            jif.setBorderPainted(false);
            jif.setOpaque(false);
            jif.setToolTipText("if(){}");
            jif.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "if(){\n"
                            + "    }\n"
                            + "else if(){\n"
                            + "         }\n"
                            + "else{\n"
                            + "    }\n";


                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> for */
            //Icon ifIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton jfor = new JButton("for");
            jfor.setBorderPainted(false);
            jfor.setOpaque(false);
            jfor.setToolTipText("for(){}");
            jfor.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "for(int i=0;i<=100;i++){\n"
                            + "}\n";


                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> do */
            //Icon ifIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton jdo = new JButton("do..while");
            jdo.setBorderPainted(false);
            jdo.setOpaque(false);
            jdo.setToolTipText("do(){}while();");
            jdo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "do{\n"
                            + "\n"
                            + "}while();\n";


                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> while */
            //Icon ifIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton jwhile = new JButton("while");
            jwhile.setBorderPainted(false);
            jwhile.setOpaque(false);
            jwhile.setToolTipText("while(){}");
            jwhile.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "while(){\n"
                            + "\n"
                            + "}\n";


                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> continue */
            //Icon ifIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton jcontinue = new JButton("continue");
            jcontinue.setBorderPainted(false);
            jcontinue.setOpaque(false);
            jcontinue.setToolTipText("continue");
            jcontinue.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "continue;";


                    txtJava.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> break */
            //Icon ifIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton jbreak = new JButton("break");
            jbreak.setBorderPainted(false);
            jbreak.setOpaque(false);
            jbreak.setToolTipText("break");
            jbreak.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "break;";


                    txtJava.replaceSelection(text);
                }
            });
            
            JButton bTry = new JButton("try...catch");
            bTry.setBorderPainted(false);
            bTry.setOpaque(false);
            bTry.setToolTipText("try Fehlerbehandlung");
            bTry.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "try{\n" +
                            "\n" +
                            "}catch(){\n" + 
                            "\n" + 
                            "}finally{\n" +
                            "\n" + 
                            "}\n";
                    txtJava.replaceSelection(text);
                }
            });
            
            JButton bSwitch = new JButton("switch()");
            bSwitch.setBorderPainted(false);
            bSwitch.setOpaque(false);
            bSwitch.setToolTipText("Switch(){case:}");
            bSwitch.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "switch () {\n" +
                            "            case   :  \n" +
                            "                     break;\n" +
                            "            default: \n" +
                            "                     break;\n" +
                            "        }";
                    txtJava.replaceSelection(text);
                }
            });

          
            
            /* Editor ToolBar -> Save */
            Icon ToolbarSaveIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/save.png"));
            JButton btnSave = new JButton(ToolbarSaveIcon);
            btnSave.setBorderPainted(false);
            btnSave.setOpaque(false);
            btnSave.setToolTipText("Speichern");
            tbMenu_generelly.add(btnSave);
            btnSave.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e02) {

                    int i = textTab.getSelectedIndex();

                    String text = txtJava.getText();
                    String pfad = sbLabel.getText();

                    try {
                        FileWriter fw = new FileWriter(pfad);
                        fw.write(text);
                        JOptionPane.showMessageDialog(null, "Datei gespeichert!", "Luzien's Editor v0.4 ", JOptionPane.INFORMATION_MESSAGE);
                        fw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

            /* Editor Toolbar ->  Save as */
            Icon ToolbarSaveasIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/saveas.png"));
            JButton btnSaveAs = new JButton(ToolbarSaveasIcon);
            btnSaveAs.setBorderPainted(false);
            btnSaveAs.setOpaque(false);
            btnSaveAs.setToolTipText("Speichern unter");
            tbMenu_generelly.add(btnSaveAs);
            btnSaveAs.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev03) {
                    int i = textTab.getSelectedIndex();


                    String text = txtJava.getText();
                    String pfad = sbLabel.getText();
                    String path = sbLabel.getText();
                    JFileChooser fc = new JFileChooser();
                    int state = fc.showSaveDialog(null);
                    if (state == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();


                        textTab.setTitleAt(i, file.getName());
                        sbLabel.setText(file.getAbsolutePath());
                        try {
                            FileWriter fw = new FileWriter(file.getAbsolutePath());
                            fw.write(text);
                            fw.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            tbMenu_generelly.addSeparator();

            /*Editor ToolBar Print */
            Icon ToolbarPrintIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/print.png"));
            JButton btnPrint = new JButton(ToolbarPrintIcon);
            btnPrint.setBorderPainted(false);
            btnPrint.setOpaque(false);
            btnPrint.setToolTipText("Datei drucken");
            tbMenu_generelly.add(btnPrint);
            btnPrint.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e05) {
                    String printname = "";

                    try {
                        PrintRequestAttributeSet attrs =
                                new HashPrintRequestAttributeSet();
                        attrs.add(OrientationRequested.PORTRAIT);
                        attrs.add(MediaSizeName.ISO_A4);
                        attrs.add(new JobName(printname, null));
                        txtJava.print(new MessageFormat(printname),
                                new MessageFormat("Page {0}"),
                                true, null, attrs, false);
                    } catch (PrinterException e) {
                        System.err.println("Print error.");
                        System.err.println(e.getMessage());
                    }
                }
            });

            tbMenu_generelly.addSeparator();

            /* Editor ToolBar -> Cut */
            Icon ToolbarCutIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/cut.png"));
            Action cutAction = new DefaultEditorKit.CutAction();
            cutAction.putValue(Action.NAME, "");
            JButton btnCut = new JButton(cutAction);
            btnCut.setBorderPainted(false);
            btnCut.setOpaque(false);
            btnCut.setText("");
            btnCut.setIcon(ToolbarCutIcon);
            btnCut.setToolTipText("Ausschneiden");
            tbMenu_generelly.add(btnCut);


            /* Editor ToolBar -> Copy */
            Icon ToolbarCopyIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/copy.png"));
            Action copyAction = new DefaultEditorKit.CopyAction();
            copyAction.putValue(Action.NAME, "");
            JButton btnCopy = new JButton(copyAction);
            btnCopy.setBorderPainted(false);
            btnCopy.setOpaque(false);
            btnCopy.setText("");
            btnCopy.setIcon(ToolbarCopyIcon);
            btnCopy.setIcon(ToolbarCopyIcon);
            btnCopy.setToolTipText("Kopieren");
            tbMenu_generelly.add(btnCopy);

            /* Editor Toolbar -> Paste */
            Icon ToolbarPasteIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/paste.png"));
            Action pasteAction = new DefaultEditorKit.PasteAction();
            pasteAction.putValue(Action.NAME, "");
            JButton btnPaste = new JButton(pasteAction);
            btnPaste.setIcon(ToolbarPasteIcon);
            btnPaste.setBorderPainted(false);
            btnPaste.setOpaque(false);
            btnPaste.setToolTipText("Einfügen");
            tbMenu_generelly.add(btnPaste);

            Icon redoIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/redo.png"));
            JButton bredo = new JButton(redoIcon);
            bredo.setBorderPainted(false);
            bredo.setOpaque(false);
            bredo.setToolTipText("");
            bredo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    txtJava.redoLastAction();
                }
            });
            
            Icon undoIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/undo.png"));
            JButton bundo = new JButton(undoIcon);
            bundo.setBorderPainted(false);
            bundo.setOpaque(false);
            bundo.setToolTipText("");
            bundo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    txtJava.undoLastAction();
                }
            });
            
            Icon findIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/find.png"));
            JButton bfind = new JButton(findIcon);
            bfind.setBorderPainted(false);
            bfind.setOpaque(false);
            bfind.setToolTipText("");
            bfind.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                  
                  
                  JFrame frmSuche = new JFrame();
                  frmSuche.setTitle("Suchen und ersetzen");
                  frmSuche.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
                  frmSuche.setLayout(new BorderLayout());
                  
                      JToolBar toolBar = new JToolBar();
                      searchField = new JTextField(30);
                      toolBar.add(searchField);
                      final JButton nextButton = new JButton("Weiter");
                      nextButton.setActionCommand("FindNext");
                      nextButton.addActionListener(this);
                      toolBar.add(nextButton);
                      nextButton.addActionListener(new ActionListener() {
                         @Override public void actionPerformed(ActionEvent e) {
                            // "FindNext" => search forward, "FindPrev" => search backward
                              String command = e.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtJava, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                         }
                      });
                      JButton prevButton = new JButton("Zurück");
                      prevButton.setActionCommand("FindPrev");
                      prevButton.addActionListener(new ActionListener(){
                          @Override public void actionPerformed(ActionEvent ev){
                              String command = ev.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtJava, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                          }
                      });
                      toolBar.add(prevButton);
                      regexCB = new JCheckBox("Regulär");
                      //toolBar.add(regexCB);
                      matchCaseCB = new JCheckBox("Genau");
                      //toolBar.add(matchCaseCB);
                      frmSuche.add(toolBar, BorderLayout.NORTH);

                      
                    frmSuche.pack();
                    frmSuche.setLocationRelativeTo(null);
                    frmSuche.setVisible(true);
                }
            });
            
            Icon stampIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/stamp.png"));
            JButton bstamp = new JButton(stampIcon);
            bstamp.setBorderPainted(false);
            bstamp.setOpaque(false);
            bstamp.setToolTipText("");
            bstamp.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                  String Projekt = sbLabel.getText();
                  String Pfad = System.getProperty("user.home") + "/LuziensProjects/";
                  int temp = textTab.getSelectedIndex();
                  String Dateiname = textTab.getTitleAt(temp);
                 
                  Projekt = Projekt.replace(Pfad, "");
                  Projekt = Projekt.replace(Dateiname,"");
                  Projekt = Projekt.replace("/src/", "");
                    try {
                        LuziensEditor.timeline_src.RunProject stampproject = new LuziensEditor.timeline_src.RunProject(Projekt);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });

            JButton b1 = new JButton("1");
            b1.setBorderPainted(false);
            b1.setOpaque(false);
            b1.setToolTipText("Dokument 1");
            b1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    Text1 = txtJava.getText();
                    Pfad1 = sbLabel.getText();
                }
            });
            
            JButton b2 = new JButton("2");
            b2.setBorderPainted(false);
            b2.setOpaque(false);
            b2.setToolTipText("Dokument 2");
            b2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    Text2 = txtJava.getText();
                    Pfad2 = sbLabel.getText();
                }
            });
            
            //tbMenu_generelly.addSeparator();
            tbMenu_left.add(bundo);
            tbMenu_left.add(bredo);
            //tbMenu_left.add(bfind);
            tbMenu_left.add(bstamp);
            tbMenu_left.add(b1);
            tbMenu_left.add(b2);
            add(tbMenu_generelly,BorderLayout.EAST);
            add(tbMenu_left,BorderLayout.WEST);
            
            JPanel pnlMenu = new JPanel();
            pnlMenu.setLayout(new BorderLayout());
            pnlMenu.add(tbMenu1,BorderLayout.NORTH);
            pnlMenu.add(tbMenu2,BorderLayout.CENTER);
            pnlMenu.add(tbMenu3,BorderLayout.SOUTH);
            
            add(pnlMenu,BorderLayout.NORTH);
            
            
            
            
            
            tbMenu1.add(bJavaRun);
            tbMenu1.addSeparator();
            tbMenu1.add(bJFread);
            tbMenu1.add(bJFwrite);
            tbMenu1.add(SimpleComment);
            tbMenu1.add(MultiComment);
            tbMenu1.addSeparator();
            tbMenu1.add(jif);
            tbMenu1.add(jfor);
            tbMenu1.add(jdo);
            tbMenu1.add(jwhile);
            tbMenu1.add(bTry);
            tbMenu1.add(bSwitch);
            tbMenu1.addSeparator();
            tbMenu1.add(jcontinue);
            tbMenu1.add(jbreak);
            tbMenu1.addSeparator();
            tbMenu1.add(cbJVar);
            tbMenu1.add(cbJImport);
            tbMenu1.add(cbJProperty);

          
            tbMenu2.add(bJavaJFrame);
            tbMenu2.add(bJavaJDialog);
            tbMenu2.add(bJavaJPanel);
            tbMenu2.addSeparator();
            tbMenu2.add(bJavaJColor);
            tbMenu2.add(bJavaJFileOpen);
            tbMenu2.add(bJavaJFileSave);
            tbMenu2.addSeparator();
            tbMenu2.add(bJavaJFOptionInput);
            tbMenu2.add(bJavaJFOptionConfirm);
            tbMenu2.add(bJavaJFOptionMessage);
            tbMenu2.add(bJavaJFOptionOption);
            tbMenu2.addSeparator();
            tbMenu2.add(bJavaJMenuBar);
            tbMenu2.add(bJavaJMenu);
            tbMenu2.add(bJavaJMenuItem);
            tbMenu2.add(bJavaJCeckBoxMenuItem);
            tbMenu2.add(bJavaJRadioButtonMenuItem);
            tbMenu2.add(bJavaJPopUpMenu);
            tbMenu2.add(bJavaJSeperator);
            tbMenu2.addSeparator();
            tbMenu2.add(bJavaJTabbedPane);
            tbMenu2.add(bJavaJSplitPane);
            tbMenu2.add(bJavaJScrollPane);
            tbMenu2.add(bJavaJToolBar);
            tbMenu2.add(bJavaJDesktopPane);
            tbMenu2.add(bJavaJInternalFrame);
            //tbMenu_java.addSeparator();

            tbMenu3.add(bJavaJLabel);
            tbMenu3.add(bJavaJButton);
            tbMenu3.add(bJavaJToggleButton);
            tbMenu3.add(bJavaJCheckBox);
            tbMenu3.add(bJavaJRadioButton);
            tbMenu3.add(bJavaButtonGroup);
            tbMenu3.add(bJavaJComboBox);
            tbMenu3.add(bJavaJList);
            tbMenu3.add(bJavaJTextField);
            tbMenu3.add(bJavaJPasswordField);
            tbMenu3.add(bJavaJFormattedTextField);
            tbMenu3.add(bJavaJTextArea);
            tbMenu3.add(bJavaJScrollBar);
            tbMenu3.add(bJavaJSlider);
            tbMenu3.add(bJavaJProgressBar);
            tbMenu3.addSeparator();
            tbMenu3.add(bJavaFlowLayout);
            tbMenu3.add(bJavaBorderLayout);
            tbMenu3.add(bJavaBoxLayout);
            tbMenu3.add(bJavaCardLayout);
            tbMenu3.add(bJavaGridLayout);
            tbMenu3.add(bJavaGridBagLayout);
            tbMenu3.add(bJavaGroupLayout);
            tbMenu3.add(bJavaSpringLayout);

            pnlMenu.add(tbMenu1,BorderLayout.NORTH);
            pnlMenu.add(tbMenu2,BorderLayout.CENTER);
            pnlMenu.add(tbMenu3,BorderLayout.SOUTH);
            add(pnlMenu,BorderLayout.NORTH);
            
                  JPanel frmSuche = new JPanel();
                  frmSuche.setLayout(new BorderLayout());
                  
                      //JToolBar toolBar = new JToolBar();
                      searchField = new JTextField(15);
                      //toolBar.add(searchField);
                      frmSuche.add(searchField,BorderLayout.WEST);
                      final JButton nextButton = new JButton("Weiter");
                      nextButton.setActionCommand("FindNext");
                      //nextButton.addActionListener(this);
                      //toolBar.add(nextButton);
                      frmSuche.add(nextButton,BorderLayout.CENTER);
                      nextButton.addActionListener(new ActionListener() {
                         @Override public void actionPerformed(ActionEvent e) {
                            // "FindNext" => search forward, "FindPrev" => search backward
                              String command = e.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtJava, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                         }
                      });
                      JButton prevButton = new JButton("Zurück");
                      prevButton.setActionCommand("FindPrev");
                      prevButton.addActionListener(new ActionListener(){
                          @Override public void actionPerformed(ActionEvent ev){
                              String command = ev.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtJava, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                          }
                      });
                      //toolBar.add(prevButton);
                      frmSuche.add(prevButton,BorderLayout.EAST);
                      regexCB = new JCheckBox("Regulär");
                      //toolBar.add(regexCB);
                      matchCaseCB = new JCheckBox("Genau");
                      //toolBar.add(matchCaseCB);
                      //frmSuche.add(toolBar, BorderLayout.NORTH);


            tbMenu_bottom.add(SimpleComment);
            tbMenu_bottom.add(MultiComment);
            tbMenu_bottom.addSeparator();
            //tbMenu_bottom.addSeparator();
            tbMenu_bottom.add(jif);
            tbMenu_bottom.add(jfor);
            tbMenu_bottom.add(jdo);
            tbMenu_bottom.add(jwhile);
            tbMenu_bottom.add(bTry);
            tbMenu_bottom.add(bSwitch);
            tbMenu_bottom.addSeparator();
            tbMenu_bottom.add(jcontinue);
            tbMenu_bottom.add(jbreak);
            tbMenu_bottom.addSeparator();
            tbMenu_bottom.add(bJClass);
            tbMenu_bottom.add(bJFunc);
            tbMenu_bottom.addSeparator();
            tbMenu_bottom.add(frmSuche);


            
            add(tbMenu_bottom,BorderLayout.SOUTH);
            
            txtJava.setBackground(Color.WHITE); // JEditorPanel
            //frmEditor = new RSyntaxTextArea();
            txtJava.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
            
//            LanguageSupportFactory.get().register(txtJava);
//            LanguageSupportFactory lsf = LanguageSupportFactory.get();
//            JavaLanguageSupport support = (JavaLanguageSupport)lsf.getSupportFor(SyntaxConstants.SYNTAX_STYLE_JAVA);

            txtJava.setText(text);
            sbLabel.setText(path);
            
            txtJava.addKeyListener(new KeyAdapter(){
              public void keyPressed(KeyEvent e)
              {
                int key = e.getKeyCode();
//                if (key == KeyEvent.VK_F4) // F4 -> Java
//                {
//                   helpWindow showhelp = new helpWindow();
//                   showhelp.Java(true);
//                }
//                else if (key == KeyEvent.VK_F5) // F5 -> C/C++
//                {
//                   helpWindow showhelp = new helpWindow();
//                   showhelp.C(true);
//                }
//                else if (key == KeyEvent.VK_F6) // F6 -> HTML
//                {
//                   helpWindow showhelp = new helpWindow();
//                   showhelp.HTML(true);
//                }
                if (key == KeyEvent.VK_ENTER)
                {
                    
                    String array[] = txtJava.getText().split("\n");
                    for(int i = 0;i<array.length; i++){ 
                            //array[i] = array[i].toLowerCase();
                            array[i] = array[i].replace(";", "");
                            
                            if((array[i].contains("byte")) || (array[i].contains("short")) 
                                    || array[i].contains("int") || (array[i].contains("long")) 
                                    || (array[i].contains("char")) || (array[i].contains("boolean")) 
                                    || (array[i].contains("float")) || (array[i].contains("double")) 
                                    || (array[i].contains("String")) || (array[i].contains("string"))){
                                String ausgabe = array[i];
                                ausgabe = ausgabe.replace("{", " ");
                                ausgabe = ausgabe.replace("}", " ");
                                
                                String text = txtVar.getText();
                                if(!text.contains(ausgabe))
                                    txtVar.append("Zeile " + i + " : " + ausgabe + "\n");
                                    
                            }
                            
                            
                            else if((array[i].contains("class"))){
                                String ausgabe = array[i];
                                ausgabe = ausgabe.replace("{", " ");
                                ausgabe = ausgabe.replace("}", " ");
                                
                                String text1 = txtKla.getText();
                                if(!text1.contains(ausgabe))
                                    txtKla.append("Zeile " + i + " : " + ausgabe + "\n");
                            }
                            
                            else if((array[i].contains("void")) || (array[i].contains("byte")) || (array[i].contains("short")) 
                                    || array[i].contains("int") || (array[i].contains("long")) 
                                    || (array[i].contains("char")) || (array[i].contains("boolean")) 
                                    || (array[i].contains("float")) || (array[i].contains("double")) 
                                    || (array[i].contains("String")) || (array[i].contains("string")) 
                                    /*&& (array[i].endsWith("{"))*/){
                                String ausgabe = array[i];
                                ausgabe = ausgabe.replace("{", " ");
                                ausgabe = ausgabe.replace("}", " ");
                                
                                String text2 = txtVar.getText();
                                if(!text2.contains(ausgabe))
                                    txtVar.append("Zeile " + i + " : " + ausgabe + "\n");
                            }
                        }
                    
                }
//                else if (key == KeyEvent.VK_F7) // F7 -> HTML 5
//                {
//                   helpWindow showhelp = new helpWindow();
//                   showhelp.HTML5(true);
//                }
                
              }
            }); 
            
           
            txtJava.setCodeFoldingEnabled(true);
            txtJava.setAntiAliasingEnabled(true);
            CompletionProvider provider = createCompletionProvider();

              // An AutoCompletion acts as a "middle-man" between a text component
              // and a CompletionProvider. It manages any options associated with
              // the auto-completion (the popup trigger key, whether to display a
              // documentation window along with completion choices, etc.). Unlike
              // CompletionProviders, instances of AutoCompletion cannot be shared
              // among multiple text components.
              AutoCompletion ac = new AutoCompletion(provider);
              ac.install(txtJava);
            add(scrollJava);
        }
        
        private CompletionProvider createCompletionProvider() {

          // A DefaultCompletionProvider is the simplest concrete implementation
          // of CompletionProvider. This provider has no understanding of
          // language semantics. It simply checks the text entered up to the
          // caret position for a match against known completions. This is all
          // that is needed in the majority of cases.
          DefaultCompletionProvider provider = new DefaultCompletionProvider();

          // Add completions for all Java keywords. A BasicCompletion is just
          // a straightforward word completion.
          provider.addCompletion(new BasicCompletion(provider, "abstract"));
          provider.addCompletion(new BasicCompletion(provider, "assert"));
          provider.addCompletion(new BasicCompletion(provider, "break"));
          provider.addCompletion(new BasicCompletion(provider, "case"));
          provider.addCompletion(new BasicCompletion(provider, "catch"));
          provider.addCompletion(new BasicCompletion(provider, "class"));
          provider.addCompletion(new BasicCompletion(provider, "const"));
          provider.addCompletion(new BasicCompletion(provider, "continue"));
          provider.addCompletion(new BasicCompletion(provider, "default"));
          provider.addCompletion(new BasicCompletion(provider, "do"));
          provider.addCompletion(new BasicCompletion(provider, "else"));
          provider.addCompletion(new BasicCompletion(provider, "enum"));
          provider.addCompletion(new BasicCompletion(provider, "extends"));
          provider.addCompletion(new BasicCompletion(provider, "final"));
          provider.addCompletion(new BasicCompletion(provider, "finally"));
          provider.addCompletion(new BasicCompletion(provider, "for"));
          provider.addCompletion(new BasicCompletion(provider, "goto"));
          provider.addCompletion(new BasicCompletion(provider, "if"));
          provider.addCompletion(new BasicCompletion(provider, "implements"));
          provider.addCompletion(new BasicCompletion(provider, "import"));
          provider.addCompletion(new BasicCompletion(provider, "instanceof"));
          provider.addCompletion(new BasicCompletion(provider, "interface"));
          provider.addCompletion(new BasicCompletion(provider, "native"));
          provider.addCompletion(new BasicCompletion(provider, "new"));
          provider.addCompletion(new BasicCompletion(provider, "package"));
          provider.addCompletion(new BasicCompletion(provider, "private"));
          provider.addCompletion(new BasicCompletion(provider, "protected"));
          provider.addCompletion(new BasicCompletion(provider, "public"));
          provider.addCompletion(new BasicCompletion(provider, "return"));
          provider.addCompletion(new BasicCompletion(provider, "static"));
          provider.addCompletion(new BasicCompletion(provider, "strictfp"));
          provider.addCompletion(new BasicCompletion(provider, "super"));
          provider.addCompletion(new BasicCompletion(provider, "switch"));
          provider.addCompletion(new BasicCompletion(provider, "synchronized"));
          provider.addCompletion(new BasicCompletion(provider, "this"));
          provider.addCompletion(new BasicCompletion(provider, "throw"));
          provider.addCompletion(new BasicCompletion(provider, "throws"));
          provider.addCompletion(new BasicCompletion(provider, "transient"));
          provider.addCompletion(new BasicCompletion(provider, "try"));
          provider.addCompletion(new BasicCompletion(provider, "void"));
          provider.addCompletion(new BasicCompletion(provider, "volatile"));
          provider.addCompletion(new BasicCompletion(provider, "while"));
          provider.addCompletion(new BasicCompletion(provider, "ABORT"));
          provider.addCompletion(new BasicCompletion(provider, "ALLBITS"));
         provider.addCompletion(new BasicCompletion(provider, "BOTTOM_ALIGNMENT"));
         provider.addCompletion(new BasicCompletion(provider, "CENTER_ALIGNMENT"));
         provider.addCompletion(new BasicCompletion(provider, "DEFAULT_MODALITY_TYPE"));
         provider.addCompletion(new BasicCompletion(provider, "DISPOSE_ON_CLOSE"));
         provider.addCompletion(new BasicCompletion(provider, "DO_NOTHING_ON_CLOSE"));
         provider.addCompletion(new BasicCompletion(provider, "EXIT"));
         provider.addCompletion(new BasicCompletion(provider, "EXIT_ON_CLOSE"));
         provider.addCompletion(new BasicCompletion(provider, "FRAMEBITS"));
         provider.addCompletion(new BasicCompletion(provider, "HEIGHTS"));
         provider.addCompletion(new BasicCompletion(provider, "HIDE_ON_CLOSE"));
         provider.addCompletion(new BasicCompletion(provider, "LEFT_ALIGNMENT"));
         provider.addCompletion(new BasicCompletion(provider, "PROPERTIES"));
         provider.addCompletion(new BasicCompletion(provider, "RIGHT_ALIGNMENT"));
         provider.addCompletion(new BasicCompletion(provider, "SOMEBITS"));
         provider.addCompletion(new BasicCompletion(provider, "TOP_ALIGNMENT"));
         provider.addCompletion(new BasicCompletion(provider, "WIDTH"));
         provider.addCompletion(new BasicCompletion(provider, "accessibleContex"));
         provider.addCompletion(new BasicCompletion(provider, "rootPane"));
         provider.addCompletion(new BasicCompletion(provider, "rootPaneCheckingEnabled"));
         provider.addCompletion(new BasicCompletion(provider, "action(Event event,Object o)"));
         provider.addCompletion(new BasicCompletion(provider, "add(Component cmpnt)"));
         provider.addCompletion(new BasicCompletion(provider, "add(PopupMenu pm)"));
         provider.addCompletion(new BasicCompletion(provider, "add(Component cmpnt, Object o)"));
         provider.addCompletion(new BasicCompletion(provider, "add(Component cmpnt, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "add(String string, Component cmpnt)"));
         provider.addCompletion(new BasicCompletion(provider, "add(Component  cmpnt, Object o, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "addActionListener(ActionListener a1)"));
         provider.addCompletion(new BasicCompletion(provider, "addComponentListener(ComponentListener c1)"));
         provider.addCompletion(new BasicCompletion(provider, "addContainerListener(ContainerListener c1)"));
         provider.addCompletion(new BasicCompletion(provider, "addFocusListener(FokusListener f1)"));
         provider.addCompletion(new BasicCompletion(provider, "addHirarchyBoundsListener(HirarchyBoundsListener h1)"));
         provider.addCompletion(new BasicCompletion(provider, "addHirarchyListener(HirarchyListener h1)"));
         provider.addCompletion(new BasicCompletion(provider, "addImpl(Object o, Component cmppnt, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "addInputMethodListener(InputMethodListener il)"));
         provider.addCompletion(new BasicCompletion(provider, "addKeyListener(KeyListener kl)"));
         provider.addCompletion(new BasicCompletion(provider, "addMouseListener(MouseListener ml)"));
         provider.addCompletion(new BasicCompletion(provider, "addMouseMotionListener(MouseMotionListener ml)"));
         provider.addCompletion(new BasicCompletion(provider, "addMouseWheelListener(MouseWheelListener ml)"));
         provider.addCompletion(new BasicCompletion(provider, "addNotify()"));
         provider.addCompletion(new BasicCompletion(provider, "addPropertyChangeListener(PropertyChangeListener pl)"));
         provider.addCompletion(new BasicCompletion(provider, "addPropertyChangeListener(String string, PropertyChangeListener pl)"));
         provider.addCompletion(new BasicCompletion(provider, "addWindowFocusListener(WindowFocusListener wl)"));
         provider.addCompletion(new BasicCompletion(provider, "addWindowListener(WindowsListener wl)"));
         provider.addCompletion(new BasicCompletion(provider, "addWindowStateListener(WindowStateListener wl)"));
         provider.addCompletion(new BasicCompletion(provider, "applyComponentOrientation(ComponentOrientation co)"));
         provider.addCompletion(new BasicCompletion(provider, "applyResourceBundle(ResourceBundle rb)"));
         provider.addCompletion(new BasicCompletion(provider, "applyResourceBundle(String string)"));
         provider.addCompletion(new BasicCompletion(provider, "areFocusTraversalKeySet(int i)"));
         provider.addCompletion(new BasicCompletion(provider, "bounds()"));
         provider.addCompletion(new BasicCompletion(provider, "CheckImage(Image image, ImageObserver io)"));
         provider.addCompletion(new BasicCompletion(provider, "CheckImage(Image image, int i, int il, ImageObserver io)"));
         provider.addCompletion(new BasicCompletion(provider, "clone()"));
         provider.addCompletion(new BasicCompletion(provider, "coalesceEvent(AWTEvent awte, AWTEvent awtel)"));
         provider.addCompletion(new BasicCompletion(provider, "contains(Point point)"));
         provider.addCompletion(new BasicCompletion(provider, "contains(int i, int il)"));
         provider.addCompletion(new BasicCompletion(provider, "countComponents()"));
         provider.addCompletion(new BasicCompletion(provider, "createBufferStrategy(int i)"));
         provider.addCompletion(new BasicCompletion(provider, "createBufferStrategy(int i, BufferCapabilities bc)"));
         provider.addCompletion(new BasicCompletion(provider, "createImage(ImageProducer ip)"));
         provider.addCompletion(new BasicCompletion(provider, "createImage(int i, int il)"));
         provider.addCompletion(new BasicCompletion(provider, "createRootPane()"));
         provider.addCompletion(new BasicCompletion(provider, "createVolatileImage(int i, int il)"));
         provider.addCompletion(new BasicCompletion(provider, "createVolatileImage(int i, int il, ImageCapabilities ic)"));
         provider.addCompletion(new BasicCompletion(provider, "deliverEvent(Event event)"));
         provider.addCompletion(new BasicCompletion(provider, "dialogInit()"));
         provider.addCompletion(new BasicCompletion(provider, "disable()"));
         provider.addCompletion(new BasicCompletion(provider, "disableEvents(long l)"));
         provider.addCompletion(new BasicCompletion(provider, "dispatchEvent(AWTEvent awte)"));
         provider.addCompletion(new BasicCompletion(provider, "dispose()"));
         provider.addCompletion(new BasicCompletion(provider, "doLayout()"));
         provider.addCompletion(new BasicCompletion(provider, "enable()"));
         provider.addCompletion(new BasicCompletion(provider, "enable(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "enableEvents(long l)"));
         provider.addCompletion(new BasicCompletion(provider, "enableInputMethods(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "equals(Object o)"));
         provider.addCompletion(new BasicCompletion(provider, "finalize()"));
         provider.addCompletion(new BasicCompletion(provider, "findComponentAt(Point point)"));
         provider.addCompletion(new BasicCompletion(provider, "findComponentAt(int i, int il)"));
         provider.addCompletion(new BasicCompletion(provider, "firePropertyChange(String string, Object o, Object o1)"));
         provider.addCompletion(new BasicCompletion(provider, "firePropertyChange(String string, boolean bln, boolean bln1)"));
         provider.addCompletion(new BasicCompletion(provider, "firePropertyChange(String string, byte b, byte b1)"));
         provider.addCompletion(new BasicCompletion(provider, "firePropertyChange(String string, char c, char c1)"));
         provider.addCompletion(new BasicCompletion(provider, "firePropertyChange(String string, double d, double d1)"));
         provider.addCompletion(new BasicCompletion(provider, "firePropertyChange(String string, float f, float f1)"));
         provider.addCompletion(new BasicCompletion(provider, "firePropertyChange(String string, int i, int i1)"));
         provider.addCompletion(new BasicCompletion(provider, "firePropertyChange(String string, long l, long l1)"));
         provider.addCompletion(new BasicCompletion(provider, "firePropertyChange(String string, short s, short s1)"));
         provider.addCompletion(new BasicCompletion(provider, "getAccessibleContext()"));
         provider.addCompletion(new BasicCompletion(provider, "getAlignmentX()"));
         provider.addCompletion(new BasicCompletion(provider, "getAlignmentY()"));
         provider.addCompletion(new BasicCompletion(provider, "getBackground()"));
         provider.addCompletion(new BasicCompletion(provider, "getBaseline(int i, int il)"));
         provider.addCompletion(new BasicCompletion(provider, "getBaselineResizeBehavior()"));
         provider.addCompletion(new BasicCompletion(provider, "getBounds()"));
         provider.addCompletion(new BasicCompletion(provider, "getBounds(Rectangle rctngl)"));
         provider.addCompletion(new BasicCompletion(provider, "getBufferStrategy()"));
         provider.addCompletion(new BasicCompletion(provider, "getClass()"));
         provider.addCompletion(new BasicCompletion(provider, "getColorModel()"));
         provider.addCompletion(new BasicCompletion(provider, "getComponent(int i)"));
         provider.addCompletion(new BasicCompletion(provider, "getComponentAt(Point point)"));
         provider.addCompletion(new BasicCompletion(provider, "getComponentAt(int i, int il)"));
         provider.addCompletion(new BasicCompletion(provider, "getComponentCount()"));
         provider.addCompletion(new BasicCompletion(provider, "getComponentListeners()"));
         provider.addCompletion(new BasicCompletion(provider, "getComponentOrientation()"));
         provider.addCompletion(new BasicCompletion(provider, "getComponentZOrder(Component cmpnt)"));
         provider.addCompletion(new BasicCompletion(provider, "getComponents()"));
         provider.addCompletion(new BasicCompletion(provider, "getContainerListeners()"));
         provider.addCompletion(new BasicCompletion(provider, "getContantPane()"));
         provider.addCompletion(new BasicCompletion(provider, "getCursor()"));
         provider.addCompletion(new BasicCompletion(provider, "getDafaultCloseOperation()"));
         provider.addCompletion(new BasicCompletion(provider, "getDropTarget()"));
         provider.addCompletion(new BasicCompletion(provider, "getFocusCycleRootAncestor()"));
         provider.addCompletion(new BasicCompletion(provider, "getFocusListeners()"));
         provider.addCompletion(new BasicCompletion(provider, "getFocusOwner()"));
         provider.addCompletion(new BasicCompletion(provider, "getFocusTraversalKeys(int i)"));
         provider.addCompletion(new BasicCompletion(provider, "getFocusTraversalKeysEnabled()"));
         provider.addCompletion(new BasicCompletion(provider, "getFocusTraversalPolicy()"));
         provider.addCompletion(new BasicCompletion(provider, "getFocusableWindowState()"));
         provider.addCompletion(new BasicCompletion(provider, "getFont()"));
         provider.addCompletion(new BasicCompletion(provider, "getFontMetrics(Font font)"));
         provider.addCompletion(new BasicCompletion(provider, "getForeground()"));
         provider.addCompletion(new BasicCompletion(provider, "getClassPane()"));
         provider.addCompletion(new BasicCompletion(provider, "getGraphics()"));
         provider.addCompletion(new BasicCompletion(provider, "getGraphicsConfiguration()"));
         provider.addCompletion(new BasicCompletion(provider, "getHeight()"));
         provider.addCompletion(new BasicCompletion(provider, "getHierarchyBoundsListeners()"));
         provider.addCompletion(new BasicCompletion(provider, "getHierarchyListeners()"));
         provider.addCompletion(new BasicCompletion(provider, "getIconImages()"));
         provider.addCompletion(new BasicCompletion(provider, "getIgnoreRepaint()"));
         provider.addCompletion(new BasicCompletion(provider, "getInputContext()"));
         provider.addCompletion(new BasicCompletion(provider, "getInputMethodListeners()"));
         provider.addCompletion(new BasicCompletion(provider, "getInputMethodRequests()"));
         provider.addCompletion(new BasicCompletion(provider, "getInsets()"));
         provider.addCompletion(new BasicCompletion(provider, "getJMenuBar()"));
         provider.addCompletion(new BasicCompletion(provider, "getKeyListeners()"));
         provider.addCompletion(new BasicCompletion(provider, "getLayeredPane()"));
         provider.addCompletion(new BasicCompletion(provider, "getLayout()"));
         provider.addCompletion(new BasicCompletion(provider, "getListeners(Class<T> type)"));
         provider.addCompletion(new BasicCompletion(provider, "getLocale()"));
         provider.addCompletion(new BasicCompletion(provider, "getLocation()"));
         provider.addCompletion(new BasicCompletion(provider, "getLocation(Point point)"));
         provider.addCompletion(new BasicCompletion(provider, "getLocationOnScreen()"));
         provider.addCompletion(new BasicCompletion(provider, "getMaximumSize()"));
         provider.addCompletion(new BasicCompletion(provider, "getMinimumSize()"));
         provider.addCompletion(new BasicCompletion(provider, "getModalExclusionType()"));
         provider.addCompletion(new BasicCompletion(provider, "getModalityType()"));
         provider.addCompletion(new BasicCompletion(provider, "getMostRecentFocusOwner()"));
         provider.addCompletion(new BasicCompletion(provider, "getMouseListeners()"));
         provider.addCompletion(new BasicCompletion(provider, "getMouseMotionListeners()"));
         provider.addCompletion(new BasicCompletion(provider, "getMousePosition()"));
         provider.addCompletion(new BasicCompletion(provider, "getMousePosition(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "getMouseWheelListeners()"));
         provider.addCompletion(new BasicCompletion(provider, "getName()"));
         provider.addCompletion(new BasicCompletion(provider, "getOwnedWindows()"));
         provider.addCompletion(new BasicCompletion(provider, "getOwner()"));
         provider.addCompletion(new BasicCompletion(provider, "getParent()"));
         provider.addCompletion(new BasicCompletion(provider, "getPeer()"));
         provider.addCompletion(new BasicCompletion(provider, "getPreferredSize()"));
         provider.addCompletion(new BasicCompletion(provider, "getPropertyChangeListeners()"));
         provider.addCompletion(new BasicCompletion(provider, "getPropertyChangeListeners(String string)"));
         provider.addCompletion(new BasicCompletion(provider, "getRootPane()"));
         provider.addCompletion(new BasicCompletion(provider, "getSize()"));
         provider.addCompletion(new BasicCompletion(provider, "getSize(Dimension dmnsn)"));
         provider.addCompletion(new BasicCompletion(provider, "getTitle()"));
         provider.addCompletion(new BasicCompletion(provider, "getToolKit()"));
         provider.addCompletion(new BasicCompletion(provider, "getTransferHandler()"));
         provider.addCompletion(new BasicCompletion(provider, "getTreeLock()"));
         provider.addCompletion(new BasicCompletion(provider, "getWarningString()"));
         provider.addCompletion(new BasicCompletion(provider, "getWidth()"));
         provider.addCompletion(new BasicCompletion(provider, "getWindowFocusListener()"));
         provider.addCompletion(new BasicCompletion(provider, "getWindowListener()"));
         provider.addCompletion(new BasicCompletion(provider, "getWindowStateListener()"));
         provider.addCompletion(new BasicCompletion(provider, "getX()"));
         provider.addCompletion(new BasicCompletion(provider, "getY()"));
         provider.addCompletion(new BasicCompletion(provider, "getFocus(Event event, Object o)"));
         provider.addCompletion(new BasicCompletion(provider, "handleEvent(Event event)"));
         provider.addCompletion(new BasicCompletion(provider, "hasFocus()"));
         provider.addCompletion(new BasicCompletion(provider, "hashCode()"));
         provider.addCompletion(new BasicCompletion(provider, "hide()"));
         provider.addCompletion(new BasicCompletion(provider, "imageUpdate(Image image, int i, int i1, int i2, int i3, int i4)"));
         provider.addCompletion(new BasicCompletion(provider, "insets()"));
         provider.addCompletion(new BasicCompletion(provider, "inside(int i, int il)"));
         provider.addCompletion(new BasicCompletion(provider, "invalidate()"));
         provider.addCompletion(new BasicCompletion(provider, "isActive()"));
         provider.addCompletion(new BasicCompletion(provider, "isAlwaysOnTop()"));
         provider.addCompletion(new BasicCompletion(provider, "isAlwaysOnTopSupported()"));
         provider.addCompletion(new BasicCompletion(provider, "isAncestorOf(Component cmpnt)"));
         provider.addCompletion(new BasicCompletion(provider, "isBackgroundSet()"));
         provider.addCompletion(new BasicCompletion(provider, "isCursorSet()"));
         provider.addCompletion(new BasicCompletion(provider, "isDisplayable()"));
         provider.addCompletion(new BasicCompletion(provider, "isDoubleBuffered()"));
         provider.addCompletion(new BasicCompletion(provider, "isEnabled()"));
         provider.addCompletion(new BasicCompletion(provider, "isFocusCycleRoot()"));
         provider.addCompletion(new BasicCompletion(provider, "isFocusCycleRoot(Container cntnr)")); 
         provider.addCompletion(new BasicCompletion(provider, "isFocusOwner()"));
         provider.addCompletion(new BasicCompletion(provider, "isFocusTraversable()"));
         provider.addCompletion(new BasicCompletion(provider, "isFocusTraversalPolicyProvider()"));
         provider.addCompletion(new BasicCompletion(provider, "isFocusTraversalPolicySet()"));
         provider.addCompletion(new BasicCompletion(provider, "isFocusable()"));
         provider.addCompletion(new BasicCompletion(provider, "isFocusableWindow()"));
         provider.addCompletion(new BasicCompletion(provider, "isFocused()"));
         provider.addCompletion(new BasicCompletion(provider, "isFontSet()"));
         provider.addCompletion(new BasicCompletion(provider, "isForegroundSet()"));
         provider.addCompletion(new BasicCompletion(provider, "isLightweight()"));
         provider.addCompletion(new BasicCompletion(provider, "isLocationByPlatform()"));
         provider.addCompletion(new BasicCompletion(provider, "isMaximumSizeSet()"));
         provider.addCompletion(new BasicCompletion(provider, "isMinimumSizeSet()"));
         provider.addCompletion(new BasicCompletion(provider, "isModal()"));
         provider.addCompletion(new BasicCompletion(provider, "isOpaque()"));
         provider.addCompletion(new BasicCompletion(provider, "isPreferredSizeSet()"));
         provider.addCompletion(new BasicCompletion(provider, "isResizable()"));
         provider.addCompletion(new BasicCompletion(provider, "isRootPaneCheckingEnabled()"));
         provider.addCompletion(new BasicCompletion(provider, "isShowing()"));
         provider.addCompletion(new BasicCompletion(provider, "isUndecorated()"));
         provider.addCompletion(new BasicCompletion(provider, "isValid()"));
         provider.addCompletion(new BasicCompletion(provider, "isVisible()"));
         provider.addCompletion(new BasicCompletion(provider, "Java(boolean visible)"));
         provider.addCompletion(new BasicCompletion(provider, "keyDown(Event event, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "keyUp(Event event, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "layout()"));
         provider.addCompletion(new BasicCompletion(provider, "list()"));
         provider.addCompletion(new BasicCompletion(provider, "list(PrintStream stream)"));
         provider.addCompletion(new BasicCompletion(provider, "list(PrintWriter writer)"));
         provider.addCompletion(new BasicCompletion(provider, "list(PrintStream stream, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "list(PrintWriter writer, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "locate(int i, int i1)"));
         provider.addCompletion(new BasicCompletion(provider, "lacation()"));
         provider.addCompletion(new BasicCompletion(provider, "lostFocus(Event event, Object o)"));
         provider.addCompletion(new BasicCompletion(provider, "minimumSize()"));
         provider.addCompletion(new BasicCompletion(provider, "mouseDown(Event event, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "mouseDrag(Event event, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "mouseEnter(Event event, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "mouseExit(Event event, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "mouseMove(Event event, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "mouseUp(Event event, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "move(int i, int i1)"));
         provider.addCompletion(new BasicCompletion(provider, "nextFocus()"));
         provider.addCompletion(new BasicCompletion(provider, "notify()"));
         provider.addCompletion(new BasicCompletion(provider, "notifyAll()"));
         provider.addCompletion(new BasicCompletion(provider, "pack()"));
         provider.addCompletion(new BasicCompletion(provider, "paint(Graphics grphcs)"));
         provider.addCompletion(new BasicCompletion(provider, "paintAll(Graphics grphcs)"));
         provider.addCompletion(new BasicCompletion(provider, "paintComponents(Graphics grphcs)"));
         provider.addCompletion(new BasicCompletion(provider, "paramString()"));
         provider.addCompletion(new BasicCompletion(provider, "postEvent(Event event)"));
         provider.addCompletion(new BasicCompletion(provider, "preferredSize()"));
         provider.addCompletion(new BasicCompletion(provider, "prepareImage(Image image, ImageObserver io)"));
         provider.addCompletion(new BasicCompletion(provider, "prepareImage(Image image, int i, int i1, ImageObserver io)"));
         provider.addCompletion(new BasicCompletion(provider, "print(Graphics grphcs)"));
         provider.addCompletion(new BasicCompletion(provider, "printAll(Graphics grphcs)"));
         provider.addCompletion(new BasicCompletion(provider, "printComponents(Graphics grphcs)"));
         provider.addCompletion(new BasicCompletion(provider, "processComponentEvent(ComponentEvent ce)"));
         provider.addCompletion(new BasicCompletion(provider, "processContainerEvent(ContainerEvent ce)"));
         provider.addCompletion(new BasicCompletion(provider, "processEvent(AWTEvent awte)"));
         provider.addCompletion(new BasicCompletion(provider, "processFocusEvent(FocusEvent fe)"));
         provider.addCompletion(new BasicCompletion(provider, "processHierarchyBoundsEvent(HierarchyEvent he)"));
         provider.addCompletion(new BasicCompletion(provider, "processHierarchyEvent(HierarchyEvent he)"));
         provider.addCompletion(new BasicCompletion(provider, "processInputMethodEvent(InputMethodEvent ime)"));
         provider.addCompletion(new BasicCompletion(provider, "processKeyEvent(KeyEvent ke)"));
         provider.addCompletion(new BasicCompletion(provider, "processMouseEvent(MouseEvent me)"));
         provider.addCompletion(new BasicCompletion(provider, "processMouseMotionEvent(MouseEvent me)"));
         provider.addCompletion(new BasicCompletion(provider, "processMouseWheelEvent(MouseWheelEvent mwe)"));
         provider.addCompletion(new BasicCompletion(provider, "processWindowEvent(WindowEvent we)"));
         provider.addCompletion(new BasicCompletion(provider, "processWindowFocusEvent(WindowEvent we)"));
         provider.addCompletion(new BasicCompletion(provider, "processWindowStateEvent(WindowEvent we)"));
         provider.addCompletion(new BasicCompletion(provider, "remove(Component cmpnt)"));
         provider.addCompletion(new BasicCompletion(provider, "remove(MenuComponent mc)"));
         provider.addCompletion(new BasicCompletion(provider, "remove(int i)"));
         provider.addCompletion(new BasicCompletion(provider, "removeAll"));
         provider.addCompletion(new BasicCompletion(provider, "removeComponentListener(ComponentListener cl)"));
         provider.addCompletion(new BasicCompletion(provider, "removeContainerListener(ContainerListener cl)"));
         provider.addCompletion(new BasicCompletion(provider, "removeFocusListener(FocusListener fl)"));
         provider.addCompletion(new BasicCompletion(provider, "removeHierarchyBoundsListener(HierarchyBoundsListener hl)"));
         provider.addCompletion(new BasicCompletion(provider, "removeHierarchyListener(HierarchyListener hl)"));
         provider.addCompletion(new BasicCompletion(provider, "removeInputMethodListener(InputMethodListener iml)"));
         provider.addCompletion(new BasicCompletion(provider, "removeKeyListener(KeyListener kl)"));
         provider.addCompletion(new BasicCompletion(provider, "removeMouseListener(MouseListener ml)"));
         provider.addCompletion(new BasicCompletion(provider, "removeMouseMotionListener(MouseMotionListener ml)"));
         provider.addCompletion(new BasicCompletion(provider, "removeMouseWheelListener(MouseWheelListener ml)"));
         provider.addCompletion(new BasicCompletion(provider, "removeNotify()"));
         provider.addCompletion(new BasicCompletion(provider, "removePropertyChangeListener(PropertyChangeListener pl)"));
         provider.addCompletion(new BasicCompletion(provider, "removePropertyChangeListener(String string, PropertyChangeListener pl)"));
         provider.addCompletion(new BasicCompletion(provider, "removeWindowFocusListener(WindowFocusListener wl)"));
         provider.addCompletion(new BasicCompletion(provider, "removeWindowListener(WindowListener wl)"));
         provider.addCompletion(new BasicCompletion(provider, "removeWindowStateListener(WindowStateListener wl)"));
         provider.addCompletion(new BasicCompletion(provider, "repaint()"));
         provider.addCompletion(new BasicCompletion(provider, "repaint(long l)"));
         provider.addCompletion(new BasicCompletion(provider, "repaint(int i, int i1, int i2, int i3)"));
         provider.addCompletion(new BasicCompletion(provider, "repaint(long l, int i, int i1, int i2, int i3)"));
         provider.addCompletion(new BasicCompletion(provider, "requestFocus()"));
         provider.addCompletion(new BasicCompletion(provider, "requestFocus(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "requestFocusInWindow()"));
         provider.addCompletion(new BasicCompletion(provider, "requestFocusInWindow(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "reshape(int i, int i1, int i2, int i3)"));
         provider.addCompletion(new BasicCompletion(provider, "resize(Dimension dmnsn)"));
         provider.addCompletion(new BasicCompletion(provider, "resize(int i, int i1)"));
         provider.addCompletion(new BasicCompletion(provider, "setAlwaysOnTop(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setBackground(Color color)"));
         provider.addCompletion(new BasicCompletion(provider, "setBounds(Rectangle rctngl)"));
         provider.addCompletion(new BasicCompletion(provider, "setBounds(int i, int i1, int i2, int i3)"));
         provider.addCompletion(new BasicCompletion(provider, "setComponentOrientation(ComponentOrientation co)"));
         provider.addCompletion(new BasicCompletion(provider, "setComponentZOrder(Component cmpnt, int i)"));
         provider.addCompletion(new BasicCompletion(provider, "setContantPane(Container cntnr)"));
         provider.addCompletion(new BasicCompletion(provider, "setCursor(Cursor cursor)"));
         provider.addCompletion(new BasicCompletion(provider, "setDefaultCloseOperation(int i)"));
         provider.addCompletion(new BasicCompletion(provider, "setDropTarged(DropTarget dt)"));
         provider.addCompletion(new BasicCompletion(provider, "setEnabled(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setFocusCycleRoot(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setFocusTraversalKeys(int i, Set<? extends AWTKeyStroke > set)"));
         provider.addCompletion(new BasicCompletion(provider, "setFocusTraversalKeysEnabled(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setFocusTraversalPolicy(FocusTraversalPolicy ftp)"));
         provider.addCompletion(new BasicCompletion(provider, "setFocusTraversalPolicyProvider(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setFocusable(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setFocusableWindowState(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setFont(Font font)"));
         provider.addCompletion(new BasicCompletion(provider, "setForeground(Color color)"));
         provider.addCompletion(new BasicCompletion(provider, "setGlassPane(Component cmpnt)"));
         provider.addCompletion(new BasicCompletion(provider, "setIconImage(Icon icon)"));
         provider.addCompletion(new BasicCompletion(provider, "setIconImages(List<? extends Image > list)"));
         provider.addCompletion(new BasicCompletion(provider, "setIgnorRepaint(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setJMenuBar(JMenuBar jmb)"));
         provider.addCompletion(new BasicCompletion(provider, "setLayeredPane(JLayeredPane jlp)"));
         provider.addCompletion(new BasicCompletion(provider, "setLayout(LayoutManager lm)"));
         provider.addCompletion(new BasicCompletion(provider, "setLocale(Locale locale)"));
         provider.addCompletion(new BasicCompletion(provider, "setLocation(Point point)"));
         provider.addCompletion(new BasicCompletion(provider, "setLocation(int i, int i1)"));
         provider.addCompletion(new BasicCompletion(provider, "setLocationByPlatform(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setLocationRelativTo(Component cmpnt)"));
         provider.addCompletion(new BasicCompletion(provider, "setMaximumSize(Dimension dmnsn)"));
         provider.addCompletion(new BasicCompletion(provider, "setMinimumSize(Dimension dmnsn)"));
         provider.addCompletion(new BasicCompletion(provider, "setModal(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setModalExclusionType(ModalExclusionType met)"));
         provider.addCompletion(new BasicCompletion(provider, "setModalityType(ModalityType mt)"));
         provider.addCompletion(new BasicCompletion(provider, "setName(String string)"));
         provider.addCompletion(new BasicCompletion(provider, "setPreferredSize(Dimension dmnsn)"));
         provider.addCompletion(new BasicCompletion(provider, "setResizable(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setRootPane(JRootPane jrp)"));
         provider.addCompletion(new BasicCompletion(provider, "setRootPaneCheckingEnabled(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setSize(Dimension dmnsn)"));
         provider.addCompletion(new BasicCompletion(provider, "setSize(int i, int i1)"));
         provider.addCompletion(new BasicCompletion(provider, "setTitle(String string)"));
         provider.addCompletion(new BasicCompletion(provider, "setTransferHandler(TransferHandler th)"));
         provider.addCompletion(new BasicCompletion(provider, "setUndecorated(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "setVisible(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "show()"));
         provider.addCompletion(new BasicCompletion(provider, "show(boolean bln)"));
         provider.addCompletion(new BasicCompletion(provider, "size()"));
         provider.addCompletion(new BasicCompletion(provider, "toBack()"));
         provider.addCompletion(new BasicCompletion(provider, "toFront()"));
         provider.addCompletion(new BasicCompletion(provider, "toString()"));
         provider.addCompletion(new BasicCompletion(provider, "transferFocus()"));
         provider.addCompletion(new BasicCompletion(provider, "transferFocusBackward()"));
         provider.addCompletion(new BasicCompletion(provider, "transferFocusDownCycle()"));
         provider.addCompletion(new BasicCompletion(provider, "transferFocusUpCycle()"));
         provider.addCompletion(new BasicCompletion(provider, "update(Graphics grphcs)"));
         provider.addCompletion(new BasicCompletion(provider, "validate()"));
         provider.addCompletion(new BasicCompletion(provider, "validateTree()"));
         provider.addCompletion(new BasicCompletion(provider, "wait()"));
         provider.addCompletion(new BasicCompletion(provider, "wait(long l)"));
         provider.addCompletion(new BasicCompletion(provider, "wait(long l, int i)"));

          // Add a couple of "shorthand" completions. These completions don't
          // require the input text to be the same thing as the replacement text.
         provider.addCompletion(new ShorthandCompletion(provider, "printf",
                "System.out.printf(\"\");", "System.out.printf(\"\");"));
          provider.addCompletion(new ShorthandCompletion(provider, "println",
                "System.out.println(\"\");", "System.out.println(\"\");"));
          provider.addCompletion(new ShorthandCompletion(provider, "printerr",
                "System.err.println(\"\");", "System.err.println(\"\");"));

          return provider;

       }
    } 
    static class CPPEditor extends JPanel{
        String text, path;
        RSyntaxTextArea txtCpp = new RSyntaxTextArea();
        
        /* App Path */
        final JLabel sbLabel = new JLabel();
        final RTextScrollPane scrollCpp = new RTextScrollPane(txtCpp);
        static JCheckBox regexCB;
        static JCheckBox matchCaseCB;
        static JTextField searchField;
        static JTextField replaceField;
        CPPEditor(String text, String path){
            text = text;
            path = path;
            
            setLayout(new BorderLayout());
            JToolBar tbMenu_bottom = new JToolBar();
            JToolBar tbMenu_generelly = new JToolBar( null, JToolBar.VERTICAL);
            JToolBar tbMenu_left = new JToolBar(null, JToolBar.VERTICAL);
            JToolBar tbMenu1 = new JToolBar();
            JToolBar tbMenu2 = new JToolBar();
            JToolBar tbMenu3 = new JToolBar();
            
            

            
            JPanel pnlMenu = new JPanel();
            pnlMenu.setLayout(new BorderLayout());
            pnlMenu.add(tbMenu1,BorderLayout.NORTH);
            pnlMenu.add(tbMenu2,BorderLayout.CENTER);
            pnlMenu.add(tbMenu3,BorderLayout.SOUTH);
            
            add(pnlMenu,BorderLayout.NORTH);
            
            // ------ C++--------------

            /* Editor ToolBar -> c++ -> Run Project */
            Icon CppRunIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bCppRun = new JButton(CppRunIcon);
            bCppRun.setBorderPainted(false);
            bCppRun.setOpaque(false);
            bCppRun.setToolTipText("Projekt erstellen und ausführen");
            bCppRun.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    ConsoleTab.setSelectedIndex(0);
                    String path = sbLabel.getText();
                    int i = textTab.getSelectedIndex();
                    String name = textTab.getTitleAt(i);
                    String option = "cpp"; // Compile java app -> Con.syntax

                    /* send arguments to the compiler 
                     * -------------------------------
                     * -> ConArgs = args f
                     * -> path = App Path
                     * -> name = App Name
                     * -> option = Script language
                     */
                    
                    txtCpponsole.setText("");
                    txtDebugger.setText("");
                    cppName = name; 
                    c Cpp = new c();
                    Cpp.gpp(true,ConArgs, path, name, option);

                }
            });
            
            JButton bFunc = new JButton("void");
            bFunc.setBorderPainted(false);
            bFunc.setOpaque(false);
            bFunc.setToolTipText("Fügt eine void Funktion ein");
            bFunc.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "void NAME(){}";
                    txtCpp.replaceSelection(text);
                }
            });
            
            /* Editor ToolBar -> c++ -> functions*/
            Icon CppFunc = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bCppFunc = new JButton("void");
            bCppFunc.setBorderPainted(false);
            bCppFunc.setOpaque(false);
            bCppFunc.setToolTipText("Fügt eine void Funktion ein");
            bCppFunc.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "void NAME(){}";
                    txtCpp.replaceSelection(text);
                }
            });
            
            JButton SimpleComment = new JButton("//");
            SimpleComment.setBorderPainted(false);
            SimpleComment.setOpaque(false);
            SimpleComment.setToolTipText("Einzeiliger Kommentar");
            SimpleComment.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "//";

                    txtCpp.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> comment*/
            //Icon tabelleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton MultiComment = new JButton("/**");
            MultiComment.setBorderPainted(false);
            MultiComment.setOpaque(false);
            MultiComment.setToolTipText("Mehrzeiliger Kommentar");
            MultiComment.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "/*\n"
                            + " *\n"
                            + " *\n"
                            + " *\n"
                            + " */\n";


                    txtCpp.replaceSelection(text);
                }
            });
            
            /* Editor ToolBar -> C++ -> enum */
            //Icon CFuncIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bCppEnum = new JButton("enum");
            bCppEnum.setBorderPainted(false);
            bCppEnum.setOpaque(false);
            bCppEnum.setToolTipText("enum einfügen");
            bCppEnum.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "enum NAME { VALUE1=1, VALUE2, VALUE3} VALUENAME;";
                    txtCpp.replaceSelection(text);
                }
            });
            
            /* Editor ToolBar -> Cpp -> struct */
            //Icon CFuncIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bCppStruct = new JButton("struct");
            bCppStruct.setBorderPainted(false);
            bCppStruct.setOpaque(false);
            bCppStruct.setToolTipText("struct einfügen");
            bCppStruct.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "struct NAME{int....}VALUENAME;";
                    txtCpp.replaceSelection(text);
                }
            });
            
            /* Editor ToolBar -> Cpp -> struct */
            //Icon CFuncIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bCppFread = new JButton("Datei lesen");
            bCppFread.setBorderPainted(false);
            bCppFread.setOpaque(false);
            bCppFread.setToolTipText("Aus einer txt Datei lesen");
            bCppFread.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "string line;\n" +
                                    "  ifstream file (\"FILENAME.txt\");\n" +
                                    "  if (file.is_open())\n" +
                                    "  {\n" +
                                    "    while ( file.good() )\n" +
                                    "    {\n" +
                                    "      getline (file,line);\n" +
                                    "      cout << line << endl;\n" +
                                    "    }\n" +
                                    "    file.close();\n" +
                                    "  }\n" +
                                    "\n" +
                                    "  else cout << \"Unable to open file\"; " + 
                                    "//#include <iostream>;" + 
                                    "//#include <fstream>;" +
                                    "//#include <string>;";
                    txtCpp.replaceSelection(text);
                }
            });
            
            /* Editor ToolBar -> Cpp -> struct */
            //Icon CFuncIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bCppWrite = new JButton("Datei schreiben");
            bCppWrite.setBorderPainted(false);
            bCppWrite.setOpaque(false);
            bCppWrite.setToolTipText("In eine txt Datei schreiben");
            bCppWrite.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "ofstream file;\n" +
                                    "  file.open (\"FILENAME.txt\");\n" +
                                    "  file << \"EXAMPLE TEXT.\\n\";\n" +
                                    "  file.close();" +
                                    "//#include <iostream>;" + 
                                    "//#include <fstream>;";
                    txtCpp.replaceSelection(text);
                }
            });
            
            /* Editor ToolBar -> Cpp -> struct */
            //Icon CFuncIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bCppClass = new JButton("class");
            bCppClass.setBorderPainted(false);
            bCppClass.setOpaque(false);
            bCppClass.setToolTipText("C++ Class");
            bCppClass.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text = "class CLASSNAME {\n" +
                                    "\n" +
                                    "  public:\n" +
                                    "    \n" +
                                    "    \n" +
                                    "  } NAME;";
                    txtCpp.replaceSelection(text);
                }
            });
            
            //JPanel CPPcb = new JPanel();
            //CPPcb.setLayout(new FlowLayout(FlowLayout.LEFT));
            
            //JPanel CPPVAR = new JPanel();
            JComboBox cbCppVar = new JComboBox();
            cbCppVar.addItem("bool");
            cbCppVar.addItem("char");
            cbCppVar.addItem("unsigned char");
            cbCppVar.addItem("int");
            cbCppVar.addItem("short");
            cbCppVar.addItem("long");
            cbCppVar.addItem("unsigned int");
            cbCppVar.addItem("unsigned short");
            cbCppVar.addItem("unsigned long");
            cbCppVar.addItem("float");
            cbCppVar.addItem("double");
            
            cbCppVar.addActionListener(new ActionListener(){
               @Override public void actionPerformed(ActionEvent ev01){
                   JComboBox comboBox = (JComboBox) ev01.getSource();
                   Object selected = comboBox.getSelectedItem();
                   if(selected.equals("bool")){
                       String text = "bool";
                       txtCpp.replaceSelection(text);
                   }
                   else if(selected.equals("char")){
                       String text = "char";
                       txtCpp.replaceSelection(text);
                   }
                   else if(selected.equals("unsigned char")){
                       String text = "unsigned char";
                       txtCpp.replaceSelection(text);
                   }
                   else if(selected.equals("int")){
                       String text = "int";
                       txtCpp.replaceSelection(text);
                   }
                   else if(selected.equals("short")){
                       String text = "short";
                       txtCpp.replaceSelection(text);
                   }
                   else if(selected.equals("long")){
                       String text = "long";
                       txtCpp.replaceSelection(text);
                   }
                   else if(selected.equals("unsigned int")){
                       String text = "unsigned int";
                       txtCpp.replaceSelection(text);
                   }
                   else if(selected.equals("unsigned short")){
                       String text = "unsigned short";
                       txtCpp.replaceSelection(text);
                   }
                   else if(selected.equals("unsigned long")){
                       String text = "unsigned long";
                       txtCpp.replaceSelection(text);
                   }
                   else if(selected.equals("float")){
                       String text = "float";
                       txtCpp.replaceSelection(text);
                   }
                   else if(selected.equals("double")){
                       String text = "double";
                       txtCpp.replaceSelection(text);
                   }
               }
            });
            //CPPVAR.add(cbCppVar);
            //CPPcb.add(CPPVAR);
            
            //JPanel CPPINCLUDE = new JPanel();
            JComboBox cbCppInclude = new JComboBox();
            cbCppInclude.addItem("#include <array>");
            cbCppInclude.addItem("#include <bitset>");
            cbCppInclude.addItem("#include <geque>");
            cbCppInclude.addItem("#include <forward_list>");
            cbCppInclude.addItem("#include <list>");
            cbCppInclude.addItem("#include <map>");
            cbCppInclude.addItem("#include <queue>");
            cbCppInclude.addItem("#include <stack>");
            cbCppInclude.addItem("#include <unordered map>");
            cbCppInclude.addItem("#include <unordered set>");
            cbCppInclude.addItem("#include <vector>");
            cbCppInclude.addItem("#include <algorithm>");
            cbCppInclude.addItem("#include <chrono>");
            cbCppInclude.addItem("#include <functional>");
            cbCppInclude.addItem("#include <iterator>");
            cbCppInclude.addItem("#include <memory>");
            cbCppInclude.addItem("#include <stdexcept>");
            cbCppInclude.addItem("#include <tuble>");
            cbCppInclude.addItem("#include <utility>");
            cbCppInclude.addItem("#include <locale>");
            cbCppInclude.addItem("#include <codacvt>");
            cbCppInclude.addItem("#include <string>");
            cbCppInclude.addItem("#include <regex>");
            cbCppInclude.addItem("#include <fstream>");
            cbCppInclude.addItem("#include <iomanip>");
            cbCppInclude.addItem("#include <ios>");
            cbCppInclude.addItem("#include <iosfwd>");
            cbCppInclude.addItem("#include <iostream>");
            cbCppInclude.addItem("#include <istream>");
            cbCppInclude.addItem("#include <ostream>");
            cbCppInclude.addItem("#include <sstream>");
            cbCppInclude.addItem("#include <sstreambuf>");
            cbCppInclude.addItem("#include <exception>");
            cbCppInclude.addItem("#include <limits>");
            cbCppInclude.addItem("#includs <new>");
            cbCppInclude.addItem("#include <typeinfo>");
            cbCppInclude.addItem("#include <thread>");
            cbCppInclude.addItem("#include <mutex>");
            cbCppInclude.addItem("#include <condition_variable>");
            cbCppInclude.addItem("#include <future>");
            cbCppInclude.addItem("#include <complex>");
            cbCppInclude.addItem("#include <random>");
            cbCppInclude.addItem("#include <valarray>");
            cbCppInclude.addItem("#include <numeric>");
            cbCppInclude.addItem("#include <assert.h>");
            cbCppInclude.addItem("#include <complex.h>");
            cbCppInclude.addItem("#include <ctype.h>");
            cbCppInclude.addItem("#include <errno.h>");
            cbCppInclude.addItem("#include <fenv.h>");
            cbCppInclude.addItem("#include <float.h>");
            cbCppInclude.addItem("#include <inttypes.h>");
            cbCppInclude.addItem("#include <iso646h>");
            cbCppInclude.addItem("#include <limits.h>");
            cbCppInclude.addItem("#include <locale.h>");
            cbCppInclude.addItem("#include <math.h>");
            cbCppInclude.addItem("#include <setjmp.h>");
            cbCppInclude.addItem("#include <signal.h>");
            cbCppInclude.addItem("#include <stdalign.h>");
            cbCppInclude.addItem("#include <stdarg.h>");
            cbCppInclude.addItem("#include <stdatomic.h>");
            cbCppInclude.addItem("#include <stdbool.h>");
            cbCppInclude.addItem("#include <stddef.h>");
            cbCppInclude.addItem("#include <stdint.h>");
            cbCppInclude.addItem("#include <stdio.h>");
            cbCppInclude.addItem("#include <stdlib.h>");
            cbCppInclude.addItem("#include <stdnoreturn.h>");
            cbCppInclude.addItem("#include <string.h>");
            cbCppInclude.addItem("#include <tgmath.h>");
            cbCppInclude.addItem("#include <threads.h>");
            cbCppInclude.addItem("#include <time.h>");
            cbCppInclude.addItem("#include <uchar.h>");
            cbCppInclude.addItem("#include <wchar.h>");
            cbCppInclude.addItem("#include <wctype.h>");

            
            cbCppInclude.addActionListener(new ActionListener(){
               @Override public void actionPerformed(ActionEvent ev01){
                   JComboBox comboBox = (JComboBox) ev01.getSource();
                   Object selected = comboBox.getSelectedItem();
                   if(selected.equals("#include <array>")){
                       txtCpp.replaceSelection("#include <array>");
                   }
                   else if(selected.equals("#include <bitset>")){
                       txtCpp.replaceSelection("#include <bitset>");
                   }
                   else if(selected.equals("#include <geque>")){
                       txtCpp.replaceSelection("#include <geque>");
                   }
                   else if(selected.equals("#include <forward_list>")){
                       txtCpp.replaceSelection("#include <forward_list>");
                   }
                   else if(selected.equals("#include <list>")){
                       txtCpp.replaceSelection("#include <list>");
                   }
                   else if(selected.equals("#include <map>")){
                       txtCpp.replaceSelection("#include <map>");
                   }
                   else if(selected.equals("#include <queue>")){
                       txtCpp.replaceSelection("#include <queue>");
                   }
                   else if(selected.equals("#include <stack>")){
                       txtCpp.replaceSelection("#include <stack>");
                   }
                   else if(selected.equals("#include <unordered map>")){
                       txtCpp.replaceSelection("#include <unordered map>");
                   }
                   else if(selected.equals("#include <unordered set>")){
                       txtCpp.replaceSelection("#include <unordered set>");
                   }
                   else if(selected.equals("#include <vector>")){
                       txtCpp.replaceSelection("#include <vector>");
                   }
                   else if(selected.equals("#include <algorithm>")){
                       txtCpp.replaceSelection("#include <algorithm>");
                   }
                   else if(selected.equals("#include <chrono>")){
                       txtCpp.replaceSelection("#include <chrono>");
                   }
                   else if(selected.equals("#include <functional>")){
                       txtCpp.replaceSelection("#include <functional>");
                   }
                   else if(selected.equals("#include <iterator>")){
                       txtCpp.replaceSelection("#include <iterator>");
                   }
                   else if(selected.equals("#include <memory>")){
                       txtCpp.replaceSelection("#include <memory>");
                   }else if(selected.equals("#include <stdexcept>")){
                       txtCpp.replaceSelection("#include <stdexcept>");
                   }
                   else if(selected.equals("#include <tuble>")){
                       txtCpp.replaceSelection("#include <tuble>");
                   }
                   else if(selected.equals("#include <utility>")){
                       txtCpp.replaceSelection("#include <utility>");
                   }
                   else if(selected.equals("#include <locale>")){
                       txtCpp.replaceSelection("#include <locale>");
                   }
                   else if(selected.equals("#include <codacvt>")){
                       txtCpp.replaceSelection("#include <codacvt>");
                   }
                   else if(selected.equals("#include <string>")){
                       txtCpp.replaceSelection("#include <string>");
                   }
                   else if(selected.equals("#include <regex>")){
                       txtCpp.replaceSelection("#include <regex>");
                   }
                   else if(selected.equals("#include <fstream>")){
                       txtCpp.replaceSelection("#include <fstream>");
                   }
                   else if(selected.equals("#include <iomanip>")){
                       txtCpp.replaceSelection("#include <iomanip>");
                   }
                   else if(selected.equals("#include <ios>")){
                       txtCpp.replaceSelection("#include <ios>");
                   }
                   else if(selected.equals("#include <iosfwd>")){
                       txtCpp.replaceSelection("#include <iosfwd>");
                   }
                   else if(selected.equals("#include <iostream>")){
                       txtCpp.replaceSelection("#include <iostream>");
                   }
                   else if(selected.equals("#include <istream>")){
                       txtCpp.replaceSelection("#include <istream>");
                   }
                   else if(selected.equals("#include <ostream>")){
                       txtCpp.replaceSelection("#include <ostream>");
                   }
                   else if(selected.equals("#include <sstream>")){
                       txtCpp.replaceSelection("#include <sstream>");
                   }
                   else if(selected.equals("#include <sstreambuf>")){
                       txtCpp.replaceSelection("#include <sstreambuf>");
                   }
                   else if(selected.equals("#include <exception>")){
                       txtCpp.replaceSelection("#include <exception>");
                   }
                   else if(selected.equals("#include <limits>")){
                       txtCpp.replaceSelection("#include <limits>");
                   }
                   else if(selected.equals("#includs <new>")){
                       txtCpp.replaceSelection("#includs <new>");
                   }
                   else if(selected.equals("#include <typeinfo>")){
                       txtCpp.replaceSelection("#include <typeinfo>");
                   }
                   else if(selected.equals("#include <thread>")){
                       txtCpp.replaceSelection("#include <thread>");
                   }
                   else if(selected.equals("#include <mutex>")){
                       txtCpp.replaceSelection("#include <mutex>");
                   }
                   else if(selected.equals("#include <condition_variable>")){
                       txtCpp.replaceSelection("#include <condition_variable>");
                   }
                   else if(selected.equals("#include <future>")){
                       txtCpp.replaceSelection("#include <future>");
                   }
                   else if(selected.equals("#include <complex>")){
                       txtCpp.replaceSelection("#include <complex>");
                   }
                   else if(selected.equals("#include <random>")){
                       txtCpp.replaceSelection("#include <random>");
                   }
                   else if(selected.equals("#include <valarray>")){
                       txtCpp.replaceSelection("#include <valarray>");
                   }
                   else if(selected.equals("#include <numeric>")){
                       txtCpp.replaceSelection("#include <numeric>");
                   }
                   else if(selected.equals("#include <assert.h>")){
                       txtCpp.replaceSelection("#include <assert.h>");
                   }
                   else if(selected.equals("#include <complex.h>")){
                       txtCpp.replaceSelection("#include <complex.h>");
                   }
                   else if(selected.equals("#include <ctype.h>")){
                       txtCpp.replaceSelection("#include <ctype.h>");
                   }
                   else if(selected.equals("#include <errno.h>")){
                       txtCpp.replaceSelection("#include <errno.h>");
                   }
                   else if(selected.equals("#include <fenv.h>")){
                       txtCpp.replaceSelection("#include <fenv.h>");
                   }
                   else if(selected.equals("#include <float.h>")){
                       txtCpp.replaceSelection("#include <float.h>");
                   }
                   else if(selected.equals("#include <inttypes.h>")){
                       txtCpp.replaceSelection("#include <inttypes.h>");
                   }
                   else if(selected.equals("#include <iso646h>")){
                       txtCpp.replaceSelection("#include <iso646h>");
                   }
                   else if(selected.equals("#include <limits.h>")){
                       txtCpp.replaceSelection("#include <limits.h>");
                   }
                   else if(selected.equals("#include <locale.h>")){
                       txtCpp.replaceSelection("#include <locale.h>");
                   }
                   else if(selected.equals("#include <math.h>")){
                       txtCpp.replaceSelection("#include <math.h>");
                   }
                   else if(selected.equals("#include <setjmp.h>")){
                       txtCpp.replaceSelection("#include <setjmp.h>");
                   }
                   else if(selected.equals("#include <signal.h>")){
                       txtCpp.replaceSelection("#include <signal.h>");
                   }
                   else if(selected.equals("#include <stdalign.h>")){
                       txtCpp.replaceSelection("#include <stdalign.h>");
                   }
                   else if(selected.equals("#include <stdarg.h>")){
                       txtCpp.replaceSelection("#include <stdarg.h>");
                   }
                   else if(selected.equals("#include <stdatomic.h>")){
                       txtCpp.replaceSelection("#include <stdatomic.h>");
                   }
                   else if(selected.equals("#include <stdbool.h>")){
                       txtCpp.replaceSelection("#include <stdbool.h>");
                   }
                   else if(selected.equals("#include <stddef.h>")){
                       txtCpp.replaceSelection("#include <stddef.h>");
                   }
                   else if(selected.equals("#include <stdint.h>")){
                       txtCpp.replaceSelection("#include <stdint.h>");
                   }
                   else if(selected.equals("#include <stdio.h>")){
                       txtCpp.replaceSelection("#include <stdio.h>");
                   }
                   else if(selected.equals("#include <stdlib.h>")){
                      txtCpp.replaceSelection("#include <stdlib.h>");
                   }
                   else if(selected.equals("#include <stdnoreturn.h>")){
                       txtCpp.replaceSelection("#include <stdnoreturn.h>");
                   }
                   else if(selected.equals("#include <string.h>")){
                       txtCpp.replaceSelection("#include <string.h>");
                   }
                   else if(selected.equals("#include <tgmath.h>")){
                       txtCpp.replaceSelection("#include <tgmath.h>");
                   }
                   else if(selected.equals("#include <threads.h>")){
                       txtCpp.replaceSelection("#include <threads.h>");
                   }
                   else if(selected.equals("#include <time.h>")){
                       txtCpp.replaceSelection("#include <time.h>");
                   }
                   else if(selected.equals("#include <uchar.h>")){
                       txtCpp.replaceSelection("#include <uchar.h>");
                   }
                   else if(selected.equals("#include <wchar.h>")){
                       txtCpp.replaceSelection("#include <wchar.h>");
                   }
                   else if(selected.equals("#include <wctype.h>")){
                       txtCpp.replaceSelection("#include <wctype.h>");
                   } 
               }
            });
            //CPPINCLUDE.add(cbCppInclude);
            //CPPcb.add(CPPINCLUDE);
            
            //JPanel CPPDEF = new JPanel();
            JComboBox cbCppDef = new JComboBox();
            cbCppDef.addItem("#define");
            cbCppDef.addItem("#elif");
            cbCppDef.addItem("#else");
            cbCppDef.addItem("#endif");
            cbCppDef.addItem("#error");
            cbCppDef.addItem("#if");
            cbCppDef.addItem("#ifdef");
            cbCppDef.addItem("#ifndef");
            cbCppDef.addItem("#import");
            cbCppDef.addItem("#include");
            cbCppDef.addItem("#line");
            cbCppDef.addItem("#pragma");
            cbCppDef.addItem("#undef");
            cbCppDef.addItem("#using");
            
            cbCppDef.addActionListener(new ActionListener(){
               @Override public void actionPerformed(ActionEvent ev01){
                   JComboBox comboBox = (JComboBox) ev01.getSource();
                   Object selected = comboBox.getSelectedItem();
                   if(selected.equals("#define")){
                       txtCpp.replaceSelection("#define");
                   }
                   else if(selected.equals("#elif")){
                       txtCpp.replaceSelection("#elif");
                   }
                   else if(selected.equals("#else")){
                       txtCpp.replaceSelection("#else");
                   }
                   else if(selected.equals("#endif")){
                       txtCpp.replaceSelection("#endif");
                   }
                   else if(selected.equals("#error")){
                       txtCpp.replaceSelection("#error");
                   }
                   else if(selected.equals("#if")){
                       txtCpp.replaceSelection("#if");
                   }
                   else if(selected.equals("#ifdef")){
                       txtCpp.replaceSelection("#ifdef");
                   }
                   else if(selected.equals("#ifndef")){
                       txtCpp.replaceSelection("#ifndef");
                   }
                   else if(selected.equals("#import")){
                       txtCpp.replaceSelection("#import");
                   }
                   else if(selected.equals("#include")){
                       txtCpp.replaceSelection("#include");
                   }
                   else if(selected.equals("#line")){
                      txtCpp.replaceSelection("#line");
                   }
                   else if(selected.equals("#pragma")){
                       txtCpp.replaceSelection("#pragma");
                   }
                   else if(selected.equals("#undef")){
                       txtCpp.replaceSelection("#undef");
                   }
                   else if(selected.equals("#using")){
                       txtCpp.replaceSelection("#using");
                   }
               }
            });
            //CPPDEF.add(cbCppDef);
            //CPPcb.add(CPPDEF);

            //JPanel CPPMACRO = new JPanel();
            JComboBox cbCppMacro = new JComboBox();
            cbCppMacro.addItem("__DATE__");
            cbCppMacro.addItem("__FILE__");
            cbCppMacro.addItem("__LINE__");
            cbCppMacro.addItem("__STDC__");
            cbCppMacro.addItem("__TIME__");
            cbCppMacro.addItem("__TIMESTAMP__");
            
            cbCppMacro.addActionListener(new ActionListener(){
               @Override public void actionPerformed(ActionEvent ev01){
                   JComboBox comboBox = (JComboBox) ev01.getSource();
                   Object selected = comboBox.getSelectedItem();
                   if(selected.equals("__DATE__")){
                       txtCpp.replaceSelection("__DATE__");
                   }
                   else if(selected.equals("__FILE__")){
                       txtCpp.replaceSelection("__FILE__");
                   }
                   else if(selected.equals("__LINE__")){
                       txtCpp.replaceSelection("__LINE__");
                   }
                   else if(selected.equals("__STDC__")){
                       txtCpp.replaceSelection("__STDC__");
                   }
                   else if(selected.equals("__TIME__")){
                       txtCpp.replaceSelection("__TIME__");
                   }
                   else if(selected.equals("__TIMESTAMP__")){
                       txtCpp.replaceSelection("__TIMESTAMP__");
                   }
               }
            }); 

            /* Editor ToolBar -> Save */
            Icon ToolbarSaveIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/save.png"));
            JButton btnSave = new JButton(ToolbarSaveIcon);
            btnSave.setBorderPainted(false);
            btnSave.setOpaque(false);
            btnSave.setToolTipText("Speichern");
            tbMenu_generelly.add(btnSave);
            btnSave.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e02) {

                    int i = textTab.getSelectedIndex();

                    String text = txtCpp.getText();
                    String pfad = sbLabel.getText();

                    try {
                        FileWriter fw = new FileWriter(pfad);
                        fw.write(text);
                        JOptionPane.showMessageDialog(null, "Datei gespeichert!", "Luzien's Editor v0.4 ", JOptionPane.INFORMATION_MESSAGE);
                        fw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

            /* Editor Toolbar ->  Save as */
            Icon ToolbarSaveasIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/saveas.png"));
            JButton btnSaveAs = new JButton(ToolbarSaveasIcon);
            btnSaveAs.setBorderPainted(false);
            btnSaveAs.setOpaque(false);
            btnSaveAs.setToolTipText("Speichern unter");
            tbMenu_generelly.add(btnSaveAs);
            btnSaveAs.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev03) {
                    int i = textTab.getSelectedIndex();


                    String text = txtCpp.getText();
                    String pfad = sbLabel.getText();
                    String path = sbLabel.getText();
                    JFileChooser fc = new JFileChooser();
                    int state = fc.showSaveDialog(null);
                    if (state == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();


                        textTab.setTitleAt(i, file.getName());
                        sbLabel.setText(file.getAbsolutePath());
                        try {
                            FileWriter fw = new FileWriter(file.getAbsolutePath());
                            fw.write(text);
                            fw.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            tbMenu_generelly.addSeparator();

            /*Editor ToolBar Print */
            Icon ToolbarPrintIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/print.png"));
            JButton btnPrint = new JButton(ToolbarPrintIcon);
            btnPrint.setBorderPainted(false);
            btnPrint.setOpaque(false);
            btnPrint.setToolTipText("Datei drucken");
            tbMenu_generelly.add(btnPrint);
            btnPrint.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e05) {
                    String printname = "";

                    try {
                        PrintRequestAttributeSet attrs =
                                new HashPrintRequestAttributeSet();
                        attrs.add(OrientationRequested.PORTRAIT);
                        attrs.add(MediaSizeName.ISO_A4);
                        attrs.add(new JobName(printname, null));
                        txtCpp.print(new MessageFormat(printname),
                                new MessageFormat("Page {0}"),
                                true, null, attrs, false);
                    } catch (PrinterException e) {
                        System.err.println("Print error.");
                        System.err.println(e.getMessage());
                    }
                }
            });

            tbMenu_generelly.addSeparator();

            /* Editor ToolBar -> Cut */
            Icon ToolbarCutIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/cut.png"));
            Action cutAction = new DefaultEditorKit.CutAction();
            cutAction.putValue(Action.NAME, "");
            JButton btnCut = new JButton(cutAction);
            btnCut.setBorderPainted(false);
            btnCut.setOpaque(false);
            btnCut.setText("");
            btnCut.setIcon(ToolbarCutIcon);
            btnCut.setToolTipText("Ausschneiden");
            tbMenu_generelly.add(btnCut);


            /* Editor ToolBar -> Copy */
            Icon ToolbarCopyIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/copy.png"));
            Action copyAction = new DefaultEditorKit.CopyAction();
            copyAction.putValue(Action.NAME, "");
            JButton btnCopy = new JButton(copyAction);
            btnCopy.setBorderPainted(false);
            btnCopy.setOpaque(false);
            btnCopy.setText("");
            btnCopy.setIcon(ToolbarCopyIcon);
            btnCopy.setIcon(ToolbarCopyIcon);
            btnCopy.setToolTipText("Kopieren");
            tbMenu_generelly.add(btnCopy);

            /* Editor Toolbar -> Paste */
            Icon ToolbarPasteIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/paste.png"));
            Action pasteAction = new DefaultEditorKit.PasteAction();
            pasteAction.putValue(Action.NAME, "");
            JButton btnPaste = new JButton(pasteAction);
            btnPaste.setIcon(ToolbarPasteIcon);
            btnPaste.setBorderPainted(false);
            btnPaste.setOpaque(false);
            btnPaste.setToolTipText("Einfügen");
            tbMenu_generelly.add(btnPaste);

            Icon redoIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/redo.png"));
            JButton bredo = new JButton(redoIcon);
            bredo.setBorderPainted(false);
            bredo.setOpaque(false);
            bredo.setToolTipText("");
            bredo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    txtCpp.redoLastAction();
                }
            });
            
            Icon undoIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/undo.png"));
            JButton bundo = new JButton(undoIcon);
            bundo.setBorderPainted(false);
            bundo.setOpaque(false);
            bundo.setToolTipText("");
            bundo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    txtCpp.undoLastAction();
                }
            });
            
            Icon findIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/find.png"));
            JButton bfind = new JButton(findIcon);
            bfind.setBorderPainted(false);
            bfind.setOpaque(false);
            bfind.setToolTipText("");
            bfind.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                  
                  
                  JFrame frmSuche = new JFrame();
                  frmSuche.setTitle("Suchen und ersetzen");
                  frmSuche.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
                  frmSuche.setLayout(new BorderLayout());
                  
                      JToolBar toolBar = new JToolBar();
                      searchField = new JTextField(30);
                      toolBar.add(searchField);
                      final JButton nextButton = new JButton("Weiter");
                      nextButton.setActionCommand("FindNext");
                      nextButton.addActionListener(this);
                      toolBar.add(nextButton);
                      nextButton.addActionListener(new ActionListener() {
                         @Override public void actionPerformed(ActionEvent e) {
                            // "FindNext" => search forward, "FindPrev" => search backward
                              String command = e.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtCpp, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                         }
                      });
                      JButton prevButton = new JButton("Zurück");
                      prevButton.setActionCommand("FindPrev");
                      prevButton.addActionListener(new ActionListener(){
                          @Override public void actionPerformed(ActionEvent ev){
                              String command = ev.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtCpp, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                          }
                      });
                      toolBar.add(prevButton);
                      regexCB = new JCheckBox("Regulär");
                      //toolBar.add(regexCB);
                      matchCaseCB = new JCheckBox("Genau");
                      //toolBar.add(matchCaseCB);
                      frmSuche.add(toolBar, BorderLayout.NORTH);

                      
                    frmSuche.pack();
                    frmSuche.setLocationRelativeTo(null);
                    frmSuche.setVisible(true);
                }
            });
            
            Icon stampIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/stamp.png"));
            JButton bstamp = new JButton(stampIcon);
            bstamp.setBorderPainted(false);
            bstamp.setOpaque(false);
            bstamp.setToolTipText("");
            bstamp.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                  String Projekt = sbLabel.getText();
                  String Pfad = System.getProperty("user.home") + "/LuziensProjects/";
                  int temp = textTab.getSelectedIndex();
                  String Dateiname = textTab.getTitleAt(temp);
                 
                  Projekt = Projekt.replace(Pfad, "");
                  Projekt = Projekt.replace(Dateiname,"");
                  Projekt = Projekt.replace("/src/", "");
                    try {
                        LuziensEditor.timeline_src.RunProject stampproject = new LuziensEditor.timeline_src.RunProject(Projekt);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            
                      JButton bTry = new JButton("try...catch");
            bTry.setBorderPainted(false);
            bTry.setOpaque(false);
            bTry.setToolTipText("try Fehlerbehandlung");
            bTry.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "try{\n" +
                            "\n" +
                            "}catch(){\n" + 
                            "\n" + 
                            "}finally{\n" +
                            "\n" + 
                            "}\n";
                    txtCpp.replaceSelection(text);
                }
            });
            
            JButton bSwitch = new JButton("switch()");
            bSwitch.setBorderPainted(false);
            bSwitch.setOpaque(false);
            bSwitch.setToolTipText("Switch(){case:}");
            bSwitch.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "switch () {\n" +
                            "            case   :  \n" +
                            "                     break;\n" +
                            "            default: \n" +
                            "                     break;\n" +
                            "        }";
                    txtCpp.replaceSelection(text);
                }
            });
            
            JButton jif = new JButton("if");
            jif.setBorderPainted(false);
            jif.setOpaque(false);
            jif.setToolTipText("if(){}");
            jif.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "if(){\n"
                            + "    }\n"
                            + "else if(){\n"
                            + "         }\n"
                            + "else{\n"
                            + "    }\n";


                    txtCpp.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> for */
            //Icon ifIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton jfor = new JButton("for");
            jfor.setBorderPainted(false);
            jfor.setOpaque(false);
            jfor.setToolTipText("for(){}");
            jfor.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "for(int i=0;i<=100;i++){\n"
                            + "}\n";


                    txtCpp.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> do */
            //Icon ifIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton jdo = new JButton("do..while");
            jdo.setBorderPainted(false);
            jdo.setOpaque(false);
            jdo.setToolTipText("do(){}while();");
            jdo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "do{\n"
                            + "\n"
                            + "}while();\n";


                    txtCpp.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> while */
            //Icon ifIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton jwhile = new JButton("while");
            jwhile.setBorderPainted(false);
            jwhile.setOpaque(false);
            jwhile.setToolTipText("while(){}");
            jwhile.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "while(){\n"
                            + "\n"
                            + "}\n";


                    txtCpp.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> continue */
            //Icon ifIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton jcontinue = new JButton("continue");
            jcontinue.setBorderPainted(false);
            jcontinue.setOpaque(false);
            jcontinue.setToolTipText("continue");
            jcontinue.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "continue;";


                    txtCpp.replaceSelection(text);
                }
            });

            /* Editor ToolBar -> java -> break */
            //Icon ifIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/HTMLtable.png"));
            JButton jbreak = new JButton("break");
            jbreak.setBorderPainted(false);
            jbreak.setOpaque(false);
            jbreak.setToolTipText("break");
            jbreak.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    String text =
                            "break;";


                    txtCpp.replaceSelection(text);
                }
            });
            
            JButton b1 = new JButton("1");
            b1.setBorderPainted(false);
            b1.setOpaque(false);
            b1.setToolTipText("Dokument 1");
            b1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    Text1 = txtCpp.getText();
                    Pfad1 = sbLabel.getText();
                }
            });
            
            JButton b2 = new JButton("2");
            b2.setBorderPainted(false);
            b2.setOpaque(false);
            b2.setToolTipText("Dokument 2");
            b2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    Text2 = txtCpp.getText();
                    Pfad2 = sbLabel.getText();
                }
            });

            //tbMenu_generelly.addSeparator();
            tbMenu_left.add(bundo);
            tbMenu_left.add(bredo);
            //tbMenu_left.add(bfind);
            tbMenu_left.add(bstamp);
            tbMenu_left.add(b1);
            tbMenu_left.add(b2);
            add(tbMenu_generelly,BorderLayout.EAST);
            add(tbMenu_left,BorderLayout.WEST);
            
            tbMenu1.add(bCppRun);
            //tbMenu1.add(bCppFunc);
            tbMenu1.add(bCppFread);
            tbMenu1.add(bCppWrite);
            //tbMenu_cpp.add(CPPcb);
            tbMenu1.add(cbCppVar);
            tbMenu1.add(cbCppInclude);
            tbMenu1.add(cbCppDef);
            tbMenu1.add(cbCppMacro);
            
            
            JPanel frmSuche = new JPanel();
                  frmSuche.setLayout(new BorderLayout());
                  
                      //JToolBar toolBar = new JToolBar();
                      searchField = new JTextField(15);
                      //toolBar.add(searchField);
                      frmSuche.add(searchField,BorderLayout.WEST);
                      final JButton nextButton = new JButton("Weiter");
                      nextButton.setActionCommand("FindNext");
                      //nextButton.addActionListener(this);
                      //toolBar.add(nextButton);
                      frmSuche.add(nextButton,BorderLayout.CENTER);
                      nextButton.addActionListener(new ActionListener() {
                         @Override public void actionPerformed(ActionEvent e) {
                            // "FindNext" => search forward, "FindPrev" => search backward
                              String command = e.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtCpp, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                         }
                      });
                      JButton prevButton = new JButton("Zurück");
                      prevButton.setActionCommand("FindPrev");
                      prevButton.addActionListener(new ActionListener(){
                          @Override public void actionPerformed(ActionEvent ev){
                              String command = ev.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtCpp, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                          }
                      });
                      //toolBar.add(prevButton);
                      frmSuche.add(prevButton,BorderLayout.EAST);
                      regexCB = new JCheckBox("Regulär");
                      //toolBar.add(regexCB);
                      matchCaseCB = new JCheckBox("Genau");
                      //toolBar.add(matchCaseCB);
                      //frmSuche.add(toolBar, BorderLayout.NORTH);
            
             tbMenu_bottom.add(SimpleComment);
            tbMenu_bottom.add(MultiComment);
            //tbMenu_bottom.addSeparator();
            tbMenu_bottom.addSeparator();
            tbMenu_bottom.add(jif);
            tbMenu_bottom.add(jfor);
            tbMenu_bottom.add(jdo);
            tbMenu_bottom.add(jwhile);
            tbMenu_bottom.add(bTry);
            tbMenu_bottom.add(bSwitch);
            tbMenu_bottom.addSeparator();
            tbMenu_bottom.add(jcontinue);
            tbMenu_bottom.add(jbreak);
            tbMenu_bottom.addSeparator();
            tbMenu_bottom.add(bCppEnum);
            tbMenu_bottom.add(bCppStruct);
            tbMenu_bottom.add(bCppClass);
            tbMenu_bottom.add(bFunc);
            tbMenu_bottom.addSeparator();
            tbMenu_bottom.add(frmSuche);
            
            add(tbMenu_bottom,BorderLayout.SOUTH);
            
            //pnlMenu.add(tbMenu1,BorderLayout.NORTH);
            //pnlMenu.add(tbMenu2,BorderLayout.CENTER);
            //pnlMenu.add(tbMenu3,BorderLayout.SOUTH);
            add(tbMenu1,BorderLayout.NORTH);
            
            txtCpp.setBackground(Color.WHITE); // JEditorPanel
            //frmEditor = new RSyntaxTextArea();
            txtCpp.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CPLUSPLUS);
            
            txtCpp.setText(text);
            sbLabel.setText(path);
            
            txtCpp.addKeyListener(new KeyAdapter(){
              public void keyPressed(KeyEvent e)
              {
                int key = e.getKeyCode();
                if (key == KeyEvent.VK_F5) // F5 -> C/C++
                {
                   helpWindow showhelp = new helpWindow();
                   showhelp.C(true);
                }

                else if (key == KeyEvent.VK_ENTER)
                {
                    
                    String array[] = txtCpp.getText().split("\n");
                    for(int i = 0;i<array.length; i++){ 
                            //array[i] = array[i].toLowerCase();
                            array[i] = array[i].replace(";", "");
                            
                            if((array[i].contains("byte")) || (array[i].contains("short")) 
                                    || array[i].contains("int") || (array[i].contains("long")) 
                                    || (array[i].contains("char")) || (array[i].contains("boolean")) 
                                    || (array[i].contains("float")) || (array[i].contains("double")) 
                                    || (array[i].contains("String")) || (array[i].contains("string"))){
                                String ausgabe = array[i];
                                ausgabe = ausgabe.replace("{", " ");
                                ausgabe = ausgabe.replace("}", " ");
                                
                                String text = txtVar.getText();
                                if(!text.contains(ausgabe))
                                    txtVar.append("Zeile " + i + " : " + ausgabe + "\n");
                                    
                            }
                            
                            
                            else if((array[i].contains("class"))){
                                String ausgabe = array[i];
                                ausgabe = ausgabe.replace("{", " ");
                                ausgabe = ausgabe.replace("}", " ");
                                
                                String text1 = txtKla.getText();
                                if(!text1.contains(ausgabe))
                                    txtKla.append("Zeile " + i + " : " + ausgabe + "\n");
                            }
                            
                            else if((array[i].contains("void")) || (array[i].contains("byte")) || (array[i].contains("short")) 
                                    || array[i].contains("int") || (array[i].contains("long")) 
                                    || (array[i].contains("char")) || (array[i].contains("boolean")) 
                                    || (array[i].contains("float")) || (array[i].contains("double")) 
                                    || (array[i].contains("String")) || (array[i].contains("string")) 
                                    /*&& (array[i].endsWith("{"))*/){
                                String ausgabe = array[i];
                                ausgabe = ausgabe.replace("{", " ");
                                ausgabe = ausgabe.replace("}", " ");
                                
                                String text2 = txtVar.getText();
                                if(!text2.contains(ausgabe))
                                    txtVar.append("Zeile " + i + " : " + ausgabe + "\n");
                            }
                        }
                    
                }

                
              }
            }); 
            
            add(scrollCpp);
        }
    } 
    static class ASMEditor extends JPanel{
        String text, path;
        RSyntaxTextArea txtASM = new RSyntaxTextArea();
        
        /* App Path */
        final JLabel sbLabel = new JLabel();
        final RTextScrollPane scrollASM = new RTextScrollPane(txtASM);
        static JCheckBox regexCB;
        static JCheckBox matchCaseCB;
        static JTextField searchField;
        static JTextField replaceField;
        ASMEditor(String text, String path){
            text = text;
            path = path;
            
            setLayout(new BorderLayout());
            JToolBar tbMenu_generelly = new JToolBar( null, JToolBar.VERTICAL);
            JToolBar tbMenu_left = new JToolBar(null, JToolBar.VERTICAL);
            JToolBar tbMenu1 = new JToolBar();
            JToolBar tbMenu2 = new JToolBar();
            JToolBar tbMenu3 = new JToolBar();
            
            

            
            JPanel pnlMenu = new JPanel();
            pnlMenu.setLayout(new BorderLayout());
            pnlMenu.add(tbMenu1,BorderLayout.NORTH);
            pnlMenu.add(tbMenu2,BorderLayout.CENTER);
            pnlMenu.add(tbMenu3,BorderLayout.SOUTH);
            
            add(pnlMenu,BorderLayout.NORTH);
            
            // ------ GAS --------------

            /* Editor ToolBar -> GAS -> Run Project */
            Icon GASRunIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton bGASRun = new JButton(GASRunIcon);
            bGASRun.setBorderPainted(false);
            bGASRun.setOpaque(false);
            bGASRun.setToolTipText("Projekt erstellen und ausführen");
            bGASRun.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    ConsoleTab.setSelectedIndex(0);
                    String path = sbLabel.getText();
                    int i = textTab.getSelectedIndex();
                    String name = textTab.getTitleAt(i);
                    String option = "GAS"; // Compile java app -> Con.syntax

                    /* send arguments to the compiler 
                     * -------------------------------
                     * -> ConArgs = args f
                     * -> path = App Path
                     * -> name = App Name
                     * -> option = Script language
                     */
                    
                    txtCpponsole.setText("");
                    txtDebugger.setText("");
                    cppName = name; 
                    c asm = new c();
                    asm.GAS(true,ConArgs, path, name, option);

                }
            });
            
             /* Editor ToolBar -> GAS -> Run Project */
            Icon hexIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/hex.png"));
            JButton bhex = new JButton(hexIcon);
            bhex.setBorderPainted(false);
            bhex.setOpaque(false);
            bhex.setToolTipText("HEX Editor");
            bhex.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    
                    
                }
            });
            
            String Variabeln[] = {".ascii",".byte",".short",".int",".long",".float",".double"};
            final JComboBox cbVar = new JComboBox(Variabeln);
              cbVar.addActionListener(new ActionListener(){
                       @Override public void actionPerformed(ActionEvent ev01){
                           JComboBox comboBox = (JComboBox) ev01.getSource();
                           Object selected = comboBox.getSelectedItem();
                           String text = String.valueOf(selected);
                           txtASM.replaceSelection(text);
                       }
              });
              
            //Icon hexIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/hex.png"));
            JButton bASMKommentar = new JButton("#");
            bASMKommentar.setBorderPainted(false);
            bASMKommentar.setOpaque(false);
            bASMKommentar.setToolTipText("Kommentar");
            bASMKommentar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    
                    String text = "#";
                    txtASM.replaceSelection(text);
                }
            });
            
            /* Editor ToolBar -> Save */
            Icon ToolbarSaveIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/save.png"));
            JButton btnSave = new JButton(ToolbarSaveIcon);
            btnSave.setBorderPainted(false);
            btnSave.setOpaque(false);
            btnSave.setToolTipText("Speichern");
            tbMenu_generelly.add(btnSave);
            btnSave.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e02) {

                    int i = textTab.getSelectedIndex();

                    String text = txtASM.getText();
                    String pfad = sbLabel.getText();

                    try {
                        FileWriter fw = new FileWriter(pfad);
                        fw.write(text);
                        JOptionPane.showMessageDialog(null, "Datei gespeichert!", "Luzien's Editor v0.4 ", JOptionPane.INFORMATION_MESSAGE);
                        fw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

            /* Editor Toolbar ->  Save as */
            Icon ToolbarSaveasIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/saveas.png"));
            JButton btnSaveAs = new JButton(ToolbarSaveasIcon);
            btnSaveAs.setBorderPainted(false);
            btnSaveAs.setOpaque(false);
            btnSaveAs.setToolTipText("Speichern unter");
            tbMenu_generelly.add(btnSaveAs);
            btnSaveAs.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev03) {
                    int i = textTab.getSelectedIndex();


                    String text = txtASM.getText();
                    String pfad = sbLabel.getText();
                    String path = sbLabel.getText();
                    JFileChooser fc = new JFileChooser();
                    int state = fc.showSaveDialog(null);
                    if (state == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();


                        textTab.setTitleAt(i, file.getName());
                        sbLabel.setText(file.getAbsolutePath());
                        try {
                            FileWriter fw = new FileWriter(file.getAbsolutePath());
                            fw.write(text);
                            fw.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            tbMenu_generelly.addSeparator();

            /*Editor ToolBar Print */
            Icon ToolbarPrintIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/print.png"));
            JButton btnPrint = new JButton(ToolbarPrintIcon);
            btnPrint.setBorderPainted(false);
            btnPrint.setOpaque(false);
            btnPrint.setToolTipText("Datei drucken");
            tbMenu_generelly.add(btnPrint);
            btnPrint.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e05) {
                    String printname = "";

                    try {
                        PrintRequestAttributeSet attrs =
                                new HashPrintRequestAttributeSet();
                        attrs.add(OrientationRequested.PORTRAIT);
                        attrs.add(MediaSizeName.ISO_A4);
                        attrs.add(new JobName(printname, null));
                        txtASM.print(new MessageFormat(printname),
                                new MessageFormat("Page {0}"),
                                true, null, attrs, false);
                    } catch (PrinterException e) {
                        System.err.println("Print error.");
                        System.err.println(e.getMessage());
                    }
                }
            });

            tbMenu_generelly.addSeparator();

            /* Editor ToolBar -> Cut */
            Icon ToolbarCutIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/cut.png"));
            Action cutAction = new DefaultEditorKit.CutAction();
            cutAction.putValue(Action.NAME, "");
            JButton btnCut = new JButton(cutAction);
            btnCut.setBorderPainted(false);
            btnCut.setOpaque(false);
            btnCut.setText("");
            btnCut.setIcon(ToolbarCutIcon);
            btnCut.setToolTipText("Ausschneiden");
            tbMenu_generelly.add(btnCut);


            /* Editor ToolBar -> Copy */
            Icon ToolbarCopyIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/copy.png"));
            Action copyAction = new DefaultEditorKit.CopyAction();
            copyAction.putValue(Action.NAME, "");
            JButton btnCopy = new JButton(copyAction);
            btnCopy.setBorderPainted(false);
            btnCopy.setOpaque(false);
            btnCopy.setText("");
            btnCopy.setIcon(ToolbarCopyIcon);
            btnCopy.setIcon(ToolbarCopyIcon);
            btnCopy.setToolTipText("Kopieren");
            tbMenu_generelly.add(btnCopy);

            /* Editor Toolbar -> Paste */
            Icon ToolbarPasteIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/paste.png"));
            Action pasteAction = new DefaultEditorKit.PasteAction();
            pasteAction.putValue(Action.NAME, "");
            JButton btnPaste = new JButton(pasteAction);
            btnPaste.setIcon(ToolbarPasteIcon);
            btnPaste.setBorderPainted(false);
            btnPaste.setOpaque(false);
            btnPaste.setToolTipText("Einfügen");
            tbMenu_generelly.add(btnPaste);

            Icon redoIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/redo.png"));
            JButton bredo = new JButton(redoIcon);
            bredo.setBorderPainted(false);
            bredo.setOpaque(false);
            bredo.setToolTipText("");
            bredo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    txtASM.redoLastAction();
                }
            });
            
            Icon undoIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/undo.png"));
            JButton bundo = new JButton(undoIcon);
            bundo.setBorderPainted(false);
            bundo.setOpaque(false);
            bundo.setToolTipText("");
            bundo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    txtASM.undoLastAction();
                }
            });
            
            Icon findIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/find.png"));
            JButton bfind = new JButton(findIcon);
            bfind.setBorderPainted(false);
            bfind.setOpaque(false);
            bfind.setToolTipText("");
            bfind.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                  
                  
                  JFrame frmSuche = new JFrame();
                  frmSuche.setTitle("Suchen und ersetzen");
                  frmSuche.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
                  frmSuche.setLayout(new BorderLayout());
                  
                      JToolBar toolBar = new JToolBar();
                      searchField = new JTextField(30);
                      toolBar.add(searchField);
                      final JButton nextButton = new JButton("Weiter");
                      nextButton.setActionCommand("FindNext");
                      nextButton.addActionListener(this);
                      toolBar.add(nextButton);
                      nextButton.addActionListener(new ActionListener() {
                         @Override public void actionPerformed(ActionEvent e) {
                            // "FindNext" => search forward, "FindPrev" => search backward
                              String command = e.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtASM, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                         }
                      });
                      JButton prevButton = new JButton("Zurück");
                      prevButton.setActionCommand("FindPrev");
                      prevButton.addActionListener(new ActionListener(){
                          @Override public void actionPerformed(ActionEvent ev){
                              String command = ev.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtASM, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                          }
                      });
                      toolBar.add(prevButton);
                      regexCB = new JCheckBox("Regulär");
                      //toolBar.add(regexCB);
                      matchCaseCB = new JCheckBox("Genau");
                      //toolBar.add(matchCaseCB);
                      frmSuche.add(toolBar, BorderLayout.NORTH);

                      
                    frmSuche.pack();
                    frmSuche.setLocationRelativeTo(null);
                    frmSuche.setVisible(true);
                }
            });
            
            Icon stampIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/stamp.png"));
            JButton bstamp = new JButton(stampIcon);
            bstamp.setBorderPainted(false);
            bstamp.setOpaque(false);
            bstamp.setToolTipText("");
            bstamp.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                  String Projekt = sbLabel.getText();
                  String Pfad = System.getProperty("user.home") + "/LuziensProjects/";
                  int temp = textTab.getSelectedIndex();
                  String Dateiname = textTab.getTitleAt(temp);
                 
                  Projekt = Projekt.replace(Pfad, "");
                  Projekt = Projekt.replace(Dateiname,"");
                  Projekt = Projekt.replace("/src/", "");
                    try {
                        LuziensEditor.timeline_src.RunProject stampproject = new LuziensEditor.timeline_src.RunProject(Projekt);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            
            JButton b1 = new JButton("1");
            b1.setBorderPainted(false);
            b1.setOpaque(false);
            b1.setToolTipText("Dokument 1");
            b1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    Text1 = txtASM.getText();
                    Pfad1 = sbLabel.getText();
                }
            });
            
            JButton b2 = new JButton("2");
            b2.setBorderPainted(false);
            b2.setOpaque(false);
            b2.setToolTipText("Dokument 2");
            b2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    Text2 = txtASM.getText();
                    Pfad2 = sbLabel.getText();
                }
            });

            
            tbMenu1.add(bGASRun);
            tbMenu1.add(bhex);
            tbMenu1.add(cbVar);
            tbMenu1.add(bASMKommentar);
            
            tbMenu_generelly.addSeparator();
            tbMenu_left.add(bundo);
            tbMenu_left.add(bredo);
            //tbMenu_left.add(bfind);
            tbMenu_left.add(bstamp);
            tbMenu_left.add(b1);
            tbMenu_left.add(b2);
            add(tbMenu_generelly,BorderLayout.EAST);
            add(tbMenu_left,BorderLayout.WEST);
            
//            pnlMenu.add(tbMenu1,BorderLayout.NORTH);
//            pnlMenu.add(tbMenu2,BorderLayout.CENTER);
//            pnlMenu.add(tbMenu3,BorderLayout.SOUTH);
            add(tbMenu1,BorderLayout.NORTH);
            
            
            JToolBar tbbottom = new JToolBar();
            add(tbbottom,BorderLayout.SOUTH);
            
            JPanel frmSuche = new JPanel();
                  frmSuche.setLayout(new BorderLayout());
                  
                      //JToolBar toolBar = new JToolBar();
                      searchField = new JTextField(15);
                      //toolBar.add(searchField);
                      frmSuche.add(searchField,BorderLayout.WEST);
                      final JButton nextButton = new JButton("Weiter");
                      nextButton.setActionCommand("FindNext");
                      //nextButton.addActionListener(this);
                      //toolBar.add(nextButton);
                      frmSuche.add(nextButton,BorderLayout.CENTER);
                      nextButton.addActionListener(new ActionListener() {
                         @Override public void actionPerformed(ActionEvent e) {
                            // "FindNext" => search forward, "FindPrev" => search backward
                              String command = e.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtASM, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                         }
                      });
                      JButton prevButton = new JButton("Zurück");
                      prevButton.setActionCommand("FindPrev");
                      prevButton.addActionListener(new ActionListener(){
                          @Override public void actionPerformed(ActionEvent ev){
                              String command = ev.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txtASM, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                          }
                      });
                      //toolBar.add(prevButton);
                      frmSuche.add(prevButton,BorderLayout.EAST);
                      regexCB = new JCheckBox("Regulär");
                      //toolBar.add(regexCB);
                      matchCaseCB = new JCheckBox("Genau");
                      //toolBar.add(matchCaseCB);
                      //frmSuche.add(toolBar, BorderLayout.NORTH);
            
            tbbottom.add(frmSuche);
                      
            txtASM.setBackground(Color.WHITE); // JEditorPanel
            //frmEditor = new RSyntaxTextArea();
            txtASM.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_ASSEMBLER_X86);
            
            txtASM.setText(text);
            sbLabel.setText(path);
            
//            txtASM.addKeyListener(new KeyAdapter(){
//              public void keyPressed(KeyEvent e)
//              {
//                int key = e.getKeyCode();
//                if (key == KeyEvent.VK_F5) // F5 -> C/C++
//                {
//                   helpWindow showhelp = new helpWindow();
//                   showhelp.C(true);
//                }
//
//                else if (key == KeyEvent.VK_ENTER)
//                {
//                    
//                    String array[] = txtCpp.getText().split("\n");
//                    for(int i = 0;i<array.length; i++){ 
//                            //array[i] = array[i].toLowerCase();
//                            array[i] = array[i].replace(";", "");
//                            
//                            if((array[i].contains("byte")) || (array[i].contains("short")) 
//                                    || array[i].contains("int") || (array[i].contains("long")) 
//                                    || (array[i].contains("char")) || (array[i].contains("boolean")) 
//                                    || (array[i].contains("float")) || (array[i].contains("double")) 
//                                    || (array[i].contains("String")) || (array[i].contains("string"))){
//                                String ausgabe = array[i];
//                                ausgabe = ausgabe.replace("{", " ");
//                                ausgabe = ausgabe.replace("}", " ");
//                                
//                                String text = txtVar.getText();
//                                if(!text.contains(ausgabe))
//                                    txtVar.append("Zeile " + i + " : " + ausgabe + "\n");
//                                    
//                            }
//                            
//                            
//                            else if((array[i].contains("class"))){
//                                String ausgabe = array[i];
//                                ausgabe = ausgabe.replace("{", " ");
//                                ausgabe = ausgabe.replace("}", " ");
//                                
//                                String text1 = txtKla.getText();
//                                if(!text1.contains(ausgabe))
//                                    txtKla.append("Zeile " + i + " : " + ausgabe + "\n");
//                            }
//                            
//                            else if((array[i].contains("void")) || (array[i].contains("byte")) || (array[i].contains("short")) 
//                                    || array[i].contains("int") || (array[i].contains("long")) 
//                                    || (array[i].contains("char")) || (array[i].contains("boolean")) 
//                                    || (array[i].contains("float")) || (array[i].contains("double")) 
//                                    || (array[i].contains("String")) || (array[i].contains("string")) 
//                                    /*&& (array[i].endsWith("{"))*/){
//                                String ausgabe = array[i];
//                                ausgabe = ausgabe.replace("{", " ");
//                                ausgabe = ausgabe.replace("}", " ");
//                                
//                                String text2 = txtVar.getText();
//                                if(!text2.contains(ausgabe))
//                                    txtVar.append("Zeile " + i + " : " + ausgabe + "\n");
//                            }
//                        }
//                    
//                }
//
//                
//              }
//            }); 
            
            add(scrollASM);
        }
    } 
    static class Editor extends JPanel{
        String text, path;
        RSyntaxTextArea txt = new RSyntaxTextArea();
        
        /* App Path */
        final JLabel sbLabel = new JLabel();
        final RTextScrollPane scrollTXT = new RTextScrollPane(txt);
        
        static JCheckBox regexCB;
        static JCheckBox matchCaseCB;
        static JTextField searchField;
        static JTextField replaceField;
        
        Editor(String text, String path){
            text = text;
            path = path;
            
            setLayout(new BorderLayout());
//            JToolBar bottom =new JToolBar();
//            JToolBar tbMenu_generelly = new JToolBar( null, JToolBar.VERTICAL);
//            JToolBar tbMenu_left = new JToolBar(null, JToolBar.VERTICAL);
            JToolBar tbMenu1 = new JToolBar();
//            JToolBar tbMenu2 = new JToolBar();
//            JToolBar tbMenu3 = new JToolBar();
            
            

            
//            JPanel pnlMenu = new JPanel();
//            pnlMenu.setLayout(new BorderLayout());
              add(tbMenu1,BorderLayout.NORTH);
//            pnlMenu.add(tbMenu2,BorderLayout.CENTER);
//            pnlMenu.add(tbMenu3,BorderLayout.SOUTH);
//            
//            add(pnlMenu,BorderLayout.NORTH);
            
            Icon stampIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/stamp.png"));
            JButton bstamp = new JButton(stampIcon);
            bstamp.setBorderPainted(false);
            bstamp.setOpaque(false);
            bstamp.setToolTipText("");
            bstamp.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                  String Projekt = sbLabel.getText();
                  String Pfad = System.getProperty("user.home") + "/LuziensProjects/";
                  int temp = textTab.getSelectedIndex();
                  String Dateiname = textTab.getTitleAt(temp);
                 
                  Projekt = Projekt.replace(Pfad, "");
                  Projekt = Projekt.replace(Dateiname,"");
                  Projekt = Projekt.replace("/src/", "");
                    try {
                        LuziensEditor.timeline_src.RunProject stampproject = new LuziensEditor.timeline_src.RunProject(Projekt);
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });

            
            /* Editor ToolBar -> Save */
            Icon ToolbarSaveIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/save.png"));
            JButton btnSave = new JButton(ToolbarSaveIcon);
            btnSave.setBorderPainted(false);
            btnSave.setOpaque(false);
            btnSave.setToolTipText("Speichern");
            
            btnSave.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e02) {

                    int i = textTab.getSelectedIndex();

                    String text = txt.getText();
                    String pfad = sbLabel.getText();

                    try {
                        FileWriter fw = new FileWriter(pfad);
                        fw.write(text);
                        JOptionPane.showMessageDialog(null, "Datei gespeichert!", "Luzien's Editor v0.4 ", JOptionPane.INFORMATION_MESSAGE);
                        fw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

            /* Editor Toolbar ->  Save as */
            Icon ToolbarSaveasIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/saveas.png"));
            JButton btnSaveAs = new JButton(ToolbarSaveasIcon);
            btnSaveAs.setBorderPainted(false);
            btnSaveAs.setOpaque(false);
            btnSaveAs.setToolTipText("Speichern unter");
            
            btnSaveAs.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ev03) {
                    int i = textTab.getSelectedIndex();


                    String text = txt.getText();
                    String pfad = sbLabel.getText();
                    String path = sbLabel.getText();
                    JFileChooser fc = new JFileChooser();
                    int state = fc.showSaveDialog(null);
                    if (state == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();


                        textTab.setTitleAt(i, file.getName());
                        sbLabel.setText(file.getAbsolutePath());
                        try {
                            FileWriter fw = new FileWriter(file.getAbsolutePath());
                            fw.write(text);
                            fw.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            //tbMenu_generelly.addSeparator();

            /*Editor ToolBar Print */
            Icon ToolbarPrintIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/print.png"));
            JButton btnPrint = new JButton(ToolbarPrintIcon);
            btnPrint.setBorderPainted(false);
            btnPrint.setOpaque(false);
            btnPrint.setToolTipText("Datei drucken");
            
            btnPrint.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e05) {
                    String printname = "";

                    try {
                        PrintRequestAttributeSet attrs =
                                new HashPrintRequestAttributeSet();
                        attrs.add(OrientationRequested.PORTRAIT);
                        attrs.add(MediaSizeName.ISO_A4);
                        attrs.add(new JobName(printname, null));
                        txt.print(new MessageFormat(printname),
                                new MessageFormat("Page {0}"),
                                true, null, attrs, false);
                    } catch (PrinterException e) {
                        System.err.println("Print error.");
                        System.err.println(e.getMessage());
                    }
                }
            });

            //tbMenu_generelly.addSeparator();

            /* Editor ToolBar -> Cut */
            Icon ToolbarCutIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/cut.png"));
            Action cutAction = new DefaultEditorKit.CutAction();
            cutAction.putValue(Action.NAME, "");
            JButton btnCut = new JButton(cutAction);
            btnCut.setBorderPainted(false);
            btnCut.setOpaque(false);
            btnCut.setText("");
            btnCut.setIcon(ToolbarCutIcon);
            btnCut.setToolTipText("Ausschneiden");
            


            /* Editor ToolBar -> Copy */
            Icon ToolbarCopyIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/copy.png"));
            Action copyAction = new DefaultEditorKit.CopyAction();
            copyAction.putValue(Action.NAME, "");
            JButton btnCopy = new JButton(copyAction);
            btnCopy.setBorderPainted(false);
            btnCopy.setOpaque(false);
            btnCopy.setText("");
            btnCopy.setIcon(ToolbarCopyIcon);
            btnCopy.setIcon(ToolbarCopyIcon);
            btnCopy.setToolTipText("Kopieren");
            

            /* Editor Toolbar -> Paste */
            Icon ToolbarPasteIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/paste.png"));
            Action pasteAction = new DefaultEditorKit.PasteAction();
            pasteAction.putValue(Action.NAME, "");
            JButton btnPaste = new JButton(pasteAction);
            btnPaste.setIcon(ToolbarPasteIcon);
            btnPaste.setBorderPainted(false);
            btnPaste.setOpaque(false);
            btnPaste.setToolTipText("Einfügen");
            

            Icon redoIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/redo.png"));
            JButton bredo = new JButton(redoIcon);
            bredo.setBorderPainted(false);
            bredo.setOpaque(false);
            bredo.setToolTipText("");
            bredo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    txt.redoLastAction();
                }
            });
            
            Icon undoIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/undo.png"));
            JButton bundo = new JButton(undoIcon);
            bundo.setBorderPainted(false);
            bundo.setOpaque(false);
            bundo.setToolTipText("");
            bundo.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    txt.undoLastAction();
                }
            });
            
            Icon findIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/find.png"));
            JButton bfind = new JButton(findIcon);
            bfind.setBorderPainted(false);
            bfind.setOpaque(false);
            bfind.setToolTipText("");
            bfind.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                  
                  
                  JFrame frmSuche = new JFrame();
                  frmSuche.setTitle("Suchen und ersetzen");
                  frmSuche.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
                  frmSuche.setLayout(new BorderLayout());
                  
                      JToolBar toolBar = new JToolBar();
                      searchField = new JTextField(30);
                      toolBar.add(searchField);
                      final JButton nextButton = new JButton("Weiter");
                      nextButton.setActionCommand("FindNext");
                      nextButton.addActionListener(this);
                      toolBar.add(nextButton);
                      nextButton.addActionListener(new ActionListener() {
                         @Override public void actionPerformed(ActionEvent e) {
                            // "FindNext" => search forward, "FindPrev" => search backward
                              String command = e.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txt, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                         }
                      });
                      JButton prevButton = new JButton("Zurück");
                      prevButton.setActionCommand("FindPrev");
                      prevButton.addActionListener(new ActionListener(){
                          @Override public void actionPerformed(ActionEvent ev){
                              String command = ev.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txt, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                          }
                      });
                      toolBar.add(prevButton);
                      regexCB = new JCheckBox("Regulär");
                      //toolBar.add(regexCB);
                      matchCaseCB = new JCheckBox("Genau");
                      //toolBar.add(matchCaseCB);
                      frmSuche.add(toolBar, BorderLayout.NORTH);

                      
                    frmSuche.pack();
                    frmSuche.setLocationRelativeTo(null);
                    frmSuche.setVisible(true);
                }
            });
            
            JButton b1 = new JButton("1");
            b1.setBorderPainted(false);
            b1.setOpaque(false);
            b1.setToolTipText("Dokument 1");
            b1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    Text1 = txt.getText();
                    Pfad1 = sbLabel.getText();
                }
            });
            
            JButton b2 = new JButton("2");
            b2.setBorderPainted(false);
            b2.setOpaque(false);
            b2.setToolTipText("Dokument 2");
            b2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e07) {
                    Text2 = txt.getText();
                    Pfad2 = sbLabel.getText();
                }
            });
            
            tbMenu1.add(btnSave);
            tbMenu1.add(btnSaveAs);
            tbMenu1.add(btnPrint);
            tbMenu1.addSeparator();
            tbMenu1.add(btnCut);
            tbMenu1.add(btnCopy);
            tbMenu1.add(btnPaste);
            tbMenu1.addSeparator();
            tbMenu1.add(bundo);
            tbMenu1.add(bredo);
            tbMenu1.addSeparator();
            //tbMenu1.add(bfind);
            tbMenu1.add(bstamp);
            tbMenu1.add(b1);
            tbMenu1.add(b2);
            //add(tbMenu_generelly,BorderLayout.EAST);
            //add(tbMenu_left,BorderLayout.WEST);
            
//            pnlMenu.add(tbMenu1,BorderLayout.NORTH);
//            pnlMenu.add(tbMenu2,BorderLayout.CENTER);
//            pnlMenu.add(tbMenu3,BorderLayout.SOUTH);
            //add(pnlMenu,BorderLayout.NORTH);
            
            
            JToolBar tbbottom = new JToolBar();
            add(tbbottom,BorderLayout.SOUTH);
            
            JPanel frmSuche = new JPanel();
                  frmSuche.setLayout(new BorderLayout());
                  
                      //JToolBar toolBar = new JToolBar();
                      searchField = new JTextField(15);
                      //toolBar.add(searchField);
                      frmSuche.add(searchField,BorderLayout.WEST);
                      final JButton nextButton = new JButton("Weiter");
                      nextButton.setActionCommand("FindNext");
                      //nextButton.addActionListener(this);
                      //toolBar.add(nextButton);
                      frmSuche.add(nextButton,BorderLayout.CENTER);
                      nextButton.addActionListener(new ActionListener() {
                         @Override public void actionPerformed(ActionEvent e) {
                            // "FindNext" => search forward, "FindPrev" => search backward
                              String command = e.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txt, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                         }
                      });
                      JButton prevButton = new JButton("Zurück");
                      prevButton.setActionCommand("FindPrev");
                      prevButton.addActionListener(new ActionListener(){
                          @Override public void actionPerformed(ActionEvent ev){
                              String command = ev.getActionCommand();
                              boolean forward = "FindNext".equals(command);

                              // Create an object defining our search parameters.
                              SearchContext context = new SearchContext();
                              String text = searchField.getText();
                              if (text.length() == 0) {
                                 return;
                              }
                              context.setSearchFor(text);
                              context.setMatchCase(matchCaseCB.isSelected());
                              context.setRegularExpression(regexCB.isSelected());
                              context.setSearchForward(forward);
                              context.setWholeWord(false);

                              boolean found = SearchEngine.find(txt, context);
                              if (!found) {
                                 JOptionPane.showMessageDialog(null, "Text nicht gefunden!");
                              }
                          }
                      });
                      //toolBar.add(prevButton);
                      frmSuche.add(prevButton,BorderLayout.EAST);
                      regexCB = new JCheckBox("Regulär");
                      //toolBar.add(regexCB);
                      matchCaseCB = new JCheckBox("Genau");
                      //toolBar.add(matchCaseCB);
                      //frmSuche.add(toolBar, BorderLayout.NORTH);
            
            tbbottom.add(frmSuche);
                      
            JPanel SyntaxPanel = new JPanel();
            /* Syntax
             * c / c++ / java / javascript / xml / sql 
             */


            JComboBox cmbSyntax = new JComboBox();
            cmbSyntax.addItem("Assembler x86");
            cmbSyntax.addItem("C");
            cmbSyntax.addItem("C++");
            cmbSyntax.addItem("HTML");
            cmbSyntax.addItem("JavaScript");
            cmbSyntax.addItem("XML");
            cmbSyntax.addItem("SQL");
            cmbSyntax.addItem("PHP");
            cmbSyntax.addItem("Shell");
            cmbSyntax.addItem("CSS");
            cmbSyntax.addItem("Java");

            cmbSyntax.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent event) {
                    JComboBox comboBox = (JComboBox) event.getSource();
                    Object selected = comboBox.getSelectedItem();
                    if (selected.equals("Assembler x86")) {
                        String text = txt.getText();
                        txt.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_ASSEMBLER_X86);
                        txt.setText(text);
                    } else if (selected.equals("C")) {
                        String text = txt.getText();
                        txt.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_C);
                        txt.setText(text);
                    } else if (selected.equals("C++")) {
                        String text = txt.getText();
                        txt.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CPLUSPLUS);
                        txt.setText(text);
                    } else if (selected.equals("HTML")) {
                        String text = txt.getText();
                        txt.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_HTML);
                        txt.setText(text);
                    }
                    else if (selected.equals("JavaScript")) {
                        String text = txt.getText();
                        txt.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
                        txt.setText(text);
                    }
                    else if (selected.equals("XML")) {
                        String text = txt.getText();
                        txt.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_XML);
                        txt.setText(text);
                    }
                    else if (selected.equals("SQL")) {
                        String text = txt.getText();
                        txt.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
                        txt.setText(text);
                    }
                    else if (selected.equals("PHP")) {
                        String text = txt.getText();
                        txt.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PHP);
                        txt.setText(text);
                    }
                    else if (selected.equals("Shell")) {
                        String text = txt.getText();
                        txt.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_UNIX_SHELL);
                        txt.setText(text);
                    }
                    else if (selected.equals("CSS")) {
                        String text = txt.getText();
                        txt.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_CSS);
                        txt.setText(text);
                    }
                    else if (selected.equals("Java")) {
                        String text = txt.getText();
                        txt.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
                        txt.setText(text);
                    }
                }
            });

            
            SyntaxPanel.add(cmbSyntax);

  //          bottom.add(SyntaxPanel, BorderLayout.WEST);
  //          add(bottom,BorderLayout.SOUTH);
            
            txt.setBackground(Color.WHITE); // JEditorPanel
            //frmEditor = new RSyntaxTextArea();
            txt.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_NONE);
            
            txt.setText(text);
            sbLabel.setText(path);
            
            
            add(scrollTXT);
        }
    } 
    static class Vergleich extends JFrame{
        String text1, text2;
        
        RSyntaxTextArea txt1 = new RSyntaxTextArea();
        RSyntaxTextArea txt2 = new RSyntaxTextArea();
        JTextArea txtErg = new JTextArea(15,5);
        /* App Path */
        final JLabel sbLabel = new JLabel();
        final RTextScrollPane scTXT1 = new RTextScrollPane(txt1);
        final RTextScrollPane scTXT2 = new RTextScrollPane(txt2);
        final JScrollPane scTXTERG = new JScrollPane(txtErg);
        
        Vergleich(String text1, String text2){
            text1 = text1;
            text2 = text2;
            
            
            text1 = Text1;
            text2 = Text2;
            
            txt1.setText(text1);
            txt2.setText(text2);
            
            setTitle("Dokumente vergleichen");
            setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            
            setLayout(new BorderLayout());
            JPanel pnlNORTH = new JPanel();
            pnlNORTH.setLayout(new BorderLayout());
                /* Info */
            JPanel pnlInfo = new JPanel();
            pnlInfo.setLayout(new BorderLayout());
            pnlInfo.setBackground(Color.WHITE);
            JEditorPane txtInfo = new JEditorPane();
                txtInfo.setContentType("text/html");
            txtInfo.setEditable(false);
            txtInfo.setBackground(Color.WHITE);
            txtInfo.setText("<b>Dokumente vergleichen</b><br>Hier kannst du zwei Dokumente miteinander vergleichen.<br><br>");
            pnlInfo.add(txtInfo,BorderLayout.PAGE_START);
            pnlNORTH.add(pnlInfo,BorderLayout.NORTH);
            
            JToolBar tbMenu = new JToolBar();
            Icon SuchenIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
            JButton btnStart = new JButton(SuchenIcon);
            btnStart.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ev){
                    String line1;
                    String array1[] =  new String[30000];

                    try{
                            FileReader fread1 = new FileReader(Pfad1); 
                            BufferedReader in1 = new BufferedReader(fread1); 

                            for(int i = 0;(line1 = in1.readLine())!=null; i++){
                                    array1[i] = line1; 			
                            }
                    }
                    catch(IOException e){
                            // Fehlerbehandlung
                    }



                    // 2. Datei in array einlesen
                    String line2;
                    String array2[] =  new String[30000];

                    try{
                            FileReader fread2 = new FileReader(Pfad2); 
                            BufferedReader in2 = new BufferedReader(fread2); 

                            for(int j = 0;(line2 = in2.readLine())!=null; j++){
                                    array2[j] = line2; 			
                            }
                    }
                    catch(IOException e){
                            // Fehlerbehandlung
                    }



                    // Datei 1 & 2 vergleichen
                    int diff = 0;

                    //System.out.println("Unterschiede \n");
                    try{
                        for(int k = 0;k<=Pfad2.length();k++){
                            if(!array1[k].equals(array2[k])){
                                int zeile=k;
                                zeile+=1;
                                txtErg.append("Zeile " + zeile + " : " + array2[k] + "\n");
                                diff++;
                            }
                        }
                    }
                    catch(NullPointerException e1){
                            // Fehlerbehandlung
                    }

                    txtErg.append("\n" + diff + " ungleiche Zeilen gefunden!\n");
                }
            });
            
            tbMenu.add(btnStart);
            pnlNORTH.add(tbMenu,BorderLayout.CENTER);
            
            add(pnlNORTH,BorderLayout.NORTH);
            
            JPanel pnlFiles = new JPanel();
            pnlFiles.setLayout(new GridLayout(0,2));
            pnlFiles.add(scTXT1,BorderLayout.WEST);
            pnlFiles.add(scTXT2,BorderLayout.EAST);
            
            add(pnlFiles,BorderLayout.CENTER);
            add(scTXTERG,BorderLayout.SOUTH);
            
            setSize(600,600);
            setVisible(true);
        }
    } 
    static class UML{
 
        UML(){
            JFrame frmUML = new JFrame();
            frmUML.setTitle("UML");
            frmUML.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            
            frmUML.setLayout(new BorderLayout());
            
            JToolBar tbMenu = new JToolBar();
            frmUML.add(tbMenu,BorderLayout.NORTH);
            
            JButton btnDraw = new JButton("?");
            
            tbMenu.add(btnDraw);
            
            // --------- Zeichenfläche ---------------------
            JPanel pnlDraw = new JPanel();
            pnlDraw.setBackground(Color.WHITE);
            frmUML.add(pnlDraw,BorderLayout.CENTER);
            
            frmUML.setSize(600,600);
            frmUML.setVisible(true);
        }
    } 
    /*
     * Die Klassen FileSystemModel und Project, dienen zu Anzeige der Projekte 
     * aus dem Ordner "LuziensProjects".
     * Dieser wird im Editor auf der linken Seite neben dem TextFeld angezeigt.
     * Leider aktualisiert sich die Projektanzeige nicht automatisch (erst ab v0.4)
     */
    static class FileSystemModel implements TreeModel {

        private File root;
        private Vector listeners = new Vector();

        public FileSystemModel(File rootDirectory) {
            root = rootDirectory;
        }

        public Object getRoot() {
            return root;
        }

        public Object getChild(Object parent, int index) {
            File directory = (File) parent;
            String[] children = directory.list();
            return new TreeFile(directory, children[index]);
        }

        public int getChildCount(Object parent) {
            File file = (File) parent;
            if (file.isDirectory()) {
                String[] fileList = file.list();
                if (fileList != null) {
                    return file.list().length;
                }
            }
            return 0;
        }

        public boolean isLeaf(Object node) {
            File file = (File) node;
            return file.isFile();
        }

        public int getIndexOfChild(Object parent, Object child) {
            File directory = (File) parent;
            File file = (File) child;
            String[] children = directory.list();
            for (int i = 0; i < children.length; i++) {
                if (file.getName().equals(children[i])) {
                    return i;
                }
            }
            return -1;

        }

        public void valueForPathChanged(TreePath path, Object value) {
            File oldFile = (File) path.getLastPathComponent();
            String fileParentPath = oldFile.getParent();
            String newFileName = (String) value;
            File targetFile = new File(fileParentPath, newFileName);
            oldFile.renameTo(targetFile);
            File parent = new File(fileParentPath);
            int[] changedChildrenIndices = {getIndexOfChild(parent, targetFile)};
            Object[] changedChildren = {targetFile};
            fireTreeNodesChanged(path.getParentPath(), changedChildrenIndices, changedChildren);

        }

        private void fireTreeNodesChanged(TreePath parentPath, int[] indices, Object[] children) {
            TreeModelEvent event = new TreeModelEvent(this, parentPath, indices, children);
            Iterator iterator = listeners.iterator();
            TreeModelListener listener = null;
            while (iterator.hasNext()) {
                listener = (TreeModelListener) iterator.next();
                listener.treeNodesChanged(event);
            }
        }

        public void addTreeModelListener(TreeModelListener listener) {
            listeners.add(listener);
        }

        public void removeTreeModelListener(TreeModelListener listener) {
            listeners.remove(listener);
        }
        

        private class TreeFile extends File {

            public TreeFile(File parent, String child) {
                super(parent, child);
            }

            public String toString() {
                return getName();
            }
        }
    }
     static class FileSystemModelXXX implements TreeModel {

        private File root;
        private Vector listeners = new Vector();

        public FileSystemModelXXX(File rootDirectory) {
            root = rootDirectory;
        }

        public Object getRoot() {
            return root;
        }

        public Object getChild(Object parent, int index) {
            File directory = (File) parent;
            String[] children = directory.list();
            return new FileSystemModelXXX.TreeFile(directory, children[index]);
        }

        public int getChildCount(Object parent) {
            File file = (File) parent;
            if (file.isDirectory()) {
                String[] fileList = file.list();
                if (fileList != null) {
                    return file.list().length;
                }
            }
            return 0;
        }

        public boolean isLeaf(Object node) {
            File file = (File) node;
            return file.isFile();
        }

        public int getIndexOfChild(Object parent, Object child) {
            File directory = (File) parent;
            File file = (File) child;
            String[] children = directory.list();
            for (int i = 0; i < children.length; i++) {
                if (file.getName().equals(children[i])) {
                    return i;
                }
            }
            return -1;

        }

        public void valueForPathChanged(TreePath path, Object value) {
            File oldFile = (File) path.getLastPathComponent();
            String fileParentPath = oldFile.getParent();
            String newFileName = (String) value;
            File targetFile = new File(fileParentPath, newFileName);
            oldFile.renameTo(targetFile);
            File parent = new File(fileParentPath);
            int[] changedChildrenIndices = {getIndexOfChild(parent, targetFile)};
            Object[] changedChildren = {targetFile};
            fireTreeNodesChanged(path.getParentPath(), changedChildrenIndices, changedChildren);

        }

        private void fireTreeNodesChanged(TreePath parentPath, int[] indices, Object[] children) {
            TreeModelEvent event = new TreeModelEvent(this, parentPath, indices, children);
            Iterator iterator = listeners.iterator();
            TreeModelListener listener = null;
            while (iterator.hasNext()) {
                listener = (TreeModelListener) iterator.next();
                listener.treeNodesChanged(event);
            }
        }

        public void addTreeModelListener(TreeModelListener listener) {
            listeners.add(listener);
        }

        public void removeTreeModelListener(TreeModelListener listener) {
            listeners.remove(listener);
        }
        

        private class TreeFile extends File {

            public TreeFile(File parent, String child) {
                super(parent, child);
            }

            public String toString() {
                return getName();
            }
        }
    }

    static class Project extends JPanel {

        //private JTree fileTree;
        String UserHome = System.getProperty("user.home");
        String LuzienPath = "/LuziensProjects/";
        String directory = UserHome + LuzienPath;
        FileSystemModel fileSystemModel = new FileSystemModel(new File(directory));

        public Project() {

            this.setBackground(Color.WHITE);

            fileTree = new JTree(fileSystemModel);
            
            fileTree.setRootVisible(false);


            fileTree.setEditable(true);
            ProjectScroll = new JScrollPane(fileTree,
                    ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

            
            fileTree.addTreeSelectionListener(new TreeSelectionListener() {
                public void valueChanged(TreeSelectionEvent event) {
                    File file = (File) fileTree.getLastSelectedPathComponent();
                    if (file.isDirectory()) {
                    } else {
                        String path = String.valueOf(file.getPath());

                        String text = "";
                        try {
                            StringBuffer buffer = new StringBuffer();
                            FileReader in = new FileReader(path);
                            for (int n; (n = in.read()) != -1; buffer.append((char) n));
                            in.close();

                            text = buffer.toString();
                        } catch (FileNotFoundException e) {
                        } catch (IOException e) {
                        }

                        if(file.getName().endsWith("html")|| file.getName().endsWith("HTML")|| 
                                file.getName().endsWith("php")|| file.getName().endsWith("PHP")||
                                file.getName().endsWith("css")|| file.getName().endsWith("CSS")){
                                    textTab.addTab(file.getName(), new LuziensEditor.HTMLEditor(text, path));
                        }else if(file.getName().endsWith("java")|| file.getName().endsWith("JAVA")){
                            try {
                                textTab.addTab(file.getName(), new LuziensEditor.JavaEditor(text, path));
                            } catch (IOException ex) {
                                Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }else if(file.getName().endsWith(".c")|| file.getName().endsWith(".C")/* || 
                                file.getName().endsWith("h")|| file.getName().endsWith("H"))*/){
                                    textTab.addTab(file.getName(), new LuziensEditor.CPPEditor(text, path));
                        }else if(file.getName().endsWith(".cpp")|| file.getName().endsWith(".CPP") || 
                                file.getName().endsWith(".hpp")|| file.getName().endsWith(".HPP")||
                                file.getName().endsWith(".class")){
                                    textTab.addTab(file.getName(), new LuziensEditor.CPPEditor(text, path));
                        }else if(file.getName().endsWith(".s")|| file.getName().endsWith(".S")){
                                    textTab.addTab(file.getName(), new LuziensEditor.ASMEditor(text, path));
                        }else if(file.getName().endsWith(".h")|| file.getName().endsWith(".H")){
                                    textTab.addTab(file.getName(), new LuziensEditor.CPPEditor(text, path));
                        }
                        else{
                            textTab.addTab(file.getName(), new LuziensEditor.Editor(text, path));
                        }
                        
                        int i = textTab.getTabCount() - 1;
                        textTab.setTabComponentAt(i, new ButtonTabComponent(textTab));
                    }

                }
            });
            this.setLayout(new BorderLayout());
            ProjectScroll.setBorder(new EmptyBorder(0, 0, 0, 0));
            this.add(ProjectScroll, BorderLayout.NORTH);
        }
    }
    static class Timeline_tree extends JPanel {

        //private JTree fileTree;
        String UserHome = System.getProperty("user.home");
        String LuzienPath = "/timeline/";
        String directory = UserHome + LuzienPath;
        FileSystemModelXXX fileSystemModelxxx = new FileSystemModelXXX(new File(directory));

        public Timeline_tree() {

            this.setBackground(Color.WHITE);

            fileTreeXXX = new JTree(fileSystemModelxxx);
            
            fileTreeXXX.setRootVisible(false);


            fileTreeXXX.setEditable(true);
            ProjectScroll = new JScrollPane(fileTreeXXX,
                    ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                    ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

            
            fileTreeXXX.addTreeSelectionListener(new TreeSelectionListener() {
                public void valueChanged(TreeSelectionEvent event) {
                    File file = (File) fileTreeXXX.getLastSelectedPathComponent();
                    if (file.isDirectory()) {
                    } else {
                        String path = String.valueOf(file.getPath());

                        String text = "";
                        try {
                            StringBuffer buffer = new StringBuffer();
                            FileReader in = new FileReader(path);
                            for (int n; (n = in.read()) != -1; buffer.append((char) n));
                            in.close();

                            text = buffer.toString();
                        } catch (FileNotFoundException e) {
                        } catch (IOException e) {
                        }

                        if(file.getName().endsWith("html")|| file.getName().endsWith("HTML")|| 
                                file.getName().endsWith("php")|| file.getName().endsWith("PHP")||
                                file.getName().endsWith("css")|| file.getName().endsWith("CSS")){
                                    textTab.addTab(file.getName(), new LuziensEditor.HTMLEditor(text, path));
                        }else if(file.getName().endsWith("java")|| file.getName().endsWith("JAVA")){
                            try {
                                textTab.addTab(file.getName(), new LuziensEditor.JavaEditor(text, path));
                            } catch (IOException ex) {
                                Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }else if(file.getName().endsWith(".c")|| file.getName().endsWith(".C")/* || 
                                file.getName().endsWith("h")|| file.getName().endsWith("H"))*/){
                                    textTab.addTab(file.getName(), new LuziensEditor.CPPEditor(text, path));
                        }else if(file.getName().endsWith(".cpp")|| file.getName().endsWith(".CPP") || 
                                file.getName().endsWith(".hpp")|| file.getName().endsWith(".HPP")||
                                file.getName().endsWith(".class")){
                                    textTab.addTab(file.getName(), new LuziensEditor.CPPEditor(text, path));
                        }else if(file.getName().endsWith(".s")|| file.getName().endsWith(".S")){
                                    textTab.addTab(file.getName(), new LuziensEditor.ASMEditor(text, path));
                        }else if(file.getName().endsWith(".h")|| file.getName().endsWith(".H")){
                                    textTab.addTab(file.getName(), new LuziensEditor.CPPEditor(text, path));
                        }
                        else{
                            textTab.addTab(file.getName(), new LuziensEditor.Editor(text, path));
                        }
                        
                        int i = textTab.getTabCount() - 1;
                        textTab.setTabComponentAt(i, new ButtonTabComponent(textTab));
                    }

                }
            });
            this.setLayout(new BorderLayout());
            ProjectScroll.setBorder(new EmptyBorder(0, 0, 0, 0));
            this.add(ProjectScroll, BorderLayout.NORTH);
        }
    }
    static JTabbedPane textTab = new JTabbedPane(); // texttab from editor
    
    
    /*
     * Im frmMain wird das Layout des Editors festgelegt.
     * Desweiteren findet man hier auch die JMenuBar.
     */
    static class frmMain {
    boolean visible;
    
    frmMain(boolean visible) throws IOException, InterruptedException{
        this.visible = visible;
        
        /* build Frame */
        final JFrame frmMain = new JFrame("Luzien's Editor v0.4");
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmMain.setLayout(new BorderLayout());


        /* ------------------------------------------------------------------------------------------- */

        /* Menubar */
        JMenuBar mnuBar = new JMenuBar();
        frmMain.setJMenuBar(mnuBar);

        // File
        ImageIcon MenuNewFileIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/new.png"));
        ImageIcon MenuNewProjectIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/project.png"));
        ImageIcon MenuExitIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/exit.png"));
        ImageIcon MenuImportIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/import.png"));
        
        JMenu mnuFile = new JMenu("Datei");
        JMenuItem mnuNewFile = new JMenuItem("Neue Datei", MenuNewFileIcon);
        JMenuItem mnuNewProject = new JMenuItem("Neues Projekt", MenuNewProjectIcon);
        JMenuItem mnuImport = new JMenuItem("Projekt importieren",MenuImportIcon);
        JMenuItem mnuExit = new JMenuItem("Luzien beenden", MenuExitIcon);
        
        mnuBar.add(mnuFile);
        mnuFile.add(mnuNewProject);
        mnuFile.add(mnuNewFile);
        mnuFile.addSeparator();
        mnuFile.add(mnuImport);
        mnuFile.addSeparator();
        mnuFile.add(mnuExit);
       
      mnuImport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev01) {
                ProjectImport ProjectIn = new ProjectImport(true);
            }
        });  
        
      mnuNewProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev01) {
                LuziensEditor.NewProject addProject = new LuziensEditor.NewProject(true);
            }
        });
      
      mnuNewFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev01) {
                LuziensEditor.NewFile addFile = new LuziensEditor.NewFile(true);
            }
        });
        
       mnuExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev01) {
                System.exit(0);
            }
        });
       
       // Java
        //ImageIcon MenuXAMPPIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/xampp.png"));
        
        JMenu Java = new JMenu("Java");
        mnuBar.add(Java);
        
        JMenuItem LnF = new JMenuItem("LookAndFeel");
        JMenuItem Image = new JMenuItem("Bild einfügen");
        JMenuItem JavaDocs6 = new JMenuItem("JavaDocs v1.6");
        JMenuItem JavaDocs7 = new JMenuItem("JavaDocs v1.7");
        Java.add(LnF);
        Java.add(Image);
        Java.addSeparator();
        Java.add(JavaDocs6);
        Java.add(JavaDocs7);
        
        // JavaDocs
        JavaDocs6.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ae){
               String path = "http://docs.oracle.com/javase/6/docs/api/";


                Desktop desktop = Desktop.getDesktop();
                try {
                    desktop.browse(new URL(path).toURI());
                } catch (IOException ex) {
                    Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);



                } catch (URISyntaxException ex) {
                    Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                } 
            }
        });
        
        JavaDocs7.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ae){
               String path = "http://docs.oracle.com/javase/7/docs/api/";


                Desktop desktop = Desktop.getDesktop();
                try {
                    desktop.browse(new URL(path).toURI());
                } catch (IOException ex) {
                    Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);



                } catch (URISyntaxException ex) {
                    Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                } 
            }
        });
        
        // Look and Feel
        LnF.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ae){
                JavaLook lnf = new JavaLook(true);
            }
        });
        
        // Image
        Image.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ae){
                JavaIcon Image = new JavaIcon(true);
            }
        });
        
        // Source
        ImageIcon MenuCIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/c.png"));
        ImageIcon MenuCppIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/cpp.png"));
        
        JMenu mnuSource = new JMenu("C/C++");
        JMenu mnuQuickCode = new JMenu("QuickCode");
        JMenuItem mnuC = new JMenuItem("C",MenuCIcon);
        JMenuItem mnuCpp = new JMenuItem("C++",MenuCppIcon);
        JMenuItem mnuGAS = new JMenuItem("Inline Assembler");
        mnuBar.add(mnuSource);
        mnuSource.add(mnuQuickCode);
        mnuSource.add(mnuGAS);
        mnuQuickCode.add(mnuC);
        mnuQuickCode.add(mnuCpp);
        
        // GAS
        mnuGAS.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ae){
                ASMin showGAS = new ASMin();
            }
        });

        // QuickCheck C
        mnuC.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ae){
                LuziensEditor.QuickCheck qcC = new LuziensEditor.QuickCheck();
                qcC.C(true);
            }
        });
        
        // QuickCheck C++
        mnuCpp.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ae){
                LuziensEditor.QuickCheck qcCpp = new LuziensEditor.QuickCheck();
                qcCpp.Cpp(true);
            }
        });
        
        // XAMPP
        ImageIcon MenuXAMPPIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/xampp.png"));
        
        JMenu web = new JMenu("Web");
        JMenu XAMPP = new JMenu("XAMPP");
        JMenuItem WebColor = new JMenuItem("Farben");
        web.add(XAMPP);
        web.add(WebColor);
        mnuBar.add(web);
        
        // Farben
        WebColor.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ae){
                WebColor showcolor = new WebColor(true);
                
            }
        });
        
        JMenuItem mnuXAMPP_start = new JMenuItem("Start",MenuXAMPPIcon);
        JMenuItem mnuXAMPP_stop = new JMenuItem("Stop",MenuXAMPPIcon);
        JMenuItem mnuXAMPP_restart = new JMenuItem("Neustart",MenuXAMPPIcon);
        XAMPP.add(mnuXAMPP_start);
        XAMPP.add(mnuXAMPP_stop);
        XAMPP.add(mnuXAMPP_restart);
        
        // start
        mnuXAMPP_start.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ae){
                btnRun.setText("Stop");
                try {
                        run(ConArgs,"","start","xampp");
                        XAMPP_RUN = "start";
                        
                    } catch (IOException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                
            }
        });
        
        // stop
        mnuXAMPP_stop.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ae){
                btnRun.setText("Start");
                try {
                        run(ConArgs,"","stop","xampp");
                        XAMPP_RUN = "stop";
                    } catch (IOException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                
            }
        });
        
        // restart
        mnuXAMPP_restart.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ae){
                btnRun.setText("Stop");
                try {
                        run(ConArgs,"","restart","xampp");
                        XAMPP_RUN = "start";
                    } catch (IOException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                
            }
        });
        
        // Script
        //ImageIcon MenuChmodIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/xampp.png"));
        
        JMenu Script = new JMenu("Script");
        mnuBar.add(Script);
        
        JMenuItem Script_Chmod = new JMenuItem("Chmod"/*,MenuChmodIcon*/);
        Script.add(Script_Chmod);
        
        // chmod
        Script_Chmod.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ae){
                Chmod chmod = new Chmod(true);
            }
        });
        

        
        // Project
        //ImageIcon MenuChmodIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/xampp.png"));
        
        JMenu Project = new JMenu("Tools");
        mnuBar.add(Project);
        
        JMenuItem Timeline = new JMenuItem("Timeline"/*,MenuChmodIcon*/);
        JMenuItem Debpackage = new JMenuItem("Ein *.deb Paket erstellen"/*,MenuChmodIcon*/);
        Project.add(Timeline);
        Project.add(Debpackage);
        
        Timeline.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ev){
                
                timelineGui tlGUI = new timelineGui(true);
            }
        });
        

        Debpackage.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent ev){
                if(os.equals("Linux")){
                    debpackageGUI showGUI = new debpackageGUI(true);
                }
                else{
                    JOptionPane.showMessageDialog(null, "Dein Betriebssystem wird nicht unterstützt!","Luzien's Editor v0.4",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        
        // UML
        //ImageIcon MenuChmodIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/xampp.png"));
        
//        JMenu mnuUML = new JMenu("UML");
//        mnuBar.add(mnuUML);
//        JMenuItem mnuiZeichnen = new JMenuItem("Klassendiagramm erstellen");
//        mnuUML.add(mnuiZeichnen);
//        
//        mnuiZeichnen.addActionListener(new ActionListener(){
//            @Override public void actionPerformed(ActionEvent ev111111){
//                UML showUML = new UML();
//            }
//        });

        
        /* ------------------------------------------------------------------------------------------- */



        /* Frame ToolBar */
        JToolBar frmMenu = new JToolBar();
        frmMain.add(frmMenu, BorderLayout.NORTH);


        /* Frame ToolBar -> New File*/
        ImageIcon ToolbarNewIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/big/new.png"));
        JButton btnNew = new JButton();
        btnNew.setLayout ( new  BorderLayout ());
        JLabel lblIcon = new  JLabel (ToolbarNewIcon);
        JLabel lblName = new  JLabel ("Neue Datei");
        btnNew.add (lblIcon,BorderLayout.NORTH);
        btnNew.add (lblName,BorderLayout.CENTER);
        btnNew.setBorderPainted(false);
        btnNew.setOpaque(false);
        btnNew.setToolTipText("Neue datei");
        frmMenu.add(btnNew);
        btnNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev01) {

                LuziensEditor.NewFile addFile = new LuziensEditor.NewFile(true);
            }
        });

        /* Frame ToolBar -> New Project*/
        ImageIcon ToolbarNewProjectIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/big/project.png"));
        JButton btnNewProject = new JButton();
        btnNewProject.setLayout(new BorderLayout());
        JLabel lblIcon1 = new  JLabel (ToolbarNewProjectIcon);
        JLabel lblName1 = new  JLabel ("Neues Projekt");
        btnNewProject.add (lblIcon1,BorderLayout.NORTH);
        btnNewProject.add (lblName1,BorderLayout.CENTER);
        btnNewProject.setBorderPainted(false);
        btnNewProject.setOpaque(false);
        btnNewProject.setToolTipText("Neues Projekt");
        frmMenu.add(btnNewProject);
        btnNewProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev01) {
                LuziensEditor.NewProject addProject = new LuziensEditor.NewProject(true);
            }
        });
        
         /* Frame ToolBar -> QuickCode C */
        ImageIcon QCcIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/big/c.png"));
        JButton btnC = new JButton();
        btnC.setLayout(new BorderLayout());
        JLabel lblIcon2 = new  JLabel (QCcIcon);
        JLabel lblName2 = new  JLabel ("QuickCode C");
        btnC.add (lblIcon2,BorderLayout.NORTH);
        btnC.add (lblName2,BorderLayout.CENTER);
        btnC.setBorderPainted(false);
        btnC.setOpaque(false);
        btnC.setToolTipText("QuickCode C");
        frmMenu.addSeparator();
        frmMenu.add(btnC);
        btnC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev01) {
                LuziensEditor.QuickCheck qcC = new LuziensEditor.QuickCheck();
                qcC.C(true);
            }
        });
        
         /* Frame ToolBar -> QuickCode Cpp */
        ImageIcon QCcppIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/big/cpp.png"));
        JButton btnCpp = new JButton();
        btnCpp.setLayout(new BorderLayout());
        JLabel lblIcon3 = new  JLabel (QCcppIcon);
        JLabel lblName3 = new  JLabel ("QuickCode C++");
        btnCpp.add (lblIcon3,BorderLayout.NORTH);
        btnCpp.add (lblName3,BorderLayout.CENTER);
        btnCpp.setBorderPainted(false);
        btnCpp.setOpaque(false);
        btnCpp.setToolTipText("QuickCode C++");
        //frmMenu.addSeparator();
        frmMenu.add(btnCpp);
        frmMenu.addSeparator();
        btnCpp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev01) {
                LuziensEditor.QuickCheck qcCpp = new LuziensEditor.QuickCheck();
                qcCpp.Cpp(true);
            }
        });
        
         /* Frame ToolBar -> Notizen */
        ImageIcon NotizenIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/big/note.png"));
        JButton btnNote = new JButton();
        btnNote.setLayout(new BorderLayout());
        JLabel lblIcon30 = new  JLabel (NotizenIcon);
        JLabel lblName30 = new  JLabel ("Notizen");
        btnNote.add (lblIcon30,BorderLayout.NORTH);
        btnNote.add (lblName30,BorderLayout.CENTER);
        btnNote.setBorderPainted(false);
        btnNote.setOpaque(false);
        btnNote.setToolTipText("Notizen anzeigen und erstellen");
        //frmMenu.addSeparator();
        frmMenu.add(btnNote);
        //frmMenu.addSeparator();
        btnNote.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev01) {
                Notizen zeigeNotizen = new Notizen();
            }
        });
        
        ImageIcon vergleichIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/big/vergleich.png"));
        JButton btnComp = new JButton();
        btnComp.setLayout(new BorderLayout());
        JLabel lblIcon4 = new  JLabel (vergleichIcon);
        JLabel lblName4 = new  JLabel ("Vergleichen");
        btnComp.add (lblIcon4,BorderLayout.NORTH);
        btnComp.add (lblName4,BorderLayout.CENTER);
        btnComp.setBorderPainted(false);
        btnComp.setOpaque(false);
        btnComp.setToolTipText("Dateien vergleichen");
        frmMenu.add(btnComp);
        btnComp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev0100) {
                Vergleich comp = new Vergleich(Text1,Text2);
 
            }
        });
        
         /* Frame ToolBar -> Info */
        ImageIcon InfoIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/big/info.png"));
        JButton btnInfo = new JButton();
        btnInfo.setLayout(new BorderLayout());
        JLabel lblIcon5 = new  JLabel (InfoIcon);
        JLabel lblName5 = new  JLabel ("Informationen");
        btnInfo.add (lblIcon5,BorderLayout.NORTH);
        btnInfo.add (lblName5,BorderLayout.CENTER);
        btnInfo.setBorderPainted(false);
        btnInfo.setOpaque(false);
        btnInfo.setToolTipText("Über...");
        frmMenu.addSeparator();
        frmMenu.add(btnInfo);
        btnInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev01) {
                JOptionPane.showMessageDialog(null, "(c) 2013 Markus Schnittker\nnmschnittker@gmail.com","Luzien's Editor v0.4",JOptionPane.INFORMATION_MESSAGE);
            }
        });

        /* Frame ToolBar -> Exit */
        ImageIcon ToolbarExitIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/big/exit.png"));
        JButton btnExit = new JButton();
        btnExit.setLayout(new BorderLayout());
        JLabel lblIcon6 = new  JLabel (ToolbarExitIcon);
        JLabel lblName6 = new  JLabel ("Ende");
        btnExit.add (lblIcon6,BorderLayout.NORTH);
        btnExit.add (lblName6,BorderLayout.CENTER);
        btnExit.setBorderPainted(false);
        btnExit.setOpaque(false);
        btnExit.setToolTipText("Luzien beenden");
        //frmMenu.addSeparator();
        frmMenu.add(btnExit);
        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev01) {
                System.exit(0);
            }
        });

        /* ------------------------------------------------------------------------------------------- */




        String newName = "Ohne Name";
        String newText = "";
        String newPath = "New Document";


//        JPanel pnlStart = new JPanel();
//        pnlStart.setLayout(new BorderLayout());
//        JEditorPane txtStart = new JEditorPane();
//        JScrollPane scStart = new JScrollPane(txtStart);
//        
//        txtStart.setContentType("text/html");
//        //txtStart.setPage(new URL(""));
//        txtStart.setText("");
//        pnlStart.add(scStart,BorderLayout.CENTER);
//        pnlStart.setVisible(true);

        textTab.addTab(newName, new Editor("",newPath));
        //int i = textTab.getTabCount();
        textTab.setTabComponentAt(0, new ButtonTabComponent(textTab));

        /* Tags */
        

        JTabbedPane tagsTab = new JTabbedPane();



        // GROUND
        JSplitPane groundSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                textTab, ConsoleTab);

        groundSplit.setOneTouchExpandable(true);
        groundSplit.setDividerLocation(400);

        // txtCpponsole ------------------------------------------------------------------------------
        JPanel pnlCompiler = new JPanel();
        pnlCompiler.setLayout(new BorderLayout());
        
        JToolBar tbCompiler = new JToolBar();
        
        Icon btnCopyIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/copy.png"));
        JButton btnCopy = new JButton(btnCopyIcon);
        btnCopy.setBorderPainted(false);
        btnCopy.setOpaque(false);
        btnCopy.setToolTipText("Pfad in die Zwischenablage kopieren");
        btnCopy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e07) {
                String Path = Console_Path;
                StringSelection selection = new StringSelection(Path);
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection, selection);
            }
        });
        
        Icon btnSHIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/sh.png"));
        JButton btnSH = new JButton(btnSHIcon);
        btnSH.setBorderPainted(false);
        btnSH.setOpaque(false);
        btnSH.setToolTipText("Bash Datei bearbeiten");
        btnSH.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e07) {
                String Path = Console_Path;
                try {
                    EditSH edit = new EditSH(true,sh_path);
                } catch (IOException ex) {
                    Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        Icon btnManIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/java.png"));
        JButton btnMan = new JButton(btnManIcon);
        btnMan.setBorderPainted(false);
        btnMan.setOpaque(false);
        btnMan.setToolTipText("Manifest Datei bearbeiten");
        btnMan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e07) {
                //String Path = man_path;
                try {
                    EditMan editman = new EditMan(true,man_path);
                } catch (IOException ex) {
                    Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        

        tbCompiler.add(btnCopy);
        tbCompiler.add(btnSH);
        tbCompiler.add(btnMan);
        
        JScrollPane spConsole = new JScrollPane(txtCpponsole);
        
//        txtCpponsole.setBackground(Color.BLACK);
//        txtCpponsole.setForeground(Color.WHITE);
//        txtCpponsole.setFont(new Font("Courier New", Font.PLAIN, 13));
        txtCpponsole.setEditable(false);
        
        pnlCompiler.add(tbCompiler,BorderLayout.NORTH);
        pnlCompiler.add(spConsole,BorderLayout.CENTER);
        //-------------------------------------------------------------------------------------------

        // txtDebugger ------------------------------------------------------------------------------
        JPanel pnlDebugger = new JPanel();
        pnlDebugger.setLayout(new BorderLayout());
        
        JToolBar tbDebugger = new JToolBar();
        
        Icon btnDebuggerCopyIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/copy.png"));
        JButton btnDebuggerCopy = new JButton(btnDebuggerCopyIcon);
        btnDebuggerCopy.setBorderPainted(false);
        btnDebuggerCopy.setOpaque(false);
        btnDebuggerCopy.setToolTipText("Pfad in die Zwischenablage kopieren");
        btnDebuggerCopy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e07) {
                String Path = Debugger_Path;
                StringSelection selection = new StringSelection(Path);
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection, selection);
            }
        });
        

        tbDebugger.add(btnDebuggerCopy);
        
        JScrollPane spDebugger = new JScrollPane(txtDebugger);
        
//        txtDebugger.setBackground(Color.BLACK);
//        txtDebugger.setForeground(Color.WHITE);
//        txtDebugger.setFont(new Font("Courier New", Font.PLAIN, 13));
        txtDebugger.setEditable(false);
        
        pnlDebugger.add(tbDebugger,BorderLayout.NORTH);
        pnlDebugger.add(spDebugger,BorderLayout.CENTER);
        //-------------------------------------------------------------------------------------------

        JPanel pnlKlassen = new JPanel();
        pnlKlassen.setLayout(new GridLayout(1,3));
        
        txtVar = new JTextArea(40,30);
        txtKla = new JTextArea(40,30);
        //txtFun = new JTextArea(40,30);
        
        txtVar.setText("Variabeln und Funktionen  \n");
        txtKla.setText("Klassen  \n");
        //txtFun.setText("Funktionen  \n");
        
        txtVar.setEditable(false);
        txtKla.setEditable(false);
        //txtFun.setEditable(false);
        
//        txtVar.setBackground(Color.BLACK);
//        txtVar.setForeground(Color.WHITE);
//        txtVar.setFont(new Font("Courier New", Font.PLAIN, 13));
//        
//        txtKla.setBackground(Color.BLACK);
//        txtKla.setForeground(Color.WHITE);
//        txtKla.setFont(new Font("Courier New", Font.PLAIN, 13));
//        
//        txtFun.setBackground(Color.BLACK);
//        txtFun.setForeground(Color.WHITE);
//        txtFun.setFont(new Font("Courier New", Font.PLAIN, 13));
        
        JScrollPane scVar = new JScrollPane(txtVar);
        JScrollPane scKla = new JScrollPane(txtKla);
        //JScrollPane scFun = new JScrollPane(txtFun);
        
        pnlKlassen.add(scVar);
        pnlKlassen.add(scKla);
        //pnlKlassen.add(scFun);
        
        
        /* ConsoleTab */
        
        Commands commands = new Commands();

        Icon konsoleIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/ConCopy.png"));
        Icon debugIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/bug.png"));
        Icon watchIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/watch.png"));
        //Icon luzienIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/ConCopy.png"));
        ConsoleTab.addTab("Ausgabe",konsoleIcon, pnlCompiler);
        ConsoleTab.addTab("Debugger",debugIcon, pnlDebugger);
        ConsoleTab.addTab("Klassen",watchIcon, pnlKlassen);
        //ConsoleTab.addTab("",luzienIcon, commands);

        
        textTab.addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    txtVar.setText("Variabeln und Funktionen\n");
                    txtKla.setText("Klassen\n");
                    //txtFun.setText("Funktionen\n");
                }
            });
        
        JPanel pnlXAMPP = new JPanel();
        
        pnlXAMPP.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        JLabel lblInstall = new JLabel();
        btnRun = new JButton();
        
        if(os.equals("MacOS")){
            File file = new File("/Applications/XAMPP/xamppfiles/");
            if(file.exists()){
                lblInstall.setText("");
                btnRun.setEnabled(true);
            }
            else{
                lblInstall.setText("XAMPP ist nicht installiert");
                btnRun.setEnabled(false);
            }      
        }
        else if(os.equals("Linux")){
            File file = new File("/opt/lampp/lampp/");
            if(file.exists()){
                lblInstall.setText("");
                btnRun.setEnabled(true);
            }
            else{
                lblInstall.setText("XAMPP ist nicht installiert");
                btnRun.setEnabled(false);
            }      
        }
        
        c.gridx = 0;
        c.gridy = 0;
        c.fill = 1;
        
        pnlXAMPP.add(lblInstall,c);
        
        JPanel pnlRun = new JPanel();
        pnlRun.setLayout(new FlowLayout());
        JLabel lblStatic = new JLabel("Status : ");
        lblRun = new JLabel();
        
        new Timer(3000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(XAMPP_RUN.equals("start")){
                    lblRun.setText("gestartet");
                    lblRun.setOpaque(true);
                    lblRun.setBackground(Color.GREEN);
                    btnRun.setEnabled(true);
                    btnRun.setText("stop");
                    btnRun.addActionListener(new ActionListener(){
                        @Override public void actionPerformed(ActionEvent ae){
                            XAMPP_RUN = "stop";
                            try {
                                run(ConArgs,"","stop","xampp");
                            } catch (IOException ex) {
                                Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    });
                }
                else if(XAMPP_RUN.equals("stop")){
                    lblRun.setText("gestoppt");
                    lblRun.setOpaque(true);
                    lblRun.setBackground(Color.RED);
                    btnRun.setEnabled(true);
                    btnRun.setText("start");
                    btnRun.addActionListener(new ActionListener(){
                        @Override public void actionPerformed(ActionEvent ae){
                            XAMPP_RUN = "start";
                            try {
                                run(ConArgs,"","start","xampp");
                            } catch (IOException ex) {
                                Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    });
                }
            }
        }).start();
 
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch(InterruptedException e1) {
            e1.printStackTrace();
        }
       
        
 
        
        pnlRun.add(lblStatic);
        pnlRun.add(lblRun);
        //pnlRun.add(btnRun);
        
        c.gridx = 0;
        c.gridy = 1;
        c.fill = 1;
        
        pnlXAMPP.add(pnlRun,c);
        
        Icon xamppIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/xampp.png"));
        tagsTab.addTab("XAMPP",xamppIcon, pnlXAMPP);
        
        //------------------------------------------------------
        JPanel sql = new JPanel();
        sql.setLayout(new BorderLayout());
        
        JPanel MySql = new JPanel();
        MySql.setLayout(new GridBagLayout());
        GridBagConstraints e = new GridBagConstraints();

        JLabel lblDB = new JLabel("Adresse : ");
        e.gridx = 0;
        e.gridy = 1;
        e.insets = new Insets(5,0,5,0);
        e.fill = 1;
        e.ipadx = 10;
        MySql.add(lblDB,e);

        txtDB = new JTextField();
        txtDB.setText("localhost");
        e.gridx = 1;
        e.gridy = 1;
        e.ipadx = 30;
        e.gridwidth = 1;
        e.fill = 1;
        MySql.add(txtDB,e);

        JLabel lblBenutzer = new JLabel("Name : ");
        e.gridx = 0;
        e.gridy = 2;
        MySql.add(lblBenutzer,e);

        txtBenutzer = new JTextField();
        txtBenutzer.setText("root");
        e.gridx = 1;
        e.gridy = 2;
        //e.fill = 1;
        MySql.add(txtBenutzer,e);

        JLabel lblPasswort = new JLabel("Passwort : ");
        e.gridx = 0;
        e.gridy = 3;
        MySql.add(lblPasswort,e);

        txtPasswort = new JPasswordField();
        txtPasswort.setText("");
        e.gridx = 1;
        e.gridy = 3;
        //e.fill = 1;
        MySql.add(txtPasswort,e);
        
        sql.add(MySql,BorderLayout.NORTH);
        
        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new FlowLayout());
        
        JButton btnMySqlMonitor = new JButton("Verbinden");
        btnMySqlMonitor.addActionListener(new ActionListener(){
           @Override public void actionPerformed(ActionEvent ev){
               Datenbank = txtDB.getText();
               Benutzername = txtBenutzer.getText();
               Passwort = txtPasswort.getText();
               MySqlMonitor DBanzeigen = new MySqlMonitor(Datenbank,Benutzername,Passwort);
              
           } 
        });
        
//        JButton btnMySqlMonitor2 = new JButton("Verbinden 2");
//        btnMySqlMonitor2.addActionListener(new ActionListener(){
//           @Override public void actionPerformed(ActionEvent ev){
//               Datenbank = txtDB.getText();
//               Benutzername = txtBenutzer.getText();
//               Passwort = txtPasswort.getText();
//               MySQL_JFrame mysql2 = new MySQL_JFrame(/*Datenbank,Benutzername,Passwort*/);
//              mysql2.show();
//           } 
//        });
        btnPanel.add(btnMySqlMonitor);
//        btnPanel.add(btnMySqlMonitor2);
        
        
        
        sql.add(btnPanel,BorderLayout.CENTER);
        
        Icon DBIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/database.png"));
        tagsTab.addTab("MySQL",DBIcon,sql);
        
        // LEFT
        tbLeft = new JTabbedPane(JTabbedPane.SCROLL_TAB_LAYOUT);
       
         //LuziensEditor.Project Files = new LuziensEditor.Project(); // Project Files

        //MySQL mysql = new MySQL(); // MYSQL Monitor
        //ASM asm = new ASM(); // Show the asm register
        
        final Icon ProjectIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/project.png"));
        
        final LuziensEditor.Project Files = new LuziensEditor.Project(); // Project Files
        final LuziensEditor.Timeline_tree TL = new LuziensEditor.Timeline_tree(); // Project Files
//        
//        new Timer(10000, new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
               tbLeft.addTab("Projekte",ProjectIcon, Files);
               tbLeft.addTab("Timeline",TL);
//                
//            }
//        }).start();
// 
//        try {
//            TimeUnit.SECONDS.sleep(1);
//        } catch(InterruptedException e1) {
//            e1.printStackTrace();
//        }
        
        //tbLeft.addTab("MySQL",DBIcon,sql);
        

        //tbLeft.addTab("ASM",asm);


        JSplitPane tagSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                tbLeft, tagsTab);

        tagSplit.setOneTouchExpandable(true);
        tagSplit.setDividerLocation(350);


        JSplitPane frmSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                tagSplit, groundSplit);

        frmSplit.setOneTouchExpandable(true);
        frmSplit.setDividerLocation(200);


        frmMain.add(frmSplit);

        /* ------------------------------------------------------------------------------------------- */

        /* Statusbar */
        JPanel statusBar = new JPanel();
        statusBar.setLayout(new FlowLayout());
        frmMain.add(statusBar, BorderLayout.SOUTH);
        statusBar.setLayout(new FlowLayout(FlowLayout.LEFT));
        statusBar.setBorder(BorderFactory.createEtchedBorder());
        statusBar.add(new JLabel("[F6] HTML Tags"));

        // F4 -> Java  
        // F5 -> c/c++
        // F6 -> HTML
        // F7 -> HTML 5

        
        frmMain.setLocationRelativeTo(null);
        frmMain.setExtendedState(frmMain.MAXIMIZED_BOTH);
        //frmMain.setSize(900, 600);
        frmMain.setVisible(visible);
    }
 }
   

    /**
     * 
     * @param args
     * @throws IOException
     * @throws InterruptedException 
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        
        XAMPP_RUN = "stop";
        ConArgs = args;
        
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        } 
        catch (UnsupportedLookAndFeelException e) {
           // handle exception
        }
        catch (ClassNotFoundException e) {
           // handle exception
        }
        catch (InstantiationException e) {
           // handle exception
        }
        catch (IllegalAccessException e) {
           // handle exception
        }
       
        frmMain frmmain = new frmMain(true);


    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static LuziensEditor.LuziensEditor.Console_Path;
import LuziensEditor.LuziensEditor.FileStream;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

/**
 *
 * @author markusschnittker
 */
public class EditSH extends JDialog{
    boolean visible;
    String Path;
    static RSyntaxTextArea txtCode;
    
    
    EditSH(boolean visible,final String Path) throws IOException{
        this.visible = visible;
        this.Path = Path;
        
        setTitle(Path);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setLayout(new BorderLayout());

        JToolBar tbMenu = new JToolBar();
        
        Icon btnSaveIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/save.png"));
        JButton btnSave = new JButton(btnSaveIcon);
        btnSave.setBorderPainted(false);
        btnSave.setOpaque(false);
        btnSave.setToolTipText("Speichern");
        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e07) {
                
                String Content = txtCode.getText();
                FileStream file = new FileStream();
                try {
                    file.write(Path, Content);
                } catch (IOException ex) {
                    Logger.getLogger(EditSH.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        tbMenu.add(btnSave);
        
        Icon btnCopyIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/copy.png"));
        JButton btnCopy = new JButton(btnCopyIcon);
        btnCopy.setBorderPainted(false);
        btnCopy.setOpaque(false);
        btnCopy.setToolTipText("Pfad in die Zwischenablage kopieren");
        btnCopy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e07) {
                
                StringSelection selection = new StringSelection(Path);
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(selection, selection);
            }
        });
        tbMenu.add(btnCopy);
        
        
//        Icon btnRunIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/play.png"));
//        JButton btnRun = new JButton(btnRunIcon);
//        btnRun.setBorderPainted(false);
//        btnRun.setOpaque(false);
//        btnRun.setToolTipText("Build & Run");
//        btnRun.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e07) {
//                
//            }
//        });
//        tbMenu.add(btnRun);
        
        Icon btnCloseIcon = new ImageIcon(LuziensEditor.class.getResource("Toolbar/exit.png"));
        JButton btnClose = new JButton(btnCloseIcon);
        btnClose.setBorderPainted(false);
        btnClose.setOpaque(false);
        btnClose.setToolTipText("Abbrechen");
        btnClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e07) {
                dispose();
            }
        });
        tbMenu.add(btnClose);
        
        
        
        add(tbMenu,BorderLayout.NORTH);
        
        txtCode = new RSyntaxTextArea(20,60);
        txtCode.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_UNIX_SHELL);
        
        try {
            txtCode.read(new FileReader(Path), "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        RTextScrollPane scCode = new RTextScrollPane(txtCode);
        add(scCode,BorderLayout.CENTER);
        pack();
        setVisible(visible);
    }
}

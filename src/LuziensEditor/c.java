/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LuziensEditor;

import static LuziensEditor.LuziensEditor.CMain;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author markus
 */
/* Die Klasse c erstellt für c/c++ und ab Version 0.2 auch für GAS 
     * eine grafische Benutzeroberfläche zur Einstellung der Kompiler "flags"
     */
    public class c extends JDialog{
        boolean visible;
        static String[] ConArgs, CppItems;
        static String path, name, option, AppPath, AppName;
        JList lstFiles;
        
        c(){}
        
        void gcc(boolean visible, final String[] ConArgs, final String path, final String name, final String option){
            visible = visible;
            
            
            // build gui for gcc
            setTitle("gcc options");
            setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
            setLayout(new BorderLayout());
            
            
            JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
            txtInfo.setText("<b>GCC</b><br>Hier kannst du die Einstellungen für GCC machen.<br>" +
                                        "\"gcc -o\" wird immer gesetzt.<br><br>");
            txtInfo.setEditable(false);
            add(txtInfo,BorderLayout.NORTH);
            
            JPanel pnlOpt = new JPanel();
            pnlOpt.setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            
            // Projectname
            JLabel lblName = new JLabel("Projektname : ");
            c.gridx = 0;
            c.gridy = 0;
            c.fill = 1;
            pnlOpt.add(lblName,c);

            AppName = name.replace(".c",".o");
            final JTextField txtName = new JTextField(AppName);
            c.gridx = 1;
            c.gridy = 0;
            c.fill = 1;
            pnlOpt.add(txtName,c);
            
            // Project path
            JLabel lblPath = new JLabel("Pfad : ");
            c.gridx = 0;
            c.gridy = 1;
            c.fill = 1;
            pnlOpt.add(lblPath,c);

            AppPath = path.replace(name,"");
            
            final JTextField txtPath = new JTextField(AppPath);
            txtPath.setEditable(false);
            c.gridx = 1;
            c.gridy = 1;
            c.fill = 1;
            pnlOpt.add(txtPath,c);
            
            // File list
            JLabel lblFiles = new JLabel("Main Datei : ");
            c.gridx = 0;
            c.gridy = 2;
            c.fill = 1;
            pnlOpt.add(lblFiles,c);

            File folder = new File(AppPath);
            File[] listOfFiles = folder.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    lstFiles = new JList(listOfFiles);
                    lstFiles.setDragEnabled(true);
                }
            }
            // CppMain including the selected Main file for g++
            lstFiles.addListSelectionListener(new ListSelectionListener() {
                public void valueChanged(ListSelectionEvent e) {
                    if (!e.getValueIsAdjusting()) {
                        String item = String.valueOf(lstFiles.getSelectedValue());
                        CMain = item.replace(AppPath, "");
                    }
                }
            });
            
            JScrollPane lstPane = new JScrollPane (lstFiles, 
            ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            
            c.gridx = 1;
            c.gridy = 2;
            c.fill = 1;
            pnlOpt.add(lstPane,c);
            
            // add files
            JLabel lblAdd = new JLabel("Andere *.c Dateien (dnd)");
            c.gridx = 0;
            c.gridy = 3;
            c.fill = 1;
            pnlOpt.add(lblAdd,c);
            
            final JTextField txtFiles = new JTextField();
            txtFiles.setDragEnabled(true);
            c.gridx = 1;
            c.gridy = 3;
            c.fill = 1;
            pnlOpt.add(txtFiles,c);
            
            // -c
            final JCheckBox cbc = new JCheckBox("-c");
            c.gridx = 0;
            c.gridy = 4;
            c.fill = 1;
            pnlOpt.add(cbc,c);
            
            JLabel lblc = new JLabel("  Kompilieren ohne linken");
            c.gridx = 1;
            c.gridy = 4;
            c.fill = 1;
            pnlOpt.add(lblc,c);
            
            // -O
            final JCheckBox cbO = new JCheckBox("-O");
            c.gridx = 0;
            c.gridy = 5;
            pnlOpt.add(cbO,c);
            
            JLabel lblO = new JLabel("  Optimierung des Codes");
            c.gridx = 1;
            c.gridy = 5;
            pnlOpt.add(lblO,c);
            
            // -O1
            final JCheckBox cbO1 = new JCheckBox("-O1");
            c.gridx = 0;
            c.gridy = 6;
            pnlOpt.add(cbO1,c);
            
            JLabel lblO1 = new JLabel("  Optimierung Stufe 1");
            c.gridx = 1;
            c.gridy = 6;
            pnlOpt.add(lblO1,c);
            
            // -O2
            final JCheckBox cbO2 = new JCheckBox("-O2");
            c.gridx = 0;
            c.gridy = 7;
            pnlOpt.add(cbO2,c);
            
            JLabel lblO2 = new JLabel("  Optimierung Stufe 2");
            c.gridx = 1;
            c.gridy = 7;
            pnlOpt.add(lblO2,c);
            
            // -O3
            final JCheckBox cbO3 = new JCheckBox("-O3");
            c.gridx = 0;
            c.gridy = 8;
            pnlOpt.add(cbO3,c);
            
            JLabel lblO3 = new JLabel("  Optimierung Stufe 3");
            c.gridx = 1;
            c.gridy = 8;
            pnlOpt.add(lblO3,c);
            
            // -g
            final JCheckBox cbg = new JCheckBox("-g");
            c.gridx = 0;
            c.gridy = 9;
            pnlOpt.add(cbg,c);
            
            JLabel lblg = new JLabel("  Standart Debug Informationen");
            c.gridx = 1;
            c.gridy = 9;
            pnlOpt.add(lblg,c);
            
            // -ansi
            final JCheckBox cbansi = new JCheckBox("-ansi");
            c.gridx = 0;
            c.gridy = 10;
            pnlOpt.add(cbansi,c);
            
            JLabel lblansi = new JLabel("  Nur ANSI C");
            c.gridx = 1;
            c.gridy = 10;
            pnlOpt.add(lblansi,c);
            
            // -pedantic
            final JCheckBox cbpedantic = new JCheckBox("-pedantic");
            c.gridx = 0;
            c.gridy = 11;
            pnlOpt.add(cbpedantic,c);
            
            JLabel lblpedantic = new JLabel("  Zeige alle ANSI C Warnungen");
            c.gridx = 1;
            c.gridy = 11;
            pnlOpt.add(lblpedantic,c);
            
            // -pedantic-errors
            final JCheckBox cbpedanticerrors = new JCheckBox("-pedantic-errors");
            c.gridx = 0;
            c.gridy = 12;
            pnlOpt.add(cbpedanticerrors,c);
            
            JLabel lblpedanticerrors = new JLabel("  Zeige alle ANSI C Fehler");
            c.gridx = 1;
            c.gridy = 12;
            pnlOpt.add(lblpedanticerrors,c);
            
            // -Wall
            final JCheckBox cbWall= new JCheckBox("-Wall");
            c.gridx = 0;
            c.gridy = 13;
            pnlOpt.add(cbWall,c);
            
            JLabel lblWall = new JLabel("  Zeige GCC Warnungen");
            c.gridx = 1;
            c.gridy = 13;
            pnlOpt.add(lblWall,c);
            
            // -v
            final JCheckBox cbv= new JCheckBox("-v");
            c.gridx = 0;
            c.gridy = 14;
            pnlOpt.add(cbv,c);
            
            JLabel lblv = new JLabel("  Zeige alle Compiler Schritte");
            c.gridx = 1;
            c.gridy = 14;
            pnlOpt.add(lblv,c);
            
            // run
            final JCheckBox cbrun= new JCheckBox("run");
            c.gridx = 0;
            c.gridy = 15;
            pnlOpt.add(cbrun,c);
            
            JLabel lblrun = new JLabel("  Starte das Programm in Luzien (Experimentell)");
            c.gridx = 1;
            c.gridy = 15;
            pnlOpt.add(lblrun,c);
            
            // create programm 
            JPanel pnlButtons = new JPanel();
            pnlButtons.setLayout(new FlowLayout());
            JButton btnCreate = new JButton("Einstellungen übernehmen");

            btnCreate.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    String o =" -o",c="",O="",O1="",O2="",O3="",g="",ansi="",pedantic="",pedanticerrors="",Wall="",v="";
                    String run = "";
                    CProject = txtName.getText();
                    String Path2Build = AppPath.replace("src", "build");
                    String temp = txtFiles.getText();
                    LuziensEditor.gccFiles = temp.replace(AppPath, "");
                    if(gccFiles == null)
                        gccFiles = "";
                    
                    if(cbc.isSelected())
                        c = " -c";
                    else
                        c = "";
                    
                    if(cbO.isSelected())
                        O = " -O";
                    else
                        O = "";
                    
                    if(cbO1.isSelected())
                        O1 = " -O1";
                    else
                        O1 = "";
                    
                    if(cbO2.isSelected())
                        O2 = " -O2";
                    else
                        O2 = "";
                    
                    if(cbO3.isSelected())
                        O3 = " -O3";
                    else
                        O3 = "";
                    
                    if(cbg.isSelected()){
                        g = " -g";
                        txtDebugger.setText("Debug:\n" +
                                            "Zum debuggen bitte die Konsole öffnen und folgendes eingeben : " +
                                            "gdb " + Path2Build + CProject + "\n" +
                                            "Zum starten \"run\" eingeben\n");
                        Debugger_Path = "gdb " + Path2Build + CProject + "\n";
                    }
                    else
                        g = "";
                    
                    if(cbansi.isSelected())
                        ansi = " -ansi";
                    else
                        ansi = "";
                    
                    if(cbpedantic.isSelected())
                        pedantic = " -pedantic";
                    else
                        pedantic = "";
                    
                    if(cbpedanticerrors.isSelected())
                        pedanticerrors = " -pedantic-errors";
                    else
                        pedanticerrors = "";
                    
                    if(cbWall.isSelected())
                        Wall = " -Wall";
                    else
                        Wall = "";
                    
                    if(cbv.isSelected())
                        v = " -v";
                    else
                        v = "";
                    
                    if(cbrun.isSelected())
                        run = "echo run " + Path2Build + CProject + "\n" +
                              Path2Build + CProject;
                    else
//                        run = "echo Zum Ausführen bitte die Konsole öffnen und folgendes eingeben : " + Path2Build + CProject + "\n";
                    run = Path2Build + CProject + "\n";
                    
                    
                    runC = run;
                    gccFlags = c + O + O1 + O2 + O3 + g + ansi + pedantic + pedanticerrors + Wall + v + o;
                    try {
                        run(ConArgs, path, name, option);
                    } catch (IOException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    dispose();
                }
            });
            
            JButton btnDispose = new JButton("Abbrechen");
            btnDispose.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    dispose();
                }
            });
            
            pnlButtons.add(btnCreate);
            pnlButtons.add(btnDispose);
            
            c.gridx = 0;
            c.gridy = 16;
            c.gridwidth = 2;
            c.fill = 1;
            pnlOpt.add(pnlButtons,c);
            
            JScrollPane spPanel = new JScrollPane(pnlOpt);
            add(spPanel,BorderLayout.CENTER);
            setSize(650,500);
            setVisible(visible);
        }
           
        void gpp(boolean visible, final String[] ConArgs, final String path, final String name, final String option){
            visible = visible;
            
            
            // build gui for g++
            setTitle("G++ Einstellungen");
            setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
            setLayout(new BorderLayout());
            
            
            JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
            txtInfo.setText("<b>G++</b><br>Hier kannst du die Einstellungen für G++ machen.<br>" +
                                        "\"g++ -o\" wird immer gesetzt<br><br>");
            txtInfo.setEditable(false);
            add(txtInfo,BorderLayout.NORTH);
            
            JPanel pnlOpt = new JPanel();
            pnlOpt.setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            
            // Projectname
            JLabel lblName = new JLabel("Projektnname : ");
            c.gridx = 0;
            c.gridy = 0;
            c.fill = 1;
            pnlOpt.add(lblName,c);

            AppName = name.replace(".cpp",".o");
            final JTextField txtName = new JTextField(AppName);
            c.gridx = 1;
            c.gridy = 0;
            c.fill = 1;
            pnlOpt.add(txtName,c);
            
            // Project path
            JLabel lblPath = new JLabel("Pfad : ");
            c.gridx = 0;
            c.gridy = 1;
            c.fill = 1;
            pnlOpt.add(lblPath,c);

            AppPath = path.replace(name,"");
            
            JTextField txtPath = new JTextField(AppPath);
            txtPath.setEditable(false);
            c.gridx = 1;
            c.gridy = 1;
            c.fill = 1;
            pnlOpt.add(txtPath,c);
            
            // File list
            JLabel lblFiles = new JLabel("Main Datei auswählen : ");
            c.gridx = 0;
            c.gridy = 2;
            c.fill = 1;
            pnlOpt.add(lblFiles,c);

            File folder = new File(AppPath);
            File[] listOfFiles = folder.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    lstFiles = new JList(listOfFiles);
                }
            }
            // CppMain including the selected Main file for g++
            lstFiles.addListSelectionListener(new ListSelectionListener() {
                public void valueChanged(ListSelectionEvent e) {
                    if (!e.getValueIsAdjusting()) {
                        String item = String.valueOf(lstFiles.getSelectedValue());
                        CppMain = item.replace(AppPath, "");
                    }
                }
            });
            
            JScrollPane lstPane = new JScrollPane (lstFiles, 
            ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            
            c.gridx = 1;
            c.gridy = 2;
            c.fill = 1;
            pnlOpt.add(lstPane,c);
            
            
            // add files
            JLabel lblAdd = new JLabel("Andere *.cpp Dateien (dnd)");
            c.gridx = 0;
            c.gridy = 3;
            c.fill = 1;
            pnlOpt.add(lblAdd,c);
            
            final JTextField txtFiles = new JTextField();
            txtFiles.setDragEnabled(true);
            c.gridx = 1;
            c.gridy = 3;
            c.fill = 1;
            pnlOpt.add(txtFiles,c);
            
            // -c
            final JCheckBox cbc = new JCheckBox("-c");
            c.gridx = 0;
            c.gridy = 4;
            c.fill = 1;
            pnlOpt.add(cbc,c);
            
            JLabel lblc = new JLabel("  Kompilieren ohne linken");
            c.gridx = 1;
            c.gridy = 4;
            c.fill = 1;
            pnlOpt.add(lblc,c);
            
            // -O
            final JCheckBox cbO = new JCheckBox("-O");
            c.gridx = 0;
            c.gridy = 5;
            pnlOpt.add(cbO,c);
            
            JLabel lblO = new JLabel("  Optimierung des Codes");
            c.gridx = 1;
            c.gridy = 5;
            pnlOpt.add(lblO,c);
            
            // -O1
            final JCheckBox cbO1 = new JCheckBox("-O1");
            c.gridx = 0;
            c.gridy = 6;
            pnlOpt.add(cbO1,c);
            
            JLabel lblO1 = new JLabel("  Optimierung Stufe 1");
            c.gridx = 1;
            c.gridy = 6;
            pnlOpt.add(lblO1,c);
            
            // -O2
            final JCheckBox cbO2 = new JCheckBox("-O2");
            c.gridx = 0;
            c.gridy = 7;
            pnlOpt.add(cbO2,c);
            
            JLabel lblO2 = new JLabel("  Optimierung Stufe 2");
            c.gridx = 1;
            c.gridy = 7;
            pnlOpt.add(lblO2,c);
            
            // -O3
            final JCheckBox cbO3 = new JCheckBox("-O3");
            c.gridx = 0;
            c.gridy = 8;
            pnlOpt.add(cbO3,c);
            
            JLabel lblO3 = new JLabel("  Optimierung Stufe 3");
            c.gridx = 1;
            c.gridy = 8;
            pnlOpt.add(lblO3,c);
            
            // -g
            final JCheckBox cbg = new JCheckBox("-g");
            c.gridx = 0;
            c.gridy = 9;
            pnlOpt.add(cbg,c);
            
            JLabel lblg = new JLabel("  Standart Debug Informationen");
            c.gridx = 1;
            c.gridy = 9;
            pnlOpt.add(lblg,c);
            
            // -ansi
            final JCheckBox cbansi = new JCheckBox("-ansi");
            c.gridx = 0;
            c.gridy = 10;
            pnlOpt.add(cbansi,c);
            
            JLabel lblansi = new JLabel("  Nur ANSI C++");
            c.gridx = 1;
            c.gridy = 10;
            pnlOpt.add(lblansi,c);
            
            // -pedantic
            final JCheckBox cbpedantic = new JCheckBox("-pedantic");
            c.gridx = 0;
            c.gridy = 11;
            pnlOpt.add(cbpedantic,c);
            
            JLabel lblpedantic = new JLabel("  Zeige alle ANSI C++ Warnungen");
            c.gridx = 1;
            c.gridy = 11;
            pnlOpt.add(lblpedantic,c);
            
            // -pedantic-errors
            final JCheckBox cbpedanticerrors = new JCheckBox("-pedantic-errors");
            c.gridx = 0;
            c.gridy = 12;
            pnlOpt.add(cbpedanticerrors,c);
            
            JLabel lblpedanticerrors = new JLabel("  Zeige alle ANSI C++ Fehler");
            c.gridx = 1;
            c.gridy = 12;
            pnlOpt.add(lblpedanticerrors,c);
            
            // -Wall
            final JCheckBox cbWall= new JCheckBox("-Wall");
            c.gridx = 0;
            c.gridy = 13;
            pnlOpt.add(cbWall,c);
            
            JLabel lblWall = new JLabel("  Zeige GPP Warnungen");
            c.gridx = 1;
            c.gridy = 13;
            pnlOpt.add(lblWall,c);
            
            // -v
            final JCheckBox cbv= new JCheckBox("-v");
            c.gridx = 0;
            c.gridy = 14;
            pnlOpt.add(cbv,c);
            
            JLabel lblv = new JLabel("  Zeige alle Compiler Schritte");
            c.gridx = 1;
            c.gridy = 14;
            pnlOpt.add(lblv,c);
            
            // run
            final JCheckBox cbrun= new JCheckBox("run");
            c.gridx = 0;
            c.gridy = 15;
            pnlOpt.add(cbrun,c);
            
            JLabel lblrun = new JLabel("  Startet das Programm in Luzien (Experimentell)");
            c.gridx = 1;
            c.gridy = 15;
            pnlOpt.add(lblrun,c);
            
            // create programm 
            JPanel pnlButtons = new JPanel();
            pnlButtons.setLayout(new FlowLayout());
            JButton btnCreate = new JButton("Einstellungen übernehmen");
            
            btnCreate.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    String o =" -o",c="",O="",O1="",O2="",O3="",g="",ansi="",pedantic="",pedanticerrors="",Wall="",v="";
                    String run ="";
                    CppProject = txtName.getText();
                    String Path2Build = AppPath.replace("src", "build");
                    String temp = txtFiles.getText();
                    LuziensEditor.cppFiles = temp.replace(AppPath, "");
                    if(cppFiles == null)
                        cppFiles = "";
                    
                    if(cbc.isSelected())
                        c = " -c";
                    else
                        c = "";
                    
                    if(cbO.isSelected())
                        O = " -O";
                    else
                        O = "";
                    
                    if(cbO1.isSelected())
                        O1 = " -O1";
                    else
                        O1 = "";
                    
                    if(cbO2.isSelected())
                        O2 = " -O2";
                    else
                        O2 = "";
                    
                    if(cbO3.isSelected())
                        O3 = " -O3";
                    else
                        O3 = "";
                    
                    if(cbg.isSelected()){
                        g = " -g";
                        txtDebugger.setText("Debug:\n" +
                                            "Zum debuggen bitte die Konsole öffnen und folgendes eingeben : " +
                                            "gdb " + Path2Build + CppProject + "\n" +
                                            "Starten mit \"run\"\n");
                        Debugger_Path = "gdb " + Path2Build + CppProject + "\n";
                    }
                    else
                        g = "";
                    
                    if(cbansi.isSelected())
                        ansi = " -ansi";
                    else
                        ansi = "";
                    
                    if(cbpedantic.isSelected())
                        pedantic = " -pedantic";
                    else
                        pedantic = "";
                    
                    if(cbpedanticerrors.isSelected())
                        pedanticerrors = " -pedantic-errors";
                    else
                        pedanticerrors = "";
                    
                    if(cbWall.isSelected())
                        Wall = " -Wall";
                    else
                        Wall = "";
                    
                    if(cbv.isSelected())
                        v = " -v";
                    else
                        v = "";
                    
                    if(cbrun.isSelected())
                        run = "echo run " + Path2Build + CppProject + "\n" +
                              Path2Build + CppProject;
                    else
//                        run = "echo Zum Ausführen bitte die Konsole starten und folgendes eingeben : " + Path2Build + CppProject + "\n";
                          run = Path2Build + CppProject + "\n";
                    
                    
                    
                    runCPP = run;
                    gppFlags = c + O + O1 + O2 + O3 + g + ansi + pedantic + pedanticerrors + Wall + v + o;
                    try {
                        run(ConArgs, path, name, option);
                    } catch (IOException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    
                    dispose();
                }
            });
            
            JButton btnDispose = new JButton("Abbrechen");
            btnDispose.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    dispose();
                }
            });
            
            pnlButtons.add(btnCreate);
            pnlButtons.add(btnDispose);
            
            c.gridx = 0;
            c.gridy = 16;
            c.gridwidth = 2;
            c.fill = 1;
            pnlOpt.add(pnlButtons,c);
            
            JScrollPane spPanel = new JScrollPane(pnlOpt);
            add(spPanel,BorderLayout.CENTER);
            pack();
            setVisible(visible);
        }
        
        void GAS(boolean visible, final String[] ConArgs, final String path, final String name, final String option){
            visible = visible;
            
            
            // build gui for GAS
            setTitle("GAS Einstellungen");
            setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
            setLayout(new BorderLayout());
            
            
            JEditorPane txtInfo = new JEditorPane();
            txtInfo.setContentType("text/html");
            txtInfo.setText("<b>GAS</b><br>Hier kannst du die Einstellungen für GAS machen.<br>" +
                                        "\"GAS -o\" wird immer gesetzt<br><br>");
            txtInfo.setEditable(false);
            add(txtInfo,BorderLayout.NORTH);
            
            JPanel pnlOpt = new JPanel();
            pnlOpt.setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            
            // Projectname
            JLabel lblName = new JLabel("Projektname : ");
            c.gridx = 0;
            c.gridy = 0;
            c.fill = 1;
            pnlOpt.add(lblName,c);

            AppName = name.replace(".S",".o");
            final JTextField txtName = new JTextField(AppName);
            c.gridx = 1;
            c.gridy = 0;
            c.fill = 1;
            pnlOpt.add(txtName,c);
            
            // Project path
            JLabel lblPath = new JLabel("Pfad : ");
            c.gridx = 0;
            c.gridy = 1;
            c.fill = 1;
            pnlOpt.add(lblPath,c);

            AppPath = path.replace(name,"");
            
            JTextField txtPath = new JTextField(AppPath);
            txtPath.setEditable(false);
            c.gridx = 1;
            c.gridy = 1;
            c.fill = 1;
            pnlOpt.add(txtPath,c);
            
            // File list
            JLabel lblFiles = new JLabel("Main Datei : ");
            c.gridx = 0;
            c.gridy = 2;
            c.fill = 1;
            pnlOpt.add(lblFiles,c);

            File folder = new File(AppPath);
            File[] listOfFiles = folder.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    lstFiles = new JList(listOfFiles);
                }
            }
            // CppMain including the selected Main file for g++
            lstFiles.addListSelectionListener(new ListSelectionListener() {
                public void valueChanged(ListSelectionEvent e) {
                    if (!e.getValueIsAdjusting()) {
                        String item = String.valueOf(lstFiles.getSelectedValue());
                        asmMain = item.replace(AppPath, "");
                    }
                }
            });
            
            JScrollPane lstPane = new JScrollPane (lstFiles, 
            ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            
            c.gridx = 1;
            c.gridy = 2;
            c.fill = 1;
            pnlOpt.add(lstPane,c);
            
            
            // add files
            JLabel lblAdd = new JLabel("Andere *.S Dateien (dnd)");
            c.gridx = 0;
            c.gridy = 3;
            c.fill = 1;
            pnlOpt.add(lblAdd,c);
            
            final JTextField txtFiles = new JTextField();
            txtFiles.setDragEnabled(true);
            c.gridx = 1;
            c.gridy = 3;
            c.fill = 1;
            pnlOpt.add(txtFiles,c);
            
//            // -c
//            final JCheckBox cbc = new JCheckBox("-c");
//            c.gridx = 0;
//            c.gridy = 4;
//            c.fill = 1;
//            pnlOpt.add(cbc,c);
//            
//            JLabel lblc = new JLabel("  Compiling without linking");
//            c.gridx = 1;
//            c.gridy = 4;
//            c.fill = 1;
//            pnlOpt.add(lblc,c);
//            
//            // -O
//            final JCheckBox cbO = new JCheckBox("-O");
//            c.gridx = 0;
//            c.gridy = 5;
//            pnlOpt.add(cbO,c);
//            
//            JLabel lblO = new JLabel("  Optimized the code");
//            c.gridx = 1;
//            c.gridy = 5;
//            pnlOpt.add(lblO,c);
//            
//            // -O1
//            final JCheckBox cbO1 = new JCheckBox("-O1");
//            c.gridx = 0;
//            c.gridy = 6;
//            pnlOpt.add(cbO1,c);
//            
//            JLabel lblO1 = new JLabel("  Optimization stage 1");
//            c.gridx = 1;
//            c.gridy = 6;
//            pnlOpt.add(lblO1,c);
//            
//            // -O2
//            final JCheckBox cbO2 = new JCheckBox("-O2");
//            c.gridx = 0;
//            c.gridy = 7;
//            pnlOpt.add(cbO2,c);
//            
//            JLabel lblO2 = new JLabel("  Optimization stage 2");
//            c.gridx = 1;
//            c.gridy = 7;
//            pnlOpt.add(lblO2,c);
//            
//            // -O3
//            final JCheckBox cbO3 = new JCheckBox("-O3");
//            c.gridx = 0;
//            c.gridy = 8;
//            pnlOpt.add(cbO3,c);
//            
//            JLabel lblO3 = new JLabel("  Optimization stage 3");
//            c.gridx = 1;
//            c.gridy = 8;
//            pnlOpt.add(lblO3,c);
//            
//            // -g
//            final JCheckBox cbg = new JCheckBox("-g");
//            c.gridx = 0;
//            c.gridy = 9;
//            pnlOpt.add(cbg,c);
//            
//            JLabel lblg = new JLabel("  Standard debug informations");
//            c.gridx = 1;
//            c.gridy = 9;
//            pnlOpt.add(lblg,c);
//            
//            // -ansi
//            final JCheckBox cbansi = new JCheckBox("-ansi");
//            c.gridx = 0;
//            c.gridy = 10;
//            pnlOpt.add(cbansi,c);
//            
//            JLabel lblansi = new JLabel("  Just ANSI C++");
//            c.gridx = 1;
//            c.gridy = 10;
//            pnlOpt.add(lblansi,c);
//            
//            // -pedantic
//            final JCheckBox cbpedantic = new JCheckBox("-pedantic");
//            c.gridx = 0;
//            c.gridy = 11;
//            pnlOpt.add(cbpedantic,c);
//            
//            JLabel lblpedantic = new JLabel("  Show ANSI C++ warnings");
//            c.gridx = 1;
//            c.gridy = 11;
//            pnlOpt.add(lblpedantic,c);
//            
//            // -pedantic-errors
//            final JCheckBox cbpedanticerrors = new JCheckBox("-pedantic-errors");
//            c.gridx = 0;
//            c.gridy = 12;
//            pnlOpt.add(cbpedanticerrors,c);
//            
//            JLabel lblpedanticerrors = new JLabel("  Show ANSI C++ errors");
//            c.gridx = 1;
//            c.gridy = 12;
//            pnlOpt.add(lblpedanticerrors,c);
//            
//            // -Wall
//            final JCheckBox cbWall= new JCheckBox("-Wall");
//            c.gridx = 0;
//            c.gridy = 13;
//            pnlOpt.add(cbWall,c);
//            
//            JLabel lblWall = new JLabel("  Show gpp warnings");
//            c.gridx = 1;
//            c.gridy = 13;
//            pnlOpt.add(lblWall,c);
//            
//            // -v
//            final JCheckBox cbv= new JCheckBox("-v");
//            c.gridx = 0;
//            c.gridy = 14;
//            pnlOpt.add(cbv,c);
//            
//            JLabel lblv = new JLabel("  Shows all compiler steps");
//            c.gridx = 1;
//            c.gridy = 14;
//            pnlOpt.add(lblv,c);
            
            // run
            final JCheckBox cbrun= new JCheckBox("run");
            c.gridx = 0;
            c.gridy = 15;
            pnlOpt.add(cbrun,c);
            
            JLabel lblrun = new JLabel("  Startet das Programm in Luzien (Experimentell)");
            c.gridx = 1;
            c.gridy = 15;
            pnlOpt.add(lblrun,c);
            
            // create programm 
            JPanel pnlButtons = new JPanel();
            pnlButtons.setLayout(new FlowLayout());
            JButton btnCreate = new JButton("Einstellungen übernehmen");
            
            btnCreate.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    String o =" -o",c="",O="",O1="",O2="",O3="",g="",ansi="",pedantic="",pedanticerrors="",Wall="",v="";
                    String run ="";
                    asmProject = txtName.getText();
                    //String X = asmProject.replaceAll(".o", "");
                    String Path2Build = AppPath.replace("src", "build");
                    String temp = txtFiles.getText();
                    LuziensEditor.asmFiles = temp.replace(AppPath, "");
                    if(asmFiles == null)
                        asmFiles = "";
                    
//                    if(cbc.isSelected())
//                        c = " -c";
//                    else
//                        c = "";
//                    
//                    if(cbO.isSelected())
//                        O = " -O";
//                    else
//                        O = "";
//                    
//                    if(cbO1.isSelected())
//                        O1 = " -O1";
//                    else
//                        O1 = "";
//                    
//                    if(cbO2.isSelected())
//                        O2 = " -O2";
//                    else
//                        O2 = "";
//                    
//                    if(cbO3.isSelected())
//                        O3 = " -O3";
//                    else
//                        O3 = "";
//                    
//                    if(cbg.isSelected()){
//                        g = " -g";
//                        txtDebugger.setText("debug:\n" +
//                                            "to debug please open the console and type : " +
//                                            "gdb " + Path2Build + CppProject + "\n" +
//                                            "start with -> run\n");
//                        Debugger_Path = "gdb " + Path2Build + CppProject + "\n";
//                    }
//                    else
//                        g = "";
//                    
//                    if(cbansi.isSelected())
//                        ansi = " -ansi";
//                    else
//                        ansi = "";
//                    
//                    if(cbpedantic.isSelected())
//                        pedantic = " -pedantic";
//                    else
//                        pedantic = "";
//                    
//                    if(cbpedanticerrors.isSelected())
//                        pedanticerrors = " -pedantic-errors";
//                    else
//                        pedanticerrors = "";
//                    
//                    if(cbWall.isSelected())
//                        Wall = " -Wall";
//                    else
//                        Wall = "";
//                    
//                    if(cbv.isSelected())
//                        v = " -v";
//                    else
//                        v = "";
                    
                    String X = "a.out";
                    if(cbrun.isSelected())
                        run = "echo run " + AppPath + X + "\n" +
                              AppPath + X;
                    else
                        
//                        run = "echo Zum Ausführen bitte die Konsole öffnen und folgendes eingeben : " + Path2Build + X + "\n";
                          run = AppPath + X + "\n";
                    
                    
                    
                    runASM = run;
                    asmFlags = c + O + O1 + O2 + O3 + g + ansi + pedantic + pedanticerrors + Wall + v + o;
                    try {
                        run(ConArgs, path, name, option);
                    } catch (IOException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LuziensEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    
                    dispose();
                }
            });
            
            JButton btnDispose = new JButton("Abbrechen");
            btnDispose.addActionListener(new ActionListener(){
                @Override public void actionPerformed(ActionEvent ae){
                    dispose();
                }
            });
            
            pnlButtons.add(btnCreate);
            pnlButtons.add(btnDispose);
            
            c.gridx = 0;
            c.gridy = 16;
            c.gridwidth = 2;
            c.fill = 1;
            pnlOpt.add(pnlButtons,c);
            
            JScrollPane spPanel = new JScrollPane(pnlOpt);
            add(spPanel,BorderLayout.CENTER);
            pack();
            setVisible(visible);
        }
    }
